package com.xianzaishi.wms.trade.callback.client.impl;

import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.trade.client.LocationService;
import com.xianzaishi.trade.client.OrderService;
import com.xianzaishi.trade.client.Result;
import com.xianzaishi.trade.client.vo.DeliveryAddressVO;
import com.xianzaishi.trade.client.vo.OrderItemVO;
import com.xianzaishi.trade.client.vo.OrderVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryVO;
import com.xianzaishi.wms.job.domain.client.itf.IJobDomainClient;
import com.xianzaishi.wms.job.vo.JobVO;
import com.xianzaishi.wms.tmscore.domain.client.itf.IDistributionDomainClient;
import com.xianzaishi.wms.tmscore.vo.DistributionAreaVO;
import com.xianzaishi.wms.tmscore.vo.DistributionDetailVO;
import com.xianzaishi.wms.tmscore.vo.DistributionQueryVO;
import com.xianzaishi.wms.tmscore.vo.DistributionVO;
import com.xianzaishi.wms.track.domain.client.itf.IOutgoingSaleDomainClient;
import com.xianzaishi.wms.track.domain.service.itf.IOutgoingSaleDomainService;
import com.xianzaishi.wms.track.vo.OutgoingSaleDetailVO;
import com.xianzaishi.wms.track.vo.OutgoingSaleVO;
import com.xianzaishi.wms.trade.callback.client.itf.ITrackCallbackClient;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import net.sf.json.JSONObject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class TrackCallbackClient implements ITrackCallbackClient {
	public static final Logger logger = Logger
			.getLogger(TrackCallbackClient.class);
	@Autowired
	private OrderService orderService = null;
	@Autowired
	private IDistributionDomainClient distributionDomainClient = null;
	@Autowired
	private IOutgoingSaleDomainService outgoingSaleDomainService = null;
	@Autowired
	private IInventoryDomainClient inventoryDomainClient = null;
	@Autowired
	private LocationService locationService = null;
	@Autowired
	private IJobDomainClient jobDomainClient = null;
	@Autowired
	private SkuService skuService = null;
	@Autowired
	private ValueService valueService = null;
	@Autowired
	private IOutgoingSaleDomainClient outgoingSaleDomainClient = null;

	public SimpleResultVO<Boolean> onOrderCreate(Long orderID) {
		try {
			DecimalFormat df = new DecimalFormat("######0");
			logger.error("订单id==" + orderID);
			Result<OrderVO> result = orderService.getOrder(orderID);
			OrderVO orderVO = result.getModel();
			logger.error("订单信息=="+JackSonUtil.getJson(orderVO));
			if (orderVO != null) {
				if (orderVO.getChannelType() == 1) {//1 表示线上  2 表示线下
					updateOnLine(orderID, orderVO);
				} else if (orderVO.getChannelType() == 2){
					updateOffLIne(orderVO);
				}
			}
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			return SimpleResultVO.buildBizErrorResult(e);
		} catch (Throwable e) {
			logger.error(e.getMessage(), e);
			return SimpleResultVO.buildErrorResult();
		}
		return SimpleResultVO.buildSuccessResult(true);
	}


	/**
	 * 扣除库存
	 */
	public void takeOffInventoty(DistributionVO tmp) {

		OutgoingSaleVO outgoingSaleVO = new OutgoingSaleVO();
		List<OutgoingSaleDetailVO> details = new LinkedList<OutgoingSaleDetailVO>();

		outgoingSaleVO.setAgencyId(tmp.getAgencyId());
		outgoingSaleVO.setOpTime(new Date());
		outgoingSaleVO.setStatu(0);
		outgoingSaleVO.setOrderId(tmp.getOrderId());//设置订单号,从配送单中获取的

		//tmp.getDetails获取的是配送单中要配送的商品的列表
		for (DistributionDetailVO distributionDetailVO : tmp.getDetails()) {
			OutgoingSaleDetailVO detail = new OutgoingSaleDetailVO();
			detail.setOrderId(tmp.getOrderId());
			detail.setSkuId(distributionDetailVO.getSkuId());//skuId
			detail.setSkuName(distributionDetailVO.getSkuName());//skuName
			ItemCommoditySkuDTO itemCommoditySkuDTO = skuService
					.queryItemSku(SkuQuery.queryBaseSkuBySku(distributionDetailVO.getSkuId())).getModule();
			detail.setSaleUnit(valueService.queryValueNameByValueId(itemCommoditySkuDTO.getSkuUnit()).getModule());//销售规格

			detail.setSaleUnitId(new Long(itemCommoditySkuDTO.getSkuUnit()));//销售规格id
			detail.setSaleUnitType(new Integer(itemCommoditySkuDTO.querySaleUnitType()));//销售规格类型
			if (distributionDetailVO.getSaleUnitId() != null
					&& !distributionDetailVO.getSaleUnitId().equals(new Long(itemCommoditySkuDTO.getSkuUnit()))) {
				detail.setNumberReal(distributionDetailVO.getAllNoReal() * distributionDetailVO.getSpec() / 1000);//称重，如，瓜子 散称
			} else {
				detail.setNumberReal(distributionDetailVO.getAllNoReal());//非称重 ，例如，一瓶矿泉水
			}
			detail.setSpec(itemCommoditySkuDTO.getSkuSpecificationNum());
			detail.setSpecUnit(valueService.queryValueNameByValueId(itemCommoditySkuDTO.getSkuSpecificationUnit())
					.getModule());
			detail.setSpecUnitId(new Long(itemCommoditySkuDTO.getSkuSpecificationUnit()));
			InventoryVO inventory = inventoryDomainClient.getSkuStand(1l, distributionDetailVO.getSkuId())
					.getTarget();
			if (inventory == null) {
				JobVO jobVO = new JobVO();
				jobVO.setType(4);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("id", distributionDetailVO.getDistributionId());
				jsonObject.put("reason", "check sku-position config , skuID:" + distributionDetailVO.getSkuId());
				jobVO.setParameter(jsonObject.toString());
				jobVO.setStatu(0);
				SimpleResultVO<Boolean> flag = jobDomainClient.addJob(jobVO);
				if (!flag.isSuccess() || !flag.getTarget()) {
					throw new BizException("工单创建失败");
				}
				throw new BizException("没有找到对应的库存信息");
			}
			detail.setPositionId(inventory.getPositionId());//库位
			detail.setPositionCode(inventory.getPositionCode());
			details.add(detail);
		}

		outgoingSaleVO.setDetails(details);
		logger.error("扣除库存开始了");
		outgoingSaleDomainClient.createAndAccountedOutgoingSaleDomain(outgoingSaleVO);
	}

	private void updateOnLine(Long orderID, OrderVO orderVO) {
		logger.error("订单信息=" + JackSonUtil.getJson(orderVO));
		List<OrderItemVO> orderItemVOs = new LinkedList<>();
		DistributionVO distributionVO = new DistributionVO();
		if (orderVO.getUserAddressId() != null) {
      DistributionAreaVO area = distributionDomainClient
          .getDistributionAreaByAddressID(1l,
              orderVO.getUserAddressId()).getTarget();
      if (area != null) {
        distributionVO.setAreaId(area.getId());
      }
    }
		if (orderVO.getGmtDistribution() == null) {
      distributionVO.setDeliveryAging(Short.parseShort("0"));
    } else {
      distributionVO.setDeliveryAging(Short.parseShort("1"));
      distributionVO.setAppointTime(orderVO
          .getGmtDistribution());
    }
		distributionVO.setOrderId(orderVO.getId());
		distributionVO.setAgencyId(1l);

		distributionVO.setOrderTime(orderVO.getGmtCreate());
		distributionVO.setStatu(0);
		distributionVO.setPickup(0);
		distributionVO.setUserAddress(orderVO.getUserAddress());

		distributionVO.setPayAmountReal(new BigDecimal(orderVO
        .getEffeAmount()).multiply(new BigDecimal(100))
        .intValue());
		distributionVO.setUserAddressId(orderVO.getUserAddressId());
		DeliveryAddressVO deliveryAddressVO = locationService
        .getDeliveryAddressById(orderVO.getUserAddressId())
        .getModel();
		distributionVO.setUserName(deliveryAddressVO.getName());
		distributionVO.setUserPhone(deliveryAddressVO.getPhone());
		distributionVO.setDesOrder(orderVO.getAttribute());

		List<DistributionDetailVO> details = new LinkedList<DistributionDetailVO>();
		for (OrderItemVO orderItemVO : orderVO.getItems()) {
      SkuQuery skuquery = SkuQuery.querySkuById(orderItemVO
          .getSkuId());
      ItemCommoditySkuDTO temp = skuService.queryItemSku(
          skuquery).getModule();
      if ((null != temp.getTags())
          && (temp.getTags() & 2048) == 2048) {
        orderItemVOs.add(orderItemVO);
        continue;
      }

      DistributionDetailVO detail = new DistributionDetailVO();
      detail.setSkuId(orderItemVO.getSkuId());
      detail.setSkuName(orderItemVO.getName());
      detail.setPriceReal(new BigDecimal(orderItemVO
          .getEffePrice()).multiply(new BigDecimal(100))
          .intValue());
      ItemCommoditySkuDTO itemCommoditySkuDTO = skuService
          .queryItemSku(
              SkuQuery.queryBaseSkuBySku(orderItemVO
                  .getSkuId())).getModule();
      detail.setSaleUnit(valueService
          .queryValueNameByValueId(
              itemCommoditySkuDTO.getOnlineSkuUnit())
          .getModule());
      detail.setSaleUnitId(new Long(itemCommoditySkuDTO
          .getOnlineSkuUnit()));
      detail.setSaleUnitType(new Integer(itemCommoditySkuDTO
          .querySaleUnitType()));

      detail.setAllNoReal(new BigDecimal(orderItemVO
          .getCount()).multiply(new BigDecimal(1000))
          .intValue());
      detail.setDelivedNoReal(0);
      detail.setDelivNoReal(detail.getAllNoReal());
      detail.setSpec(itemCommoditySkuDTO
          .getSkuSpecificationNum());
      detail.setSpecUnit(valueService
          .queryValueNameByValueId(
              itemCommoditySkuDTO
                  .getSkuSpecificationUnit())
          .getModule());
      detail.setSpecUnitId(new Long(itemCommoditySkuDTO
          .getSkuSpecificationUnit()));
      detail.setOrderSpec(itemCommoditySkuDTO
          .getSkuSpecificationNum()
          * detail.getAllNoReal() / 1000);
      detail.setDelivedSpec(0);
      detail.setDelivSpec(detail.getOrderSpec());

      details.add(detail);
    }
		distributionVO.setDetails(details);
		SimpleResultVO<Boolean> distributionDomain = distributionDomainClient
				.createDistributionDomain(distributionVO);
		if (distributionDomain.isSuccess()) {
			logger.error("配送单创建成功");
		}
		DistributionQueryVO distributionQueryVO = new DistributionQueryVO();
		distributionQueryVO.setOrderId(orderVO.getId());
		SimpleResultVO<List<DistributionVO>> simpleResultVO = distributionDomainClient
				.queryDistributionVOList(distributionQueryVO);
		if (null != simpleResultVO && simpleResultVO.isSuccess() && null != simpleResultVO.getTarget()
				&& CollectionUtils.isNotEmpty(simpleResultVO.getTarget())) {
			distributionVO.setId(simpleResultVO.getTarget().get(0).getId());
			logger.error("配送单信息=" + JackSonUtil.getJson(distributionVO));
			takeOffInventoty(distributionVO);//扣除库存
		} else {
			logger.error("通过订单查询配送单信息失败");
		}

		Result<Boolean> tmp = orderService.updateOrderStatus(
        orderID, Short.parseShort("4"));

		for (OrderItemVO orderItemVO : orderItemVOs) {
      details = new LinkedList<DistributionDetailVO>();
      DistributionDetailVO detail = new DistributionDetailVO();
      detail.setSkuId(orderItemVO.getSkuId());
      detail.setSkuName("配送预售商品—"+orderItemVO.getName());
      detail.setPriceReal(new BigDecimal(orderItemVO
          .getEffePrice()).multiply(new BigDecimal(100))
          .intValue());
      ItemCommoditySkuDTO itemCommoditySkuDTO = skuService
          .queryItemSku(
              SkuQuery.queryBaseSkuBySku(orderItemVO
                  .getSkuId())).getModule();
      detail.setSaleUnit(valueService
          .queryValueNameByValueId(
              itemCommoditySkuDTO.getOnlineSkuUnit())
          .getModule());
      detail.setSaleUnitId(new Long(itemCommoditySkuDTO
          .getOnlineSkuUnit()));
      detail.setSaleUnitType(new Integer(itemCommoditySkuDTO
          .querySaleUnitType()));

      detail.setAllNoReal(new BigDecimal(orderItemVO
          .getCount()).multiply(new BigDecimal(1000))
          .intValue());
      detail.setDelivedNoReal(0);
      detail.setDelivNoReal(detail.getAllNoReal());
      detail.setSpec(itemCommoditySkuDTO
          .getSkuSpecificationNum());
      detail.setSpecUnit(valueService
          .queryValueNameByValueId(
              itemCommoditySkuDTO
                  .getSkuSpecificationUnit())
          .getModule());
      detail.setSpecUnitId(new Long(itemCommoditySkuDTO
          .getSkuSpecificationUnit()));
      detail.setOrderSpec(itemCommoditySkuDTO
          .getSkuSpecificationNum()
          * detail.getAllNoReal() / 1000);
      detail.setDelivedSpec(0);
      detail.setDelivSpec(detail.getOrderSpec());

      details.add(detail);
      distributionVO.setDetails(details);
      distributionDomainClient
          .onlyCreateDomain(distributionVO);
    }
	}

	private void updateOffLIne(OrderVO orderVO) {
		OutgoingSaleVO outgoingSaleVO = new OutgoingSaleVO();
		List<OutgoingSaleDetailVO> details = new LinkedList<OutgoingSaleDetailVO>();

		outgoingSaleVO.setAgencyId(1l);
		outgoingSaleVO.setOperate(new Long(orderVO.getCashierId()));
		outgoingSaleVO.setOpTime(new Date());
		outgoingSaleVO.setStatu(0);
		outgoingSaleVO.setOrderId(orderVO.getId());

		for (OrderItemVO orderItemVO : orderVO.getItems()) {
      OutgoingSaleDetailVO detail = new OutgoingSaleDetailVO();
      detail.setOrderId(orderVO.getId());
      detail.setSkuId(orderItemVO.getSkuId());
      detail.setSkuName(orderItemVO.getName());
      ItemCommoditySkuDTO itemCommoditySkuDTO = skuService
          .queryItemSku(
              SkuQuery.queryBaseSkuBySku(orderItemVO
                  .getSkuId())).getModule();
      detail.setSaleUnit(valueService
          .queryValueNameByValueId(
              itemCommoditySkuDTO.getSkuUnit())
          .getModule());

      detail.setSaleUnitId(new Long(itemCommoditySkuDTO
          .getSkuUnit()));
      detail.setSaleUnitType(new Integer(itemCommoditySkuDTO
          .querySaleUnitType()));

      detail.setNumberReal(new BigDecimal(orderItemVO
          .getCount()).multiply(new BigDecimal(1000))
          .intValue());
      detail.setSpec(itemCommoditySkuDTO
          .getSkuSpecificationNum());
      detail.setSpecUnit(valueService
          .queryValueNameByValueId(
              itemCommoditySkuDTO
                  .getSkuSpecificationUnit())
          .getModule());
      detail.setSpecUnitId(new Long(itemCommoditySkuDTO
          .getSkuSpecificationUnit()));

      InventoryVO inventory = inventoryDomainClient
          .getSkuStand(1l, orderItemVO.getSkuId())
          .getTarget();
      if (inventory == null) {
        JobVO jobVO = new JobVO();
        jobVO.setType(4);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", orderVO.getId());
        jsonObject.put("reason",
            "check sku-position config , skuID:"
                + orderItemVO.getSkuId());
        jobVO.setParameter(jsonObject.toString());
        jobVO.setStatu(0);
        SimpleResultVO<Boolean> flag = jobDomainClient
            .addJob(jobVO);
        if (!flag.isSuccess() || !flag.getTarget()) {
          throw new BizException("工单创建失败");
        }
        throw new BizException("没有找到对应的库存信息");
      }
      detail.setPositionId(inventory.getPositionId());
      detail.setPositionCode(inventory.getPositionCode());
      details.add(detail);
    }

		outgoingSaleVO.setDetails(details);
		outgoingSaleDomainService
        .createAndAccountedOutgoingSaleDomain(outgoingSaleVO);

	}

	private Boolean isStandard(ItemCommoditySkuDTO itemCommoditySkuDTO) {
		Boolean flag = true;
		if (itemCommoditySkuDTO.querySaleUnitType() == 4) {
			flag = false;
		}
		return flag;
	}

	public SimpleResultVO<Boolean> onOrderClose(Long orderID) {
		try {
			DistributionQueryVO distributionQueryVO = new DistributionQueryVO();
			distributionQueryVO.setOrderId(orderID);
			List<DistributionVO> distributions = distributionDomainClient
					.queryDistributionVOList(distributionQueryVO).getTarget();
			if (distributions != null) {
				for (DistributionVO distributionVO : distributions) {
					distributionDomainClient.closeDistribution(distributionVO);
				}
			}
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			return SimpleResultVO.buildBizErrorResult(e);
		} catch (Throwable e) {
			logger.error(e.getMessage(), e);
			return SimpleResultVO.buildErrorResult();
		}
		return SimpleResultVO.buildSuccessResult(true);
	}

	public OrderService getOrderService() {
		return orderService;
	}

	public void setOrderService(OrderService orderService) {
		this.orderService = orderService;
	}

	public IDistributionDomainClient getDistributionDomainClient() {
		return distributionDomainClient;
	}

	public void setDistributionDomainClient(
			IDistributionDomainClient distributionDomainClient) {
		this.distributionDomainClient = distributionDomainClient;
	}

	public IOutgoingSaleDomainService getOutgoingSaleDomainService() {
		return outgoingSaleDomainService;
	}

	public void setOutgoingSaleDomainService(
			IOutgoingSaleDomainService outgoingSaleDomainService) {
		this.outgoingSaleDomainService = outgoingSaleDomainService;
	}

	public IInventoryDomainClient getInventoryDomainClient() {
		return inventoryDomainClient;
	}

	public void setInventoryDomainClient(
			IInventoryDomainClient inventoryDomainClient) {
		this.inventoryDomainClient = inventoryDomainClient;
	}

	public LocationService getLocationService() {
		return locationService;
	}

	public void setLocationService(LocationService locationService) {
		this.locationService = locationService;
	}

	public IJobDomainClient getJobDomainClient() {
		return jobDomainClient;
	}

	public void setJobDomainClient(IJobDomainClient jobDomainClient) {
		this.jobDomainClient = jobDomainClient;
	}

	public SkuService getSkuService() {
		return skuService;
	}

	public void setSkuService(SkuService skuService) {
		this.skuService = skuService;
	}

	public ValueService getValueService() {
		return valueService;
	}

	public void setValueService(ValueService valueService) {
		this.valueService = valueService;
	}

	public IOutgoingSaleDomainClient getOutgoingSaleDomainClient() {
		return outgoingSaleDomainClient;
	}

	public void setOutgoingSaleDomainClient(
			IOutgoingSaleDomainClient outgoingSaleDomainClient) {
		this.outgoingSaleDomainClient = outgoingSaleDomainClient;
	}
}
