package com.xianzaishi.wms.track.hessian.test;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.wms.base.test.BaseTest;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryConfigDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IPositionDomainClient;
import com.xianzaishi.wms.hive.vo.PositionDetailVO;
import com.xianzaishi.wms.track.domain.client.itf.IOutgoingDomainClient;
import com.xianzaishi.wms.track.vo.OutgoingDetailVO;
import com.xianzaishi.wms.track.vo.OutgoingVO;
import com.xianzaishi.wms.track.vo.OutgoingDetailVO;

@ContextConfiguration(locations = "classpath:spring/ac-*.xml")
public class OutgoingHessianTest extends BaseTest {
	@Autowired
	private IOutgoingDomainClient outgoingDomainClient = null;
	@Autowired
	private IPositionDomainClient positionDomainClient = null;
	@Autowired
	private IInventoryConfigDomainClient inventoryConfigDomainClient = null;
	@Autowired
	private SkuService skuService = null;

	@Test
	public void addOutgoingTest() {
		OutgoingVO outgoingVO = new OutgoingVO();
		outgoingVO.setOpReason(1);
		outgoingVO.setAgencyId(1l);
		outgoingVO.setOperate(46l);
		List<OutgoingDetailVO> outgoingDetailVOs = new LinkedList<OutgoingDetailVO>();

		// 10046l,10661l,20745l;

		Long skuID = 10046l;
		OutgoingDetailVO outgoingDetailVO = new OutgoingDetailVO();
		outgoingDetailVO.setSkuId(skuID);
		List<PositionDetailVO> positionDetailVOs = inventoryConfigDomainClient
				.getPositionDetailBySkuID(1l, skuID).getTarget();
		outgoingDetailVO
				.setPositionId(positionDetailVOs.get(0).getPositionId());
		outgoingDetailVO.setSaleUnitId(new Long(skuService
				.queryItemSku(
						SkuQuery.querySkuById(outgoingDetailVO.getSkuId()))
				.getModule().getSkuUnit()));
		outgoingDetailVO.setNumberReal(1);
		outgoingDetailVOs.add(outgoingDetailVO);

		skuID = 10661l;
		outgoingDetailVO = new OutgoingDetailVO();
		outgoingDetailVO.setSkuId(skuID);
		positionDetailVOs = inventoryConfigDomainClient
				.getPositionDetailBySkuID(1l, skuID).getTarget();
		outgoingDetailVO
				.setPositionId(positionDetailVOs.get(0).getPositionId());
		outgoingDetailVO.setSaleUnitId(new Long(skuService
				.queryItemSku(
						SkuQuery.querySkuById(outgoingDetailVO.getSkuId()))
				.getModule().getSkuUnit()));
		outgoingDetailVO.setNumberReal(1);
		outgoingDetailVOs.add(outgoingDetailVO);

		skuID = 20745l;
		outgoingDetailVO = new OutgoingDetailVO();
		outgoingDetailVO.setSkuId(skuID);
		positionDetailVOs = inventoryConfigDomainClient
				.getPositionDetailBySkuID(1l, skuID).getTarget();
		outgoingDetailVO
				.setPositionId(positionDetailVOs.get(0).getPositionId());
		outgoingDetailVO.setSaleUnitId(new Long(skuService
				.queryItemSku(
						SkuQuery.querySkuById(outgoingDetailVO.getSkuId()))
				.getModule().getSkuUnit()));
		outgoingDetailVO.setNumberReal(1);
		outgoingDetailVOs.add(outgoingDetailVO);

		outgoingVO.setDetails(outgoingDetailVOs);
		outgoingDomainClient.createOutgoingDomain(outgoingVO);
	}

	@Test
	public void submitOutgoingTest() {
		outgoingDomainClient.submit(1l);
	}

	@Test
	public void auditOutgoingTest() {
		outgoingDomainClient.auditOutgoing(1l, 46l);
	}

	public IPositionDomainClient getPositionDomainClient() {
		return positionDomainClient;
	}

	public void setPositionDomainClient(
			IPositionDomainClient positionDomainClient) {
		this.positionDomainClient = positionDomainClient;
	}

	public IInventoryConfigDomainClient getInventoryConfigDomainClient() {
		return inventoryConfigDomainClient;
	}

	public void setInventoryConfigDomainClient(
			IInventoryConfigDomainClient inventoryConfigDomainClient) {
		this.inventoryConfigDomainClient = inventoryConfigDomainClient;
	}

	public SkuService getSkuService() {
		return skuService;
	}

	public void setSkuService(SkuService skuService) {
		this.skuService = skuService;
	}

	public IOutgoingDomainClient getOutgoingDomainClient() {
		return outgoingDomainClient;
	}

	public void setOutgoingDomainClient(
			IOutgoingDomainClient outgoingDomainClient) {
		this.outgoingDomainClient = outgoingDomainClient;
	}

}
