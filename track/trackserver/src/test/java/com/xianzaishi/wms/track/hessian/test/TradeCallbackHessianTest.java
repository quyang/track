package com.xianzaishi.wms.track.hessian.test;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.xianzaishi.wms.base.test.BaseTest;
import com.xianzaishi.wms.trade.callback.client.itf.ITrackCallbackClient;

@ContextConfiguration(locations = { "classpath:spring/ac-trade-hessian.xml",
		"classpath:spring/ac-hessian-trade.xml",
		"classpath:spring/ac-hessian-trade-callback.xml" })
public class TradeCallbackHessianTest extends BaseTest {

	@Autowired
	private ITrackCallbackClient trackCallbackClient = null;

	@Test
	public void onOrderCreateTest() {
		trackCallbackClient.onOrderCreate(11243l);
	}

	public ITrackCallbackClient getTrackCallbackClient() {
		return trackCallbackClient;
	}

	public void setTrackCallbackClient(ITrackCallbackClient trackCallbackClient) {
		this.trackCallbackClient = trackCallbackClient;
	}

}
