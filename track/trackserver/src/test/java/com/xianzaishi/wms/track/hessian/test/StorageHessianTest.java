package com.xianzaishi.wms.track.hessian.test;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.wms.base.test.BaseTest;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryConfigDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IPositionDomainClient;
import com.xianzaishi.wms.hive.vo.PositionDetailVO;
import com.xianzaishi.wms.track.domain.client.itf.IStorageDomainClient;
import com.xianzaishi.wms.track.vo.StorageDetailVO;
import com.xianzaishi.wms.track.vo.StorageVO;

@ContextConfiguration(locations = "classpath:spring/ac-*.xml")
public class StorageHessianTest extends BaseTest {
	@Autowired
	private IStorageDomainClient storageDomainClient = null;
	@Autowired
	private IPositionDomainClient positionDomainClient = null;
	@Autowired
	private IInventoryConfigDomainClient inventoryConfigDomainClient = null;
	@Autowired
	private SkuService skuService = null;

	@Test
	public void addStorageTest() {
		StorageVO storageVO = new StorageVO();
		storageVO.setOpReason(5);
		storageVO.setAgencyId(1l);
		storageVO.setOperate(46l);
		List<StorageDetailVO> storageDetailVOs = new LinkedList<StorageDetailVO>();

		// 10046l,10661l,20745l;

		Long skuID = 10046l;
		StorageDetailVO storageDetailVO = new StorageDetailVO();
		storageDetailVO.setSkuId(skuID);
		List<PositionDetailVO> positionDetailVOs = inventoryConfigDomainClient
				.getPositionDetailBySkuID(1l, skuID).getTarget();
		storageDetailVO.setPositionId(positionDetailVOs.get(0).getPositionId());
		storageDetailVO
				.setSaleUnitId(new Long(skuService
						.queryItemSku(
								SkuQuery.querySkuById(storageDetailVO
										.getSkuId())).getModule().getSkuUnit()));
		storageDetailVO.setNumberReal(100);
		storageDetailVOs.add(storageDetailVO);

		skuID = 10661l;
		storageDetailVO = new StorageDetailVO();
		storageDetailVO.setSkuId(skuID);
		positionDetailVOs = inventoryConfigDomainClient
				.getPositionDetailBySkuID(1l, skuID).getTarget();
		storageDetailVO.setPositionId(positionDetailVOs.get(0).getPositionId());
		storageDetailVO
				.setSaleUnitId(new Long(skuService
						.queryItemSku(
								SkuQuery.querySkuById(storageDetailVO
										.getSkuId())).getModule().getSkuUnit()));
		storageDetailVO.setNumberReal(100);
		storageDetailVOs.add(storageDetailVO);

		skuID = 20745l;
		storageDetailVO = new StorageDetailVO();
		storageDetailVO.setSkuId(skuID);
		positionDetailVOs = inventoryConfigDomainClient
				.getPositionDetailBySkuID(1l, skuID).getTarget();
		storageDetailVO.setPositionId(positionDetailVOs.get(0).getPositionId());
		storageDetailVO
				.setSaleUnitId(new Long(skuService
						.queryItemSku(
								SkuQuery.querySkuById(storageDetailVO
										.getSkuId())).getModule().getSkuUnit()));
		storageDetailVO.setNumberReal(100);
		storageDetailVOs.add(storageDetailVO);

		storageVO.setDetails(storageDetailVOs);
		storageDomainClient.createStorageDomain(storageVO);
	}

	@Test
	public void submitStorageTest() {
		storageDomainClient.submit(2l);
	}

	@Test
	public void auditStorageTest() {
		storageDomainClient.audit(2l, 46l);
	}

	public IPositionDomainClient getPositionDomainClient() {
		return positionDomainClient;
	}

	public void setPositionDomainClient(
			IPositionDomainClient positionDomainClient) {
		this.positionDomainClient = positionDomainClient;
	}

	public IInventoryConfigDomainClient getInventoryConfigDomainClient() {
		return inventoryConfigDomainClient;
	}

	public void setInventoryConfigDomainClient(
			IInventoryConfigDomainClient inventoryConfigDomainClient) {
		this.inventoryConfigDomainClient = inventoryConfigDomainClient;
	}

	public SkuService getSkuService() {
		return skuService;
	}

	public void setSkuService(SkuService skuService) {
		this.skuService = skuService;
	}

	public IStorageDomainClient getStorageDomainClient() {
		return storageDomainClient;
	}

	public void setStorageDomainClient(IStorageDomainClient storageDomainClient) {
		this.storageDomainClient = storageDomainClient;
	}
}
