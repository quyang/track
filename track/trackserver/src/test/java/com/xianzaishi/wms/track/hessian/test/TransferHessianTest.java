package com.xianzaishi.wms.track.hessian.test;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.wms.base.test.BaseTest;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryConfigDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IPositionDomainClient;
import com.xianzaishi.wms.hive.vo.PositionDetailVO;
import com.xianzaishi.wms.track.domain.client.itf.ITransferDomainClient;
import com.xianzaishi.wms.track.vo.TransferDetailVO;
import com.xianzaishi.wms.track.vo.TransferVO;

@ContextConfiguration(locations = "classpath:spring/ac-*.xml")
public class TransferHessianTest extends BaseTest {
	@Autowired
	private ITransferDomainClient transferDomainClient = null;
	@Autowired
	private IPositionDomainClient positionDomainClient = null;
	@Autowired
	private IInventoryConfigDomainClient inventoryConfigDomainClient = null;
	@Autowired
	private SkuService skuService = null;

	@Test
	public void addTransferTest() {
		TransferVO transferVO = new TransferVO();
		transferVO.setAgencyId(1l);
		transferVO.setOperate(46l);
		List<TransferDetailVO> transferDetailVOs = new LinkedList<TransferDetailVO>();

		// 10046l,10661l,20745l;

		Long skuID = 10046l;
		TransferDetailVO transferDetailVO = new TransferDetailVO();
		transferDetailVO.setSkuId(skuID);
		List<PositionDetailVO> positionDetailVOs = inventoryConfigDomainClient
				.getPositionDetailBySkuID(1l, skuID).getTarget();
		transferDetailVO.setPositionOld(positionDetailVOs.get(0)
				.getPositionId());
		transferDetailVO.setPositionNew(871l);
		transferDetailVO.setSaleUnitId(new Long(skuService
				.queryItemSku(
						SkuQuery.querySkuById(transferDetailVO.getSkuId()))
				.getModule().getSkuUnit()));
		transferDetailVO.setNumberReal(1);
		transferDetailVOs.add(transferDetailVO);

		skuID = 10661l;
		transferDetailVO = new TransferDetailVO();
		transferDetailVO.setSkuId(skuID);
		positionDetailVOs = inventoryConfigDomainClient
				.getPositionDetailBySkuID(1l, skuID).getTarget();
		transferDetailVO.setPositionOld(positionDetailVOs.get(0)
				.getPositionId());
		transferDetailVO.setPositionNew(871l);
		transferDetailVO.setSaleUnitId(new Long(skuService
				.queryItemSku(
						SkuQuery.querySkuById(transferDetailVO.getSkuId()))
				.getModule().getSkuUnit()));
		transferDetailVO.setNumberReal(1);
		transferDetailVOs.add(transferDetailVO);

		skuID = 20745l;
		transferDetailVO = new TransferDetailVO();
		transferDetailVO.setSkuId(skuID);
		positionDetailVOs = inventoryConfigDomainClient
				.getPositionDetailBySkuID(1l, skuID).getTarget();
		transferDetailVO.setPositionOld(positionDetailVOs.get(0)
				.getPositionId());
		transferDetailVO.setPositionNew(871l);
		transferDetailVO.setSaleUnitId(new Long(skuService
				.queryItemSku(
						SkuQuery.querySkuById(transferDetailVO.getSkuId()))
				.getModule().getSkuUnit()));
		transferDetailVO.setNumberReal(1);
		transferDetailVOs.add(transferDetailVO);

		transferVO.setDetails(transferDetailVOs);
		transferDomainClient.createTransferDomain(transferVO);
	}

	@Test
	public void submitTransferTest() {
		transferDomainClient.submit(2l);
	}

	@Test
	public void auditTransferTest() {
		transferDomainClient.audit(2l, 46l);
	}

	public IPositionDomainClient getPositionDomainClient() {
		return positionDomainClient;
	}

	public void setPositionDomainClient(
			IPositionDomainClient positionDomainClient) {
		this.positionDomainClient = positionDomainClient;
	}

	public IInventoryConfigDomainClient getInventoryConfigDomainClient() {
		return inventoryConfigDomainClient;
	}

	public void setInventoryConfigDomainClient(
			IInventoryConfigDomainClient inventoryConfigDomainClient) {
		this.inventoryConfigDomainClient = inventoryConfigDomainClient;
	}

	public SkuService getSkuService() {
		return skuService;
	}

	public void setSkuService(SkuService skuService) {
		this.skuService = skuService;
	}

	public ITransferDomainClient getTransferDomainClient() {
		return transferDomainClient;
	}

	public void setTransferDomainClient(
			ITransferDomainClient transferDomainClient) {
		this.transferDomainClient = transferDomainClient;
	}

}
