package com.xianzaishi.wms.oplog.vo;

import java.io.Serializable;
import java.util.Date;

import com.xianzaishi.wms.common.vo.BaseVO;

public class ProductionShelvingLogVO extends BaseVO implements Serializable {

	private Long agencyId = null;

	private Long skuId = null;

	private Integer shelveNo = null;

	private Long slottingId = null;

	private Long positionId = null;

	private String skuBarcode = null;

	private String positionBarcode = null;

	private Long operate = null;

	private Date opTime = null;

	private Long opDay = null;

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getShelveNo() {
		return shelveNo;
	}

	public void setShelveNo(Integer shelveNo) {
		this.shelveNo = shelveNo;
	}

	public Long getSlottingId() {
		return slottingId;
	}

	public void setSlottingId(Long slottingId) {
		this.slottingId = slottingId;
	}

	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

	public Long getOperate() {
		return operate;
	}

	public void setOperate(Long operate) {
		this.operate = operate;
	}

	public Date getOpTime() {
		return opTime;
	}

	public void setOpTime(Date opTime) {
		this.opTime = opTime;
	}

	public String getSkuBarcode() {
		return skuBarcode;
	}

	public void setSkuBarcode(String skuBarcode) {
		this.skuBarcode = skuBarcode;
	}

	public String getPositionBarcode() {
		return positionBarcode;
	}

	public void setPositionBarcode(String positionBarcode) {
		this.positionBarcode = positionBarcode;
	}

	public Long getOpDay() {
		return opDay;
	}

	public void setOpDay(Long opDay) {
		this.opDay = opDay;
	}

}