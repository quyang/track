package com.xianzaishi.wms.track.vo;

import java.io.Serializable;

import com.xianzaishi.wms.common.vo.QueryVO;

public class OutgoingDetailQueryVO extends QueryVO implements Serializable {

	private Long outgoingId = null;

	private Long skuId = null;

	private Integer number = null;

	private Long positionId = null;

	public Long getOutgoingId() {
		return outgoingId;
	}

	public void setOutgoingId(Long outgoingId) {
		this.outgoingId = outgoingId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

}