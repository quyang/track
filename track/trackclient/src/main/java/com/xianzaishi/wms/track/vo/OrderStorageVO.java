package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.xianzaishi.wms.common.vo.BaseVO;

public class OrderStorageVO extends BaseVO implements Serializable {

	private Long agencyId = null;

	private Long deliveryId = null;

	private Long operate = null;

	private String operateName = null;

	private String sopTime = null;

	private Date opTime = null;

	private Long auditor = null;

	private Date auditTime = null;

	private Integer statu = null;

	private List<OrderStorageDetailVO> details = null;

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}

	public Long getOperate() {
		return operate;
	}

	public void setOperate(Long operate) {
		this.operate = operate;
	}

	public Date getOpTime() {
		return opTime;
	}

	public void setOpTime(Date opTime) {
		this.opTime = opTime;
	}

	public Long getAuditor() {
		return auditor;
	}

	public void setAuditor(Long auditor) {
		this.auditor = auditor;
	}

	public Date getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}

	public String getOperateName() {
		return operateName;
	}

	public void setOperateName(String operateName) {
		this.operateName = operateName;
	}

	public String getSopTime() {
		return sopTime;
	}

	public void setSopTime(String sopTime) {
		this.sopTime = sopTime;
	}

	public Integer getStatu() {
		return statu;
	}

	public void setStatu(Integer statu) {
		this.statu = statu;
	}

	public List<OrderStorageDetailVO> getDetails() {
		return details;
	}

	public void setDetails(List<OrderStorageDetailVO> details) {
		this.details = details;
	}
}