package com.xianzaishi.wms.oplog.vo;

import java.io.Serializable;
import java.util.Date;

import com.xianzaishi.wms.common.vo.QueryVO;

public class OutgoingSaleLogQueryVO extends QueryVO implements Serializable {

	private Long agencyId = null;

	private Long orderId = null;

	private Long skuId = null;

	private Integer saleNo = null;

	private Long slottingId = null;

	private Long positionId = null;

	private Long opDay = null;

	private Short dr = null;

	private Date ts = null;

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getSaleNo() {
		return saleNo;
	}

	public void setSaleNo(Integer saleNo) {
		this.saleNo = saleNo;
	}

	public Long getSlottingId() {
		return slottingId;
	}

	public void setSlottingId(Long slottingId) {
		this.slottingId = slottingId;
	}

	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

	public Short getDr() {
		return dr;
	}

	public void setDr(Short dr) {
		this.dr = dr;
	}

	public Date getTs() {
		return ts;
	}

	public void setTs(Date ts) {
		this.ts = ts;
	}

	public Long getOpDay() {
		return opDay;
	}

	public void setOpDay(Long opDay) {
		this.opDay = opDay;
	}

}