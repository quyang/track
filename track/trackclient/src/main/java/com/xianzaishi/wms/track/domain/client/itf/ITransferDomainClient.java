package com.xianzaishi.wms.track.domain.client.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.vo.TransferDetailQueryVO;
import com.xianzaishi.wms.track.vo.TransferDetailVO;
import com.xianzaishi.wms.track.vo.TransferQueryVO;
import com.xianzaishi.wms.track.vo.TransferVO;

public interface ITransferDomainClient {

	// domain
	public SimpleResultVO<Boolean> createTransferDomain(TransferVO transferVO);

	public SimpleResultVO<TransferVO> getTransferDomainByID(Long transferID);

	public SimpleResultVO<Boolean> deleteTransferDomain(Long id);

	public SimpleResultVO<Boolean> createAndAccount(TransferVO transferVO);

	// head
	public SimpleResultVO<Boolean> addTransferVO(TransferVO transferVO);

	public SimpleResultVO<QueryResultVO<TransferVO>> queryTransferVOList(
			TransferQueryVO transferQueryVO);

	public SimpleResultVO<TransferVO> getTransferVOByID(Long id);

	public SimpleResultVO<Boolean> modifyTransferVO(TransferVO transferVO);

	public SimpleResultVO<Boolean> deleteTransferVO(Long transferID);

	// body
	public SimpleResultVO<Boolean> createTransferDetailVO(
			TransferDetailVO transferDetailVO);

	public SimpleResultVO<Boolean> batchCreateTransferDetailVO(
			List<TransferDetailVO> transferDetailVOs);

	public SimpleResultVO<List<TransferDetailVO>> queryTransferDetailVOList(
			TransferDetailQueryVO transferDetailQueryVO);

	public SimpleResultVO<List<TransferDetailVO>> getTransferDetailVOListByTransferID(
			Long transferID);

	public SimpleResultVO<TransferDetailQueryVO> getTransferDetailVOByID(Long id);

	public SimpleResultVO<Boolean> modifyTransferDetailVO(
			TransferDetailVO transferDetailVO);

	/**
	 * 按ID更新Detail
	 * 
	 * @param transferDetailVOs
	 * @return
	 */
	public SimpleResultVO<Boolean> batchModifyTransferDetailVOs(
			List<TransferDetailVO> transferDetailVOs);

	public SimpleResultVO<Boolean> deleteTransferDetailVO(Long id);

	public SimpleResultVO<Boolean> deleteTransferDetailVOByTransferID(
			Long transferID);

	public SimpleResultVO<Boolean> batchDeleteTransferDetailVOByTransferID(
			List<Long> storyDetailIDs);

	public SimpleResultVO<Boolean> submit(Long id);

	public SimpleResultVO<Boolean> audit(Long id, Long auditor);

	public SimpleResultVO<Boolean> accounted(Long id);

}
