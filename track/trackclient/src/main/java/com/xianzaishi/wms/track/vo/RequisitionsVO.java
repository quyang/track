package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.xianzaishi.wms.common.vo.BaseVO;

public class RequisitionsVO extends BaseVO implements Serializable {

	private Long agencyId = null;

	private Long receiver = null;

	private String receiverNmae = null;

	private Date createTime = null;

	private Long operate = null;

	private Long outgoingAuditor = null;

	private Date opTime = null;

	private Long auditor = null;

	private Date auditTime = null;

	private Long stallsId = null;

	private Short statu = null;

	private String stallsName = null;

	private String sopTime = null;

	private String sauditTime = null;

	private List<RequisitionsDetailVO> details = null;

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getReceiver() {
		return receiver;
	}

	public void setReceiver(Long receiver) {
		this.receiver = receiver;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getOperate() {
		return operate;
	}

	public void setOperate(Long operate) {
		this.operate = operate;
	}

	public Date getOpTime() {
		return opTime;
	}

	public void setOpTime(Date opTime) {
		this.opTime = opTime;
	}

	public Long getAuditor() {
		return auditor;
	}

	public void setAuditor(Long auditor) {
		this.auditor = auditor;
	}

	public Date getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}

	public Long getStallsId() {
		return stallsId;
	}

	public void setStallsId(Long stallsId) {
		this.stallsId = stallsId;
	}

	public String getReceiverNmae() {
		return receiverNmae;
	}

	public void setReceiverNmae(String receiverNmae) {
		this.receiverNmae = receiverNmae;
	}

	public String getStallsName() {
		return stallsName;
	}

	public void setStallsName(String stallsName) {
		this.stallsName = stallsName;
	}

	public Short getStatu() {
		return statu;
	}

	public void setStatu(Short statu) {
		this.statu = statu;
	}

	public String getSopTime() {
		return sopTime;
	}

	public void setSopTime(String sopTime) {
		this.sopTime = sopTime;
	}

	public String getSauditTime() {
		return sauditTime;
	}

	public void setSauditTime(String sauditTime) {
		this.sauditTime = sauditTime;
	}

	public List<RequisitionsDetailVO> getDetails() {
		return details;
	}

	public void setDetails(List<RequisitionsDetailVO> details) {
		this.details = details;
	}

	public Long getOutgoingAuditor() {
		return outgoingAuditor;
	}

	public void setOutgoingAuditor(Long outgoingAuditor) {
		this.outgoingAuditor = outgoingAuditor;
	}

}