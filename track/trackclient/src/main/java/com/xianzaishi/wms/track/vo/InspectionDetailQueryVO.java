package com.xianzaishi.wms.track.vo;

import java.io.Serializable;

import com.xianzaishi.wms.common.vo.QueryVO;

public class InspectionDetailQueryVO extends QueryVO implements Serializable {

	private Long inspectionId = null;

	private Long agencyId = null;

	private Long skuId = null;

	private Integer delivedNo = null;

	private Integer bookingNo = null;

	private Integer qualifiedNo = null;

	public Long getInspectionId() {
		return inspectionId;
	}

	public void setInspectionId(Long inspectionId) {
		this.inspectionId = inspectionId;
	}

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getDelivedNo() {
		return delivedNo;
	}

	public void setDelivedNo(Integer delivedNo) {
		this.delivedNo = delivedNo;
	}

	public Integer getBookingNo() {
		return bookingNo;
	}

	public void setBookingNo(Integer bookingNo) {
		this.bookingNo = bookingNo;
	}

	public Integer getQualifiedNo() {
		return qualifiedNo;
	}

	public void setQualifiedNo(Integer qualifiedNo) {
		this.qualifiedNo = qualifiedNo;
	}

}