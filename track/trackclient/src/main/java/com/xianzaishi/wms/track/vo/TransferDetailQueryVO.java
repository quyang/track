package com.xianzaishi.wms.track.vo;

import java.io.Serializable;

import com.xianzaishi.wms.common.vo.QueryVO;

public class TransferDetailQueryVO extends QueryVO implements Serializable {

	private Long transferId = null;

	private Long positionOld = null;

	private Long skuId = null;

	private Integer number = null;

	private Long positionNew = null;

	private Long operator = null;

	public Long getTransferId() {
		return transferId;
	}

	public void setTransferId(Long transferId) {
		this.transferId = transferId;
	}

	public Long getPositionOld() {
		return positionOld;
	}

	public void setPositionOld(Long positionOld) {
		this.positionOld = positionOld;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Long getPositionNew() {
		return positionNew;
	}

	public void setPositionNew(Long positionNew) {
		this.positionNew = positionNew;
	}

	public Long getOperator() {
		return operator;
	}

	public void setOperator(Long operator) {
		this.operator = operator;
	}

}