package com.xianzaishi.wms.track.domain.client.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryDetailQueryVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryDetailVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryQueryVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryVO;

public interface IBookingDeliveryDomainClient {

	// domain
	public SimpleResultVO<Boolean> createBookingDeliveryDomain(
			BookingDeliveryVO bookingDeliveryVO);

	public SimpleResultVO<BookingDeliveryVO> getBookingDeliveryDomainByID(
			Long bookingDeliveryID);

	public SimpleResultVO<Boolean> deleteBookingDeliveryDomain(Long id);

	// head
	public SimpleResultVO<Boolean> addBookingDeliveryVO(
			BookingDeliveryVO bookingDeliveryVO);

	public SimpleResultVO<QueryResultVO<BookingDeliveryVO>> queryBookingDeliveryVOList(
			BookingDeliveryQueryVO bookingDeliveryQueryVO);

	public SimpleResultVO<BookingDeliveryVO> getBookingDeliveryVOByID(Long id);

	public SimpleResultVO<Boolean> modifyBookingDeliveryVO(
			BookingDeliveryVO bookingDeliveryVO);

	public SimpleResultVO<Boolean> deleteBookingDeliveryVO(
			Long bookingDeliveryID);

	// body

	public SimpleResultVO<Boolean> createBookingDeliveryDetailVO(
			BookingDeliveryDetailVO bookingDeliveryDetailVO);

	public SimpleResultVO<Boolean> batchCreateBookingDeliveryDetailVO(
			List<BookingDeliveryDetailVO> bookingDeliveryDetailVOs);

	public SimpleResultVO<List<BookingDeliveryDetailVO>> queryBookingDeliveryDetailVOList(
			BookingDeliveryDetailQueryVO bookingDeliveryDetailQueryVO);

	public SimpleResultVO<List<BookingDeliveryDetailVO>> getBookingDeliveryDetailVOListByBookingDeliveryID(
			Long bookingDeliveryID);

	public SimpleResultVO<BookingDeliveryDetailQueryVO> getBookingDeliveryDetailVOByID(
			Long id);

	public SimpleResultVO<Boolean> modifyBookingDeliveryDetailVO(
			BookingDeliveryDetailVO bookingDeliveryDetailVO);

	public SimpleResultVO<Boolean> batchModifyBookingDeliveryDetailVOs(
			List<BookingDeliveryDetailVO> bookingDeliveryDetailVOs);

	public SimpleResultVO<Boolean> deleteBookingDeliveryDetailVO(Long id);

	public SimpleResultVO<Boolean> deleteBookingDeliveryDetailVOByBookingDeliveryID(
			Long bookingDeliveryID);

	public SimpleResultVO<Boolean> batchDeleteBookingDeliveryDetailVOByBookingDeliveryID(
			List<Long> storyDetailIDs);

	public SimpleResultVO<Boolean> submit(Long id);

	public SimpleResultVO<Boolean> confirm(Long id, Long auditor);

}
