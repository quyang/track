package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.util.Date;

import com.xianzaishi.wms.common.vo.QueryVO;

public class BookingDeliveryQueryVO extends QueryVO implements Serializable {

	private Long agencyId = null;

	private Long operate = null;

	private Date opTime = null;

	private String sopDay = null;

	private Long supplierId = null;

	private Date deliveryTime = null;

	private String sdeliveryDay = null;

	private Integer statu = null;

	private Long auditor = null;

	private Date auditTime = null;

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getOperate() {
		return operate;
	}

	public void setOperate(Long operate) {
		this.operate = operate;
	}

	public Date getOpTime() {
		return opTime;
	}

	public void setOpTime(Date opTime) {
		this.opTime = opTime;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public Date getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(Date deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public Long getAuditor() {
		return auditor;
	}

	public void setAuditor(Long auditor) {
		this.auditor = auditor;
	}

	public Date getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}

	public String getSopDay() {
		return sopDay;
	}

	public void setSopDay(String sopDay) {
		this.sopDay = sopDay;
	}

	public String getSdeliveryDay() {
		return sdeliveryDay;
	}

	public void setSdeliveryDay(String sdeliveryDay) {
		this.sdeliveryDay = sdeliveryDay;
	}

	public Integer getStatu() {
		return statu;
	}

	public void setStatu(Integer statu) {
		this.statu = statu;
	}

}