package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.xianzaishi.wms.common.vo.BaseVO;

public class AdjustVO extends BaseVO implements Serializable {
	private Long id = null;

	private Long agencyId = null;

	private Long operate = null;

	private Integer opReason = null;

	private Date opTime = null;

	private Long auditor = null;

	private Date auditTime = null;

	private Integer statu = null;

	private Short dr = null;

	private Date ts = null;

	private List<AdjustDetailVO> details = null;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getOperate() {
		return operate;
	}

	public void setOperate(Long operate) {
		this.operate = operate;
	}

	public Date getOpTime() {
		return opTime;
	}

	public void setOpTime(Date opTime) {
		this.opTime = opTime;
	}

	public Long getAuditor() {
		return auditor;
	}

	public void setAuditor(Long auditor) {
		this.auditor = auditor;
	}

	public Date getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}

	public Short getDr() {
		return dr;
	}

	public void setDr(Short dr) {
		this.dr = dr;
	}

	public Date getTs() {
		return ts;
	}

	public void setTs(Date ts) {
		this.ts = ts;
	}

	public Integer getOpReason() {
		return opReason;
	}

	public void setOpReason(Integer opReason) {
		this.opReason = opReason;
	}

	public Integer getStatu() {
		return statu;
	}

	public void setStatu(Integer statu) {
		this.statu = statu;
	}

	public List<AdjustDetailVO> getDetails() {
		return details;
	}

	public void setDetails(List<AdjustDetailVO> details) {
		this.details = details;
	}

}