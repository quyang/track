package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import com.xianzaishi.wms.common.vo.BaseVO;

public class StocktakingDetailVO extends BaseVO implements Serializable {

	private Long stocktakingId = null;

	private Long positionId = null;

	private String positionCode = null;

	private String positionBarcode = null;

	private Long skuId = null;

	private String skuBarcode = null;

	private Long operator = null;

	private Long auditor = null;

	private String theoreticalNo = null;

	private String firstNo = null;

	private String secondNo = null;

	private Integer theoreticalNoReal = null;

	private Integer firstNoReal = null;

	private Integer secondNoReal = null;

	private String saleUnit = null;

	private Long saleUnitId = null;

	private String specUnit = null;

	private Long specUnitId = null;

	private Integer spec = null;

	private String skuName = null;

	private Integer saleUnitType = null;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStocktakingId() {
		return stocktakingId;
	}

	public void setStocktakingId(Long stocktakingId) {
		this.stocktakingId = stocktakingId;
	}

	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Long getOperator() {
		return operator;
	}

	public void setOperator(Long operator) {
		this.operator = operator;
	}

	public Long getAuditor() {
		return auditor;
	}

	public void setAuditor(Long auditor) {
		this.auditor = auditor;
	}

	public String getPositionCode() {
		return positionCode;
	}

	public void setPositionCode(String positionCode) {
		this.positionCode = positionCode;
	}

	public String getPositionBarcode() {
		return positionBarcode;
	}

	public void setPositionBarcode(String positionBarcode) {
		this.positionBarcode = positionBarcode;
	}

	public String getSkuBarcode() {
		return skuBarcode;
	}

	public void setSkuBarcode(String skuBarcode) {
		this.skuBarcode = skuBarcode;
	}

	public String getSaleUnit() {
		return saleUnit;
	}

	public void setSaleUnit(String saleUnit) {
		this.saleUnit = saleUnit;
	}

	public Long getSaleUnitId() {
		return saleUnitId;
	}

	public void setSaleUnitId(Long saleUnitId) {
		this.saleUnitId = saleUnitId;
	}

	public String getSpecUnit() {
		return specUnit;
	}

	public void setSpecUnit(String specUnit) {
		this.specUnit = specUnit;
	}

	public Long getSpecUnitId() {
		return specUnitId;
	}

	public void setSpecUnitId(Long specUnitId) {
		this.specUnitId = specUnitId;
	}

	public Integer getSpec() {
		return spec;
	}

	public void setSpec(Integer spec) {
		this.spec = spec;
	}

	public String getSkuName() {
		return skuName;
	}

	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}

	public Integer getSaleUnitType() {
		return saleUnitType;
	}

	public void setSaleUnitType(Integer saleUnitType) {
		this.saleUnitType = saleUnitType;
	}

	public String getTheoreticalNo() {
		if (theoreticalNo == null && theoreticalNoReal != null) {
			theoreticalNo = new BigDecimal(theoreticalNoReal).divide(
					new BigDecimal(1000)).toString();
		}
		return theoreticalNo;
	}

	public void setTheoreticalNo(String theoreticalNo) {
		this.theoreticalNo = theoreticalNo;
	}

	public String getFirstNo() {
		if (firstNo == null && firstNoReal != null) {
			firstNo = new BigDecimal(firstNoReal).divide(new BigDecimal(1000))
					.toString();
		}
		return firstNo;
	}

	public void setFirstNo(String firstNo) {
		this.firstNo = firstNo;
	}

	public String getSecondNo() {
		if (secondNo == null && secondNoReal != null) {
			secondNo = new BigDecimal(secondNoReal)
					.divide(new BigDecimal(1000)).toString();
		}
		return secondNo;
	}

	public void setSecondNo(String secondNo) {
		this.secondNo = secondNo;
	}

	public Integer getTheoreticalNoReal() {
		return theoreticalNoReal;
	}

	public void setTheoreticalNoReal(Integer theoreticalNoReal) {
		this.theoreticalNoReal = theoreticalNoReal;
	}

	public Integer getFirstNoReal() {
		return firstNoReal;
	}

	public void setFirstNoReal(Integer firstNoReal) {
		this.firstNoReal = firstNoReal;
	}

	public Integer getSecondNoReal() {
		return secondNoReal;
	}

	public void setSecondNoReal(Integer secondNoReal) {
		this.secondNoReal = secondNoReal;
	}

}