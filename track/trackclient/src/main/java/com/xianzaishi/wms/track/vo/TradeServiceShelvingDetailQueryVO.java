package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.util.Date;

import com.xianzaishi.wms.common.vo.QueryVO;

public class TradeServiceShelvingDetailQueryVO extends QueryVO implements
		Serializable {

	private Long shelveId = null;

	private Long agencyId = null;

	private Long skuId = null;

	private Integer shelveNo = null;

	private Long slottingId = null;

	private Long positionId = null;

	public Long getShelveId() {
		return shelveId;
	}

	public void setShelveId(Long shelveId) {
		this.shelveId = shelveId;
	}

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getShelveNo() {
		return shelveNo;
	}

	public void setShelveNo(Integer shelveNo) {
		this.shelveNo = shelveNo;
	}

	public Long getSlottingId() {
		return slottingId;
	}

	public void setSlottingId(Long slottingId) {
		this.slottingId = slottingId;
	}

	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

}