package com.xianzaishi.wms.barcode.domain.client.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.SimpleResultVO;

public interface IBarcodeSequenceDomainClient {
	public SimpleResultVO<String> getBarcode(Integer type);

	public SimpleResultVO<List<String>> getBarcodes(Integer type, Integer no);
}
