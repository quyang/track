package com.xianzaishi.wms.track.constants;

public class TrackConstants {
	public static Integer IM_REASON_OUT_PRODUCT_CONSUME = 1;

	public static Integer IM_REASON_OUT_SPOILAGE = 2;

	public static Integer IM_REASON_OUT_REQUISITIONS = 3;

	public static Integer IM_REASON_OUT_SALE = 4;

	public static Integer IM_REASON_IN_ORDER = 5;

	public static Integer IM_REASON_IN_TRADESERVICE = 6;

	public static Integer IM_REASON_IN_PRODUCT = 7;

	public static Integer IM_REASON_ADJ_TRANSFER = 8;

	public static Integer IM_REASON_ADJ_STOCKTAKING = 9;

	public static Integer IM_TYPE_IN = 2;

	public static Integer IM_TYPE_OUT = 1;

	public static Integer IM_TYPE_ADJ = 3;
}
