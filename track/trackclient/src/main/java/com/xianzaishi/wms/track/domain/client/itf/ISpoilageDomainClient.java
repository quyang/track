package com.xianzaishi.wms.track.domain.client.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.vo.SpoilageDetailQueryVO;
import com.xianzaishi.wms.track.vo.SpoilageDetailVO;
import com.xianzaishi.wms.track.vo.SpoilageQueryVO;
import com.xianzaishi.wms.track.vo.SpoilageVO;

public interface ISpoilageDomainClient {

	// domain
	public SimpleResultVO<Boolean> createSpoilageDomain(SpoilageVO spoilageVO);

	public SimpleResultVO<SpoilageVO> getSpoilageDomainByID(Long spoilageID);

	public SimpleResultVO<Boolean> deleteSpoilageDomain(Long id);

	// head
	public SimpleResultVO<Boolean> addSpoilageVO(SpoilageVO spoilageVO);

	public SimpleResultVO<QueryResultVO<SpoilageVO>> querySpoilageVOList(
			SpoilageQueryVO spoilageQueryVO);

	public SimpleResultVO<SpoilageVO> getSpoilageVOByID(Long id);

	public SimpleResultVO<Boolean> modifySpoilageVO(SpoilageVO spoilageVO);

	public SimpleResultVO<Boolean> deleteSpoilageVO(Long spoilageID);

	// body
	public SimpleResultVO<Boolean> createSpoilageDetailVO(
			SpoilageDetailVO spoilageDetailVO);

	public SimpleResultVO<Boolean> batchCreateSpoilageDetailVO(
			List<SpoilageDetailVO> spoilageDetailVOs);

	public SimpleResultVO<List<SpoilageDetailVO>> querySpoilageDetailVOList(
			SpoilageDetailQueryVO spoilageDetailQueryVO);

	public SimpleResultVO<List<SpoilageDetailVO>> getSpoilageDetailVOListBySpoilageID(
			Long spoilageID);

	public SimpleResultVO<SpoilageDetailQueryVO> getSpoilageDetailVOByID(Long id);

	public SimpleResultVO<Boolean> modifySpoilageDetailVO(
			SpoilageDetailVO spoilageDetailVO);

	/**
	 * 按ID更新Detail
	 * 
	 * @param spoilageDetailVOs
	 * @return
	 */
	public SimpleResultVO<Boolean> batchModifySpoilageDetailVOs(
			List<SpoilageDetailVO> spoilageDetailVOs);

	public SimpleResultVO<Boolean> deleteSpoilageDetailVO(Long id);

	public SimpleResultVO<Boolean> deleteSpoilageDetailVOBySpoilageID(
			Long spoilageID);

	public SimpleResultVO<Boolean> batchDeleteSpoilageDetailVOBySpoilageID(
			List<Long> storyDetailIDs);

	public SimpleResultVO<Boolean> submit(Long id);

	public SimpleResultVO<Boolean> audit(Long id, Long auditor);

	public SimpleResultVO<Boolean> accounted(Long id);
}
