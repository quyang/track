package com.xianzaishi.wms.oplog.domain.client.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.oplog.vo.TradeServiceShelvingLogVO;

public interface ITradeServiceShelvingLogDomainClient {
	public SimpleResultVO<Boolean> batchAddTradeServiceShelvingLog(
			List<TradeServiceShelvingLogVO> tradeServiceShelvingLogVOs);
}
