package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.util.Date;

import com.xianzaishi.wms.common.vo.QueryVO;

public class OutgoingSaleDetailQueryVO extends QueryVO implements Serializable {

	private Long outgoingSaleId = null;

	private Long agencyId = null;

	private Long skuId = null;

	private Integer number = null;

	private Long slottingId = null;

	private Long positionId = null;

	public Long getOutgoingSaleId() {
		return outgoingSaleId;
	}

	public void setOutgoingSaleId(Long outgoingSaleId) {
		this.outgoingSaleId = outgoingSaleId;
	}

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Long getSlottingId() {
		return slottingId;
	}

	public void setSlottingId(Long slottingId) {
		this.slottingId = slottingId;
	}

	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

}