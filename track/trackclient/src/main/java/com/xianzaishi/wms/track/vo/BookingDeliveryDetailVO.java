package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import com.xianzaishi.wms.common.vo.BaseVO;

public class BookingDeliveryDetailVO extends BaseVO implements Serializable {

	private Long deliveryId = null;

	private Long orderId = null;

	private Long skuId = null;

	private String allNo = null;

	private String delivedNo = null;

	private String delivNo = null;

	private Integer allNoReal = null;

	private Integer delivedNoReal = null;

	private Integer delivNoReal = null;

	private String saleUnit = null;

	private Long saleUnitId = null;

	private String specUnit = null;

	private Long specUnitId = null;

	private Integer spec = null;

	private String skuName = null;

	private Integer saleUnitType = null;

	public Long getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public String getAllNo() {
		if (allNo == null && allNoReal != null) {
			allNo = new BigDecimal(allNoReal).divide(new BigDecimal(1000))
					.toString();
		}
		return allNo;
	}

	public void setAllNo(String allNo) {
		this.allNo = allNo;
	}

	public String getDelivedNo() {
		if (delivedNo == null && delivedNoReal != null) {
			delivedNo = new BigDecimal(delivedNoReal).divide(
					new BigDecimal(1000)).toString();
		}
		return delivedNo;
	}

	public void setDelivedNo(String delivedNo) {
		this.delivedNo = delivedNo;
	}

	public String getDelivNo() {
		if (delivNo == null && delivNoReal != null) {
			delivNo = new BigDecimal(delivNoReal).divide(new BigDecimal(1000))
					.toString();
		}
		return delivNo;
	}

	public void setDelivNo(String delivNo) {
		this.delivNo = delivNo;
	}

	public String getSaleUnit() {
		return saleUnit;
	}

	public void setSaleUnit(String saleUnit) {
		this.saleUnit = saleUnit;
	}

	public Long getSaleUnitId() {
		return saleUnitId;
	}

	public void setSaleUnitId(Long saleUnitId) {
		this.saleUnitId = saleUnitId;
	}

	public String getSpecUnit() {
		return specUnit;
	}

	public void setSpecUnit(String specUnit) {
		this.specUnit = specUnit;
	}

	public Long getSpecUnitId() {
		return specUnitId;
	}

	public void setSpecUnitId(Long specUnitId) {
		this.specUnitId = specUnitId;
	}

	public Integer getSpec() {
		return spec;
	}

	public void setSpec(Integer spec) {
		this.spec = spec;
	}

	public String getSkuName() {
		return skuName;
	}

	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}

	public Integer getSaleUnitType() {
		return saleUnitType;
	}

	public void setSaleUnitType(Integer saleUnitType) {
		this.saleUnitType = saleUnitType;
	}

	public Integer getAllNoReal() {
		return allNoReal;
	}

	public void setAllNoReal(Integer allNoReal) {
		this.allNoReal = allNoReal;
	}

	public Integer getDelivedNoReal() {
		return delivedNoReal;
	}

	public void setDelivedNoReal(Integer delivedNoReal) {
		this.delivedNoReal = delivedNoReal;
	}

	public Integer getDelivNoReal() {
		return delivNoReal;
	}

	public void setDelivNoReal(Integer delivNoReal) {
		this.delivNoReal = delivNoReal;
	}

}