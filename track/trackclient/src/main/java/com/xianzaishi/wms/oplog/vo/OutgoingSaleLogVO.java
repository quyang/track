package com.xianzaishi.wms.oplog.vo;

import java.io.Serializable;

import com.xianzaishi.wms.common.vo.BaseVO;

public class OutgoingSaleLogVO extends BaseVO implements Serializable {

	private Long agencyId = null;

	private Long orderId = null;

	private Long skuId = null;

	private Integer saleNo = null;

	private Long slottingId = null;

	private Long positionId = null;

	private Long opDay = null;

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getSaleNo() {
		return saleNo;
	}

	public void setSaleNo(Integer saleNo) {
		this.saleNo = saleNo;
	}

	public Long getSlottingId() {
		return slottingId;
	}

	public void setSlottingId(Long slottingId) {
		this.slottingId = slottingId;
	}

	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

	public Long getOpDay() {
		return opDay;
	}

	public void setOpDay(Long opDay) {
		this.opDay = opDay;
	}

}