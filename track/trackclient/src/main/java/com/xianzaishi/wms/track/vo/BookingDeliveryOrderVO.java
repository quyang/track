package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.util.Date;

import com.xianzaishi.wms.common.vo.BaseVO;

public class BookingDeliveryOrderVO extends BaseVO implements Serializable {

	private Long deliveryId = null;

	private Long orderId = null;

	private Short allRemain = null;

	public Long getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Short getAllRemain() {
		return allRemain;
	}

	public void setAllRemain(Short allRemain) {
		this.allRemain = allRemain;
	}

}