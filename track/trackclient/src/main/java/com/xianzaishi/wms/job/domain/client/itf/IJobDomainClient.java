package com.xianzaishi.wms.job.domain.client.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.job.vo.JobQueryVO;
import com.xianzaishi.wms.job.vo.JobVO;

public interface IJobDomainClient {
	public SimpleResultVO<Boolean> addJob(JobVO jobVO);

	public SimpleResultVO<JobVO> getJob(Long jobID);

	public SimpleResultVO<QueryResultVO<JobVO>> getJobPending(Long jobID);

	public SimpleResultVO<Boolean> processed(Long jobID);

	public SimpleResultVO<List<JobVO>> queryJobVOList(JobQueryVO jobQueryVO);
}
