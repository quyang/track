package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.xianzaishi.wms.common.vo.BaseVO;

public class StocktakingVO extends BaseVO implements Serializable {

	private Long agencyId = null;

	private Long operate = null;

	private Date opTime = null;

	private Long auditor = null;

	private Date auditTime = null;

	// 0,未提交,1,已提交,2,已复查,3,已审核
	private Integer statu = null;

	private Integer statuOld = null;

	//0,按区域,1,按商品
	private Integer type = null;

	private List<StocktakingDetailVO> details = null;

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getOperate() {
		return operate;
	}

	public void setOperate(Long operate) {
		this.operate = operate;
	}

	public Date getOpTime() {
		return opTime;
	}

	public void setOpTime(Date opTime) {
		this.opTime = opTime;
	}

	public Long getAuditor() {
		return auditor;
	}

	public void setAuditor(Long auditor) {
		this.auditor = auditor;
	}

	public Date getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}

	public Integer getStatu() {
		return statu;
	}

	public void setStatu(Integer statu) {
		this.statu = statu;
	}

	public List<StocktakingDetailVO> getDetails() {
		return details;
	}

	public void setDetails(List<StocktakingDetailVO> details) {
		this.details = details;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStatuOld() {
		return statuOld;
	}

	public void setStatuOld(Integer statuOld) {
		this.statuOld = statuOld;
	}

}