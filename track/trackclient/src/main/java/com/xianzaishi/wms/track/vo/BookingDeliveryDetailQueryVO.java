package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.util.Date;

import com.xianzaishi.wms.common.vo.QueryVO;

public class BookingDeliveryDetailQueryVO extends QueryVO implements
		Serializable {

	private Long deliveryId = null;

	private Long orderId = null;

	private Long skuId = null;

	private Integer allNo = null;

	private Integer delivedNo = null;

	private Integer delivNo = null;

	public Long getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getAllNo() {
		return allNo;
	}

	public void setAllNo(Integer allNo) {
		this.allNo = allNo;
	}

	public Integer getDelivedNo() {
		return delivedNo;
	}

	public void setDelivedNo(Integer delivedNo) {
		this.delivedNo = delivedNo;
	}

	public Integer getDelivNo() {
		return delivNo;
	}

	public void setDelivNo(Integer delivNo) {
		this.delivNo = delivNo;
	}

}