package com.xianzaishi.wms.oplog.domain.client.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.oplog.vo.OutgoingSaleLogVO;

public interface IOutgoingSaleLogDomainClient {
	public SimpleResultVO<Boolean> batchAddOutgoingSaleLog(
			List<OutgoingSaleLogVO> outgoingSaleLogVOs);
}
