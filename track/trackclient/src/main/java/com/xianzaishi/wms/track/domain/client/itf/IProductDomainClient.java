package com.xianzaishi.wms.track.domain.client.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.vo.ProductDetailQueryVO;
import com.xianzaishi.wms.track.vo.ProductDetailVO;
import com.xianzaishi.wms.track.vo.ProductQueryVO;
import com.xianzaishi.wms.track.vo.ProductVO;

public interface IProductDomainClient {

	// domain
	public SimpleResultVO<Boolean> createProductDomain(ProductVO productVO);

	public SimpleResultVO<ProductVO> getProductDomainByID(Long productID);

	public SimpleResultVO<Boolean> deleteProductDomain(Long id);

	// head
	public SimpleResultVO<Boolean> addProductVO(ProductVO productVO);

	public SimpleResultVO<QueryResultVO<ProductVO>> queryProductVOList(
			ProductQueryVO productQueryVO);

	public SimpleResultVO<ProductVO> getProductVOByID(Long id);

	public SimpleResultVO<Boolean> modifyProductVO(ProductVO productVO);

	public SimpleResultVO<Boolean> deleteProductVO(Long productID);

	// body
	public SimpleResultVO<Boolean> createProductDetailVO(
			ProductDetailVO productDetailVO);

	public SimpleResultVO<Boolean> batchCreateProductDetailVO(
			List<ProductDetailVO> productDetailVOs);

	public SimpleResultVO<List<ProductDetailVO>> queryProductDetailVOList(
			ProductDetailQueryVO productDetailQueryVO);

	public SimpleResultVO<List<ProductDetailVO>> getProductDetailVOListByProductID(
			Long productID);

	public SimpleResultVO<ProductDetailQueryVO> getProductDetailVOByID(Long id);

	public SimpleResultVO<Boolean> modifyProductDetailVO(
			ProductDetailVO productDetailVO);

	public SimpleResultVO<Boolean> batchModifyProductDetailVOs(
			List<ProductDetailVO> productDetailVOs);

	public SimpleResultVO<Boolean> deleteProductDetailVO(Long id);

	public SimpleResultVO<Boolean> deleteProductDetailVOByProductID(
			Long productID);

	public SimpleResultVO<Boolean> batchDeleteProductDetailVOByProductID(
			List<Long> storyDetailIDs);

	public SimpleResultVO<Boolean> submit(Long id);

	public SimpleResultVO<Boolean> audit(Long id, Long auditor);

	public SimpleResultVO<Boolean> accounted(Long id);

}
