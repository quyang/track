package com.xianzaishi.wms.track.domain.client.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.vo.RequisitionsDetailQueryVO;
import com.xianzaishi.wms.track.vo.RequisitionsDetailVO;
import com.xianzaishi.wms.track.vo.RequisitionsQueryVO;
import com.xianzaishi.wms.track.vo.RequisitionsVO;

public interface IRequisitionsDomainClient {

	// domain
	public SimpleResultVO<Boolean> createRequisitionsDomain(
			RequisitionsVO requisitionsVO);

	public SimpleResultVO<RequisitionsVO> getRequisitionsDomainByID(
			Long requisitionsID);

	public SimpleResultVO<Boolean> deleteRequisitionsDomain(Long id);

	// head
	public SimpleResultVO<Boolean> addRequisitionsVO(
			RequisitionsVO requisitionsVO);

	public SimpleResultVO<QueryResultVO<RequisitionsVO>> queryRequisitionsVOList(
			RequisitionsQueryVO requisitionsQueryVO);

	public SimpleResultVO<RequisitionsVO> getRequisitionsVOByID(Long id);

	public SimpleResultVO<Boolean> modifyRequisitionsVO(
			RequisitionsVO requisitionsVO);

	public SimpleResultVO<Boolean> deleteRequisitionsVO(Long requisitionsID);

	// body
	public SimpleResultVO<Boolean> createRequisitionsDetailVO(
			RequisitionsDetailVO requisitionsDetailVO);

	public SimpleResultVO<Boolean> batchCreateRequisitionsDetailVO(
			List<RequisitionsDetailVO> requisitionsDetailVOs);

	public SimpleResultVO<List<RequisitionsDetailVO>> queryRequisitionsDetailVOList(
			RequisitionsDetailQueryVO requisitionsDetailQueryVO);

	public SimpleResultVO<List<RequisitionsDetailVO>> getRequisitionsDetailVOListByRequisitionsID(
			Long requisitionsID);

	public SimpleResultVO<RequisitionsDetailQueryVO> getRequisitionsDetailVOByID(
			Long id);

	public SimpleResultVO<Boolean> modifyRequisitionsDetailVO(
			RequisitionsDetailVO requisitionsDetailVO);

	/**
	 * 按ID更新Detail
	 * 
	 * @param requisitionsDetailVOs
	 * @return
	 */
	public SimpleResultVO<Boolean> batchModifyRequisitionsDetailVOs(
			List<RequisitionsDetailVO> requisitionsDetailVOs);

	public SimpleResultVO<Boolean> deleteRequisitionsDetailVO(Long id);

	public SimpleResultVO<Boolean> deleteRequisitionsDetailVOByRequisitionsID(
			Long requisitionsID);

	public SimpleResultVO<Boolean> batchDeleteRequisitionsDetailVOByRequisitionsID(
			List<Long> storyDetailIDs);

	public SimpleResultVO<Boolean> submit(Long id);

	public SimpleResultVO<Boolean> audit(Long id, Long auditor);

	public SimpleResultVO<Boolean> outgoing(Long id, Long auditor);

	public SimpleResultVO<Boolean> outgoingAudit(Long id, Long auditor);

	public SimpleResultVO<Boolean> accounted(Long id);
}
