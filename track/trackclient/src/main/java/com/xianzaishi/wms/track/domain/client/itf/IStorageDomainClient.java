package com.xianzaishi.wms.track.domain.client.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.vo.StorageDetailQueryVO;
import com.xianzaishi.wms.track.vo.StorageDetailVO;
import com.xianzaishi.wms.track.vo.StorageQueryVO;
import com.xianzaishi.wms.track.vo.StorageVO;

public interface IStorageDomainClient {

	// domain
	public SimpleResultVO<Boolean> auditStorage(Long storageID, Long operate);

	public SimpleResultVO<Boolean> createStorageDomain(StorageVO storageVO);

	public SimpleResultVO<StorageVO> getStorageDomainByID(Long storageID);

	public SimpleResultVO<Boolean> deleteStorageDomain(Long id);

	public SimpleResultVO<Boolean> createAndAccount(StorageVO storageVO);

	// head
	public SimpleResultVO<Boolean> addStorageVO(StorageVO storageVO);

	public SimpleResultVO<QueryResultVO<StorageVO>> queryStorageVOList(
			StorageQueryVO storageQueryVO);

	public SimpleResultVO<StorageVO> getStorageVOByID(Long id);

	public SimpleResultVO<Boolean> modifyStorageVO(StorageVO storageVO);

	public SimpleResultVO<Boolean> deleteStorageVO(Long storageID);

	// body
	public SimpleResultVO<Boolean> createStorageDetailVO(
			StorageDetailVO storageDetailVO);

	public SimpleResultVO<Boolean> batchCreateStorageDetailVO(
			List<StorageDetailVO> storageDetailVOs);

	public SimpleResultVO<List<StorageDetailVO>> queryStorageDetailVOList(
			StorageDetailQueryVO storageDetailQueryVO);

	public SimpleResultVO<List<StorageDetailVO>> getStorageDetailVOListByStorageID(
			Long storageID);

	public SimpleResultVO<StorageDetailQueryVO> getStorageDetailVOByID(Long id);

	public SimpleResultVO<Boolean> modifyStorageDetailVO(
			StorageDetailVO storageDetailVO);

	public SimpleResultVO<Boolean> batchModifyStorageDetailVOs(
			List<StorageDetailVO> storageDetailVOs);

	public SimpleResultVO<Boolean> deleteStorageDetailVO(Long id);

	public SimpleResultVO<Boolean> deleteStorageDetailVOByStorageID(
			Long storageID);

	public SimpleResultVO<Boolean> batchDeleteStorageDetailVOByStorageID(
			List<Long> storyDetailIDs);

	public SimpleResultVO<Boolean> submit(Long id);

	public SimpleResultVO<Boolean> audit(Long id, Long auditor);

	public SimpleResultVO<Boolean> accounted(Long id);

	/**
	 * 入库并返回某次入库id
	 * @param storageVO
	 * @return
	 */
	public SimpleResultVO<Long> getStoreIdCreateAndAccount(StorageVO storageVO);

}
