package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.xianzaishi.wms.common.vo.BaseVO;

public class InspectionDetailVO extends BaseVO implements Serializable {

	private Long inspectionId = null;

	private Long agencyId = null;

	private Long skuId = null;

	private String delivedNo = null;

	private String bookingNo = null;

	private String qualifiedNo = null;

	private Integer delivedNoReal = null;

	private Integer bookingNoReal = null;

	private Integer qualifiedNoReal = null;

	private String saleUnit = null;

	private Long saleUnitId = null;

	private String specUnit = null;

	private Long specUnitId = null;

	private Integer spec = null;

	private String skuName = null;

	private Integer saleUnitType = null;

	public Long getInspectionId() {
		return inspectionId;
	}

	public void setInspectionId(Long inspectionId) {
		this.inspectionId = inspectionId;
	}

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public String getSaleUnit() {
		return saleUnit;
	}

	public void setSaleUnit(String saleUnit) {
		this.saleUnit = saleUnit;
	}

	public Long getSaleUnitId() {
		return saleUnitId;
	}

	public void setSaleUnitId(Long saleUnitId) {
		this.saleUnitId = saleUnitId;
	}

	public String getSpecUnit() {
		return specUnit;
	}

	public void setSpecUnit(String specUnit) {
		this.specUnit = specUnit;
	}

	public Long getSpecUnitId() {
		return specUnitId;
	}

	public void setSpecUnitId(Long specUnitId) {
		this.specUnitId = specUnitId;
	}

	public Integer getSpec() {
		return spec;
	}

	public void setSpec(Integer spec) {
		this.spec = spec;
	}

	public String getSkuName() {
		return skuName;
	}

	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}

	public Integer getSaleUnitType() {
		return saleUnitType;
	}

	public void setSaleUnitType(Integer saleUnitType) {
		this.saleUnitType = saleUnitType;
	}

	public String getDelivedNo() {
		if (delivedNo == null && delivedNoReal != null) {
			delivedNo = new BigDecimal(delivedNoReal).divide(
					new BigDecimal(1000)).toString();
		}
		return delivedNo;
	}

	public void setDelivedNo(String delivedNo) {
		this.delivedNo = delivedNo;
	}

	public String getBookingNo() {
		if (bookingNo == null && bookingNoReal != null) {
			bookingNo = new BigDecimal(bookingNoReal).divide(
					new BigDecimal(1000)).toString();
		}
		return bookingNo;
	}

	public void setBookingNo(String bookingNo) {
		this.bookingNo = bookingNo;
	}

	public String getQualifiedNo() {
		if (qualifiedNo == null && qualifiedNoReal != null) {
			qualifiedNo = new BigDecimal(qualifiedNoReal).divide(
					new BigDecimal(1000)).toString();
		}
		return qualifiedNo;
	}

	public void setQualifiedNo(String qualifiedNo) {
		this.qualifiedNo = qualifiedNo;
	}

	public Integer getDelivedNoReal() {
		return delivedNoReal;
	}

	public void setDelivedNoReal(Integer delivedNoReal) {
		this.delivedNoReal = delivedNoReal;
	}

	public Integer getBookingNoReal() {
		return bookingNoReal;
	}

	public void setBookingNoReal(Integer bookingNoReal) {
		this.bookingNoReal = bookingNoReal;
	}

	public Integer getQualifiedNoReal() {
		return qualifiedNoReal;
	}

	public void setQualifiedNoReal(Integer qualifiedNoReal) {
		this.qualifiedNoReal = qualifiedNoReal;
	}

}