package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.xianzaishi.wms.common.vo.BaseVO;

public class OutgoingSaleVO extends BaseVO implements Serializable {

	private Long agencyId = null;

	private Long operate = null;

	private Date opTime = null;

	private Integer statu = null;

	private Long auditor = null;

	private Date auditTime = null;

	private Long orderId = null;

	private List<OutgoingSaleDetailVO> details = null;

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getOperate() {
		return operate;
	}

	public void setOperate(Long operate) {
		this.operate = operate;
	}

	public Date getOpTime() {
		return opTime;
	}

	public void setOpTime(Date opTime) {
		this.opTime = opTime;
	}

	public Long getAuditor() {
		return auditor;
	}

	public void setAuditor(Long auditor) {
		this.auditor = auditor;
	}

	public Date getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}

	public Integer getStatu() {
		return statu;
	}

	public void setStatu(Integer statu) {
		this.statu = statu;
	}

	public List<OutgoingSaleDetailVO> getDetails() {
		return details;
	}

	public void setDetails(List<OutgoingSaleDetailVO> details) {
		this.details = details;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

}