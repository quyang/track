package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import com.xianzaishi.wms.common.vo.BaseVO;

public class TransferDetailVO extends BaseVO implements Serializable {

	private Long transferId = null;

	private Long positionOld = null;

	private String positionOldCode = null;

	private String positionNewCode = null;

	private String positionOldBarcode = null;

	private String positionNewBarcode = null;

	private String skuBarcode = null;

	private Long skuId = null;

	private String number = null;

	private Integer numberReal = null;

	private Long positionNew = null;

	private Long operator = null;

	private String saleUnit = null;

	private Long saleUnitId = null;

	private String specUnit = null;

	private Long specUnitId = null;

	private Integer spec = null;

	private String skuName = null;

	private Integer saleUnitType = null;

	public Long getTransferId() {
		return transferId;
	}

	public void setTransferId(Long transferId) {
		this.transferId = transferId;
	}

	public Long getPositionOld() {
		return positionOld;
	}

	public void setPositionOld(Long positionOld) {
		this.positionOld = positionOld;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Long getPositionNew() {
		return positionNew;
	}

	public void setPositionNew(Long positionNew) {
		this.positionNew = positionNew;
	}

	public Long getOperator() {
		return operator;
	}

	public void setOperator(Long operator) {
		this.operator = operator;
	}

	public String getPositionOldBarcode() {
		return positionOldBarcode;
	}

	public void setPositionOldBarcode(String positionOldBarcode) {
		this.positionOldBarcode = positionOldBarcode;
	}

	public String getPositionNewBarcode() {
		return positionNewBarcode;
	}

	public void setPositionNewBarcode(String positionNewBarcode) {
		this.positionNewBarcode = positionNewBarcode;
	}

	public String getSkuBarcode() {
		return skuBarcode;
	}

	public void setSkuBarcode(String skuBarcode) {
		this.skuBarcode = skuBarcode;
	}

	public String getPositionOldCode() {
		return positionOldCode;
	}

	public void setPositionOldCode(String positionOldCode) {
		this.positionOldCode = positionOldCode;
	}

	public String getPositionNewCode() {
		return positionNewCode;
	}

	public void setPositionNewCode(String positionNewCode) {
		this.positionNewCode = positionNewCode;
	}

	public String getSaleUnit() {
		return saleUnit;
	}

	public void setSaleUnit(String saleUnit) {
		this.saleUnit = saleUnit;
	}

	public Long getSaleUnitId() {
		return saleUnitId;
	}

	public void setSaleUnitId(Long saleUnitId) {
		this.saleUnitId = saleUnitId;
	}

	public String getSpecUnit() {
		return specUnit;
	}

	public void setSpecUnit(String specUnit) {
		this.specUnit = specUnit;
	}

	public Long getSpecUnitId() {
		return specUnitId;
	}

	public void setSpecUnitId(Long specUnitId) {
		this.specUnitId = specUnitId;
	}

	public Integer getSpec() {
		return spec;
	}

	public void setSpec(Integer spec) {
		this.spec = spec;
	}

	public String getSkuName() {
		return skuName;
	}

	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}

	public Integer getSaleUnitType() {
		return saleUnitType;
	}

	public void setSaleUnitType(Integer saleUnitType) {
		this.saleUnitType = saleUnitType;
	}

	public String getNumber() {
		if (number == null && numberReal != null) {
			number = new BigDecimal(numberReal).divide(new BigDecimal(1000))
					.toString();
		}
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Integer getNumberReal() {
		return numberReal;
	}

	public void setNumberReal(Integer numberReal) {
		this.numberReal = numberReal;
	}

}