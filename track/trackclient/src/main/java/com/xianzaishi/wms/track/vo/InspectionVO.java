package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.xianzaishi.wms.common.vo.BaseVO;

public class InspectionVO extends BaseVO implements Serializable {

	private Long deliveryId = null;

	private Long agencyId = null;

	private Long operate = null;

	private Date opTime = null;

	private Long auditor = null;

	private Integer statu = null;

	private Date auditTime = null;

	private List details = null;

	public Long getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getOperate() {
		return operate;
	}

	public void setOperate(Long operate) {
		this.operate = operate;
	}

	public Date getOpTime() {
		return opTime;
	}

	public void setOpTime(Date opTime) {
		this.opTime = opTime;
	}

	public Long getAuditor() {
		return auditor;
	}

	public void setAuditor(Long auditor) {
		this.auditor = auditor;
	}

	public Date getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}

	public List getDetails() {
		return details;
	}

	public void setDetails(List details) {
		this.details = details;
	}

	public Integer getStatu() {
		return statu;
	}

	public void setStatu(Integer statu) {
		this.statu = statu;
	}

}