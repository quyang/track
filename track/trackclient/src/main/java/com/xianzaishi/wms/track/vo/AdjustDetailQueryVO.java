package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.util.Date;

import com.xianzaishi.wms.common.vo.QueryVO;

public class AdjustDetailQueryVO extends QueryVO  implements Serializable{
	private Long id = null;
	
	private Long adjustId = null;
	
	private Long skuId = null;
	
	private Integer number = null;
	
	private Long positionId = null;
	
	private Short dr = null;
	
	private Date ts = null;
	
	private Integer saleUnitType = null;
	
	private String positionCode = null;
	
	private Integer spec = null;
	
	private String saleUnit = null;
	
	private Long saleUnitId = null;
	
	private String specUnit = null;
	
	private Long specUnitId = null;
	
	private String skuName = null;
	
	private Integer numberOld = null;
	
	public Long getId () {
		return id;
	}
	
	public void setId ( Long id) {
		this.id = id;
	}
	public Long getAdjustId () {
		return adjustId;
	}
	
	public void setAdjustId ( Long adjustId) {
		this.adjustId = adjustId;
	}
	public Long getSkuId () {
		return skuId;
	}
	
	public void setSkuId ( Long skuId) {
		this.skuId = skuId;
	}
	public Integer getNumber () {
		return number;
	}
	
	public void setNumber ( Integer number) {
		this.number = number;
	}
	public Long getPositionId () {
		return positionId;
	}
	
	public void setPositionId ( Long positionId) {
		this.positionId = positionId;
	}
	public Short getDr () {
		return dr;
	}
	
	public void setDr ( Short dr) {
		this.dr = dr;
	}
	public Date getTs () {
		return ts;
	}
	
	public void setTs ( Date ts) {
		this.ts = ts;
	}
	public Integer getSaleUnitType () {
		return saleUnitType;
	}
	
	public void setSaleUnitType ( Integer saleUnitType) {
		this.saleUnitType = saleUnitType;
	}
	public String getPositionCode () {
		return positionCode;
	}
	
	public void setPositionCode ( String positionCode) {
		this.positionCode = positionCode;
	}
	public Integer getSpec () {
		return spec;
	}
	
	public void setSpec ( Integer spec) {
		this.spec = spec;
	}
	public String getSaleUnit () {
		return saleUnit;
	}
	
	public void setSaleUnit ( String saleUnit) {
		this.saleUnit = saleUnit;
	}
	public Long getSaleUnitId () {
		return saleUnitId;
	}
	
	public void setSaleUnitId ( Long saleUnitId) {
		this.saleUnitId = saleUnitId;
	}
	public String getSpecUnit () {
		return specUnit;
	}
	
	public void setSpecUnit ( String specUnit) {
		this.specUnit = specUnit;
	}
	public Long getSpecUnitId () {
		return specUnitId;
	}
	
	public void setSpecUnitId ( Long specUnitId) {
		this.specUnitId = specUnitId;
	}
	public String getSkuName () {
		return skuName;
	}
	
	public void setSkuName ( String skuName) {
		this.skuName = skuName;
	}
	public Integer getNumberOld () {
		return numberOld;
	}
	
	public void setNumberOld ( Integer numberOld) {
		this.numberOld = numberOld;
	}

}