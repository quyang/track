package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.util.Date;

import com.xianzaishi.wms.common.vo.QueryVO;

public class SpoilageDetailQueryVO extends QueryVO implements Serializable {

	private Long spoilageId = null;

	private Long skuId = null;

	private Integer spoilageNo = null;

	private Long positionId = null;

	public Long getSpoilageId() {
		return spoilageId;
	}

	public void setSpoilageId(Long spoilageId) {
		this.spoilageId = spoilageId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getSpoilageNo() {
		return spoilageNo;
	}

	public void setSpoilageNo(Integer spoilageNo) {
		this.spoilageNo = spoilageNo;
	}

	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

}