package com.xianzaishi.wms.trade.callback.client.itf;

import com.xianzaishi.wms.common.vo.SimpleResultVO;

public interface ITrackCallbackClient {
	public SimpleResultVO<Boolean> onOrderCreate(Long orderID);
	
	public SimpleResultVO<Boolean> onOrderClose(Long orderID);
}
