package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.util.Date;

import com.xianzaishi.wms.common.vo.QueryVO;

public class StorageQueryVO extends QueryVO implements Serializable {

	private Long agencyId = null;

	private Long operate = null;

	private Date opTime = null;

	private String sopDay = null;

	private Integer statu = null;

	private Date opTimeStart = null;

	private Date opTimeEnd = null;

	private Long auditor = null;

	private Date auditTime = null;

	private Integer opReason = null;

	private Long relationId;

	public Long getRelationId() {
		return relationId;
	}

	public void setRelationId(Long relationId) {
		this.relationId = relationId;
	}

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getOperate() {
		return operate;
	}

	public void setOperate(Long operate) {
		this.operate = operate;
	}

	public Date getOpTime() {
		return opTime;
	}

	public void setOpTime(Date opTime) {
		this.opTime = opTime;
	}

	public Long getAuditor() {
		return auditor;
	}

	public void setAuditor(Long auditor) {
		this.auditor = auditor;
	}

	public Date getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}

	public String getSopDay() {
		return sopDay;
	}

	public void setSopDay(String sopDay) {
		this.sopDay = sopDay;
	}

	public Date getOpTimeStart() {
		return opTimeStart;
	}

	public void setOpTimeStart(Date opTimeStart) {
		this.opTimeStart = opTimeStart;
	}

	public Date getOpTimeEnd() {
		return opTimeEnd;
	}

	public void setOpTimeEnd(Date opTimeEnd) {
		this.opTimeEnd = opTimeEnd;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getOpReason() {
		return opReason;
	}

	public void setOpReason(Integer opReason) {
		this.opReason = opReason;
	}

	public Integer getStatu() {
		return statu;
	}

	public void setStatu(Integer statu) {
		this.statu = statu;
	}
}