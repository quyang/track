package com.xianzaishi.wms.track.domain.client.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.vo.OutgoingDetailQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingDetailVO;
import com.xianzaishi.wms.track.vo.OutgoingQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingVO;

public interface IOutgoingDomainClient {

	// domain
	public SimpleResultVO<Boolean> auditOutgoing(Long outgoingID, Long operate);

	public SimpleResultVO<Boolean> createOutgoingDomain(OutgoingVO outgoingVO);

	public SimpleResultVO<OutgoingVO> getOutgoingDomainByID(Long outgoingID);

	public SimpleResultVO<Boolean> deleteOutgoingDomain(Long id);

	public SimpleResultVO<Boolean> createAndAccount(OutgoingVO outgoingVO);

	// head
	public SimpleResultVO<Boolean> addOutgoingVO(OutgoingVO outgoingVO);

	public SimpleResultVO<QueryResultVO<OutgoingVO>> queryOutgoingVOList(
			OutgoingQueryVO outgoingQueryVO);

	public SimpleResultVO<OutgoingVO> getOutgoingVOByID(Long id);

	public SimpleResultVO<Boolean> modifyOutgoingVO(OutgoingVO outgoingVO);

	public SimpleResultVO<Boolean> deleteOutgoingVO(Long outgoingID);

	// body

	public SimpleResultVO<Boolean> createOutgoingDetailVO(
			OutgoingDetailVO outgoingDetailVO);

	public SimpleResultVO<Boolean> batchCreateOutgoingDetailVO(
			List<OutgoingDetailVO> outgoingDetailVOs);

	public SimpleResultVO<List<OutgoingDetailVO>> queryOutgoingDetailVOList(
			OutgoingDetailQueryVO outgoingDetailQueryVO);

	public SimpleResultVO<List<OutgoingDetailVO>> getOutgoingDetailVOListByOutgoingID(
			Long outgoingID);

	public SimpleResultVO<OutgoingDetailQueryVO> getOutgoingDetailVOByID(Long id);

	public SimpleResultVO<Boolean> modifyOutgoingDetailVO(
			OutgoingDetailVO outgoingDetailVO);

	public SimpleResultVO<Boolean> batchModifyOutgoingDetailVOs(
			List<OutgoingDetailVO> outgoingDetailVOs);

	public SimpleResultVO<Boolean> deleteOutgoingDetailVO(Long id);

	public SimpleResultVO<Boolean> deleteOutgoingDetailVOByOutgoingID(
			Long outgoingID);

	public SimpleResultVO<Boolean> batchDeleteOutgoingDetailVOByOutgoingID(
			List<Long> storyDetailIDs);

	public SimpleResultVO<Boolean> submit(Long id);

	public SimpleResultVO<Boolean> accounted(Long id);

}
