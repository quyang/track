package com.xianzaishi.wms.track.domain.client.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.vo.InspectionDetailQueryVO;
import com.xianzaishi.wms.track.vo.InspectionDetailVO;
import com.xianzaishi.wms.track.vo.InspectionQueryVO;
import com.xianzaishi.wms.track.vo.InspectionVO;

public interface IInspectionDomainClient {

	// domain
	public SimpleResultVO<Boolean> createInspectionDomain(
			InspectionVO inspectionVO);

	public SimpleResultVO<InspectionVO> getInspectionDomainByID(
			Long inspectionID);

	public SimpleResultVO<Boolean> deleteInspectionDomain(Long id);

	// head
	public SimpleResultVO<Boolean> addInspectionVO(InspectionVO inspectionVO);

	public SimpleResultVO<QueryResultVO<InspectionVO>> queryInspectionVOList(
			InspectionQueryVO inspectionQueryVO);

	public SimpleResultVO<InspectionVO> getInspectionVOByID(Long id);

	public SimpleResultVO<Boolean> modifyInspectionVO(InspectionVO inspectionVO);

	public SimpleResultVO<Boolean> deleteInspectionVO(Long inspectionID);

	// body

	public SimpleResultVO<Boolean> createInspectionDetailVO(
			InspectionDetailVO inspectionDetailVO);

	public SimpleResultVO<Boolean> batchCreateInspectionDetailVO(
			List<InspectionDetailVO> inspectionDetailVOs);

	public SimpleResultVO<List<InspectionDetailVO>> queryInspectionDetailVOList(
			InspectionDetailQueryVO inspectionDetailQueryVO);

	public SimpleResultVO<List<InspectionDetailVO>> getInspectionDetailVOListByInspectionID(
			Long inspectionID);

	public SimpleResultVO<InspectionDetailQueryVO> getInspectionDetailVOByID(
			Long id);

	public SimpleResultVO<Boolean> modifyInspectionDetailVO(
			InspectionDetailVO inspectionDetailVO);

	public SimpleResultVO<Boolean> batchModifyInspectionDetailVOs(
			List<InspectionDetailVO> inspectionDetailVOs);

	public SimpleResultVO<Boolean> deleteInspectionDetailVO(Long id);

	public SimpleResultVO<Boolean> deleteInspectionDetailVOByInspectionID(
			Long inspectionID);

	public SimpleResultVO<Boolean> batchDeleteInspectionDetailVOByInspectionID(
			List<Long> storyDetailIDs);

	public SimpleResultVO<Boolean> submit(Long id);

	public SimpleResultVO<Boolean> audit(Long id, Long auditor);

}
