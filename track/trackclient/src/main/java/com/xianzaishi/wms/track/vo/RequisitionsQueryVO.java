package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.util.Date;

import com.xianzaishi.wms.common.vo.QueryVO;

public class RequisitionsQueryVO extends QueryVO implements Serializable {

	private Long agencyId = null;

	private Long receiver = null;

	private Long operate = null;

	private String sopTimeStart = null;

	private Date opTimeStart = null;

	private Date opTimeEnd = null;

	private String sopTimeEnd = null;

	private Long auditor = null;

	private Long stallsId = null;

	private Integer statu = null;

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getReceiver() {
		return receiver;
	}

	public void setReceiver(Long receiver) {
		this.receiver = receiver;
	}

	public Long getOperate() {
		return operate;
	}

	public void setOperate(Long operate) {
		this.operate = operate;
	}

	public Long getAuditor() {
		return auditor;
	}

	public void setAuditor(Long auditor) {
		this.auditor = auditor;
	}

	public Long getStallsId() {
		return stallsId;
	}

	public void setStallsId(Long stallsId) {
		this.stallsId = stallsId;
	}

	public Integer getStatu() {
		return statu;
	}

	public void setStatu(Integer statu) {
		this.statu = statu;
	}

	public String getSopTimeStart() {
		return sopTimeStart;
	}

	public void setSopTimeStart(String sopTimeStart) {
		this.sopTimeStart = sopTimeStart;
	}

	public Date getOpTimeStart() {
		return opTimeStart;
	}

	public void setOpTimeStart(Date opTimeStart) {
		this.opTimeStart = opTimeStart;
	}

	public Date getOpTimeEnd() {
		return opTimeEnd;
	}

	public void setOpTimeEnd(Date opTimeEnd) {
		this.opTimeEnd = opTimeEnd;
	}

	public String getSopTimeEnd() {
		return sopTimeEnd;
	}

	public void setSopTimeEnd(String sopTimeEnd) {
		this.sopTimeEnd = sopTimeEnd;
	}
}