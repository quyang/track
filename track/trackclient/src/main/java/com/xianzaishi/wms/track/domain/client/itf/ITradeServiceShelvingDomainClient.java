package com.xianzaishi.wms.track.domain.client.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingDetailQueryVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingDetailVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingQueryVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingVO;

public interface ITradeServiceShelvingDomainClient {

	// domain
	public SimpleResultVO<Boolean> createTradeServiceShelvingDomain(
			TradeServiceShelvingVO tradeServiceShelvingVO);

	public SimpleResultVO<TradeServiceShelvingVO> getTradeServiceShelvingDomainByID(
			Long tradeServiceShelvingID);

	public SimpleResultVO<Boolean> deleteTradeServiceShelvingDomain(Long id);

	public SimpleResultVO<Boolean> createAndAccount(
			TradeServiceShelvingVO tradeServiceShelvingVO);

	// head
	public SimpleResultVO<Boolean> addTradeServiceShelvingVO(
			TradeServiceShelvingVO tradeServiceShelvingVO);

	public SimpleResultVO<QueryResultVO<TradeServiceShelvingVO>> queryTradeServiceShelvingVOList(
			TradeServiceShelvingQueryVO tradeServiceShelvingQueryVO);

	public SimpleResultVO<TradeServiceShelvingVO> getTradeServiceShelvingVOByID(
			Long id);

	public SimpleResultVO<Boolean> modifyTradeServiceShelvingVO(
			TradeServiceShelvingVO tradeServiceShelvingVO);

	public SimpleResultVO<Boolean> deleteTradeServiceShelvingVO(
			Long tradeServiceShelvingID);

	// body
	public SimpleResultVO<Boolean> createTradeServiceShelvingDetailVO(
			TradeServiceShelvingDetailVO tradeServiceShelvingDetailVO);

	public SimpleResultVO<Boolean> batchCreateTradeServiceShelvingDetailVO(
			List<TradeServiceShelvingDetailVO> tradeServiceShelvingDetailVOs);

	public SimpleResultVO<List<TradeServiceShelvingDetailVO>> queryTradeServiceShelvingDetailVOList(
			TradeServiceShelvingDetailQueryVO tradeServiceShelvingDetailQueryVO);

	public SimpleResultVO<List<TradeServiceShelvingDetailVO>> getTradeServiceShelvingDetailVOListByTradeServiceShelvingID(
			Long tradeServiceShelvingID);

	public SimpleResultVO<TradeServiceShelvingDetailQueryVO> getTradeServiceShelvingDetailVOByID(
			Long id);

	public SimpleResultVO<Boolean> modifyTradeServiceShelvingDetailVO(
			TradeServiceShelvingDetailVO tradeServiceShelvingDetailVO);

	public SimpleResultVO<Boolean> batchModifyTradeServiceShelvingDetailVOs(
			List<TradeServiceShelvingDetailVO> tradeServiceShelvingDetailVOs);

	public SimpleResultVO<Boolean> deleteTradeServiceShelvingDetailVO(Long id);

	public SimpleResultVO<Boolean> deleteTradeServiceShelvingDetailVOByTradeServiceShelvingID(
			Long tradeServiceShelvingID);

	public SimpleResultVO<Boolean> batchDeleteTradeServiceShelvingDetailVOByTradeServiceShelvingID(
			List<Long> storyDetailIDs);

	public SimpleResultVO<Boolean> submit(Long id);

	public SimpleResultVO<Boolean> audit(Long id, Long auditor);

	public SimpleResultVO<Boolean> accounted(Long id);
}
