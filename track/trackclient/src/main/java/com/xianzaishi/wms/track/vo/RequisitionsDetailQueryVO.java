package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.util.Date;

import com.xianzaishi.wms.common.vo.QueryVO;

public class RequisitionsDetailQueryVO extends QueryVO implements Serializable {

	private Long requisitionsId = null;

	private Long skuId = null;

	private Integer number = null;

	private Long positionId = null;

	public Long getRequisitionsId() {
		return requisitionsId;
	}

	public void setRequisitionsId(Long requisitionsId) {
		this.requisitionsId = requisitionsId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

}