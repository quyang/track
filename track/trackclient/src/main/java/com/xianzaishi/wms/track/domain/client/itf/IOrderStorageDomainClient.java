package com.xianzaishi.wms.track.domain.client.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.vo.OrderStorageDetailQueryVO;
import com.xianzaishi.wms.track.vo.OrderStorageDetailVO;
import com.xianzaishi.wms.track.vo.OrderStorageQueryVO;
import com.xianzaishi.wms.track.vo.OrderStorageVO;

public interface IOrderStorageDomainClient {

	// domain
	public SimpleResultVO<Boolean> createOrderStorageDomain(
			OrderStorageVO orderStorageVO);

	public SimpleResultVO<OrderStorageVO> getOrderStorageDomainByID(
			Long orderStorageID);

	public SimpleResultVO<Boolean> deleteOrderStorageDomain(Long id);

	// head
	public SimpleResultVO<Boolean> addOrderStorageVO(
			OrderStorageVO orderStorageVO);

	public SimpleResultVO<QueryResultVO<OrderStorageVO>> queryOrderStorageVOList(
			OrderStorageQueryVO orderStorageQueryVO);

	public SimpleResultVO<OrderStorageVO> getOrderStorageVOByID(Long id);

	public SimpleResultVO<Boolean> modifyOrderStorageVO(
			OrderStorageVO orderStorageVO);

	public SimpleResultVO<Boolean> deleteOrderStorageVO(Long orderStorageID);

	// body
	public SimpleResultVO<Boolean> createOrderStorageDetailVO(
			OrderStorageDetailVO orderStorageDetailVO);

	public SimpleResultVO<Boolean> batchCreateOrderStorageDetailVO(
			List<OrderStorageDetailVO> orderStorageDetailVOs);

	public SimpleResultVO<List<OrderStorageDetailVO>> queryOrderStorageDetailVOList(
			OrderStorageDetailQueryVO orderStorageDetailQueryVO);

	public SimpleResultVO<List<OrderStorageDetailVO>> getOrderStorageDetailVOListByOrderStorageID(
			Long orderStorageID);

	public SimpleResultVO<OrderStorageDetailQueryVO> getOrderStorageDetailVOByID(
			Long id);

	public SimpleResultVO<Boolean> modifyOrderStorageDetailVO(
			OrderStorageDetailVO orderStorageDetailVO);

	public SimpleResultVO<Boolean> batchModifyOrderStorageDetailVOs(
			List<OrderStorageDetailVO> orderStorageDetailVOs);

	public SimpleResultVO<Boolean> deleteOrderStorageDetailVO(Long id);

	public SimpleResultVO<Boolean> deleteOrderStorageDetailVOByOrderStorageID(
			Long orderStorageID);

	public SimpleResultVO<Boolean> batchDeleteOrderStorageDetailVOByOrderStorageID(
			List<Long> storyDetailIDs);

	public SimpleResultVO<Boolean> submit(Long id);

	public SimpleResultVO<Boolean> audit(Long id, Long auditor);

	public SimpleResultVO<Boolean> accounted(Long id);
}
