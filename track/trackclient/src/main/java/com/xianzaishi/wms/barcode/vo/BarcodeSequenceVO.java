package com.xianzaishi.wms.barcode.vo;

import java.io.Serializable;
import java.util.Date;

import com.xianzaishi.wms.common.vo.BaseVO;

public class BarcodeSequenceVO extends BaseVO implements Serializable {

	private Integer type = null;

	private String code = null;

	private Long maxseqno = null;

	private Long seqno = null;

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getSeqno() {
		return seqno;
	}

	public void setSeqno(Long seqno) {
		this.seqno = seqno;
	}

	public Long getMaxseqno() {
		return maxseqno;
	}

	public void setMaxseqno(Long maxseqno) {
		this.maxseqno = maxseqno;
	}

}