package com.xianzaishi.wms.track.vo;

import java.io.Serializable;

import com.xianzaishi.wms.common.vo.QueryVO;

public class StocktakingDetailQueryVO extends QueryVO implements Serializable {

	private Long stocktakingId = null;

	private Long positionId = null;

	private Long skuId = null;

	private Long operator = null;

	private Long auditor = null;

	public Long getStocktakingId() {
		return stocktakingId;
	}

	public void setStocktakingId(Long stocktakingId) {
		this.stocktakingId = stocktakingId;
	}

	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Long getOperator() {
		return operator;
	}

	public void setOperator(Long operator) {
		this.operator = operator;
	}

	public Long getAuditor() {
		return auditor;
	}

	public void setAuditor(Long auditor) {
		this.auditor = auditor;
	}

}