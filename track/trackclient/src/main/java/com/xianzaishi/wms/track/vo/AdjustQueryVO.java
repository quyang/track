package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.util.Date;

import com.xianzaishi.wms.common.vo.QueryVO;

public class AdjustQueryVO extends QueryVO  implements Serializable{
	private Long id = null;
	
	private Long agencyId = null;
	
	private Long operate = null;
	
	private Short opReason = null;
	
	private Date opTime = null;
	
	private Long auditor = null;
	
	private Date auditTime = null;
	
	private Short statu = null;
	
	private Short dr = null;
	
	private Date ts = null;
	
	public Long getId () {
		return id;
	}
	
	public void setId ( Long id) {
		this.id = id;
	}
	public Long getAgencyId () {
		return agencyId;
	}
	
	public void setAgencyId ( Long agencyId) {
		this.agencyId = agencyId;
	}
	public Long getOperate () {
		return operate;
	}
	
	public void setOperate ( Long operate) {
		this.operate = operate;
	}
	public Short getOpReason () {
		return opReason;
	}
	
	public void setOpReason ( Short opReason) {
		this.opReason = opReason;
	}
	public Date getOpTime () {
		return opTime;
	}
	
	public void setOpTime ( Date opTime) {
		this.opTime = opTime;
	}
	public Long getAuditor () {
		return auditor;
	}
	
	public void setAuditor ( Long auditor) {
		this.auditor = auditor;
	}
	public Date getAuditTime () {
		return auditTime;
	}
	
	public void setAuditTime ( Date auditTime) {
		this.auditTime = auditTime;
	}
	public Short getStatu () {
		return statu;
	}
	
	public void setStatu ( Short statu) {
		this.statu = statu;
	}
	public Short getDr () {
		return dr;
	}
	
	public void setDr ( Short dr) {
		this.dr = dr;
	}
	public Date getTs () {
		return ts;
	}
	
	public void setTs ( Date ts) {
		this.ts = ts;
	}

}