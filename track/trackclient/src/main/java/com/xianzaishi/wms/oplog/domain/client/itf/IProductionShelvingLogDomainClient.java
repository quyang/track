package com.xianzaishi.wms.oplog.domain.client.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.oplog.vo.ProductionShelvingLogVO;

public interface IProductionShelvingLogDomainClient {
	public SimpleResultVO<Boolean> batchAddProductionShelvingLog(
			List<ProductionShelvingLogVO> productionShelvingLogVOs);
}
