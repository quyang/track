package com.xianzaishi.wms.track.domain.client.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.vo.AdjustDetailQueryVO;
import com.xianzaishi.wms.track.vo.AdjustDetailVO;
import com.xianzaishi.wms.track.vo.AdjustQueryVO;
import com.xianzaishi.wms.track.vo.AdjustVO;

public interface IAdjustDomainClient {

	// domain
	public SimpleResultVO<Boolean> auditAdjust(Long adjustID, Long operate);

	public SimpleResultVO<Boolean> createAdjustDomain(AdjustVO adjustVO);

	public SimpleResultVO<AdjustVO> getAdjustDomainByID(Long adjustID);

	public SimpleResultVO<Boolean> deleteAdjustDomain(Long id);

	public SimpleResultVO<Boolean> createAndAccount(AdjustVO adjustVO);

	// head
	public SimpleResultVO<Boolean> addAdjustVO(AdjustVO adjustVO);

	public SimpleResultVO<QueryResultVO<AdjustVO>> queryAdjustVOList(
			AdjustQueryVO adjustQueryVO);

	public SimpleResultVO<AdjustVO> getAdjustVOByID(Long id);

	public SimpleResultVO<Boolean> modifyAdjustVO(AdjustVO adjustVO);

	public SimpleResultVO<Boolean> deleteAdjustVO(Long adjustID);

	// body

	public SimpleResultVO<Boolean> createAdjustDetailVO(
			AdjustDetailVO adjustDetailVO);

	public SimpleResultVO<Boolean> batchCreateAdjustDetailVO(
			List<AdjustDetailVO> adjustDetailVOs);

	public SimpleResultVO<List<AdjustDetailVO>> queryAdjustDetailVOList(
			AdjustDetailQueryVO adjustDetailQueryVO);

	public SimpleResultVO<List<AdjustDetailVO>> getAdjustDetailVOListByAdjustID(
			Long adjustID);

	public SimpleResultVO<AdjustDetailQueryVO> getAdjustDetailVOByID(Long id);

	public SimpleResultVO<Boolean> modifyAdjustDetailVO(
			AdjustDetailVO adjustDetailVO);

	public SimpleResultVO<Boolean> batchModifyAdjustDetailVOs(
			List<AdjustDetailVO> adjustDetailVOs);

	public SimpleResultVO<Boolean> deleteAdjustDetailVO(Long id);

	public SimpleResultVO<Boolean> deleteAdjustDetailVOByAdjustID(Long adjustID);

	public SimpleResultVO<Boolean> batchDeleteAdjustDetailVOByAdjustID(
			List<Long> storyDetailIDs);

	public SimpleResultVO<Boolean> submit(Long id);

	public SimpleResultVO<Boolean> accounted(Long id);

}
