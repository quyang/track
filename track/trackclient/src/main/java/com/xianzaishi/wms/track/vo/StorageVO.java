package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.xianzaishi.wms.common.vo.BaseVO;

public class StorageVO extends BaseVO implements Serializable {

	private Long agencyId = null;

	private String agencyName = null;

	private Long operate = null;

	private String operateName = null;

	private Integer opReason = null;

	private Date opTime = null;

	private String sopTime = null;

	private Long auditor = null;

	private String auditorName = null;

	private Date auditTime = null;

	private String sauditTime = null;

	private Integer statu = null;
	
	private Integer purchaseId=null;//总采购单编号

	private List<StorageDetailVO> details = null;

	private Long relationId;//总采购单id

	public Long getRelationId() {
		return relationId;
	}

	public void setRelationId(Long relationId) {
		this.relationId = relationId;
	}

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getOperate() {
		return operate;
	}

	public void setOperate(Long operate) {
		this.operate = operate;
	}

	public Date getOpTime() {
		return opTime;
	}

	public void setOpTime(Date opTime) {
		this.opTime = opTime;
	}

	public Long getAuditor() {
		return auditor;
	}

	public void setAuditor(Long auditor) {
		this.auditor = auditor;
	}

	public Date getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}

	public String getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	public String getOperateName() {
		return operateName;
	}

	public void setOperateName(String operateName) {
		this.operateName = operateName;
	}

	public String getSopTime() {
		return sopTime;
	}

	public void setSopTime(String sopTime) {
		this.sopTime = sopTime;
	}

	public String getAuditorName() {
		return auditorName;
	}

	public void setAuditorName(String auditorName) {
		this.auditorName = auditorName;
	}

	public String getSauditTime() {
		return sauditTime;
	}

	public void setSauditTime(String sauditTime) {
		this.sauditTime = sauditTime;
	}

	public List<StorageDetailVO> getDetails() {
		return details;
	}

	public void setDetails(List<StorageDetailVO> details) {
		this.details = details;
	}

	public Integer getStatu() {
		return statu;
	}

	public void setStatu(Integer statu) {
		this.statu = statu;
	}

	public Integer getOpReason() {
		return opReason;
	}

	public void setOpReason(Integer opReason) {
		this.opReason = opReason;
	}

	public Integer getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(Integer purchaseId) {
		this.purchaseId = purchaseId;
	}


	public static class StorageReasonType{
		/**
		 * 采购
		 */
		public static final Integer PUCHASE = 5;
		/**
		 * 退换货
		 */
		public static final Integer RETURN_GOODS = 6;
		/**
		 * 生产
		 */
		public static final Integer PRODUCE = 7;
		/**
		 * 其他
		 */
		public static final Integer OTHER = 11;
	}
}