package com.xianzaishi.wms.track.domain.client.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.vo.OutgoingSaleDetailQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingSaleDetailVO;
import com.xianzaishi.wms.track.vo.OutgoingSaleQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingSaleVO;

public interface IOutgoingSaleDomainClient {

	// domain
	public SimpleResultVO<Boolean> createOutgoingSaleDomain(
			OutgoingSaleVO outgoingSaleVO);

	public SimpleResultVO<OutgoingSaleVO> getOutgoingSaleDomainByID(
			Long outgoingSaleID);

	public SimpleResultVO<Boolean> deleteOutgoingSaleDomain(Long id);
	
	public SimpleResultVO<Boolean> createAndAccountedOutgoingSaleDomain(
			OutgoingSaleVO outgoingSaleVO);

	// head
	public SimpleResultVO<Boolean> addOutgoingSaleVO(
			OutgoingSaleVO outgoingSaleVO);

	public SimpleResultVO<QueryResultVO<OutgoingSaleVO>> queryOutgoingSaleVOList(
			OutgoingSaleQueryVO outgoingSaleQueryVO);

	public SimpleResultVO<OutgoingSaleVO> getOutgoingSaleVOByID(Long id);

	public SimpleResultVO<Boolean> modifyOutgoingSaleVO(
			OutgoingSaleVO outgoingSaleVO);

	public SimpleResultVO<Boolean> deleteOutgoingSaleVO(Long outgoingSaleID);

	// body
	public SimpleResultVO<Boolean> createOutgoingSaleDetailVO(
			OutgoingSaleDetailVO outgoingSaleDetailVO);

	public SimpleResultVO<Boolean> batchCreateOutgoingSaleDetailVO(
			List<OutgoingSaleDetailVO> outgoingSaleDetailVOs);

	public SimpleResultVO<List<OutgoingSaleDetailVO>> queryOutgoingSaleDetailVOList(
			OutgoingSaleDetailQueryVO outgoingSaleDetailQueryVO);

	public SimpleResultVO<List<OutgoingSaleDetailVO>> getOutgoingSaleDetailVOListByOutgoingSaleID(
			Long outgoingSaleID);

	public SimpleResultVO<OutgoingSaleDetailQueryVO> getOutgoingSaleDetailVOByID(
			Long id);

	public SimpleResultVO<Boolean> modifyOutgoingSaleDetailVO(
			OutgoingSaleDetailVO outgoingSaleDetailVO);

	public SimpleResultVO<Boolean> batchModifyOutgoingSaleDetailVOs(
			List<OutgoingSaleDetailVO> outgoingSaleDetailVOs);

	public SimpleResultVO<Boolean> deleteOutgoingSaleDetailVO(Long id);

	public SimpleResultVO<Boolean> deleteOutgoingSaleDetailVOByOutgoingSaleID(
			Long outgoingSaleID);

	public SimpleResultVO<Boolean> batchDeleteOutgoingSaleDetailVOByOutgoingSaleID(
			List<Long> storyDetailIDs);

	public SimpleResultVO<Boolean> submit(Long id);

	public SimpleResultVO<Boolean> audit(Long id, Long auditor);

	public SimpleResultVO<Boolean> accounted(Long id);
}
