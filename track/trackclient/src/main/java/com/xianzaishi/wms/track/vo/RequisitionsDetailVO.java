package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import com.xianzaishi.wms.common.vo.BaseVO;

public class RequisitionsDetailVO extends BaseVO implements Serializable {

	private Long requisitionsId = null;

	private Long skuId = null;

	private String number = null;

	private Integer numberReal = null;

	private Long positionId = null;

	private String positionBarcode = null;

	private String skuName = null;

	private String positionCode = null;

	private String saleUnit = null;

	private Long saleUnitId = null;

	private String specUnit = null;

	private Long specUnitId = null;

	private Integer spec = null;

	private Integer saleUnitType = null;

	public Long getRequisitionsId() {
		return requisitionsId;
	}

	public void setRequisitionsId(Long requisitionsId) {
		this.requisitionsId = requisitionsId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

	public String getPositionBarcode() {
		return positionBarcode;
	}

	public void setPositionBarcode(String positionBarcode) {
		this.positionBarcode = positionBarcode;
	}

	public String getSkuName() {
		return skuName;
	}

	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}

	public String getPositionCode() {
		return positionCode;
	}

	public void setPositionCode(String positionCode) {
		this.positionCode = positionCode;
	}

	public String getSaleUnit() {
		return saleUnit;
	}

	public void setSaleUnit(String saleUnit) {
		this.saleUnit = saleUnit;
	}

	public Long getSaleUnitId() {
		return saleUnitId;
	}

	public void setSaleUnitId(Long saleUnitId) {
		this.saleUnitId = saleUnitId;
	}

	public String getSpecUnit() {
		return specUnit;
	}

	public void setSpecUnit(String specUnit) {
		this.specUnit = specUnit;
	}

	public Long getSpecUnitId() {
		return specUnitId;
	}

	public void setSpecUnitId(Long specUnitId) {
		this.specUnitId = specUnitId;
	}

	public Integer getSpec() {
		return spec;
	}

	public void setSpec(Integer spec) {
		this.spec = spec;
	}

	public Integer getSaleUnitType() {
		return saleUnitType;
	}

	public void setSaleUnitType(Integer saleUnitType) {
		this.saleUnitType = saleUnitType;
	}

	public String getNumber() {
		if (number == null && numberReal != null) {
			number = new BigDecimal(numberReal).divide(new BigDecimal(1000))
					.toString();
		}
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Integer getNumberReal() {
		return numberReal;
	}

	public void setNumberReal(Integer numberReal) {
		this.numberReal = numberReal;
	}

}