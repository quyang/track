package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.xianzaishi.wms.common.vo.BaseVO;

public class BookingDeliveryVO extends BaseVO implements Serializable {

	private Long agencyId = null;

	private Long agencyName = null;

	private Long operate = null;

	private Long operateName = null;

	private Date opTime = null;

	private String sopTime = null;

	private Long supplierId = null;

	private String supplierName = null;

	private Date deliveryTime = null;

	private String sdeliveryTime = null;

	private Long auditor = null;

	private String auditorName = null;

	private Date auditTime = null;

	private Integer statu = null;

	private List details = null;

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getOperate() {
		return operate;
	}

	public void setOperate(Long operate) {
		this.operate = operate;
	}

	public Date getOpTime() {
		return opTime;
	}

	public void setOpTime(Date opTime) {
		this.opTime = opTime;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public Date getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(Date deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public Long getAuditor() {
		return auditor;
	}

	public void setAuditor(Long auditor) {
		this.auditor = auditor;
	}

	public Date getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}

	public Long getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(Long agencyName) {
		this.agencyName = agencyName;
	}

	public Integer getStatu() {
		return statu;
	}

	public void setStatu(Integer statu) {
		this.statu = statu;
	}

	public String getAuditorName() {
		return auditorName;
	}

	public void setAuditorName(String auditorName) {
		this.auditorName = auditorName;
	}

	public Long getOperateName() {
		return operateName;
	}

	public void setOperateName(Long operateName) {
		this.operateName = operateName;
	}

	public String getSdeliveryTime() {
		return sdeliveryTime;
	}

	public void setSdeliveryTime(String sdeliveryTime) {
		this.sdeliveryTime = sdeliveryTime;
	}

	public String getSopTime() {
		return sopTime;
	}

	public void setSopTime(String sopTime) {
		this.sopTime = sopTime;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public List getDetails() {
		return details;
	}

	public void setDetails(List details) {
		this.details = details;
	}

}