package com.xianzaishi.wms.track.domain.client.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.vo.StocktakingDetailQueryVO;
import com.xianzaishi.wms.track.vo.StocktakingDetailVO;
import com.xianzaishi.wms.track.vo.StocktakingQueryVO;
import com.xianzaishi.wms.track.vo.StocktakingVO;

public interface IStocktakingDomainClient {

	// domain
	public SimpleResultVO<Boolean> createStocktakingDomain(
			StocktakingVO stocktakingVO);

	public SimpleResultVO<StocktakingVO> getStocktakingDomainByID(
			Long stocktakingID);

	public SimpleResultVO<Boolean> deleteStocktakingDomain(Long id);

	// head
	public SimpleResultVO<Boolean> addStocktakingVO(StocktakingVO stocktakingVO);

	public SimpleResultVO<QueryResultVO<StocktakingVO>> queryStocktakingVOList(
			StocktakingQueryVO stocktakingQueryVO);

	public SimpleResultVO<StocktakingVO> getStocktakingVOByID(Long id);

	public SimpleResultVO<Boolean> modifyStocktakingVO(
			StocktakingVO stocktakingVO);

	public SimpleResultVO<Boolean> deleteStocktakingVO(Long stocktakingID);

	public SimpleResultVO<Boolean> submitStocktakingToCheck(
			StocktakingVO stocktakingVO);

	public SimpleResultVO<Boolean> submitStocktakingToAudit(
			StocktakingVO stocktakingVO);

	// body
	public SimpleResultVO<Boolean> createStocktakingDetailVO(
			StocktakingDetailVO stocktakingDetailVO);

	public SimpleResultVO<Boolean> batchCreateStocktakingDetailVO(
			List<StocktakingDetailVO> stocktakingDetailVOs);

	public SimpleResultVO<List<StocktakingDetailVO>> queryStocktakingDetailVOList(
			StocktakingDetailQueryVO stocktakingDetailQueryVO);

	public SimpleResultVO<List<StocktakingDetailVO>> getStocktakingDetailVOListByStocktakingID(
			Long stocktakingID);

	public SimpleResultVO<StocktakingDetailQueryVO> getStocktakingDetailVOByID(
			Long id);

	public SimpleResultVO<Boolean> modifyStocktakingDetailVO(
			StocktakingDetailVO stocktakingDetailVO);

	/**
	 * 按ID更新Detail
	 * 
	 * @param stocktakingDetailVOs
	 * @return
	 */
	public SimpleResultVO<Boolean> batchModifyStocktakingDetailVOs(
			List<StocktakingDetailVO> stocktakingDetailVOs);

	public SimpleResultVO<Boolean> deleteStocktakingDetailVO(Long id);

	public SimpleResultVO<Boolean> deleteStocktakingDetailVOByStocktakingID(
			Long stocktakingID);

	public SimpleResultVO<Boolean> batchDeleteStocktakingDetailVOByStocktakingID(
			List<Long> storyDetailIDs);

	public SimpleResultVO<Boolean> submit(Long id);

	public SimpleResultVO<Boolean> audit(Long id, Long auditor);

	public SimpleResultVO<Boolean> accounted(Long id);
}
