package com.xianzaishi.wms.track.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.xianzaishi.wms.common.vo.BaseVO;

public class OutgoingVO extends BaseVO implements Serializable {

	private Long agencyId = null;

	private Long operate = null;
	
	private String operateName = null;

	private Integer opReason = null;

	private Date opTime = null;

	private Long auditor = null;
	
	private String auditorName = null;

	private Date auditTime = null;

	private Integer statu = null;

	private List<OutgoingDetailVO> details = null;
	
	private Long relationId = null;

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}

	public Long getOperate() {
		return operate;
	}

	public void setOperate(Long operate) {
		this.operate = operate;
	}

	public Date getOpTime() {
		return opTime;
	}

	public void setOpTime(Date opTime) {
		this.opTime = opTime;
	}

	public Long getAuditor() {
		return auditor;
	}

	public void setAuditor(Long auditor) {
		this.auditor = auditor;
	}

	public Date getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}

	public List<OutgoingDetailVO> getDetails() {
		return details;
	}

	public void setDetails(List<OutgoingDetailVO> details) {
		this.details = details;
	}

	public Integer getStatu() {
		return statu;
	}

	public void setStatu(Integer statu) {
		this.statu = statu;
	}

	public Integer getOpReason() {
		return opReason;
	}

	public void setOpReason(Integer opReason) {
		this.opReason = opReason;
	}

    public String getOperateName() {
        return operateName;
    }

    public void setOperateName(String operateName) {
      this.operateName = operateName;
    }

    public String getAuditorName() {
      return auditorName;
    }
  
    public void setAuditorName(String auditorName) {
      this.auditorName = auditorName;
    }

    public Long getRelationId() {
      return relationId;
    }

    public void setRelationId(Long relationId) {
      this.relationId = relationId;
    }
  	
}