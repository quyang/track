package com.xianzaishi.wms.track.dao.test;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.xianzaishi.wms.track.base.test.TrackBaseDaoTest;
import com.xianzaishi.wms.track.dao.itf.IRequisitionsDetailDao;
import com.xianzaishi.wms.track.vo.RequisitionsDetailDO;

@ContextConfiguration(locations = "classpath:spring/ac-track-RequisitionsDetail.xml")
public class RequisitionsDetailsDaoTest extends TrackBaseDaoTest {

	@Autowired
	private IRequisitionsDetailDao requisitionsDetailDao = null;

	@Test
	public void addRequisitionsDetailTest() {
		RequisitionsDetailDO requisitionsDetailDO = new RequisitionsDetailDO();
		requisitionsDetailDO.setSkuId(1l);
		requisitionsDetailDO.setPositionId(1l);
		requisitionsDetailDO.setNumberReal(1);
		requisitionsDetailDao.addDO(requisitionsDetailDO);
	}

	@Test
	public void batchAddRequisitionsDetailTest() {
		List list = new LinkedList();
		for (int i = 0; i < 5; i++) {
			RequisitionsDetailDO requisitionsDetailDO = new RequisitionsDetailDO();
			requisitionsDetailDO.setSkuId(1l);
			requisitionsDetailDO.setPositionId(1l);
			requisitionsDetailDO.setNumberReal(1);
			list.add(requisitionsDetailDO);
		}
		requisitionsDetailDao.batchAddDO(list);
	}

	@Test
	public void updateRequisitionsDetailTest() {
		RequisitionsDetailDO requisitionsDetailDO = new RequisitionsDetailDO();
		requisitionsDetailDO.setId(1l);
		requisitionsDetailDO.setSkuId(2l);
		requisitionsDetailDO.setPositionId(1l);
		requisitionsDetailDO.setNumberReal(1);
		requisitionsDetailDao.updateDO(requisitionsDetailDO);
	}

	@Test
	public void batchUpdateRequisitionsDetailTest() {
		List list = new LinkedList();
		for (long i = 1; i < 5; i++) {
			RequisitionsDetailDO requisitionsDetailDO = new RequisitionsDetailDO();
			requisitionsDetailDO.setId(i);
			requisitionsDetailDO.setSkuId(i);
			requisitionsDetailDO.setPositionId(i);
			list.add(requisitionsDetailDO);
		}
		requisitionsDetailDao.batchModifyDO(list);
	}

	public IRequisitionsDetailDao getRequisitionsDetailDao() {
		return requisitionsDetailDao;
	}

	public void setRequisitionsDetailDao(
			IRequisitionsDetailDao requisitionsDetailDao) {
		this.requisitionsDetailDao = requisitionsDetailDao;
	}
}
