package com.xianzaishi.wms.track.manage.test;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.xianzaishi.wms.track.base.test.TrackBaseDaoTest;
import com.xianzaishi.wms.track.manage.itf.IRequisitionsDetailManage;
import com.xianzaishi.wms.track.vo.RequisitionsDetailVO;

@ContextConfiguration(locations = "classpath:spring/ac-track-RequisitionsDetail.xml")
public class RequisitionsDetailsManageTest extends TrackBaseDaoTest {

	@Autowired
	private IRequisitionsDetailManage requisitionsDetailManage = null;

	@Test
	public void addRequisitionsDetailTest() {
		RequisitionsDetailVO requisitionsDetailVO = new RequisitionsDetailVO();
		requisitionsDetailVO.setSkuId(1l);
		requisitionsDetailVO.setPositionId(1l);
		requisitionsDetailVO.setNumberReal(1);
		requisitionsDetailManage.addRequisitionsDetailVO(requisitionsDetailVO);
	}

	@Test
	public void batchAddRequisitionsDetailTest() {
		List list = new LinkedList();
		for (int i = 0; i < 5; i++) {
			RequisitionsDetailVO requisitionsDetailVO = new RequisitionsDetailVO();
			requisitionsDetailVO.setSkuId(1l);
			requisitionsDetailVO.setPositionId(1l);
			requisitionsDetailVO.setNumberReal(1);
			list.add(requisitionsDetailVO);
		}
		requisitionsDetailManage.batchAddRequisitionsDetailVO(list);
	}

	@Test
	public void updateRequisitionsDetailTest() {
		RequisitionsDetailVO requisitionsDetailVO = new RequisitionsDetailVO();
		requisitionsDetailVO.setId(1l);
		requisitionsDetailVO.setSkuId(2l);
		requisitionsDetailVO.setPositionId(1l);
		requisitionsDetailVO.setNumberReal(1);
		requisitionsDetailManage
				.modifyRequisitionsDetailVO(requisitionsDetailVO);
	}

	@Test
	public void batchUpdateRequisitionsDetailTest() {
		List list = new LinkedList();
		for (Long i = 1l; i < 5; i++) {
			Long j = i + 2;
			RequisitionsDetailVO requisitionsDetailVO = new RequisitionsDetailVO();
			requisitionsDetailVO.setId(i);
			requisitionsDetailVO.setPositionBarcode(j.toString());
			requisitionsDetailVO.setNumberReal(1);
			requisitionsDetailVO.setSkuId(j);
			requisitionsDetailVO.setPositionId(j);
			requisitionsDetailVO.setRequisitionsId(i);
			list.add(requisitionsDetailVO);
		}
		requisitionsDetailManage.batchModifyRequisitionsDetailVO(list);
	}

	public IRequisitionsDetailManage getRequisitionsDetailManage() {
		return requisitionsDetailManage;
	}

	public void setRequisitionsDetailManage(
			IRequisitionsDetailManage requisitionsDetailManage) {
		this.requisitionsDetailManage = requisitionsDetailManage;
	}

}
