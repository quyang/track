package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.manage.itf.IOrderStorageManage;
import com.xianzaishi.wms.track.service.itf.IOrderStorageService;
import com.xianzaishi.wms.track.vo.OrderStorageQueryVO;
import com.xianzaishi.wms.track.vo.OrderStorageVO;

public class OrderStorageServiceImpl implements IOrderStorageService {
	@Autowired
	private IOrderStorageManage orderStorageManage = null;

	public IOrderStorageManage getOrderStorageManage() {
		return orderStorageManage;
	}

	public void setOrderStorageManage(IOrderStorageManage orderStorageManage) {
		this.orderStorageManage = orderStorageManage;
	}

	public Long addOrderStorageVO(OrderStorageVO orderStorageVO) {
		return orderStorageManage.addOrderStorageVO(orderStorageVO);
	}

	public List<OrderStorageVO> queryOrderStorageVOList(
			OrderStorageQueryVO orderStorageQueryVO) {
		return orderStorageManage.queryOrderStorageVOList(orderStorageQueryVO);
	}

	public Integer queryOrderStorageVOCount(
			OrderStorageQueryVO orderStorageQueryVO) {
		return orderStorageManage.queryOrderStorageVOCount(orderStorageQueryVO);
	}

	public OrderStorageVO getOrderStorageVOByID(Long id) {
		return (OrderStorageVO) orderStorageManage.getOrderStorageVOByID(id);
	}

	public Boolean modifyOrderStorageVO(OrderStorageVO orderStorageVO) {
		return orderStorageManage.modifyOrderStorageVO(orderStorageVO);
	}

	public Boolean deleteOrderStorageVOByID(Long id) {
		return orderStorageManage.deleteOrderStorageVOByID(id);
	}

	public Boolean submit(Long id) {
		return orderStorageManage.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return orderStorageManage.audit(id, auditor);
	}

	public Boolean accounted(Long id) {
		return orderStorageManage.accounted(id);
	}
}
