package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.manage.itf.IOutgoingSaleDetailManage;
import com.xianzaishi.wms.track.service.itf.IOutgoingSaleDetailService;
import com.xianzaishi.wms.track.vo.OutgoingSaleDetailVO;
import com.xianzaishi.wms.track.vo.OutgoingSaleDetailQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingSaleDetailVO;

public class OutgoingSaleDetailServiceImpl implements
		IOutgoingSaleDetailService {
	@Autowired
	private IOutgoingSaleDetailManage outgoingSaleDetailManage = null;

	public IOutgoingSaleDetailManage getOutgoingSaleDetailManage() {
		return outgoingSaleDetailManage;
	}

	public void setOutgoingSaleDetailManage(
			IOutgoingSaleDetailManage outgoingSaleDetailManage) {
		this.outgoingSaleDetailManage = outgoingSaleDetailManage;
	}

	public Boolean addOutgoingSaleDetailVO(
			OutgoingSaleDetailVO outgoingSaleDetailVO) {
		outgoingSaleDetailManage.addOutgoingSaleDetailVO(outgoingSaleDetailVO);
		return true;
	}

	public List<OutgoingSaleDetailVO> queryOutgoingSaleDetailVOList(
			OutgoingSaleDetailQueryVO outgoingSaleDetailQueryVO) {
		return outgoingSaleDetailManage
				.queryOutgoingSaleDetailVOList(outgoingSaleDetailQueryVO);
	}

	public OutgoingSaleDetailVO getOutgoingSaleDetailVOByID(Long id) {
		return (OutgoingSaleDetailVO) outgoingSaleDetailManage
				.getOutgoingSaleDetailVOByID(id);
	}

	public Boolean modifyOutgoingSaleDetailVO(
			OutgoingSaleDetailVO outgoingSaleDetailVO) {
		return outgoingSaleDetailManage
				.modifyOutgoingSaleDetailVO(outgoingSaleDetailVO);
	}

	public Boolean deleteOutgoingSaleDetailVOByID(Long id) {
		return outgoingSaleDetailManage.deleteOutgoingSaleDetailVOByID(id);
	}

	public List<OutgoingSaleDetailVO> getOutgoingSaleDetailVOByOutgoingSaleID(
			Long id) {
		return outgoingSaleDetailManage
				.getOutgoingSaleDetailVOByOutgoingSaleID(id);
	}

	public Boolean batchAddOutgoingSaleDetailVO(
			List<OutgoingSaleDetailVO> outgoingSaleDetailVOs) {
		return outgoingSaleDetailManage
				.batchAddOutgoingSaleDetailVO(outgoingSaleDetailVOs);
	}

	public Boolean batchModifyOutgoingSaleDetailVO(
			List<OutgoingSaleDetailVO> outgoingSaleDetailVOs) {
		return outgoingSaleDetailManage
				.batchModifyOutgoingSaleDetailVO(outgoingSaleDetailVOs);
	}

	public Boolean batchDeleteOutgoingSaleDetailVO(
			List<OutgoingSaleDetailVO> outgoingSaleDetailVOs) {
		return outgoingSaleDetailManage
				.batchDeleteOutgoingSaleDetailVO(outgoingSaleDetailVOs);
	}

	public Boolean batchDeleteOutgoingSaleDetailVOByID(List<Long> storyDetailIDs) {
		return outgoingSaleDetailManage
				.batchDeleteOutgoingSaleDetailVOByID(storyDetailIDs);
	}
}
