package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.manage.itf.IAdjustManage;
import com.xianzaishi.wms.track.service.itf.IAdjustService;
import com.xianzaishi.wms.track.vo.AdjustQueryVO;
import com.xianzaishi.wms.track.vo.AdjustVO;

public class AdjustServiceImpl implements IAdjustService {
	@Autowired
	private IAdjustManage adjustManage = null;

	public IAdjustManage getAdjustManage() {
		return adjustManage;
	}

	public void setAdjustManage(IAdjustManage adjustManage) {
		this.adjustManage = adjustManage;
	}

	public Long addAdjustVO(AdjustVO adjustVO) {
		return adjustManage.addAdjustVO(adjustVO);
	}

	public List<AdjustVO> queryAdjustVOList(AdjustQueryVO adjustQueryVO) {
		return adjustManage.queryAdjustVOList(adjustQueryVO);
	}

	public Integer queryAdjustVOCount(AdjustQueryVO adjustQueryVO) {
		return adjustManage.queryAdjustVOCount(adjustQueryVO);
	}

	public AdjustVO getAdjustVOByID(Long id) {
		return (AdjustVO) adjustManage.getAdjustVOByID(id);
	}

	public Boolean modifyAdjustVO(AdjustVO adjustVO) {
		return adjustManage.modifyAdjustVO(adjustVO);
	}

	public Boolean deleteAdjustVOByID(Long id) {
		return adjustManage.deleteAdjustVOByID(id);
	}

	public Boolean submit(Long id) {
		return adjustManage.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return adjustManage.audit(id, auditor);
	}

	public Boolean accounted(Long id) {
		return adjustManage.accounted(id);
	}
}
