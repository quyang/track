package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.ISpoilageDetailDao;
import com.xianzaishi.wms.track.manage.itf.ISpoilageDetailManage;
import com.xianzaishi.wms.track.vo.SpoilageDetailQueryVO;
import com.xianzaishi.wms.track.vo.SpoilageDetailVO;

public class SpoilageDetailManageImpl implements ISpoilageDetailManage {
	@Autowired
	private ISpoilageDetailDao spoilageDetailDao = null;

	private void validate(SpoilageDetailVO spoilageDetailVO) {
		if (spoilageDetailVO.getSpoilageId() == null
				|| spoilageDetailVO.getSpoilageId() <= 0) {
			throw new BizException("spoilageID error："
					+ spoilageDetailVO.getSpoilageId());
		}
		if (spoilageDetailVO.getSkuId() == null
				|| spoilageDetailVO.getSkuId() <= 0) {
			throw new BizException("skuID error：" + spoilageDetailVO.getSkuId());
		}
		if (spoilageDetailVO.getPositionId() == null
				|| spoilageDetailVO.getPositionId() <= 0) {
			throw new BizException("positionId error："
					+ spoilageDetailVO.getPositionId());
		}
		if (spoilageDetailVO.getSpoilageNoReal() == null
				|| spoilageDetailVO.getSpoilageNoReal() <= 0) {
			throw new BizException("spoilageNo error："
					+ spoilageDetailVO.getSpoilageNoReal());
		}
		if (spoilageDetailVO.getSkuName() == null
				|| spoilageDetailVO.getSkuName().isEmpty()) {
			throw new BizException("skuName error："
					+ spoilageDetailVO.getSkuName());
		}
		if (spoilageDetailVO.getPositionCode() == null
				|| spoilageDetailVO.getPositionCode().isEmpty()) {
			throw new BizException("positionCode error："
					+ spoilageDetailVO.getPositionCode());
		}
		if (spoilageDetailVO.getSaleUnit() == null
				|| spoilageDetailVO.getSaleUnit().isEmpty()) {
			throw new BizException("saleUnit error："
					+ spoilageDetailVO.getSaleUnit());
		}
		if (spoilageDetailVO.getSaleUnitId() == null
				|| spoilageDetailVO.getSaleUnitId() <= 0) {
			throw new BizException("saleUnitId error："
					+ spoilageDetailVO.getSaleUnitId());
		}
		if (spoilageDetailVO.getSaleUnitType() == null) {
			throw new BizException("saleUnitType error："
					+ spoilageDetailVO.getSaleUnitType());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public ISpoilageDetailDao getSpoilageDetailDao() {
		return spoilageDetailDao;
	}

	public void setSpoilageDetailDao(ISpoilageDetailDao spoilageDetailDao) {
		this.spoilageDetailDao = spoilageDetailDao;
	}

	public Boolean addSpoilageDetailVO(SpoilageDetailVO spoilageDetailVO) {
		validate(spoilageDetailVO);
		spoilageDetailDao.addDO(spoilageDetailVO);
		return true;
	}

	public List<SpoilageDetailVO> querySpoilageDetailVOList(
			SpoilageDetailQueryVO spoilageDetailQueryVO) {
		return spoilageDetailDao.queryDO(spoilageDetailQueryVO);
	}

	public SpoilageDetailVO getSpoilageDetailVOByID(Long id) {
		return (SpoilageDetailVO) spoilageDetailDao.getDOByID(id);
	}

	public Boolean modifySpoilageDetailVO(SpoilageDetailVO spoilageDetailVO) {
		return spoilageDetailDao.updateDO(spoilageDetailVO);
	}

	public Boolean deleteSpoilageDetailVOByID(Long id) {
		return spoilageDetailDao.delDO(id);
	}

	public List<SpoilageDetailVO> getSpoilageDetailVOBySpoilageID(Long id) {
		SpoilageDetailQueryVO queryVO = new SpoilageDetailQueryVO();
		queryVO.setSpoilageId(id);
		queryVO.setSize(Integer.MAX_VALUE);
		return spoilageDetailDao.queryDO(queryVO);
	}

	public Boolean batchAddSpoilageDetailVO(
			List<SpoilageDetailVO> spoilageDetailVOs) {
		return spoilageDetailDao.batchAddDO(spoilageDetailVOs);
	}

	public Boolean batchModifySpoilageDetailVO(
			List<SpoilageDetailVO> spoilageDetailVOs) {
		return spoilageDetailDao.batchModifyDO(spoilageDetailVOs);
	}

	public Boolean batchDeleteSpoilageDetailVO(
			List<SpoilageDetailVO> spoilageDetailVOs) {
		return spoilageDetailDao.batchDeleteDO(spoilageDetailVOs);
	}

	public Boolean batchDeleteSpoilageDetailVOByID(List<Long> spoilageDetailIDs) {
		return spoilageDetailDao.batchDeleteDOByID(spoilageDetailIDs);
	}
}
