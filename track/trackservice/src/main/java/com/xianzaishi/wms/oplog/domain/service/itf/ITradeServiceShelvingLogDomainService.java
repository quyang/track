package com.xianzaishi.wms.oplog.domain.service.itf;

import java.util.List;

import com.xianzaishi.wms.oplog.vo.TradeServiceShelvingLogVO;

public interface ITradeServiceShelvingLogDomainService {
	public Boolean batchAddTradeServiceShelvingLog(
			List<TradeServiceShelvingLogVO> tradeServiceShelvingLogVOs);
}
