package com.xianzaishi.wms.track.domain.service.impl;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryManageDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IPositionDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryManageDetailVO;
import com.xianzaishi.wms.hive.vo.InventoryManageVO;
import com.xianzaishi.wms.hive.vo.PositionVO;
import com.xianzaishi.wms.track.constants.TrackConstants;
import com.xianzaishi.wms.track.domain.service.itf.IOutgoingSaleDomainService;
import com.xianzaishi.wms.track.service.itf.IOutgoingSaleDetailService;
import com.xianzaishi.wms.track.service.itf.IOutgoingSaleService;
import com.xianzaishi.wms.track.vo.OutgoingSaleDetailQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingSaleDetailVO;
import com.xianzaishi.wms.track.vo.OutgoingSaleQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingSaleVO;

public class OutgoingSaleDomainServiceImpl implements
		IOutgoingSaleDomainService {
	@Autowired
	private IOutgoingSaleService outgoingSaleService = null;
	@Autowired
	private IOutgoingSaleDetailService outgoingSaleDetailService = null;
	@Autowired
	private IInventoryManageDomainClient inventoryManageDomainClient = null;
	@Autowired
	private SkuService skuService = null;
	@Autowired
	private ValueService valueService = null;
	@Autowired
	private IPositionDomainClient positionDomainClient = null;

	public Boolean createOutgoingSaleDomain(OutgoingSaleVO outgoingSaleVO) {
		if (outgoingSaleVO.getDetails() == null
				|| outgoingSaleVO.getDetails().isEmpty()) {
			throw new BizException("请填充出库明细！");
		}
		outgoingSaleVO.setStatu(0);
		//做出库做一个简单记录
		Long outgoingSaleID = outgoingSaleService
				.addOutgoingSaleVO(outgoingSaleVO);

		outgoingSaleVO.setId(outgoingSaleID);

		initDetails(outgoingSaleID, outgoingSaleVO.getDetails());
		//记录出库明细
		outgoingSaleDetailService.batchAddOutgoingSaleDetailVO(outgoingSaleVO
				.getDetails());

		return true;
	}

	public OutgoingSaleVO getOutgoingSaleDomainByID(Long outgoingSaleID) {
		if (outgoingSaleID == null || outgoingSaleID <= 0) {
			throw new BizException("id error");
		}

		OutgoingSaleVO outgoingSaleVO = outgoingSaleService
				.getOutgoingSaleVOByID(outgoingSaleID);

		outgoingSaleVO.setDetails(outgoingSaleDetailService
				.getOutgoingSaleDetailVOByOutgoingSaleID(outgoingSaleID));

		return outgoingSaleVO;
	}

	public Boolean deleteOutgoingSaleDomain(Long outgoingSaleID) {
		if (outgoingSaleID == null || outgoingSaleID <= 0) {
			throw new BizException("id error");
		}

		List<OutgoingSaleDetailVO> outgoingSaleDetailVOs = outgoingSaleDetailService
				.getOutgoingSaleDetailVOByOutgoingSaleID(outgoingSaleID);

		outgoingSaleDetailService
				.batchDeleteOutgoingSaleDetailVO(outgoingSaleDetailVOs);

		outgoingSaleService.deleteOutgoingSaleVOByID(outgoingSaleID);

		return true;
	}

	public Boolean addOutgoingSaleVO(OutgoingSaleVO outgoingSaleVO) {
		outgoingSaleService.addOutgoingSaleVO(outgoingSaleVO);
		return true;
	}

	public QueryResultVO<OutgoingSaleVO> queryOutgoingSaleVOList(
			OutgoingSaleQueryVO outgoingSaleQueryVO) {
		QueryResultVO<OutgoingSaleVO> queryResultVO = new QueryResultVO<>();
		queryResultVO.setPage(outgoingSaleQueryVO.getPage());
		queryResultVO.setSize(outgoingSaleQueryVO.getSize());
		queryResultVO.setItems(outgoingSaleService
				.queryOutgoingSaleVOList(outgoingSaleQueryVO));
		queryResultVO.setTotalCount(outgoingSaleService
				.queryOutgoingSaleVOCount(outgoingSaleQueryVO));
		return queryResultVO;
	}

	public OutgoingSaleVO getOutgoingSaleVOByID(Long id) {
		return outgoingSaleService.getOutgoingSaleVOByID(id);
	}

	public Boolean modifyOutgoingSaleVO(OutgoingSaleVO outgoingSaleVO) {
		return outgoingSaleService.modifyOutgoingSaleVO(outgoingSaleVO);
	}

	public Boolean deleteOutgoingSaleVO(Long id) {
		return outgoingSaleService.deleteOutgoingSaleVOByID(id);
	}

	public Boolean createOutgoingSaleDetailVO(
			OutgoingSaleDetailVO outgoingSaleDetailVO) {
		initInfo(outgoingSaleDetailVO);
		return outgoingSaleDetailService
				.addOutgoingSaleDetailVO(outgoingSaleDetailVO);
	}

	public Boolean batchCreateOutgoingSaleDetailVO(
			List<OutgoingSaleDetailVO> outgoingSaleDetailVOs) {
		for (OutgoingSaleDetailVO outgoingSaleDetailVO : outgoingSaleDetailVOs) {
			initInfo(outgoingSaleDetailVO);
		}
		return outgoingSaleDetailService
				.batchAddOutgoingSaleDetailVO(outgoingSaleDetailVOs);
	}

	public List<OutgoingSaleDetailVO> queryOutgoingSaleDetailVOList(
			OutgoingSaleDetailQueryVO outgoingSaleDetailQueryVO) {
		return outgoingSaleDetailService
				.queryOutgoingSaleDetailVOList(outgoingSaleDetailQueryVO);
	}

	public List<OutgoingSaleDetailVO> getOutgoingSaleDetailVOListByOutgoingSaleID(
			Long outgoingSaleID) {
		return outgoingSaleDetailService
				.getOutgoingSaleDetailVOByOutgoingSaleID(outgoingSaleID);
	}

	public OutgoingSaleDetailVO getOutgoingSaleDetailVOByID(Long id) {
		return outgoingSaleDetailService.getOutgoingSaleDetailVOByID(id);
	}

	public Boolean modifyOutgoingSaleDetailVO(
			OutgoingSaleDetailVO outgoingSaleDetailVO) {
		initInfo(outgoingSaleDetailVO);
		return outgoingSaleDetailService
				.modifyOutgoingSaleDetailVO(outgoingSaleDetailVO);
	}

	public Boolean batchModifyOutgoingSaleDetailVOs(
			List<OutgoingSaleDetailVO> outgoingSaleDetailVOs) {
		for (OutgoingSaleDetailVO outgoingSaleDetailVO : outgoingSaleDetailVOs) {
			initInfo(outgoingSaleDetailVO);
		}
		return outgoingSaleDetailService
				.batchModifyOutgoingSaleDetailVO(outgoingSaleDetailVOs);
	}

	public Boolean deleteOutgoingSaleDetailVO(Long id) {
		return outgoingSaleDetailService.deleteOutgoingSaleDetailVOByID(id);
	}

	public Boolean deleteOutgoingSaleDetailVOByOutgoingSaleID(
			Long outgoingSaleID) {
		if (outgoingSaleID == null || outgoingSaleID <= 0) {
			throw new BizException("id error");
		}

		List<OutgoingSaleDetailVO> outgoingSaleDetailVOs = outgoingSaleDetailService
				.getOutgoingSaleDetailVOByOutgoingSaleID(outgoingSaleID);

		outgoingSaleDetailService
				.batchDeleteOutgoingSaleDetailVO(outgoingSaleDetailVOs);

		return true;
	}

	public Boolean batchDeleteOutgoingSaleDetailVOByOutgoingSaleID(
			List<Long> storyDetailIDs) {
		return outgoingSaleDetailService
				.batchDeleteOutgoingSaleDetailVOByID(storyDetailIDs);
	}

	private void initDetails(Long id,
			List<OutgoingSaleDetailVO> outgoingSaleDetailVOs) {
		for (int i = 0; i < outgoingSaleDetailVOs.size(); i++) {
			outgoingSaleDetailVOs.get(i).setOutgoingSaleId(id);
			initInfo(outgoingSaleDetailVOs.get(i));
		}
	}

	private void initInfo(OutgoingSaleDetailVO outgoingSaleDetailVO) {
		ItemCommoditySkuDTO itemCommoditySkuDTO = skuService.queryItemSku(
				SkuQuery.querySkuById(outgoingSaleDetailVO.getSkuId()))
				.getModule();
		if (itemCommoditySkuDTO == null) {
			throw new BizException("skuID error:"
					+ outgoingSaleDetailVO.getSkuId());
		}
		outgoingSaleDetailVO.setSkuName(itemCommoditySkuDTO.getTitle());
		outgoingSaleDetailVO.setSaleUnit(valueService.queryValueNameByValueId(
				itemCommoditySkuDTO.getSkuUnit()).getModule());
		outgoingSaleDetailVO.setSaleUnitId(new Long(itemCommoditySkuDTO
				.getSkuUnit()));
		outgoingSaleDetailVO.setSaleUnitType(new Integer(itemCommoditySkuDTO
				.querySaleUnitType()));
		// outgoingSaleDetailVO.setSpec(itemCommoditySkuDTO
		// .getSkuSpecificationNum());
		// outgoingSaleDetailVO.setSpecUnit(valueService.queryValueNameByValueId(
		// itemCommoditySkuDTO.getSkuSpecificationUnit()).getModule());
		// outgoingSaleDetailVO.setSpecUnitId(new Long(itemCommoditySkuDTO
		// .getSkuSpecificationUnit()));
		PositionVO positionVO = positionDomainClient.getPositionVOByID(
				outgoingSaleDetailVO.getPositionId()).getTarget();
		if (positionVO == null) {
			throw new BizException("positionID error:"
					+ outgoingSaleDetailVO.getPositionId());
		}
		outgoingSaleDetailVO.setPositionCode(positionVO.getCode());

		if (outgoingSaleDetailVO.getNumberReal() == null
				&& outgoingSaleDetailVO.getNumber() != null
				&& !outgoingSaleDetailVO.getNumber().isEmpty()) {
			outgoingSaleDetailVO.setNumberReal(new BigDecimal(
					outgoingSaleDetailVO.getNumber()).multiply(
					new BigDecimal(1000)).intValue());
		}
	}

	public IOutgoingSaleService getOutgoingSaleService() {
		return outgoingSaleService;
	}

	public void setOutgoingSaleService(IOutgoingSaleService outgoingSaleService) {
		this.outgoingSaleService = outgoingSaleService;
	}

	public IOutgoingSaleDetailService getOutgoingSaleDetailService() {
		return outgoingSaleDetailService;
	}

	public void setOutgoingSaleDetailService(
			IOutgoingSaleDetailService outgoingSaleDetailService) {
		this.outgoingSaleDetailService = outgoingSaleDetailService;
	}

	public Boolean submit(Long id) {
		return outgoingSaleService.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		Boolean flag = false;
		if (outgoingSaleService.audit(id, auditor)) {
			flag = accounted(id);
		}
		return flag;
	}

	public Boolean accounted(Long id) {
		Boolean flag = false;
		OutgoingSaleVO outgoingSaleVO = getOutgoingSaleDomainByID(id);
		InventoryManageVO inventoryManageVO = initInventoryManage(outgoingSaleVO);
		SimpleResultVO<Boolean> result = inventoryManageDomainClient
				.createInventoryManageDomain(inventoryManageVO);//正式出库，扣除库存
		if (result.isSuccess() && result.getTarget()) {
			flag = outgoingSaleService.accounted(id);//更新状态
		}
		return flag;
	}

	private InventoryManageVO initInventoryManage(OutgoingSaleVO outgoingSaleVO) {
		InventoryManageVO inventoryManageVO = new InventoryManageVO();
		inventoryManageVO.setAgencyId(outgoingSaleVO.getAgencyId());
		inventoryManageVO.setAuditor(outgoingSaleVO.getAuditor());
		inventoryManageVO.setAuditTime(outgoingSaleVO.getAuditTime());
		inventoryManageVO.setOperator(outgoingSaleVO.getOperate());
		inventoryManageVO.setOpReason(TrackConstants.IM_REASON_OUT_SALE);
		inventoryManageVO.setOpTime(outgoingSaleVO.getOpTime());
		inventoryManageVO.setOpType(1);
		inventoryManageVO.setRelationId(outgoingSaleVO.getOrderId());
		inventoryManageVO.setInventoryId(outgoingSaleVO.getId());
		List<InventoryManageDetailVO> details = new LinkedList<InventoryManageDetailVO>();
		for (int i = 0; i < outgoingSaleVO.getDetails().size(); i++) {
		    OutgoingSaleDetailVO outgoingSaleDetailVO = outgoingSaleVO.getDetails().get(i);
			InventoryManageDetailVO inventoryManageDetailVO = new InventoryManageDetailVO();
			inventoryManageDetailVO.setNumber(-outgoingSaleVO.getDetails()
					.get(i).getNumberReal());
			inventoryManageDetailVO.setSaleUnit(outgoingSaleVO.getDetails()
					.get(i).getSaleUnit());
			inventoryManageDetailVO.setSaleUnitId(outgoingSaleVO.getDetails()
					.get(i).getSaleUnitId());
			inventoryManageDetailVO.setSpec(outgoingSaleVO.getDetails().get(i)
					.getSpec());
			inventoryManageDetailVO.setSpecUnit(outgoingSaleVO.getDetails()
					.get(i).getSpecUnit());
			inventoryManageDetailVO.setSpecUnitId(outgoingSaleVO.getDetails()
					.get(i).getSpecUnitId());
			inventoryManageDetailVO.setSaleUnitType(outgoingSaleVO.getDetails()
					.get(i).getSaleUnitType());
			inventoryManageDetailVO.setSkuName(outgoingSaleVO.getDetails()
					.get(i).getSkuName());
			inventoryManageDetailVO.setPositionCode(outgoingSaleVO.getDetails()
					.get(i).getPositionCode());
			inventoryManageDetailVO.setSkuId(outgoingSaleVO.getDetails().get(i)
					.getSkuId());
			inventoryManageDetailVO.setPositionId(outgoingSaleVO.getDetails()
					.get(i).getPositionId());
			inventoryManageDetailVO.setRelationId(outgoingSaleDetailVO.getOrderId());
			inventoryManageDetailVO.setInventoryId(outgoingSaleDetailVO.getId());
			details.add(inventoryManageDetailVO);
		}
		inventoryManageVO.setDetails(details);
		return inventoryManageVO;
	}

	public IInventoryManageDomainClient getInventoryManageDomainClient() {
		return inventoryManageDomainClient;
	}

	public void setInventoryManageDomainClient(
			IInventoryManageDomainClient inventoryManageDomainClient) {
		this.inventoryManageDomainClient = inventoryManageDomainClient;
	}

	public Boolean createAndAccountedOutgoingSaleDomain(
			OutgoingSaleVO outgoingSaleVO) {
		//记录
		createOutgoingSaleDomain(outgoingSaleVO);
		//修改状态
		submit(outgoingSaleVO.getId());
		//修改状态 同步库存
		audit(outgoingSaleVO.getId(), outgoingSaleVO.getOperate());
		return true;
	}

	public SkuService getSkuService() {
		return skuService;
	}

	public void setSkuService(SkuService skuService) {
		this.skuService = skuService;
	}

	public ValueService getValueService() {
		return valueService;
	}

	public void setValueService(ValueService valueService) {
		this.valueService = valueService;
	}

	public IPositionDomainClient getPositionDomainClient() {
		return positionDomainClient;
	}

	public void setPositionDomainClient(
			IPositionDomainClient positionDomainClient) {
		this.positionDomainClient = positionDomainClient;
	}
}
