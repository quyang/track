package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.vo.TradeServiceShelvingVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.service.itf.ITradeServiceShelvingService;
import com.xianzaishi.wms.track.manage.itf.ITradeServiceShelvingManage;

public class TradeServiceShelvingServiceImpl implements
		ITradeServiceShelvingService {
	@Autowired
	private ITradeServiceShelvingManage tradeServiceShelvingManage = null;

	public ITradeServiceShelvingManage getTradeServiceShelvingManage() {
		return tradeServiceShelvingManage;
	}

	public void setTradeServiceShelvingManage(
			ITradeServiceShelvingManage tradeServiceShelvingManage) {
		this.tradeServiceShelvingManage = tradeServiceShelvingManage;
	}

	public Long addTradeServiceShelvingVO(
			TradeServiceShelvingVO tradeServiceShelvingVO) {
		return tradeServiceShelvingManage
				.addTradeServiceShelvingVO(tradeServiceShelvingVO);
	}

	public List<TradeServiceShelvingVO> queryTradeServiceShelvingVOList(
			TradeServiceShelvingQueryVO tradeServiceShelvingQueryVO) {
		return tradeServiceShelvingManage
				.queryTradeServiceShelvingVOList(tradeServiceShelvingQueryVO);
	}

	public Integer queryTradeServiceShelvingVOCount(
			TradeServiceShelvingQueryVO tradeServiceShelvingQueryVO) {
		return tradeServiceShelvingManage
				.queryTradeServiceShelvingVOCount(tradeServiceShelvingQueryVO);
	}

	public TradeServiceShelvingVO getTradeServiceShelvingVOByID(Long id) {
		return (TradeServiceShelvingVO) tradeServiceShelvingManage
				.getTradeServiceShelvingVOByID(id);
	}

	public Boolean modifyTradeServiceShelvingVO(
			TradeServiceShelvingVO tradeServiceShelvingVO) {
		return tradeServiceShelvingManage
				.modifyTradeServiceShelvingVO(tradeServiceShelvingVO);
	}

	public Boolean deleteTradeServiceShelvingVOByID(Long id) {
		return tradeServiceShelvingManage.deleteTradeServiceShelvingVOByID(id);
	}

	public Boolean submit(Long id) {
		return tradeServiceShelvingManage.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return tradeServiceShelvingManage.audit(id, auditor);
	}

	public Boolean accounted(Long id) {
		return tradeServiceShelvingManage.accounted(id);
	}
}
