package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.manage.itf.ITransferDetailManage;
import com.xianzaishi.wms.track.service.itf.ITransferDetailService;
import com.xianzaishi.wms.track.vo.TransferDetailQueryVO;
import com.xianzaishi.wms.track.vo.TransferDetailVO;

public class TransferDetailServiceImpl implements ITransferDetailService {
	@Autowired
	private ITransferDetailManage transferDetailManage = null;

	public ITransferDetailManage getTransferDetailManage() {
		return transferDetailManage;
	}

	public void setTransferDetailManage(
			ITransferDetailManage transferDetailManage) {
		this.transferDetailManage = transferDetailManage;
	}

	public Boolean addTransferDetailVO(TransferDetailVO transferDetailVO) {
		transferDetailManage.addTransferDetailVO(transferDetailVO);
		return true;
	}

	public List<TransferDetailVO> queryTransferDetailVOList(
			TransferDetailQueryVO transferDetailQueryVO) {
		return transferDetailManage
				.queryTransferDetailVOList(transferDetailQueryVO);
	}

	public TransferDetailVO getTransferDetailVOByID(Long id) {
		return (TransferDetailVO) transferDetailManage
				.getTransferDetailVOByID(id);
	}

	public Boolean modifyTransferDetailVO(TransferDetailVO transferDetailVO) {
		return transferDetailManage.modifyTransferDetailVO(transferDetailVO);
	}

	public Boolean deleteTransferDetailVOByID(Long id) {
		return transferDetailManage.deleteTransferDetailVOByID(id);
	}

	public List<TransferDetailVO> getTransferDetailVOByTransferID(Long id) {
		return transferDetailManage.getTransferDetailVOByTransferID(id);
	}

	public Boolean batchAddTransferDetailVO(
			List<TransferDetailVO> transferDetailVOs) {
		return transferDetailManage.batchAddTransferDetailVO(transferDetailVOs);
	}

	public Boolean batchModifyTransferDetailVO(
			List<TransferDetailVO> transferDetailVOs) {
		return transferDetailManage
				.batchModifyTransferDetailVO(transferDetailVOs);
	}

	public Boolean batchDeleteTransferDetailVO(
			List<TransferDetailVO> transferDetailVOs) {
		return transferDetailManage
				.batchDeleteTransferDetailVO(transferDetailVOs);
	}

	public Boolean batchDeleteTransferDetailVOByID(List<Long> storyDetailIDs) {
		return transferDetailManage
				.batchDeleteTransferDetailVOByID(storyDetailIDs);
	}
}
