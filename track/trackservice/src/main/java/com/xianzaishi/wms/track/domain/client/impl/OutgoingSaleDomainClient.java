package com.xianzaishi.wms.track.domain.client.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.domain.client.itf.IOutgoingSaleDomainClient;
import com.xianzaishi.wms.track.domain.service.itf.IOutgoingSaleDomainService;
import com.xianzaishi.wms.track.vo.OutgoingSaleDetailQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingSaleDetailVO;
import com.xianzaishi.wms.track.vo.OutgoingSaleQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingSaleVO;

public class OutgoingSaleDomainClient implements IOutgoingSaleDomainClient {
	private static final Logger logger = Logger
			.getLogger(OutgoingSaleDomainClient.class);
	@Autowired
	private IOutgoingSaleDomainService outgoingSaleDomainService = null;

	public SimpleResultVO<Boolean> createOutgoingSaleDomain(
			OutgoingSaleVO outgoingSaleVO) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(outgoingSaleDomainService
					.createOutgoingSaleDomain(outgoingSaleVO));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<OutgoingSaleVO> getOutgoingSaleDomainByID(
			Long outgoingSaleID) {
		SimpleResultVO<OutgoingSaleVO> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(outgoingSaleDomainService
					.getOutgoingSaleDomainByID(outgoingSaleID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> deleteOutgoingSaleDomain(Long outgoingSaleID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(outgoingSaleDomainService
					.deleteOutgoingSaleDomain(outgoingSaleID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> addOutgoingSaleVO(
			OutgoingSaleVO outgoingSaleVO) {
		return SimpleResultVO.buildSuccessResult(outgoingSaleDomainService
				.addOutgoingSaleVO(outgoingSaleVO));
	}

	public SimpleResultVO<QueryResultVO<OutgoingSaleVO>> queryOutgoingSaleVOList(
			OutgoingSaleQueryVO outgoingSaleQueryVO) {
		return SimpleResultVO.buildSuccessResult(outgoingSaleDomainService
				.queryOutgoingSaleVOList(outgoingSaleQueryVO));
	}

	public SimpleResultVO<OutgoingSaleVO> getOutgoingSaleVOByID(Long id) {
		return SimpleResultVO.buildSuccessResult(outgoingSaleDomainService
				.getOutgoingSaleVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyOutgoingSaleVO(
			OutgoingSaleVO outgoingSaleVO) {
		return SimpleResultVO.buildSuccessResult(outgoingSaleDomainService
				.modifyOutgoingSaleVO(outgoingSaleVO));
	}

	public SimpleResultVO<Boolean> deleteOutgoingSaleVO(Long id) {
		return SimpleResultVO.buildSuccessResult(outgoingSaleDomainService
				.deleteOutgoingSaleVO(id));
	}

	public SimpleResultVO<Boolean> createOutgoingSaleDetailVO(
			OutgoingSaleDetailVO outgoingSaleDetailVO) {
		return SimpleResultVO.buildSuccessResult(outgoingSaleDomainService
				.createOutgoingSaleDetailVO(outgoingSaleDetailVO));
	}

	public SimpleResultVO<Boolean> batchCreateOutgoingSaleDetailVO(
			List<OutgoingSaleDetailVO> outgoingSaleDetailVOs) {
		return SimpleResultVO.buildSuccessResult(outgoingSaleDomainService
				.batchCreateOutgoingSaleDetailVO(outgoingSaleDetailVOs));
	}

	public SimpleResultVO<List<OutgoingSaleDetailVO>> queryOutgoingSaleDetailVOList(
			OutgoingSaleDetailQueryVO outgoingSaleDetailQueryVO) {
		return SimpleResultVO.buildSuccessResult(outgoingSaleDomainService
				.queryOutgoingSaleDetailVOList(outgoingSaleDetailQueryVO));
	}

	public SimpleResultVO<List<OutgoingSaleDetailVO>> getOutgoingSaleDetailVOListByOutgoingSaleID(
			Long outgoingSaleID) {
		return SimpleResultVO.buildSuccessResult(outgoingSaleDomainService
				.getOutgoingSaleDetailVOListByOutgoingSaleID(outgoingSaleID));
	}

	public SimpleResultVO<OutgoingSaleDetailQueryVO> getOutgoingSaleDetailVOByID(
			Long id) {
		return SimpleResultVO.buildSuccessResult(outgoingSaleDomainService
				.getOutgoingSaleDetailVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyOutgoingSaleDetailVO(
			OutgoingSaleDetailVO outgoingSaleDetailVO) {
		return SimpleResultVO.buildSuccessResult(outgoingSaleDomainService
				.modifyOutgoingSaleDetailVO(outgoingSaleDetailVO));
	}

	public SimpleResultVO<Boolean> batchModifyOutgoingSaleDetailVOs(
			List<OutgoingSaleDetailVO> outgoingSaleDetailVOs) {
		return SimpleResultVO.buildSuccessResult(outgoingSaleDomainService
				.batchModifyOutgoingSaleDetailVOs(outgoingSaleDetailVOs));
	}

	public SimpleResultVO<Boolean> deleteOutgoingSaleDetailVO(Long id) {
		return SimpleResultVO.buildSuccessResult(outgoingSaleDomainService
				.deleteOutgoingSaleDetailVO(id));
	}

	public SimpleResultVO<Boolean> deleteOutgoingSaleDetailVOByOutgoingSaleID(
			Long outgoingSaleID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO
					.buildSuccessResult(outgoingSaleDomainService
							.deleteOutgoingSaleDetailVOByOutgoingSaleID(outgoingSaleID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> batchDeleteOutgoingSaleDetailVOByOutgoingSaleID(
			List<Long> storyDetailIDs) {
		return SimpleResultVO
				.buildSuccessResult(outgoingSaleDomainService
						.batchDeleteOutgoingSaleDetailVOByOutgoingSaleID(storyDetailIDs));
	}

	public IOutgoingSaleDomainService getOutgoingSaleDomainService() {
		return outgoingSaleDomainService;
	}

	public void setOutgoingSaleDomainService(
			IOutgoingSaleDomainService outgoingSaleDomainService) {
		this.outgoingSaleDomainService = outgoingSaleDomainService;
	}

	public SimpleResultVO<Boolean> submit(Long id) {
		return SimpleResultVO.buildSuccessResult(outgoingSaleDomainService
				.submit(id));
	}

	public SimpleResultVO<Boolean> audit(Long id, Long auditor) {
		return SimpleResultVO.buildSuccessResult(outgoingSaleDomainService
				.audit(id, auditor));
	}

	public SimpleResultVO<Boolean> accounted(Long id) {
		return SimpleResultVO.buildSuccessResult(outgoingSaleDomainService
				.accounted(id));
	}

	public SimpleResultVO<Boolean> createAndAccountedOutgoingSaleDomain(
			OutgoingSaleVO outgoingSaleVO) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(outgoingSaleDomainService
					.createAndAccountedOutgoingSaleDomain(outgoingSaleVO));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}
}
