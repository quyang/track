package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.manage.itf.ITradeServiceShelvingDetailManage;
import com.xianzaishi.wms.track.service.itf.ITradeServiceShelvingDetailService;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingDetailQueryVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingDetailVO;

public class TradeServiceShelvingDetailServiceImpl implements
		ITradeServiceShelvingDetailService {
	@Autowired
	private ITradeServiceShelvingDetailManage tradeServiceShelvingDetailManage = null;

	public ITradeServiceShelvingDetailManage getTradeServiceShelvingDetailManage() {
		return tradeServiceShelvingDetailManage;
	}

	public void setTradeServiceShelvingDetailManage(
			ITradeServiceShelvingDetailManage tradeServiceShelvingDetailManage) {
		this.tradeServiceShelvingDetailManage = tradeServiceShelvingDetailManage;
	}

	public Boolean addTradeServiceShelvingDetailVO(
			TradeServiceShelvingDetailVO tradeServiceShelvingDetailVO) {
		tradeServiceShelvingDetailManage
				.addTradeServiceShelvingDetailVO(tradeServiceShelvingDetailVO);
		return true;
	}

	public List<TradeServiceShelvingDetailVO> queryTradeServiceShelvingDetailVOList(
			TradeServiceShelvingDetailQueryVO tradeServiceShelvingDetailQueryVO) {
		return tradeServiceShelvingDetailManage
				.queryTradeServiceShelvingDetailVOList(tradeServiceShelvingDetailQueryVO);
	}

	public TradeServiceShelvingDetailVO getTradeServiceShelvingDetailVOByID(
			Long id) {
		return (TradeServiceShelvingDetailVO) tradeServiceShelvingDetailManage
				.getTradeServiceShelvingDetailVOByID(id);
	}

	public Boolean modifyTradeServiceShelvingDetailVO(
			TradeServiceShelvingDetailVO tradeServiceShelvingDetailVO) {
		return tradeServiceShelvingDetailManage
				.modifyTradeServiceShelvingDetailVO(tradeServiceShelvingDetailVO);
	}

	public Boolean deleteTradeServiceShelvingDetailVOByID(Long id) {
		return tradeServiceShelvingDetailManage
				.deleteTradeServiceShelvingDetailVOByID(id);
	}

	public List<TradeServiceShelvingDetailVO> getTradeServiceShelvingDetailVOByTradeServiceShelvingID(
			Long id) {
		return tradeServiceShelvingDetailManage
				.getTradeServiceShelvingDetailVOByTradeServiceShelvingID(id);
	}

	public Boolean batchAddTradeServiceShelvingDetailVO(
			List<TradeServiceShelvingDetailVO> tradeServiceShelvingDetailVOs) {
		return tradeServiceShelvingDetailManage
				.batchAddTradeServiceShelvingDetailVO(tradeServiceShelvingDetailVOs);
	}

	public Boolean batchModifyTradeServiceShelvingDetailVO(
			List<TradeServiceShelvingDetailVO> tradeServiceShelvingDetailVOs) {
		return tradeServiceShelvingDetailManage
				.batchModifyTradeServiceShelvingDetailVO(tradeServiceShelvingDetailVOs);
	}

	public Boolean batchDeleteTradeServiceShelvingDetailVO(
			List<TradeServiceShelvingDetailVO> tradeServiceShelvingDetailVOs) {
		return tradeServiceShelvingDetailManage
				.batchDeleteTradeServiceShelvingDetailVO(tradeServiceShelvingDetailVOs);
	}

	public Boolean batchDeleteTradeServiceShelvingDetailVOByID(
			List<Long> storyDetailIDs) {
		return tradeServiceShelvingDetailManage
				.batchDeleteTradeServiceShelvingDetailVOByID(storyDetailIDs);
	}
}
