package com.xianzaishi.wms.track.service.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.OutgoingVO;
import com.xianzaishi.wms.track.vo.OutgoingQueryVO;

public interface IOutgoingService {

	public Long addOutgoingVO(OutgoingVO outgoingVO);

	public List<OutgoingVO> queryOutgoingVOList(OutgoingQueryVO outgoingQueryVO);

	public Integer queryOutgoingVOCount(OutgoingQueryVO outgoingQueryVO);

	public OutgoingVO getOutgoingVOByID(Long id);

	public Boolean modifyOutgoingVO(OutgoingVO outgoingVO);

	public Boolean deleteOutgoingVOByID(Long id);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

	public Boolean accounted(Long id);
}