package com.xianzaishi.wms.job.domain.service.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.job.vo.JobQueryVO;
import com.xianzaishi.wms.job.vo.JobVO;

public interface IJobDomainService {
	public Boolean addJob(JobVO jobVO);

	public JobVO getJob(Long jobID);

	public QueryResultVO<JobVO> getJobPending(Long jobID);

	public Boolean processed(Long jobID);

	public List<JobVO> queryJobVOList(JobQueryVO jobQueryVO);
}
