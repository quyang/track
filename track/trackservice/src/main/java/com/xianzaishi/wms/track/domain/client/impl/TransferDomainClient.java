package com.xianzaishi.wms.track.domain.client.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.domain.client.itf.ITransferDomainClient;
import com.xianzaishi.wms.track.domain.service.itf.ITransferDomainService;
import com.xianzaishi.wms.track.vo.TransferDetailQueryVO;
import com.xianzaishi.wms.track.vo.TransferDetailVO;
import com.xianzaishi.wms.track.vo.TransferQueryVO;
import com.xianzaishi.wms.track.vo.TransferVO;

public class TransferDomainClient implements ITransferDomainClient {
	private static final Logger logger = Logger
			.getLogger(TransferDomainClient.class);
	@Autowired
	private ITransferDomainService transferDomainService = null;

	public SimpleResultVO<Boolean> createTransferDomain(TransferVO transferVO) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(transferDomainService
					.createTransferDomain(transferVO));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<TransferVO> getTransferDomainByID(Long transferID) {
		SimpleResultVO<TransferVO> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(transferDomainService
					.getTransferDomainByID(transferID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> deleteTransferDomain(Long transferID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(transferDomainService
					.deleteTransferDomain(transferID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> addTransferVO(TransferVO transferVO) {
		return SimpleResultVO.buildSuccessResult(transferDomainService
				.addTransferVO(transferVO));
	}

	public SimpleResultVO<QueryResultVO<TransferVO>> queryTransferVOList(
			TransferQueryVO transferQueryVO) {
		return SimpleResultVO.buildSuccessResult(transferDomainService
				.queryTransferVOList(transferQueryVO));
	}

	public SimpleResultVO<TransferVO> getTransferVOByID(Long id) {
		return SimpleResultVO.buildSuccessResult(transferDomainService
				.getTransferVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyTransferVO(TransferVO transferVO) {
		return SimpleResultVO.buildSuccessResult(transferDomainService
				.modifyTransferVO(transferVO));
	}

	public SimpleResultVO<Boolean> deleteTransferVO(Long id) {
		return SimpleResultVO.buildSuccessResult(transferDomainService
				.deleteTransferVO(id));
	}

	public SimpleResultVO<Boolean> createTransferDetailVO(
			TransferDetailVO transferDetailVO) {
		return SimpleResultVO.buildSuccessResult(transferDomainService
				.createTransferDetailVO(transferDetailVO));
	}

	public SimpleResultVO<Boolean> batchCreateTransferDetailVO(
			List<TransferDetailVO> transferDetailVOs) {
		return SimpleResultVO.buildSuccessResult(transferDomainService
				.batchCreateTransferDetailVO(transferDetailVOs));
	}

	public SimpleResultVO<List<TransferDetailVO>> queryTransferDetailVOList(
			TransferDetailQueryVO transferDetailQueryVO) {
		return SimpleResultVO.buildSuccessResult(transferDomainService
				.queryTransferDetailVOList(transferDetailQueryVO));
	}

	public SimpleResultVO<List<TransferDetailVO>> getTransferDetailVOListByTransferID(
			Long transferID) {
		return SimpleResultVO.buildSuccessResult(transferDomainService
				.getTransferDetailVOListByTransferID(transferID));
	}

	public SimpleResultVO<TransferDetailQueryVO> getTransferDetailVOByID(Long id) {
		return SimpleResultVO.buildSuccessResult(transferDomainService
				.getTransferDetailVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyTransferDetailVO(
			TransferDetailVO transferDetailVO) {
		return SimpleResultVO.buildSuccessResult(transferDomainService
				.modifyTransferDetailVO(transferDetailVO));
	}

	public SimpleResultVO<Boolean> batchModifyTransferDetailVOs(
			List<TransferDetailVO> transferDetailVOs) {
		return SimpleResultVO.buildSuccessResult(transferDomainService
				.batchModifyTransferDetailVOs(transferDetailVOs));
	}

	public SimpleResultVO<Boolean> deleteTransferDetailVO(Long id) {
		return SimpleResultVO.buildSuccessResult(transferDomainService
				.deleteTransferDetailVO(id));
	}

	public SimpleResultVO<Boolean> deleteTransferDetailVOByTransferID(
			Long transferID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(transferDomainService
					.deleteTransferDetailVOByTransferID(transferID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> batchDeleteTransferDetailVOByTransferID(
			List<Long> storyDetailIDs) {
		return SimpleResultVO.buildSuccessResult(transferDomainService
				.batchDeleteTransferDetailVOByTransferID(storyDetailIDs));
	}

	public ITransferDomainService getTransferDomainService() {
		return transferDomainService;
	}

	public void setTransferDomainService(
			ITransferDomainService transferDomainService) {
		this.transferDomainService = transferDomainService;
	}

	public SimpleResultVO<Boolean> submit(Long id) {
		return SimpleResultVO.buildSuccessResult(transferDomainService
				.submit(id));
	}

	public SimpleResultVO<Boolean> audit(Long id, Long auditor) {
		return SimpleResultVO.buildSuccessResult(transferDomainService.audit(
				id, auditor));
	}

	public SimpleResultVO<Boolean> accounted(Long id) {
		return SimpleResultVO.buildSuccessResult(transferDomainService
				.accounted(id));
	}

	public SimpleResultVO<Boolean> createAndAccount(TransferVO transferVO) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(transferDomainService
					.createAndAccount(transferVO));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}
}
