package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.IProductDetailDao;
import com.xianzaishi.wms.track.manage.itf.IProductDetailManage;
import com.xianzaishi.wms.track.vo.ProductDetailDO;
import com.xianzaishi.wms.track.vo.ProductDetailQueryVO;
import com.xianzaishi.wms.track.vo.ProductDetailVO;

public class ProductDetailManageImpl implements IProductDetailManage {
	@Autowired
	private IProductDetailDao productDetailDao = null;

	private void validate(ProductDetailVO productDetailVO) {
		if (productDetailVO.getProductId() == null
				|| productDetailVO.getProductId() <= 0) {
			throw new BizException("productID error："
					+ productDetailVO.getProductId());
		}
		if (productDetailVO.getPositionId() == null
				|| productDetailVO.getPositionId() <= 0) {
			throw new BizException("positionID error："
					+ productDetailVO.getPositionId());
		}
		if (productDetailVO.getSkuId() == null
				|| productDetailVO.getSkuId() <= 0) {
			throw new BizException("skuID error：" + productDetailVO.getSkuId());
		}
		if (productDetailVO.getNumberReal() == null
				|| productDetailVO.getNumberReal() <= 0) {
			throw new BizException("productID error："
					+ productDetailVO.getNumberReal());
		}
		if (productDetailVO.getSkuName() == null
				|| productDetailVO.getSkuName().isEmpty()) {
			throw new BizException("skuName error："
					+ productDetailVO.getSkuName());
		}
		if (productDetailVO.getPositionCode() == null
				|| productDetailVO.getPositionCode().isEmpty()) {
			throw new BizException("positionCode error："
					+ productDetailVO.getPositionCode());
		}
		if (productDetailVO.getSaleUnit() == null
				|| productDetailVO.getSaleUnit().isEmpty()) {
			throw new BizException("saleUnit error："
					+ productDetailVO.getSaleUnit());
		}
		if (productDetailVO.getSaleUnitId() == null
				|| productDetailVO.getSaleUnitId() <= 0) {
			throw new BizException("saleUnitId error："
					+ productDetailVO.getSaleUnitId());
		}
		if (productDetailVO.getSaleUnitType() == null) {
			throw new BizException("saleUnitType error："
					+ productDetailVO.getSaleUnitType());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IProductDetailDao getProductDetailDao() {
		return productDetailDao;
	}

	public void setProductDetailDao(IProductDetailDao productDetailDao) {
		this.productDetailDao = productDetailDao;
	}

	public Boolean addProductDetailVO(ProductDetailVO productDetailVO) {
		validate(productDetailVO);
		productDetailDao.addDO(productDetailVO);
		return true;
	}

	public List<ProductDetailVO> queryProductDetailVOList(
			ProductDetailQueryVO productDetailQueryVO) {
		return productDetailDao.queryDO(productDetailQueryVO);
	}

	public ProductDetailVO getProductDetailVOByID(Long id) {
		return (ProductDetailVO) productDetailDao.getDOByID(id);
	}

	public Boolean modifyProductDetailVO(ProductDetailVO productDetailVO) {
		return productDetailDao.updateDO(productDetailVO);
	}

	public Boolean deleteProductDetailVOByID(Long id) {
		return productDetailDao.delDO(id);
	}

	public List<ProductDetailVO> getProductDetailVOByProductID(Long id) {
		ProductDetailQueryVO queryVO = new ProductDetailQueryVO();
		queryVO.setProductId(id);
		queryVO.setSize(Integer.MAX_VALUE);
		return productDetailDao.queryDO(queryVO);
	}

	public Boolean batchAddProductDetailVO(
			List<ProductDetailVO> productDetailVOs) {
		return productDetailDao.batchAddDO(productDetailVOs);
	}

	public Boolean batchModifyProductDetailVO(
			List<ProductDetailVO> productDetailVOs) {
		return productDetailDao.batchModifyDO(productDetailVOs);
	}

	public Boolean batchDeleteProductDetailVO(
			List<ProductDetailVO> productDetailVOs) {
		return productDetailDao.batchDeleteDO(productDetailVOs);
	}

	public Boolean batchDeleteProductDetailVOByID(List<Long> productDetailIDs) {
		return productDetailDao.batchDeleteDOByID(productDetailIDs);
	}
}
