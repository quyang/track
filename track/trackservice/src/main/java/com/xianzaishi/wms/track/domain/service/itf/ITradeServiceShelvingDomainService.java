package com.xianzaishi.wms.track.domain.service.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingDetailQueryVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingDetailVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingQueryVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingVO;

public interface ITradeServiceShelvingDomainService {

	// domain
	public Boolean createTradeServiceShelvingDomain(
			TradeServiceShelvingVO tradeServiceShelvingVO);

	public TradeServiceShelvingVO getTradeServiceShelvingDomainByID(
			Long tradeServiceShelvingID);

	public Boolean deleteTradeServiceShelvingDomain(Long id);

	public Boolean createAndAccount(
			TradeServiceShelvingVO tradeServiceShelvingVO);

	// head
	public Boolean addTradeServiceShelvingVO(
			TradeServiceShelvingVO tradeServiceShelvingVO);

	public QueryResultVO<TradeServiceShelvingVO> queryTradeServiceShelvingVOList(
			TradeServiceShelvingQueryVO tradeServiceShelvingQueryVO);

	public TradeServiceShelvingVO getTradeServiceShelvingVOByID(Long id);

	public Boolean modifyTradeServiceShelvingVO(
			TradeServiceShelvingVO tradeServiceShelvingVO);

	public Boolean deleteTradeServiceShelvingVO(Long tradeServiceShelvingID);

	// body
	public Boolean createTradeServiceShelvingDetailVO(
			TradeServiceShelvingDetailVO tradeServiceShelvingDetailVO);

	public Boolean batchCreateTradeServiceShelvingDetailVO(
			List<TradeServiceShelvingDetailVO> tradeServiceShelvingDetailVOs);

	public List<TradeServiceShelvingDetailVO> queryTradeServiceShelvingDetailVOList(
			TradeServiceShelvingDetailQueryVO tradeServiceShelvingDetailQueryVO);

	public List<TradeServiceShelvingDetailVO> getTradeServiceShelvingDetailVOListByTradeServiceShelvingID(
			Long tradeServiceShelvingID);

	public TradeServiceShelvingDetailVO getTradeServiceShelvingDetailVOByID(
			Long id);

	public Boolean modifyTradeServiceShelvingDetailVO(
			TradeServiceShelvingDetailVO tradeServiceShelvingDetailVO);

	public Boolean batchModifyTradeServiceShelvingDetailVOs(
			List<TradeServiceShelvingDetailVO> tradeServiceShelvingDetailVOs);

	public Boolean deleteTradeServiceShelvingDetailVO(Long id);

	public Boolean deleteTradeServiceShelvingDetailVOByTradeServiceShelvingID(
			Long tradeServiceShelvingID);

	public Boolean batchDeleteTradeServiceShelvingDetailVOByTradeServiceShelvingID(
			List<Long> storyDetailIDs);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

	public Boolean accounted(Long id);
}
