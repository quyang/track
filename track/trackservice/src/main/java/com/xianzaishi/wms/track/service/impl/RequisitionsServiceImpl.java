package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.vo.ProductVO;
import com.xianzaishi.wms.track.vo.RequisitionsVO;
import com.xianzaishi.wms.track.vo.RequisitionsQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.service.itf.IRequisitionsService;
import com.xianzaishi.wms.track.manage.itf.IRequisitionsManage;

public class RequisitionsServiceImpl implements IRequisitionsService {
	@Autowired
	private IRequisitionsManage requisitionsManage = null;

	public IRequisitionsManage getRequisitionsManage() {
		return requisitionsManage;
	}

	public void setRequisitionsManage(IRequisitionsManage requisitionsManage) {
		this.requisitionsManage = requisitionsManage;
	}

	public Long addRequisitionsVO(RequisitionsVO requisitionsVO) {
		return requisitionsManage.addRequisitionsVO(requisitionsVO);
	}

	public List<RequisitionsVO> queryRequisitionsVOList(
			RequisitionsQueryVO requisitionsQueryVO) {
		return requisitionsManage.queryRequisitionsVOList(requisitionsQueryVO);
	}

	public Integer queryRequisitionsVOCount(
			RequisitionsQueryVO requisitionsQueryVO) {
		return requisitionsManage.queryRequisitionsVOCount(requisitionsQueryVO);
	}

	public RequisitionsVO getRequisitionsVOByID(Long id) {
		return (RequisitionsVO) requisitionsManage.getRequisitionsVOByID(id);
	}

	public Boolean modifyRequisitionsVO(RequisitionsVO requisitionsVO) {
		return requisitionsManage.modifyRequisitionsVO(requisitionsVO);
	}

	public Boolean deleteRequisitionsVOByID(Long id) {
		return requisitionsManage.deleteRequisitionsVOByID(id);
	}

	public Boolean submit(Long id) {
		return requisitionsManage.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return requisitionsManage.audit(id, auditor);
	}

	public Boolean outgoing(Long id, Long auditor) {
		return requisitionsManage.outgoing(id, auditor);
	}

	public Boolean outgoingAudit(Long id, Long auditor) {
		return requisitionsManage.outgoingAudit(id, auditor);
	}

	public Boolean accounted(Long id) {
		return requisitionsManage.accounted(id);
	}

}
