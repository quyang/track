package com.xianzaishi.wms.barcode.service.itf;

import java.util.List;

import com.xianzaishi.wms.barcode.vo.BarcodeSequenceVO;
import com.xianzaishi.wms.barcode.vo.BarcodeSequenceQueryVO;

public interface IBarcodeSequenceService {

	public Boolean addBarcodeSequenceVO(BarcodeSequenceVO barcodeSequenceVO);

	public List<BarcodeSequenceVO> queryBarcodeSequenceVOList(
			BarcodeSequenceQueryVO barcodeSequenceQueryVO);

	public BarcodeSequenceVO getBarcodeSequenceVOByID(Long id);

	public Boolean modifyBarcodeSequenceVO(BarcodeSequenceVO barcodeSequenceVO);

	public Boolean deleteBarcodeSequenceVOByID(Long id);

	public String getBarcode(Integer type);

	public List<String> getBarcodes(Integer type, Integer no);

}