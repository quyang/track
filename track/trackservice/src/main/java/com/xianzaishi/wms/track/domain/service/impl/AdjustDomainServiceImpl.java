package com.xianzaishi.wms.track.domain.service.impl;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryManageDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IPositionDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryManageDetailVO;
import com.xianzaishi.wms.hive.vo.InventoryManageVO;
import com.xianzaishi.wms.hive.vo.PositionVO;
import com.xianzaishi.wms.track.domain.service.itf.IAdjustDomainService;
import com.xianzaishi.wms.track.service.itf.IAdjustDetailService;
import com.xianzaishi.wms.track.service.itf.IAdjustService;
import com.xianzaishi.wms.track.vo.AdjustDetailQueryVO;
import com.xianzaishi.wms.track.vo.AdjustDetailVO;
import com.xianzaishi.wms.track.vo.AdjustQueryVO;
import com.xianzaishi.wms.track.vo.AdjustVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingVO;

public class AdjustDomainServiceImpl implements IAdjustDomainService {
	@Autowired
	private IAdjustService adjustService = null;
	@Autowired
	private IAdjustDetailService adjustDetailService = null;
	@Autowired
	private IInventoryManageDomainClient inventoryManageDomainClient = null;
	@Autowired
	private IPositionDomainClient positionDomainClient = null;
	@Autowired
	private SkuService skuService = null;
	@Autowired
	private ValueService valueService = null;

	public Boolean createAdjustDomain(AdjustVO adjustVO) {
		if (adjustVO.getDetails() == null || adjustVO.getDetails().isEmpty()) {
			throw new BizException("请填充入库明细！");
		}
		adjustVO.setStatu(0);
		Long adjustID = adjustService.addAdjustVO(adjustVO);
		adjustVO.setId(adjustID);
		initDetails(adjustVO, adjustVO.getDetails());

		adjustDetailService.batchAddAdjustDetailVO(adjustVO.getDetails());

		return true;
	}

	public Boolean createAndAccount(AdjustVO adjustVO) {
		if (adjustVO.getDetails() == null || adjustVO.getDetails().isEmpty()) {
			throw new BizException("请填充入库明细！");
		}
		adjustVO.setStatu(0);
		Long adjustID = adjustService.addAdjustVO(adjustVO);
		adjustVO.setId(adjustID);
		initDetails(adjustVO, adjustVO.getDetails());

		adjustDetailService.batchAddAdjustDetailVO(adjustVO.getDetails());
		submit(adjustID);
		auditAdjust(adjustID, adjustVO.getOperate());
		return true;
	}

	public AdjustVO getAdjustDomainByID(Long adjustID) {
		if (adjustID == null || adjustID <= 0) {
			throw new BizException("id error");
		}

		AdjustVO adjustVO = adjustService.getAdjustVOByID(adjustID);

		adjustVO.setDetails(adjustDetailService
				.getAdjustDetailVOByAdjustID(adjustID));

		return adjustVO;
	}

	public Boolean deleteAdjustDomain(Long adjustID) {
		if (adjustID == null || adjustID <= 0) {
			throw new BizException("id error");
		}

		List<AdjustDetailVO> adjustDetailVOs = adjustDetailService
				.getAdjustDetailVOByAdjustID(adjustID);

		adjustDetailService.batchDeleteAdjustDetailVO(adjustDetailVOs);

		adjustService.deleteAdjustVOByID(adjustID);

		return true;
	}

	public Boolean addAdjustVO(AdjustVO adjustVO) {
		adjustService.addAdjustVO(adjustVO);
		return true;
	}

	public QueryResultVO<AdjustVO> queryAdjustVOList(AdjustQueryVO adjustQueryVO) {
		QueryResultVO<AdjustVO> queryResultVO = new QueryResultVO<>();
		queryResultVO.setPage(adjustQueryVO.getPage());
		queryResultVO.setSize(adjustQueryVO.getSize());
		queryResultVO.setItems(adjustService.queryAdjustVOList(adjustQueryVO));
		queryResultVO.setTotalCount(adjustService
				.queryAdjustVOCount(adjustQueryVO));
		return queryResultVO;
	}

	public AdjustVO getAdjustVOByID(Long id) {
		return adjustService.getAdjustVOByID(id);
	}

	public Boolean modifyAdjustVO(AdjustVO adjustVO) {
		return adjustService.modifyAdjustVO(adjustVO);
	}

	public Boolean deleteAdjustVO(Long id) {
		return adjustService.deleteAdjustVOByID(id);
	}

	public Boolean createAdjustDetailVO(AdjustDetailVO adjustDetailVO) {
		initInfo(adjustDetailVO);
		return adjustDetailService.addAdjustDetailVO(adjustDetailVO);
	}

	public Boolean batchCreateAdjustDetailVO(
			List<AdjustDetailVO> adjustDetailVOs) {
		for (AdjustDetailVO adjustDetailVO : adjustDetailVOs) {
			initInfo(adjustDetailVO);
		}
		return adjustDetailService.batchAddAdjustDetailVO(adjustDetailVOs);
	}

	public List<AdjustDetailVO> queryAdjustDetailVOList(
			AdjustDetailQueryVO adjustDetailQueryVO) {
		return adjustDetailService.queryAdjustDetailVOList(adjustDetailQueryVO);
	}

	public List<AdjustDetailVO> getAdjustDetailVOListByAdjustID(Long adjustID) {
		return adjustDetailService.getAdjustDetailVOByAdjustID(adjustID);
	}

	public AdjustDetailVO getAdjustDetailVOByID(Long id) {
		return adjustDetailService.getAdjustDetailVOByID(id);
	}

	public Boolean modifyAdjustDetailVO(AdjustDetailVO adjustDetailVO) {
		initInfo(adjustDetailVO);
		return adjustDetailService.modifyAdjustDetailVO(adjustDetailVO);
	}

	public Boolean batchModifyAdjustDetailVOs(
			List<AdjustDetailVO> adjustDetailVOs) {
		for (AdjustDetailVO adjustDetailVO : adjustDetailVOs) {
			initInfo(adjustDetailVO);
		}
		return adjustDetailService.batchModifyAdjustDetailVO(adjustDetailVOs);
	}

	public Boolean deleteAdjustDetailVO(Long id) {
		return adjustDetailService.deleteAdjustDetailVOByID(id);
	}

	public Boolean deleteAdjustDetailVOByAdjustID(Long adjustID) {
		if (adjustID == null || adjustID <= 0) {
			throw new BizException("id error");
		}

		List<AdjustDetailVO> adjustDetailVOs = adjustDetailService
				.getAdjustDetailVOByAdjustID(adjustID);

		adjustDetailService.batchDeleteAdjustDetailVO(adjustDetailVOs);

		return true;
	}

	public Boolean batchDeleteAdjustDetailVOByAdjustID(List<Long> storyDetailIDs) {
		return adjustDetailService
				.batchDeleteAdjustDetailVOByID(storyDetailIDs);
	}

	private void initDetails(AdjustVO adjustVO,
			List<AdjustDetailVO> adjustDetailVOs) {
		for (int i = 0; i < adjustDetailVOs.size(); i++) {
			adjustDetailVOs.get(i).setAdjustId(adjustVO.getId());
			initInfo(adjustDetailVOs.get(i));
		}
	}

	private void initInfo(AdjustDetailVO adjustDetailVO) {
		ItemCommoditySkuDTO itemCommoditySkuDTO = skuService.queryItemSku(
				SkuQuery.querySkuById(adjustDetailVO.getSkuId())).getModule();
		if (itemCommoditySkuDTO == null) {
			throw new BizException("skuID error:" + adjustDetailVO.getSkuId());
		}
		adjustDetailVO.setSkuName(itemCommoditySkuDTO.getTitle());
		adjustDetailVO.setSaleUnit(valueService.queryValueNameByValueId(
				itemCommoditySkuDTO.getSkuUnit()).getModule());
		adjustDetailVO
				.setSaleUnitId(new Long(itemCommoditySkuDTO.getSkuUnit()));
		adjustDetailVO.setSaleUnitType(new Integer(itemCommoditySkuDTO
				.querySaleUnitType()));
		// adjustDetailVO.setSpec(itemCommoditySkuDTO.getSkuSpecificationNum());
		// adjustDetailVO.setSpecUnit(valueService.queryValueNameByValueId(
		// itemCommoditySkuDTO.getSkuSpecificationUnit()).getModule());
		// adjustDetailVO.setSpecUnitId(new Long(itemCommoditySkuDTO
		// .getSkuSpecificationUnit()));
		PositionVO positionVO = positionDomainClient.getPositionVOByID(
				adjustDetailVO.getPositionId()).getTarget();
		if (positionVO == null) {
			throw new BizException("positionID error:"
					+ adjustDetailVO.getPositionId());
		}
		adjustDetailVO.setPositionCode(positionVO.getCode());

		if (adjustDetailVO.getNumberReal() == null
				&& adjustDetailVO.getNumber() != null
				&& !adjustDetailVO.getNumber().isEmpty()) {
			adjustDetailVO.setNumberReal(new BigDecimal(adjustDetailVO
					.getNumber()).multiply(new BigDecimal(1000)).intValue());
		}

		if (adjustDetailVO.getNumberOldReal() == null
				&& adjustDetailVO.getNumberOld() != null
				&& !adjustDetailVO.getNumberOld().isEmpty()) {
			adjustDetailVO.setNumberOldReal(new BigDecimal(adjustDetailVO
					.getNumberOld()).multiply(new BigDecimal(1000)).intValue());
		}
	}

	public IAdjustService getAdjustService() {
		return adjustService;
	}

	public void setAdjustService(IAdjustService adjustService) {
		this.adjustService = adjustService;
	}

	public IAdjustDetailService getAdjustDetailService() {
		return adjustDetailService;
	}

	public void setAdjustDetailService(IAdjustDetailService adjustDetailService) {
		this.adjustDetailService = adjustDetailService;
	}

	public Boolean auditAdjust(Long adjustID, Long operate) {
		Boolean flag = false;
		if (adjustService.audit(adjustID, operate)) {
			flag = accounted(adjustID);
		}
		return flag;
	}

	public Boolean accounted(Long id) {
		Boolean flag = false;
		AdjustVO adjustVO = getAdjustDomainByID(id);
		InventoryManageVO inventoryManageVO = initInventoryManage(adjustVO);
		SimpleResultVO<Boolean> result = inventoryManageDomainClient
				.createInventoryManageDomain(inventoryManageVO);
		if (result.isSuccess() && result.getTarget()) {
			flag = adjustService.accounted(id);
		}
		return flag;
	}

	private InventoryManageVO initInventoryManage(AdjustVO adjustVO) {
		InventoryManageVO inventoryManageVO = new InventoryManageVO();
		inventoryManageVO.setAgencyId(adjustVO.getAgencyId());
		inventoryManageVO.setAuditor(adjustVO.getAuditor());
		inventoryManageVO.setAuditTime(adjustVO.getAuditTime());
		inventoryManageVO.setOperator(adjustVO.getOperate());
		inventoryManageVO.setOpReason(adjustVO.getOpReason());
		inventoryManageVO.setOpTime(adjustVO.getOpTime());
		inventoryManageVO.setOpType(3);
		List<InventoryManageDetailVO> details = new LinkedList<InventoryManageDetailVO>();
		for (int i = 0; i < adjustVO.getDetails().size(); i++) {
			InventoryManageDetailVO inventoryManageDetailVO = new InventoryManageDetailVO();
			inventoryManageDetailVO
					.setNumber(adjustVO.getDetails().get(i).getNumberReal()
							- (adjustVO.getDetails().get(i).getNumberOldReal() == null ? 0
									: adjustVO.getDetails().get(i)
											.getNumberOldReal()));
			inventoryManageDetailVO.setSaleUnit(adjustVO.getDetails().get(i)
					.getSaleUnit());
			inventoryManageDetailVO.setSaleUnitId(adjustVO.getDetails().get(i)
					.getSaleUnitId());
			inventoryManageDetailVO.setSpec(adjustVO.getDetails().get(i)
					.getSpec());
			inventoryManageDetailVO.setSpecUnit(adjustVO.getDetails().get(i)
					.getSpecUnit());
			inventoryManageDetailVO.setSpecUnitId(adjustVO.getDetails().get(i)
					.getSpecUnitId());
			inventoryManageDetailVO.setSaleUnitType(adjustVO.getDetails()
					.get(i).getSaleUnitType());
			inventoryManageDetailVO.setSkuName(adjustVO.getDetails().get(i)
					.getSkuName());
			inventoryManageDetailVO.setPositionCode(adjustVO.getDetails()
					.get(i).getPositionCode());
			inventoryManageDetailVO.setSkuId(adjustVO.getDetails().get(i)
					.getSkuId());
			inventoryManageDetailVO.setPositionId(adjustVO.getDetails().get(i)
					.getPositionId());
			details.add(inventoryManageDetailVO);
		}
		inventoryManageVO.setDetails(details);
		return inventoryManageVO;
	}

	public IInventoryManageDomainClient getInventoryManageDomainClient() {
		return inventoryManageDomainClient;
	}

	public void setInventoryManageDomainClient(
			IInventoryManageDomainClient inventoryManageDomainClient) {
		this.inventoryManageDomainClient = inventoryManageDomainClient;
	}

	public Boolean submit(Long id) {
		return adjustService.submit(id);
	}

	public IPositionDomainClient getPositionDomainClient() {
		return positionDomainClient;
	}

	public void setPositionDomainClient(
			IPositionDomainClient positionDomainClient) {
		this.positionDomainClient = positionDomainClient;
	}

	public SkuService getSkuService() {
		return skuService;
	}

	public void setSkuService(SkuService skuService) {
		this.skuService = skuService;
	}

	public ValueService getValueService() {
		return valueService;
	}

	public void setValueService(ValueService valueService) {
		this.valueService = valueService;
	}

}
