package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.IOutgoingDetailDao;
import com.xianzaishi.wms.track.manage.itf.IOutgoingDetailManage;
import com.xianzaishi.wms.track.vo.OutgoingDetailQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingDetailVO;

public class OutgoingDetailManageImpl implements IOutgoingDetailManage {
	@Autowired
	private IOutgoingDetailDao outgoingDetailDao = null;

	private void validate(OutgoingDetailVO outgoingDetailVO) {
		if (outgoingDetailVO.getOutgoingId() == null
				|| outgoingDetailVO.getOutgoingId() <= 0) {
			throw new BizException("outgoingID error："
					+ outgoingDetailVO.getOutgoingId());
		}
		if (outgoingDetailVO.getPositionId() == null
				|| outgoingDetailVO.getPositionId() <= 0) {
			throw new BizException("positionID error："
					+ outgoingDetailVO.getPositionId());
		}
		if (outgoingDetailVO.getSkuId() == null
				|| outgoingDetailVO.getSkuId() <= 0) {
			throw new BizException("skuID error：" + outgoingDetailVO.getSkuId());
		}
		if (outgoingDetailVO.getNumberReal() == null
				|| outgoingDetailVO.getNumberReal() <= 0) {
			throw new BizException("number error："
					+ outgoingDetailVO.getNumberReal());
		}
		if (outgoingDetailVO.getSkuName() == null
				|| outgoingDetailVO.getSkuName().isEmpty()) {
			throw new BizException("skuName error："
					+ outgoingDetailVO.getSkuName());
		}
		if (outgoingDetailVO.getPositionCode() == null
				|| outgoingDetailVO.getPositionCode().isEmpty()) {
			throw new BizException("positionCode error："
					+ outgoingDetailVO.getPositionCode());
		}
		if (outgoingDetailVO.getSaleUnit() == null
				|| outgoingDetailVO.getSaleUnit().isEmpty()) {
			throw new BizException("saleUnit error："
					+ outgoingDetailVO.getSaleUnit());
		}
		if (outgoingDetailVO.getSaleUnitId() == null
				|| outgoingDetailVO.getSaleUnitId() <= 0) {
			throw new BizException("saleUnitId error："
					+ outgoingDetailVO.getSaleUnitId());
		}
		if (outgoingDetailVO.getSaleUnitType() == null) {
			throw new BizException("saleUnitType error："
					+ outgoingDetailVO.getSaleUnitType());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IOutgoingDetailDao getOutgoingDetailDao() {
		return outgoingDetailDao;
	}

	public void setOutgoingDetailDao(IOutgoingDetailDao outgoingDetailDao) {
		this.outgoingDetailDao = outgoingDetailDao;
	}

	public Boolean addOutgoingDetailVO(OutgoingDetailVO outgoingDetailVO) {
		validate(outgoingDetailVO);
		outgoingDetailDao.addDO(outgoingDetailVO);
		return true;
	}

	public List<OutgoingDetailVO> queryOutgoingDetailVOList(
			OutgoingDetailQueryVO outgoingDetailQueryVO) {
		return outgoingDetailDao.queryDO(outgoingDetailQueryVO);
	}

	public OutgoingDetailVO getOutgoingDetailVOByID(Long id) {
		return (OutgoingDetailVO) outgoingDetailDao.getDOByID(id);
	}

	public Boolean modifyOutgoingDetailVO(OutgoingDetailVO outgoingDetailVO) {
		return outgoingDetailDao.updateDO(outgoingDetailVO);
	}

	public Boolean deleteOutgoingDetailVOByID(Long id) {
		return outgoingDetailDao.delDO(id);
	}

	public List<OutgoingDetailVO> getOutgoingDetailVOByOutgoingID(Long id) {
		OutgoingDetailQueryVO queryVO = new OutgoingDetailQueryVO();
		queryVO.setOutgoingId(id);
		queryVO.setSize(Integer.MAX_VALUE);
		return outgoingDetailDao.queryDO(queryVO);
	}

	public Boolean batchAddOutgoingDetailVO(
			List<OutgoingDetailVO> outgoingDetailVOs) {
		return outgoingDetailDao.batchAddDO(outgoingDetailVOs);
	}

	public Boolean batchModifyOutgoingDetailVO(
			List<OutgoingDetailVO> outgoingDetailVOs) {
		return outgoingDetailDao.batchModifyDO(outgoingDetailVOs);
	}

	public Boolean batchDeleteOutgoingDetailVO(
			List<OutgoingDetailVO> outgoingDetailVOs) {
		return outgoingDetailDao.batchDeleteDO(outgoingDetailVOs);
	}

	public Boolean batchDeleteOutgoingDetailVOByID(List<Long> outgoingDetailIDs) {
		return outgoingDetailDao.batchDeleteDOByID(outgoingDetailIDs);
	}
}
