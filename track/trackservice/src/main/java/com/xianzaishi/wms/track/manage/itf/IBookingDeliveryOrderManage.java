package com.xianzaishi.wms.track.manage.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.BookingDeliveryOrderQueryVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryOrderVO;

public interface IBookingDeliveryOrderManage {

	public Boolean addBookingDeliveryOrderVO(
			BookingDeliveryOrderVO bookingDeliveryOrderVO);

	public List<BookingDeliveryOrderVO> queryBookingDeliveryOrderVOList(
			BookingDeliveryOrderQueryVO bookingDeliveryOrderQueryVO);

	public BookingDeliveryOrderVO getBookingDeliveryOrderVOByID(Long id);

	public Boolean modifyBookingDeliveryOrderVO(
			BookingDeliveryOrderVO bookingDeliveryOrderVO);

	public Boolean deleteBookingDeliveryOrderVOByID(Long id);

	public List<BookingDeliveryOrderVO> getBookingDeliveryOrderVOByOrderStorageID(
			Long id);

	public Boolean batchAddBookingDeliveryOrderVO(
			List<BookingDeliveryOrderVO> bookingDeliveryOrderVOs);

	public Boolean batchModifyBookingDeliveryOrderVO(
			List<BookingDeliveryOrderVO> bookingDeliveryOrderVOs);

	public Boolean batchDeleteBookingDeliveryOrderVO(
			List<BookingDeliveryOrderVO> bookingDeliveryOrderVOs);

	public Boolean batchDeleteBookingDeliveryOrderVOByID(
			List<Long> bookingDeliveryOrderIDs);

}