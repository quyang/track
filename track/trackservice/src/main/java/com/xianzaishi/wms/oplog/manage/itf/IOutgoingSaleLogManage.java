package com.xianzaishi.wms.oplog.manage.itf;

import java.util.List;

import com.xianzaishi.wms.oplog.vo.OutgoingSaleLogQueryVO;
import com.xianzaishi.wms.oplog.vo.OutgoingSaleLogVO;

public interface IOutgoingSaleLogManage {

	public Boolean addOutgoingSaleLogVO(OutgoingSaleLogVO outgoingSaleLogVO);

	public List<OutgoingSaleLogVO> queryOutgoingSaleLogVOList(
			OutgoingSaleLogQueryVO outgoingSaleLogQueryVO);

	public OutgoingSaleLogVO getOutgoingSaleLogVOByID(Long id);

	public Boolean modifyOutgoingSaleLogVO(OutgoingSaleLogVO outgoingSaleLogVO);

	public Boolean deleteOutgoingSaleLogVOByID(Long id);

	public Boolean batchAddOutgoingSaleLog(
			List<OutgoingSaleLogVO> outgoingSaleLogVOs);
}