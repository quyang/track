package com.xianzaishi.wms.track.domain.client.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.domain.client.itf.ITradeServiceShelvingDomainClient;
import com.xianzaishi.wms.track.domain.service.itf.ITradeServiceShelvingDomainService;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingDetailQueryVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingDetailVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingQueryVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingVO;

public class TradeServiceShelvingDomainClient implements
		ITradeServiceShelvingDomainClient {
	private static final Logger logger = Logger
			.getLogger(TradeServiceShelvingDomainClient.class);
	@Autowired
	private ITradeServiceShelvingDomainService tradeServiceShelvingDomainService = null;

	public SimpleResultVO<Boolean> createTradeServiceShelvingDomain(
			TradeServiceShelvingVO tradeServiceShelvingVO) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO
					.buildSuccessResult(tradeServiceShelvingDomainService
							.createTradeServiceShelvingDomain(tradeServiceShelvingVO));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<TradeServiceShelvingVO> getTradeServiceShelvingDomainByID(
			Long tradeServiceShelvingID) {
		SimpleResultVO<TradeServiceShelvingVO> flag = null;
		try {
			flag = SimpleResultVO
					.buildSuccessResult(tradeServiceShelvingDomainService
							.getTradeServiceShelvingDomainByID(tradeServiceShelvingID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> deleteTradeServiceShelvingDomain(
			Long tradeServiceShelvingID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO
					.buildSuccessResult(tradeServiceShelvingDomainService
							.deleteTradeServiceShelvingDomain(tradeServiceShelvingID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> addTradeServiceShelvingVO(
			TradeServiceShelvingVO tradeServiceShelvingVO) {
		return SimpleResultVO
				.buildSuccessResult(tradeServiceShelvingDomainService
						.addTradeServiceShelvingVO(tradeServiceShelvingVO));
	}

	public SimpleResultVO<QueryResultVO<TradeServiceShelvingVO>> queryTradeServiceShelvingVOList(
			TradeServiceShelvingQueryVO tradeServiceShelvingQueryVO) {
		return SimpleResultVO
				.buildSuccessResult(tradeServiceShelvingDomainService
						.queryTradeServiceShelvingVOList(tradeServiceShelvingQueryVO));
	}

	public SimpleResultVO<TradeServiceShelvingVO> getTradeServiceShelvingVOByID(
			Long id) {
		return SimpleResultVO
				.buildSuccessResult(tradeServiceShelvingDomainService
						.getTradeServiceShelvingVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyTradeServiceShelvingVO(
			TradeServiceShelvingVO tradeServiceShelvingVO) {
		return SimpleResultVO
				.buildSuccessResult(tradeServiceShelvingDomainService
						.modifyTradeServiceShelvingVO(tradeServiceShelvingVO));
	}

	public SimpleResultVO<Boolean> deleteTradeServiceShelvingVO(Long id) {
		return SimpleResultVO
				.buildSuccessResult(tradeServiceShelvingDomainService
						.deleteTradeServiceShelvingVO(id));
	}

	public SimpleResultVO<Boolean> createTradeServiceShelvingDetailVO(
			TradeServiceShelvingDetailVO tradeServiceShelvingDetailVO) {
		return SimpleResultVO
				.buildSuccessResult(tradeServiceShelvingDomainService
						.createTradeServiceShelvingDetailVO(tradeServiceShelvingDetailVO));
	}

	public SimpleResultVO<Boolean> batchCreateTradeServiceShelvingDetailVO(
			List<TradeServiceShelvingDetailVO> tradeServiceShelvingDetailVOs) {
		return SimpleResultVO
				.buildSuccessResult(tradeServiceShelvingDomainService
						.batchCreateTradeServiceShelvingDetailVO(tradeServiceShelvingDetailVOs));
	}

	public SimpleResultVO<List<TradeServiceShelvingDetailVO>> queryTradeServiceShelvingDetailVOList(
			TradeServiceShelvingDetailQueryVO tradeServiceShelvingDetailQueryVO) {
		return SimpleResultVO
				.buildSuccessResult(tradeServiceShelvingDomainService
						.queryTradeServiceShelvingDetailVOList(tradeServiceShelvingDetailQueryVO));
	}

	public SimpleResultVO<List<TradeServiceShelvingDetailVO>> getTradeServiceShelvingDetailVOListByTradeServiceShelvingID(
			Long tradeServiceShelvingID) {
		return SimpleResultVO
				.buildSuccessResult(tradeServiceShelvingDomainService
						.getTradeServiceShelvingDetailVOListByTradeServiceShelvingID(tradeServiceShelvingID));
	}

	public SimpleResultVO<TradeServiceShelvingDetailQueryVO> getTradeServiceShelvingDetailVOByID(
			Long id) {
		return SimpleResultVO
				.buildSuccessResult(tradeServiceShelvingDomainService
						.getTradeServiceShelvingDetailVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyTradeServiceShelvingDetailVO(
			TradeServiceShelvingDetailVO tradeServiceShelvingDetailVO) {
		return SimpleResultVO
				.buildSuccessResult(tradeServiceShelvingDomainService
						.modifyTradeServiceShelvingDetailVO(tradeServiceShelvingDetailVO));
	}

	public SimpleResultVO<Boolean> batchModifyTradeServiceShelvingDetailVOs(
			List<TradeServiceShelvingDetailVO> tradeServiceShelvingDetailVOs) {
		return SimpleResultVO
				.buildSuccessResult(tradeServiceShelvingDomainService
						.batchModifyTradeServiceShelvingDetailVOs(tradeServiceShelvingDetailVOs));
	}

	public SimpleResultVO<Boolean> deleteTradeServiceShelvingDetailVO(Long id) {
		return SimpleResultVO
				.buildSuccessResult(tradeServiceShelvingDomainService
						.deleteTradeServiceShelvingDetailVO(id));
	}

	public SimpleResultVO<Boolean> deleteTradeServiceShelvingDetailVOByTradeServiceShelvingID(
			Long tradeServiceShelvingID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO
					.buildSuccessResult(tradeServiceShelvingDomainService
							.deleteTradeServiceShelvingDetailVOByTradeServiceShelvingID(tradeServiceShelvingID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> batchDeleteTradeServiceShelvingDetailVOByTradeServiceShelvingID(
			List<Long> storyDetailIDs) {
		return SimpleResultVO
				.buildSuccessResult(tradeServiceShelvingDomainService
						.batchDeleteTradeServiceShelvingDetailVOByTradeServiceShelvingID(storyDetailIDs));
	}

	public ITradeServiceShelvingDomainService getTradeServiceShelvingDomainService() {
		return tradeServiceShelvingDomainService;
	}

	public void setTradeServiceShelvingDomainService(
			ITradeServiceShelvingDomainService tradeServiceShelvingDomainService) {
		this.tradeServiceShelvingDomainService = tradeServiceShelvingDomainService;
	}

	public SimpleResultVO<Boolean> submit(Long id) {
		return SimpleResultVO
				.buildSuccessResult(tradeServiceShelvingDomainService
						.submit(id));
	}

	public SimpleResultVO<Boolean> audit(Long id, Long auditor) {
		return SimpleResultVO
				.buildSuccessResult(tradeServiceShelvingDomainService.audit(id,
						auditor));
	}

	public SimpleResultVO<Boolean> accounted(Long id) {
		return SimpleResultVO
				.buildSuccessResult(tradeServiceShelvingDomainService
						.accounted(id));
	}

	public SimpleResultVO<Boolean> createAndAccount(
			TradeServiceShelvingVO tradeServiceShelvingVO) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO
					.buildSuccessResult(tradeServiceShelvingDomainService
							.createAndAccount(tradeServiceShelvingVO));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}
}
