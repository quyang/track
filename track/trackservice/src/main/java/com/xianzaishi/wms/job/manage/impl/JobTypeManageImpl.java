package com.xianzaishi.wms.job.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.job.vo.JobTypeVO;
import com.xianzaishi.wms.job.vo.JobTypeDO;
import com.xianzaishi.wms.job.vo.JobTypeQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.job.manage.itf.IJobTypeManage;

import com.xianzaishi.wms.job.dao.itf.IJobTypeDao;

public class JobTypeManageImpl implements IJobTypeManage {
	@Autowired
	private IJobTypeDao jobTypeDao = null;

	private void validate(JobTypeDO jobTypeDO) {
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}
	
	public IJobTypeDao getJobTypeDao() {
		return jobTypeDao;
	}

	public void setJobTypeDao(IJobTypeDao jobTypeDao) {
		this.jobTypeDao = jobTypeDao;
	}
	
	public Boolean addJobTypeVO(JobTypeVO jobTypeVO) {
		jobTypeDao.addDO(jobTypeVO);
		return true;
	}

	public List<JobTypeVO> queryJobTypeVOList(JobTypeQueryVO jobTypeQueryVO) {
		return jobTypeDao.queryDO(jobTypeQueryVO);
	}

	public JobTypeVO getJobTypeVOByID(Long id) {
		return (JobTypeVO) jobTypeDao.getDOByID(id);
	}

	public Boolean modifyJobTypeVO(JobTypeVO jobTypeVO) {
		return jobTypeDao.updateDO(jobTypeVO);
	}

	public Boolean deleteJobTypeVOByID(Long id) {
		return jobTypeDao.delDO(id);
	}
}
