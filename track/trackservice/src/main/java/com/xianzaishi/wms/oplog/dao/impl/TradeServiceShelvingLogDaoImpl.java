package com.xianzaishi.wms.oplog.dao.impl;

import java.util.List;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.oplog.dao.itf.ITradeServiceShelvingLogDao;
import com.xianzaishi.wms.oplog.vo.TradeServiceShelvingLogVO;

public class TradeServiceShelvingLogDaoImpl extends BaseDaoAdapter implements
		ITradeServiceShelvingLogDao {
	public String getVOClassName() {
		return "TradeServiceShelvingLogDO";
	}

	public Boolean batchAddTradeServiceShelvingLog(
			List<TradeServiceShelvingLogVO> tradeServiceShelvingLogVOs) {
		for (int i = 0; i < tradeServiceShelvingLogVOs.size(); i++) {
			addDO(tradeServiceShelvingLogVOs.get(i));
		}
		return true;
	}

}
