package com.xianzaishi.wms.track.service.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.BookingDeliveryDetailQueryVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryDetailVO;

public interface IBookingDeliveryDetailService {

	public Boolean addBookingDeliveryDetailVO(
			BookingDeliveryDetailVO bookingDeliveryDetailVO);

	public List<BookingDeliveryDetailVO> queryBookingDeliveryDetailVOList(
			BookingDeliveryDetailQueryVO bookingDeliveryDetailQueryVO);

	public BookingDeliveryDetailVO getBookingDeliveryDetailVOByID(Long id);

	public Boolean modifyBookingDeliveryDetailVO(
			BookingDeliveryDetailVO bookingDeliveryDetailVO);

	public Boolean deleteBookingDeliveryDetailVOByID(Long id);

	public List<BookingDeliveryDetailVO> getBookingDeliveryDetailVOByBookingDeliveryID(
			Long id);

	public Boolean batchAddBookingDeliveryDetailVO(
			List<BookingDeliveryDetailVO> bookingDeliveryDetailVOs);

	public Boolean batchModifyBookingDeliveryDetailVO(
			List<BookingDeliveryDetailVO> bookingDeliveryDetailVOs);

	public Boolean batchDeleteBookingDeliveryDetailVO(
			List<BookingDeliveryDetailVO> bookingDeliveryDetailVOs);

	public Boolean batchDeleteBookingDeliveryDetailVOByID(
			List<Long> bookingDeliveryDetailIDs);

}