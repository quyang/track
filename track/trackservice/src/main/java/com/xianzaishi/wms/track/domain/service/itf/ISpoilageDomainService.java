package com.xianzaishi.wms.track.domain.service.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.track.vo.SpoilageDetailQueryVO;
import com.xianzaishi.wms.track.vo.SpoilageDetailVO;
import com.xianzaishi.wms.track.vo.SpoilageQueryVO;
import com.xianzaishi.wms.track.vo.SpoilageVO;

public interface ISpoilageDomainService {

	// domain
	public Boolean createSpoilageDomain(SpoilageVO spoilageVO);

	public SpoilageVO getSpoilageDomainByID(Long spoilageID);

	public Boolean deleteSpoilageDomain(Long id);

	// head
	public Boolean addSpoilageVO(SpoilageVO spoilageVO);

	public QueryResultVO<SpoilageVO> querySpoilageVOList(
			SpoilageQueryVO spoilageQueryVO);

	public SpoilageVO getSpoilageVOByID(Long id);

	public Boolean modifySpoilageVO(SpoilageVO spoilageVO);

	public Boolean deleteSpoilageVO(Long spoilageID);

	// body
	public Boolean createSpoilageDetailVO(SpoilageDetailVO spoilageDetailVO);

	public Boolean batchCreateSpoilageDetailVO(
			List<SpoilageDetailVO> spoilageDetailVOs);

	public List<SpoilageDetailVO> querySpoilageDetailVOList(
			SpoilageDetailQueryVO spoilageDetailQueryVO);

	public List<SpoilageDetailVO> getSpoilageDetailVOListBySpoilageID(
			Long spoilageID);

	public SpoilageDetailVO getSpoilageDetailVOByID(Long id);

	public Boolean modifySpoilageDetailVO(SpoilageDetailVO spoilageDetailVO);

	/**
	 * 按ID更新Detail
	 * 
	 * @param spoilageDetailVOs
	 * @return
	 */
	public Boolean batchModifySpoilageDetailVOs(
			List<SpoilageDetailVO> spoilageDetailVOs);

	public Boolean deleteSpoilageDetailVO(Long id);

	public Boolean deleteSpoilageDetailVOBySpoilageID(Long spoilageID);

	public Boolean batchDeleteSpoilageDetailVOBySpoilageID(
			List<Long> storyDetailIDs);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

	public Boolean accounted(Long id);

}
