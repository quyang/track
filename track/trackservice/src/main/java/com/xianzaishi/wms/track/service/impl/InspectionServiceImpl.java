package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.manage.itf.IInspectionManage;
import com.xianzaishi.wms.track.service.itf.IInspectionService;
import com.xianzaishi.wms.track.vo.InspectionQueryVO;
import com.xianzaishi.wms.track.vo.InspectionVO;

public class InspectionServiceImpl implements IInspectionService {
	@Autowired
	private IInspectionManage inspectionManage = null;

	public IInspectionManage getInspectionManage() {
		return inspectionManage;
	}

	public void setInspectionManage(IInspectionManage inspectionManage) {
		this.inspectionManage = inspectionManage;
	}

	public Long addInspectionVO(InspectionVO inspectionVO) {
		return inspectionManage.addInspectionVO(inspectionVO);
	}

	public List<InspectionVO> queryInspectionVOList(
			InspectionQueryVO inspectionQueryVO) {
		return inspectionManage.queryInspectionVOList(inspectionQueryVO);
	}

	public Integer queryInspectionVOCount(InspectionQueryVO inspectionQueryVO) {
		return inspectionManage.queryInspectionVOCount(inspectionQueryVO);
	}

	public InspectionVO getInspectionVOByID(Long id) {
		return (InspectionVO) inspectionManage.getInspectionVOByID(id);
	}

	public Boolean modifyInspectionVO(InspectionVO inspectionVO) {
		return inspectionManage.modifyInspectionVO(inspectionVO);
	}

	public Boolean deleteInspectionVOByID(Long id) {
		return inspectionManage.deleteInspectionVOByID(id);
	}

	public Boolean submit(Long id) {
		return inspectionManage.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return inspectionManage.audit(id, auditor);
	}
}
