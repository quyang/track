package com.xianzaishi.wms.track.domain.service.impl;

import com.xianzaishi.customercenter.client.customerservice.CustomerService;
import com.xianzaishi.customercenter.client.customerservice.dto.CustomerServiceTaskDTO;
import com.xianzaishi.customercenter.client.customerservice.dto.ProcessDTO;
import com.xianzaishi.customercenter.client.customerservice.dto.ProcessDTO.ProcessTypeConstants;
import com.xianzaishi.customercenter.client.customerservice.dto.StorageProcessDTO;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationDTO.RelationTypeConstants;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuRelationQuery;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.PagedResult;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.purchase.PurchaseService;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseOrderDTO;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseOrderDTO.PurchaseDeliveryStatusConstants;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO.PurchaseSubOrderStorageTypeConstants;
import com.xianzaishi.purchasecenter.client.purchase.query.PurchaseQuery;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.utils.ListUtils;
import com.xianzaishi.wms.common.utils.ObjectUtils;
import com.xianzaishi.wms.common.utils.StringUtils;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryConfigDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryManageDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IPositionDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryManageDetailVO;
import com.xianzaishi.wms.hive.vo.InventoryManageVO;
import com.xianzaishi.wms.hive.vo.PositionDetailVO;
import com.xianzaishi.wms.hive.vo.PositionVO;
import com.xianzaishi.wms.track.domain.service.itf.IStorageDomainService;
import com.xianzaishi.wms.track.service.itf.IStorageDetailService;
import com.xianzaishi.wms.track.service.itf.IStorageService;
import com.xianzaishi.wms.track.vo.StorageDetailQueryVO;
import com.xianzaishi.wms.track.vo.StorageDetailVO;
import com.xianzaishi.wms.track.vo.StorageQueryVO;
import com.xianzaishi.wms.track.vo.StorageVO;
import com.xianzaishi.wms.track.vo.StorageVO.StorageReasonType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class StorageDomainServiceImpl implements IStorageDomainService {
	@Autowired
	private IStorageService storageService = null;
	@Autowired
	private IStorageDetailService storageDetailService = null;
	@Autowired
	private IInventoryManageDomainClient inventoryManageDomainClient = null;
	@Autowired
	private IPositionDomainClient positionDomainClient = null;
	@Autowired
	private SkuService skuService = null;
	@Autowired
	private ValueService valueService = null;
	@Autowired
	private IInventoryConfigDomainClient inventoryConfigDomainClient = null;
	@Autowired
	private CustomerService customerService = null;
	@Autowired
	private PurchaseService purchaseService = null;


	private static final Logger logger = Logger.getLogger(StorageDomainServiceImpl.class);

	private static final String TYPE_SPLIT = ",";

	private static final char JSON_SPLIT = (char)7;

	private static final String JSON_TYPE_SPLIT = "|";

	private static final String PURCHASE_ID = "purchaseId";

	private static final String PURCHASE_SUB_ORDERS = "purchaseId";

	public Boolean createStorageDomain(StorageVO storageVO) {
		if (storageVO.getDetails() == null || storageVO.getDetails().isEmpty()) {
			throw new BizException("请填充入库明细！");
		}
		storageVO.setStatu(0);
		Long storageID = storageService.addStorageVO(storageVO);
		storageVO.setId(storageID);
		initDetails(storageVO, storageVO.getDetails());

		storageDetailService.batchAddStorageDetailVO(storageVO.getDetails());

		return true;
	}

	//TODO 修改，当OpReason=11，不再调用audit方法，t_wms_ext_storage状态保持1（待审核状态）
	public Boolean createAndAccount(StorageVO storageVO) {
//		if (storageVO.getDetails() == null || storageVO.getDetails().isEmpty()) {
//			throw new BizException("请填充入库明细！");
//		}
//		//入库  提交
//		Long storageID = addAndSumitStorage(storageVO);
//
//		//审核后就会修改库存,产生流水
//		audit(storageID, storageVO.getOperate());//审核
//		return true;

		createAndAccountNew(storageVO);

		return true;
	}

	/**
	 * 入库
	 *
	 * @param storageVO 携带入库信息的数据对象
	 */
	public Boolean createAndAccountNew(StorageVO storageVO) {
		ObjectUtils.isNull(storageVO, "入库对象参数为空");
		ObjectUtils.isNull(storageVO.getOpReason(), "入库原因为空");
		ObjectUtils.isNull(storageVO.getAuditorName(),"总采购单id不能为空");

		logger.error("入库参数=" + JackSonUtil.getJson(storageVO));

		if (StorageReasonType.OTHER.equals(storageVO.getOpReason())) {//其他入库
			//入库 提交，但不审核
			Long storageID = addAndSumitStorage(storageVO);
			//插入客服审核记录
			insertStorageCustomTask(String.valueOf(storageID));

			return true;
		}



		//是采购单
		if (StorageReasonType.PUCHASE.equals(storageVO.getOpReason())) {//采购入库

			storageVO.setRelationId(Long.parseLong(storageVO.getAuditorName()));

			//除过其他原因的入库
			Long storageId = addAndSumitStorage(storageVO);
			logger.error("入库id=" + storageId);

			//审核，这个方法不好改，很多地方调用了，只能在下面单独下一个update方法了
			audit(storageId, storageVO.getOperate());//先审核才会产生流水

			//更新入库流水
			updateManageAndDetail(storageId, Integer.parseInt(storageVO.getAuditorName()));

			return true;

		}

		//剩余的入库原因
		Long storageId = addAndSumitStorage(storageVO);
		audit(storageId, storageVO.getOperate());

		return true;
	}

	/**
	 * 对入库商品做2次记录，并submit，但不audit(审核)
	 *
	 * @return 返回本次入库id
	 */
	private Long addAndSumitStorage(StorageVO storageVO) {
		if (null == storageVO) {
			throw new BizException("入库数据对象为空");
		}

		storageVO.setStatu(0);
		logger.error("更新数据addAndSumitStorage=" + JackSonUtil.getJson(storageVO));
		Long storageID = storageService.addStorageVO(storageVO);
		logger.error("入库idaddAndSumitStorage=" + storageID);

		storageVO.setId(storageID);
		initDetails(storageVO, storageVO.getDetails());

		//插入入库明细
		logger.error("入库明细addAndSumitStorage=" + JackSonUtil.getJson(storageVO.getDetails()));
		storageDetailService.batchAddStorageDetailVO(storageVO.getDetails());

		//修改status=1（审核状态） t_wms_ext_storage
		submit(storageID);
		return storageID;
	}

	/**
	 * 采购入库
	 * @param storageVO
	 * @param storageID
	 */
	private void isPurchaseOrder(StorageVO storageVO, Long storageID) {
		ObjectUtils.isNull(storageVO,"入库数据对象不能为空");
		ObjectUtils.isNull(storageID,"入库id不能为空" );

		//采购入库
		Map<String, Object> purchaseMaps = purchaseStorage(storageID, storageVO);


		Integer purchaseId = (Integer) purchaseMaps.get(PURCHASE_ID);
		Map<Long, PurchaseSubOrderDTO> purchaseSubOrdersMap = (Map<Long, PurchaseSubOrderDTO>) purchaseMaps
				.get(PURCHASE_SUB_ORDERS);

		logger.error("总采购单id=" + purchaseId);
		logger.error("子采购单信息=" + JackSonUtil.getJson(purchaseSubOrdersMap));

		//更新入库相关表
		StorageVO storageVOByID = storageService.getStorageVOByID(storageID);
		ObjectUtils.isNull(storageID, "查询入库信息失败");

		//更新总入库表
		storageVOByID.setRelationId(Long.valueOf(purchaseId + ""));
		notTrue(storageService.modifyStorageVO(storageVOByID), "更新总入库表失败");

		//更新子入库表
		List<StorageDetailVO> storageDetailVOS = storageDetailService
				.getStorageDetailVOByStorageID(storageID);
		updateStorageDetails(storageDetailVOS, purchaseSubOrdersMap);

		//更新流水信息
		updateManageAndDetail(storageID, purchaseId);


	}

	/**
	 * 更新入库流水，包括子入库流水
	 * @param storageID 入库id
	 * @param purchaseId 总采购单id
	 */
	private void updateManageAndDetail(Long storageID, Integer purchaseId) {
		ObjectUtils.isNull(storageID,"入库id不能为空");
		ObjectUtils.isNull(purchaseId,"总采购单id不能为空");
		//更新总流水表和子流水表
		StorageVO storageDomainByID = getStorageDomainByID(storageID);
		logger.error("通过入库id查询到的manageVO,updateManageAndDetail=" + JackSonUtil.getJson(storageDomainByID));
		InventoryManageVO manageVO = initInventoryManage(
				storageDomainByID);
		manageVO.setRelationId(Long.parseLong(purchaseId + ""));
		logger.error("初始化后的manageVo，updateManageAndDetail=" + JackSonUtil.getJson(manageVO));
		SimpleResultVO<InventoryManageVO> inventoryManageVOSimpleResultVO = inventoryManageDomainClient
				.updateInventoryManageDomain(manageVO);
		logger.error(
				"更新流水结果，updateManageAndDetail=" + JackSonUtil.getJson(inventoryManageVOSimpleResultVO));
		if (!inventoryManageVOSimpleResultVO.isSuccess()) {
			throw new BizException("更新入库流水信息失败");
		}
	}

	/**
	 * 给子入库信息设置子采购单id
	 * @param storageDetailVOS
	 * @param purchaseSubOrdersMap
	 */
	public void updateStorageDetails(List<StorageDetailVO> storageDetailVOS,Map<Long, PurchaseSubOrderDTO> purchaseSubOrdersMap) {
		ListUtils.isEmpty(storageDetailVOS,"子入库信息集合为空");
		ObjectUtils.isNull(purchaseSubOrdersMap,"子采购单map集合为空" );

		int a = 0;
		for (StorageDetailVO detailVO:storageDetailVOS) {
			if (null == detailVO) {
				continue;
			}
			//获取后台skuId
			Result<List<ItemSkuRelationDTO>> listResult = skuService
					.querySkuRelationList(
							SkuRelationQuery.querySkuRelationByRelationTypeAndCskuId(detailVO.getSkuId()));
			notSuccess(listResult,"获取商品sku信息不存在");
			Long pskuId = listResult.getModule().get(0).getPskuId();//后台skuId

			//更具skuId获取map中的子采购单信息
			PurchaseSubOrderDTO purchaseSubOrderDTO = purchaseSubOrdersMap.get(pskuId);
			if (null != purchaseSubOrderDTO) {
				detailVO.setRelationId(Long.parseLong(String.valueOf(purchaseSubOrderDTO.getPurchaseSubId())));
				a++;
			}
		}
		if (a != storageDetailVOS.size()) {
			throw new BizException("子入库信息关联子采购单信息时失败");
		}

		notTrue(storageDetailService.batchModifyStorageDetailVO(storageDetailVOS), "更新子入库单信息失败");
	}

	/**
	 * 插入客服审核任务记录
	 * 返回taskId
	 */
	private Long insertStorageCustomTask(String storageId) {
		CustomerServiceTaskDTO customerServiceTaskDTO = new CustomerServiceTaskDTO();
		customerServiceTaskDTO.setType((short) 1);//入库审核
		ProcessDTO processDTO = new StorageProcessDTO();
		processDTO.setProcessId(storageId);//设置入库id

		List<ProcessDTO> list = new ArrayList<>();
		list.add(processDTO);
		customerServiceTaskDTO.setCustomerRequireProcess(list);

		//返回插入的taskId
		Result<Long> longResult = customerService.insertCustomerServiceTask(customerServiceTaskDTO);
		notSuccess(longResult, "插入客服审核任务记录失败");
		return longResult.getModule();
	}

	public void notSuccess(Result result,String showMsg) {
		if (null == result) {
			throw new BizException("结果result为空");
		}
		if (!result.getSuccess()) {
			throw new BizException(showMsg);
		}
	}

	/**
	 * 请求对象转换成json 省事就要写恶心的代码咯，哈哈哈哈哈哈哈
	 *
	 * @param processList
	 * @return
	 */
	private String getProcessJson(List<ProcessDTO> processList) {
		if (CollectionUtils.isEmpty(processList)) {
			return "";
		}
		String type = "";
		String jsonStr = "";
		for (ProcessDTO process : processList) {
			if (process instanceof StorageProcessDTO) {
				type = type + ProcessTypeConstants.PROCESS_REFUND + TYPE_SPLIT;
				jsonStr = jsonStr + JackSonUtil.getJson(process) + String.valueOf(JSON_SPLIT);
			}
		}

		return type + JSON_TYPE_SPLIT + jsonStr;
	}

	/**
	 * 和入库模块方法一样，暂时未改之前的方法
	 * @param storageVO
	 * @return 某次入库id
	 */
	public Long createAndAccountGetStoreId(StorageVO storageVO) {
		if (storageVO.getDetails() == null || storageVO.getDetails().isEmpty()) {
			throw new BizException("请填充入库明细！");
		}
		storageVO.setStatu(0);
		Long storageID = storageService.addStorageVO(storageVO);
		storageVO.setId(storageID);
		initDetails(storageVO, storageVO.getDetails());

		storageDetailService.batchAddStorageDetailVO(storageVO.getDetails());
		submit(storageID);
		audit(storageID, storageVO.getOperate());
		return storageID;
	}

	/**
	 * 获取入库信息,
	 * @param storageID
	 * @return
	 */
	public StorageVO getStorageDomainByID(Long storageID) {
		if (storageID == null || storageID <= 0) {
			throw new BizException("id error");
		}

		StorageVO storageVO = storageService.getStorageVOByID(storageID);

		storageVO.setDetails(storageDetailService
				.getStorageDetailVOByStorageID(storageID));

		return storageVO;
	}

	public Boolean deleteStorageDomain(Long storageID) {
		if (storageID == null || storageID <= 0) {
			throw new BizException("id error");
		}

		List<StorageDetailVO> storageDetailVOs = storageDetailService
				.getStorageDetailVOByStorageID(storageID);

		storageDetailService.batchDeleteStorageDetailVO(storageDetailVOs);

		storageService.deleteStorageVOByID(storageID);

		return true;
	}

	public Boolean addStorageVO(StorageVO storageVO) {
		storageService.addStorageVO(storageVO);
		return true;
	}

	public QueryResultVO<StorageVO> queryStorageVOList(
			StorageQueryVO storageQueryVO) {
		QueryResultVO<StorageVO> queryResultVO = new QueryResultVO<>();
		queryResultVO.setPage(storageQueryVO.getPage());
		queryResultVO.setSize(storageQueryVO.getSize());
		queryResultVO.setItems(storageService
				.queryStorageVOList(storageQueryVO));
		queryResultVO.setTotalCount(storageService
				.queryStorageVOCount(storageQueryVO));
		return queryResultVO;

	}


	public StorageVO getStorageVOByID(Long id) {
		return storageService.getStorageVOByID(id);
	}

	public Boolean modifyStorageVO(StorageVO storageVO) {
		return storageService.modifyStorageVO(storageVO);
	}

	public Boolean deleteStorageVO(Long id) {
		return storageService.deleteStorageVOByID(id);
	}

	public Boolean createStorageDetailVO(StorageDetailVO storageDetailVO) {
		initInfo(storageDetailVO);
		return storageDetailService.addStorageDetailVO(storageDetailVO);
	}

	public Boolean batchCreateStorageDetailVO(
			List<StorageDetailVO> storageDetailVOs) {
		for (StorageDetailVO storageDetailVO : storageDetailVOs) {
			initInfo(storageDetailVO);
		}
		return storageDetailService.batchAddStorageDetailVO(storageDetailVOs);
	}

	public List<StorageDetailVO> queryStorageDetailVOList(
			StorageDetailQueryVO storageDetailQueryVO) {
		return storageDetailService
				.queryStorageDetailVOList(storageDetailQueryVO);
	}

	public List<StorageDetailVO> getStorageDetailVOListByStorageID(
			Long storageID) {
		return storageDetailService.getStorageDetailVOByStorageID(storageID);
	}

	public StorageDetailVO getStorageDetailVOByID(Long id) {
		return storageDetailService.getStorageDetailVOByID(id);
	}

	public Boolean modifyStorageDetailVO(StorageDetailVO storageDetailVO) {
		initInfo(storageDetailVO);
		return storageDetailService.modifyStorageDetailVO(storageDetailVO);
	}

	public Boolean batchModifyStorageDetailVOs(
			List<StorageDetailVO> storageDetailVOs) {
		for (StorageDetailVO storageDetailVO : storageDetailVOs) {
			initInfo(storageDetailVO);
		}
		return storageDetailService
				.batchModifyStorageDetailVO(storageDetailVOs);
	}

	public Boolean deleteStorageDetailVO(Long id) {
		return storageDetailService.deleteStorageDetailVOByID(id);
	}

	public Boolean deleteStorageDetailVOByStorageID(Long storageID) {
		if (storageID == null || storageID <= 0) {
			throw new BizException("id error");
		}

		List<StorageDetailVO> storageDetailVOs = storageDetailService
				.getStorageDetailVOByStorageID(storageID);

		storageDetailService.batchDeleteStorageDetailVO(storageDetailVOs);

		return true;
	}

	public Boolean batchDeleteStorageDetailVOByStorageID(
			List<Long> storyDetailIDs) {
		return storageDetailService
				.batchDeleteStorageDetailVOByID(storyDetailIDs);
	}

	/**
	 * 生成入库明细
	 * @param storageVO
	 * @param storageDetailVOs
	 */
	private void initDetails(StorageVO storageVO,
			List<StorageDetailVO> storageDetailVOs) {
		ObjectUtils.isNull(storageVO,"入库参数对象为空");
		ListUtils.isEmpty(storageDetailVOs,"入库信息为空");

		Map<Long, PurchaseSubOrderDTO> purchaseSubOrders = null;
		if (StorageReasonType.PUCHASE.equals(storageVO.getOpReason())) {//采购入库
			 purchaseSubOrders = getPurchaseSubOrderDTOMap(storageVO);
		}

		for (int i = 0; i < storageDetailVOs.size(); i++) {
			List<PositionDetailVO> configs = inventoryConfigDomainClient
					.getPositionDetailBySkuID(storageVO.getAgencyId(),
							storageDetailVOs.get(i).getSkuId()).getTarget();
			if (configs == null || configs.isEmpty()) {
				throw new BizException("未设置销售出库位："+storageDetailVOs.get(i).getSkuId());
			}
			storageDetailVOs.get(i).setStorageId(storageVO.getId());
			initInfo(storageDetailVOs.get(i));
			if (StorageReasonType.PUCHASE.equals(storageVO.getOpReason())) {//采购入库
				ObjectUtils.isNull(purchaseSubOrders,"子采购单map对象不能为空");
				//设置子采购单主键id
				StorageDetailVO storageDetailVO = storageDetailVOs.get(i);
				PurchaseSubOrderDTO subOrder = getPurchaseSubOrderByPskuId(
						storageDetailVO.getSkuId(), purchaseSubOrders);
				storageDetailVO.setRelationId(Long.parseLong(String.valueOf(subOrder.getPurchaseSubId())));
			}
		}
	}

	/**
	 * 根据客户端的参数对象的总采购单id获取对应的子采购单map
	 * @param storageVO
	 * @return
	 */
	private Map<Long, PurchaseSubOrderDTO> getPurchaseSubOrderDTOMap(StorageVO storageVO) {
		ObjectUtils.isNull(storageVO,"客户端的参数对象为空");
		ObjectUtils.isNull(storageVO.getAuditorName(),"总采购单id不能为空");
		//获取子采购单信息
		Result<List<PurchaseSubOrderDTO>> querySubPurchaseListByPurId = purchaseService
				.querySubPurchaseListByPurId(Integer.parseInt(storageVO.getAuditorName()));
		ListUtils.isEmpty(querySubPurchaseListByPurId.getModule(), "查询到的自采购单集合为空");
		return getPurchaseSubOrders(
				querySubPurchaseListByPurId.getModule());
	}

	/**
	 * 根据前台skuId从子采购单map集合中获取子采购单对象
	 *@param skuId 后台skuId
	 * @return 子采购单信息对象
	 */
	private PurchaseSubOrderDTO getPurchaseSubOrderByPskuId(Long skuId,
			Map<Long, PurchaseSubOrderDTO> purchaseSubOrders) {
		ObjectUtils.isNull(skuId,"skuId不能为空");
		ObjectUtils.isNull(purchaseSubOrders,"子采购单map集合对象为空");
		PurchaseSubOrderDTO dto = purchaseSubOrders.get(skuId);
		ObjectUtils.isNull(dto, "子采购单map集合中skId：" + skuId + "对应的数据对象为空");
		return dto;
	}

	/**
	 * 更具前台skuId获取后台skuid
	 * @param cskuId 前台skuId
	 * @return 后台skuId
	 */
	private Long getPskuIdByCskuId(Long cskuId) {
		ObjectUtils.isNull(cskuId, "skuId不能为空");
		//获取后台skuId
		Result<List<ItemSkuRelationDTO>> listResult = skuService
				.querySkuRelationList(
						SkuRelationQuery.querySkuRelationByRelationTypeAndCskuId(cskuId));
		notSuccess(listResult,"获取商品sku信息不存在");
		Long pskuId = listResult.getModule().get(0).getPskuId();//后台skuId
		ObjectUtils.isNull(pskuId,"获取到的skuId不能为空");
		return pskuId ;
	}


	/**
	 * 更具后台skuId获取前台skuid
	 * @param pskuId 后台skuId
	 * @return 前台skuId
	 */
	private Long getCskuIdByPskuId(Long pskuId) {
		ObjectUtils.isNull(pskuId, "pskuId不能为空");
		//获取后台skuId
		Result<List<ItemSkuRelationDTO>> listResult = skuService
				.querySkuRelationList(
						SkuRelationQuery.getQueryByPskuIds(Arrays.asList(pskuId),
								RelationTypeConstants.RELATION_TYPE_DISCOUNT));
		notSuccess(listResult,"获取商品sku信息不存在");
		Long cskuId = listResult.getModule().get(0).getCskuId();//前台skuId
		ObjectUtils.isNull(cskuId,"获取到的skuId不能为空");
		return cskuId ;
	}

	private void initInfo(StorageDetailVO storageDetailVO) {
		ItemCommoditySkuDTO itemCommoditySkuDTO = skuService.queryItemSku(
				SkuQuery.querySkuById(storageDetailVO.getSkuId())).getModule();
		if (itemCommoditySkuDTO == null) {
			throw new BizException("skuID error:" + storageDetailVO.getSkuId());
		}
		storageDetailVO.setSkuName(itemCommoditySkuDTO.getTitle());
		storageDetailVO.setSaleUnit(valueService.queryValueNameByValueId(
				itemCommoditySkuDTO.getSkuUnit()).getModule());
		storageDetailVO
				.setSaleUnitId(new Long(itemCommoditySkuDTO.getSkuUnit()));
		storageDetailVO.setSaleUnitType(new Integer(itemCommoditySkuDTO
				.querySaleUnitType()));
		// storageDetailVO.setSpec(itemCommoditySkuDTO.getSkuSpecificationNum());
		// storageDetailVO.setSpecUnit(valueService.queryValueNameByValueId(
		// itemCommoditySkuDTO.getSkuSpecificationUnit()).getModule());
		// storageDetailVO.setSpecUnitId(new Long(itemCommoditySkuDTO
		// .getSkuSpecificationUnit()));
		PositionVO positionVO = positionDomainClient.getPositionVOByID(
				storageDetailVO.getPositionId()).getTarget();
		if (positionVO == null) {
			throw new BizException("positionID error:"
					+ storageDetailVO.getPositionId());
		}
		storageDetailVO.setPositionCode(positionVO.getCode());

		if (storageDetailVO.getNumberReal() == null
				&& storageDetailVO.getNumber() != null
				&& !storageDetailVO.getNumber().isEmpty()) {
			storageDetailVO.setNumberReal(new BigDecimal(storageDetailVO
					.getNumber()).multiply(new BigDecimal(1000)).intValue());
		}
	}

	public IStorageService getStorageService() {
		return storageService;
	}

	public void setStorageService(IStorageService storageService) {
		this.storageService = storageService;
	}

	public IStorageDetailService getStorageDetailService() {
		return storageDetailService;
	}

	public void setStorageDetailService(
			IStorageDetailService storageDetailService) {
		this.storageDetailService = storageDetailService;
	}

	public Boolean auditStorage(Long storageID, Long operate) {
		Boolean flag = false;
		StorageVO storageVO = storageService.getStorageVOByID(storageID);
		storageVO.setAuditor(operate);
		if (storageService.audit(storageID, operate)) {
			storageVO = getStorageDomainByID(storageID);
			InventoryManageVO inventoryManageVO = initInventoryManage(storageVO);
			SimpleResultVO<Boolean> result = inventoryManageDomainClient
					.createInventoryManageDomain(inventoryManageVO);
			if (result.isSuccess() && result.getTarget()) {
				flag = storageService.accounted(storageID);
			} else {
				throw new BizException(result.getMessage());
			}
		}
		return flag;
	}

	private InventoryManageVO initInventoryManage(StorageVO storageVO) {
		InventoryManageVO inventoryManageVO = new InventoryManageVO();
		inventoryManageVO.setAgencyId(storageVO.getAgencyId());
		inventoryManageVO.setAuditor(storageVO.getAuditor());
		inventoryManageVO.setAuditTime(storageVO.getAuditTime());
		inventoryManageVO.setOperator(storageVO.getOperate());
		inventoryManageVO.setOpReason(storageVO.getOpReason());
		inventoryManageVO.setOpTime(storageVO.getOpTime());
		inventoryManageVO.setOpType(2);
		List<InventoryManageDetailVO> details = new LinkedList<InventoryManageDetailVO>();
		for (int i = 0; i < storageVO.getDetails().size(); i++) {
			InventoryManageDetailVO inventoryManageDetailVO = new InventoryManageDetailVO();
			inventoryManageDetailVO.setNumber(storageVO.getDetails().get(i)
					.getNumberReal());
			inventoryManageDetailVO.setSaleUnit(storageVO.getDetails().get(i)
					.getSaleUnit());
			inventoryManageDetailVO.setSaleUnitId(storageVO.getDetails().get(i)
					.getSaleUnitId());
			inventoryManageDetailVO.setSpec(storageVO.getDetails().get(i)
					.getSpec());
			inventoryManageDetailVO.setSpecUnit(storageVO.getDetails().get(i)
					.getSpecUnit());
			inventoryManageDetailVO.setSpecUnitId(storageVO.getDetails().get(i)
					.getSpecUnitId());
			inventoryManageDetailVO.setSaleUnitType(storageVO.getDetails()
					.get(i).getSaleUnitType());
			inventoryManageDetailVO.setSkuName(storageVO.getDetails().get(i)
					.getSkuName());
			inventoryManageDetailVO.setPositionCode(storageVO.getDetails()
					.get(i).getPositionCode());
			inventoryManageDetailVO.setSkuId(storageVO.getDetails().get(i)
					.getSkuId());
			inventoryManageDetailVO.setPositionId(storageVO.getDetails().get(i)
					.getPositionId());
			details.add(inventoryManageDetailVO);
		}
		inventoryManageVO.setDetails(details);
		return inventoryManageVO;
	}

	public IInventoryManageDomainClient getInventoryManageDomainClient() {
		return inventoryManageDomainClient;
	}

	public void setInventoryManageDomainClient(
			IInventoryManageDomainClient inventoryManageDomainClient) {
		this.inventoryManageDomainClient = inventoryManageDomainClient;
	}

	public Boolean submit(Long id) {
		return storageService.submit(id);
	}

	/**
	 * 审核
	 * @param id 入库id
	 * @param auditor 操作人id
	 * @return
	 */
	public Boolean audit(Long id, Long auditor) {
		Boolean flag = false;
		if (storageService.audit(id, auditor)) {
			flag = accounted(id);
		}
		return flag;
	}

	public Boolean accounted(Long id) {
		Boolean flag = false;
		StorageVO storageVO = getStorageDomainByID(id);
		InventoryManageVO inventoryManageVO = initInventoryManage(storageVO);
		SimpleResultVO<Boolean> result = inventoryManageDomainClient
				.createInventoryManageDomain(inventoryManageVO);
		if (result.isSuccess() && result.getTarget()) {
			flag = storageService.accounted(id);
		}
		return flag;
	}

	public IPositionDomainClient getPositionDomainClient() {
		return positionDomainClient;
	}

	public void setPositionDomainClient(
			IPositionDomainClient positionDomainClient) {
		this.positionDomainClient = positionDomainClient;
	}

	public SkuService getSkuService() {
		return skuService;
	}

	public void setSkuService(SkuService skuService) {
		this.skuService = skuService;
	}

	public ValueService getValueService() {
		return valueService;
	}

	public void setValueService(ValueService valueService) {
		this.valueService = valueService;
	}

	public IInventoryConfigDomainClient getInventoryConfigDomainClient() {
		return inventoryConfigDomainClient;
	}

	public void setInventoryConfigDomainClient(
			IInventoryConfigDomainClient inventoryConfigDomainClient) {
		this.inventoryConfigDomainClient = inventoryConfigDomainClient;
	}

	/**
	 * 采购入库 修改采购相关的表
	 * @param storageVO 客户端的入库明细
	 * @param storageId 入库id
	 * @return 包含总采购单和子采购单信息的map
	 */
	private Map<String, Object> purchaseStorage(Long storageId, StorageVO storageVO) {

		//存储总采购单id和子采购单id
		Map<String, Object> map = new HashMap<>();

		//查询该采购单是否采购过
		PurchaseQuery query = new PurchaseQuery();
		query.setParentPurchaseId(Integer.parseInt(storageVO.getAuditorName()));
		PagedResult<List<PurchaseOrderDTO>> listPagedResult = purchaseService.queryPurchase(query);

		notSuccess(listPagedResult,"查询采购单信息失败");
		ListUtils.isEmpty(listPagedResult.getModule(),"不存在该采购单，采购入库失败");

		if (PurchaseDeliveryStatusConstants.STATUS_STORAGE_ALL
				.equals(listPagedResult.getModule().get(0).getDeliveryStatus())
				|| PurchaseDeliveryStatusConstants.STATUS_STORAGE_PART
				.equals(listPagedResult.getModule().get(0).getDeliveryStatus())) {

			throw new BizException("采购入库失败，该采购单已经采购入库过");
		}

		StringUtils.isEmptyString(storageVO.getAuditorName(), "采购单id为空");

		//得到总采购单
		Integer purchaseId = Integer.valueOf(storageVO.getAuditorName());
		PurchaseQuery purchaseQuery = new PurchaseQuery();
		purchaseQuery.setParentPurchaseId(purchaseId);
		PagedResult<List<PurchaseOrderDTO>> queryPurchase = purchaseService
				.queryPurchase(purchaseQuery);

		notSuccess(queryPurchase, "查询总采购单信失败");
		ListUtils.isEmpty(queryPurchase.getModule(), "总采购单信息为空");

		//获取到所有子采购单
		Result<List<PurchaseSubOrderDTO>> querySubPurchaseListByPurId = purchaseService
				.querySubPurchaseListByPurId(purchaseId);

		notSuccess(querySubPurchaseListByPurId, "查询结果失败");
		ListUtils.isEmpty(querySubPurchaseListByPurId.getModule(), "子采购单信息为空");

		List<PurchaseSubOrderDTO> module = querySubPurchaseListByPurId.getModule();
		//提交的采购单
		List<StorageDetailVO> details = storageVO.getDetails();

		//客户端采购入库商品种类数量
		int size = details.size();
		int sizeShould = module.size();//本该采购商品种类数量

		map.put(PURCHASE_ID,listPagedResult.getModule().get(0).getPurchaseId());//总采购单id
		map.put(PURCHASE_SUB_ORDERS, getPurchaseSubOrders(querySubPurchaseListByPurId.getModule()));


		boolean iswarehousing = true;//默认全部入库,指的是采购种类和要求采购商品种类一样时候的每种商品采购数量是否全部入库

		for (int a = 0; a < details.size(); a++) {

			//入库信息
			StorageDetailVO storageDetailVO = details.get(a);

			//自采购单信息
			PurchaseSubOrderDTO purchaseQualityDTO = module.get(a);
			purchaseQualityDTO.setStorageId(storageId);
			purchaseQualityDTO.setPartTag(PurchaseSubOrderStorageTypeConstants.PURCHASE_YES);

			//入库商品总价
			String number = storageDetailVO.getNumber();//入库总数量
			Integer count = purchaseQualityDTO.getCount();//要采购箱数
			Integer unitCost = purchaseQualityDTO.getUnitCost();//每箱成本,单位分
			Integer pcProportion = purchaseQualityDTO.getPcProportion();//每箱有多少件

			BigDecimal bigDecimal = new BigDecimal(number);
			BigDecimal box = bigDecimal
					.divide(new BigDecimal(pcProportion).divide(new BigDecimal("1000")), 3,
							BigDecimal.ROUND_HALF_DOWN);//入库了多少箱
			BigDecimal realPrice = box.multiply(new BigDecimal(unitCost));

			//实际入库商品总价
			logger.error("真实价格=" + realPrice.intValue());
			purchaseQualityDTO.setSettlementPrice(realPrice.intValue());

			//采购时间
			//采购员提交数量小于实际需求的数量  入库未完成
			BigDecimal srcNumber = new BigDecimal(storageDetailVO.getNumber());
			BigDecimal targetNumber = new BigDecimal(purchaseQualityDTO.getCount())
					.multiply(new BigDecimal(purchaseQualityDTO.getPcProportion())
							.divide(new BigDecimal("1000")));
			if (srcNumber.compareTo(targetNumber) == -1) {
				iswarehousing = false;
				purchaseQualityDTO.setFlowStatus(
						PurchaseSubOrderDTO.PurchaseSubOrderFlowStatusConstants.STORAGE_STATUS_PART);
			} else {
				purchaseQualityDTO.setFlowStatus(
						PurchaseSubOrderDTO.PurchaseSubOrderFlowStatusConstants.STORAGE_STATUS_ALL);
			}
		}

		//总采购单
		PurchaseOrderDTO purchaseOrderDTO = queryPurchase.getModule().get(0);
		purchaseOrderDTO.setStorageId(storageId);//入库id

		if (iswarehousing) {
			//全部入库
			purchaseOrderDTO.setDeliveryStatus(
					PurchaseOrderDTO.PurchaseDeliveryStatusConstants.STATUS_STORAGE_ALL);
			//审核状态
			purchaseOrderDTO.setAuditingStatus(
					PurchaseOrderDTO.PurchaseOrderAuditingStatusConstants.STATUS_SUPPLIER_AUDITING_RECEIVE);
		} else {
			//部分入库
			purchaseOrderDTO.setDeliveryStatus(
					PurchaseOrderDTO.PurchaseDeliveryStatusConstants.STATUS_STORAGE_PART);
		}
		//主采购单更新成功
		for (PurchaseSubOrderDTO purchaseSubOrderDTO : module) {
			purchaseSubOrderDTO.setArriveDate(new Date());
			//更新子采购单
			if (!purchaseService.updatePurchaseSubOrder(purchaseSubOrderDTO).getModule()) {
				throw new BizException("更新自采购单：" + purchaseSubOrderDTO.getPurchaseSubId() + "失败");
			}
		}
		if (!purchaseService.updatePurchaseOrder(purchaseOrderDTO).getModule()) {
			throw new BizException("更新总采购单失败");
		}

		//部分种类商品采购入库
		if (!isPurchaseAll(module)) {
			purchaseOrderDTO
					.setDeliveryStatus(PurchaseDeliveryStatusConstants.STATUS_STORAGE_PART);//总采购单部分入库
			for (PurchaseSubOrderDTO dto : module) {
				if (null == dto.getPartTag()) {
					dto.setStorageId(null);
					dto.setFlowStatus(
							PurchaseSubOrderDTO.PurchaseSubOrderFlowStatusConstants.FLOW_STATUS_NOT_START);
					dto.setArriveDate(null);
				}
			}

			if (purchaseService.updatePurchaseOrder(purchaseOrderDTO).getModule()) {
				for (PurchaseSubOrderDTO purchaseSubOrderDTO : module) {
					purchaseSubOrderDTO.setArriveDate(new Date());

					//更新子采购单
					purchaseService.updatePurchaseSubOrder(purchaseSubOrderDTO);
				}
			}
		}

		return map;
	}

	/**
	 * 将子采购单结果list转为map，key为后台sku_id，value为PurchaseSubOrderDTO
	 * @param list 自采购单结果集合
	 * @return map
	 */
	private Map<Long, PurchaseSubOrderDTO> getPurchaseSubOrders(
			List<PurchaseSubOrderDTO> list) {
		ListUtils.isEmpty(list, "子采购单集合为空");
		HashMap<Long, PurchaseSubOrderDTO> map = new HashMap<>();
		for (PurchaseSubOrderDTO purchaseOrderDTO : list) {
			if (null == purchaseOrderDTO) {
				continue;
			}
			map.put(purchaseOrderDTO.getSkuId(), purchaseOrderDTO);
		}
		logger.error("子采购单map集合数据=" + JackSonUtil.getJson(map));
		return map;
	}

	/**
	 * 判断是否全部采购，指的是商品种类
	 */
	private Boolean isPurchaseAll(List<PurchaseSubOrderDTO> module) {
		if (null == module || CollectionUtils.isEmpty(module)) {
			return false;
		}

		int count = 0;
		int size = module.size();
		for (PurchaseSubOrderDTO dto : module) {
			Short partTag = dto.getPartTag();
			if (PurchaseSubOrderStorageTypeConstants.PURCHASE_YES.equals(partTag)) {
				count = count + 1;
			}
		}
		if (size == count) {
			return true;
		}
		return false;
	}

	public void notTrue(Boolean a,String showMsg) {
		ObjectUtils.isNull(a,"布尔类型参数为null");
		if (!a) {
			throw new BizException(showMsg);
		}
	}

	public CustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(
			CustomerService customerService) {
		this.customerService = customerService;
	}

	public PurchaseService getPurchaseService() {
		return purchaseService;
	}

	public void setPurchaseService(
			PurchaseService purchaseService) {
		this.purchaseService = purchaseService;
	}
}
