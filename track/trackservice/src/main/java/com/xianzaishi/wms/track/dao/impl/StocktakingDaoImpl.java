package com.xianzaishi.wms.track.dao.impl;

import org.apache.log4j.Logger;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.IStocktakingDao;
import com.xianzaishi.wms.track.vo.StocktakingVO;

public class StocktakingDaoImpl extends BaseDaoAdapter implements
		IStocktakingDao {
	private static final Logger logger = Logger
			.getLogger(StocktakingDaoImpl.class);

	public String getVOClassName() {
		return "StocktakingDO";
	}

	public Boolean updateStocktakingStatu(StocktakingVO stocktakingVO) {
		Boolean flag = false;
		try {
			if (stocktakingVO.getId() != null && stocktakingVO.getId() > 0) {
				int no = simpleSqlMapClientTemplate.update(getVOClassName()
						+ ".updateStocktakingStatu", stocktakingVO);
				if (no == 1) {
					flag = true;
				} else if (no > 1) {
					throw new BizException("影响的数据大于1条。");
				} else {
					throw new BizException("数据不存在。");
				}
			} else {
				throw new BizException("更新数据失败，ID错误：ID = "
						+ stocktakingVO.getId());
			}
		} catch (Exception e) {
			logger.error("更新数据失败。", e);
			throw new BizException("更新数据失败。", e);
		}
		return flag;
	}

	public Boolean submit(Long id) {
		Boolean flag = false;
		try {
			if (id != null && id > 0) {
				int no = simpleSqlMapClientTemplate.update(getVOClassName()
						+ ".submit", id);
				if (no == 1) {
					flag = true;
				} else if (no > 1) {
					throw new BizException("影响的数据大于1条。");
				} else {
					throw new BizException("请检查单据状态。");
				}
			} else {
				throw new BizException("更新数据失败，ID错误：ID = " + id);
			}
		} catch (Exception e) {
			throw new BizException("更新数据失败。", e);
		}
		return flag;
	}

	public Boolean audit(Long id, Long auditor) {
		Boolean flag = false;
		try {
			if (id != null && id > 0 && auditor != null && auditor > 0) {
				StocktakingVO stocktakingVO = new StocktakingVO();
				stocktakingVO.setId(id);
				stocktakingVO.setAuditor(auditor);
				int no = simpleSqlMapClientTemplate.update(getVOClassName()
						+ ".audit", stocktakingVO);
				if (no == 1) {
					flag = true;
				} else if (no > 1) {
					throw new BizException("影响的数据大于1条。");
				} else {
					throw new BizException("请检查单据状态。");
				}
			} else {
				throw new BizException("更新数据失败，ID错误：ID = " + id);
			}
		} catch (Exception e) {
			throw new BizException("更新数据失败。", e);
		}
		return flag;
	}

	public Boolean accounted(Long id) {
		Boolean flag = false;
		try {
			if (id != null && id > 0) {
				int no = simpleSqlMapClientTemplate.update(getVOClassName()
						+ ".accounted", id);
				if (no == 1) {
					flag = true;
				} else if (no > 1) {
					throw new BizException("影响的数据大于1条。");
				} else {
					throw new BizException("请检查单据状态。");
				}
			} else {
				throw new BizException("更新数据失败，ID错误：ID = " + id);
			}
		} catch (Exception e) {
			throw new BizException("更新数据失败。", e);
		}
		return flag;
	}
}
