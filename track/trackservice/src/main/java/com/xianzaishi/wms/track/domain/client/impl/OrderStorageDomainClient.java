package com.xianzaishi.wms.track.domain.client.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.domain.client.itf.IOrderStorageDomainClient;
import com.xianzaishi.wms.track.domain.service.itf.IOrderStorageDomainService;
import com.xianzaishi.wms.track.vo.OrderStorageDetailQueryVO;
import com.xianzaishi.wms.track.vo.OrderStorageDetailVO;
import com.xianzaishi.wms.track.vo.OrderStorageQueryVO;
import com.xianzaishi.wms.track.vo.OrderStorageVO;

public class OrderStorageDomainClient implements IOrderStorageDomainClient {
	private static final Logger logger = Logger
			.getLogger(OrderStorageDomainClient.class);
	@Autowired
	private IOrderStorageDomainService orderStorageDomainService = null;

	public SimpleResultVO<Boolean> createOrderStorageDomain(
			OrderStorageVO orderStorageVO) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(orderStorageDomainService
					.createOrderStorageDomain(orderStorageVO));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<OrderStorageVO> getOrderStorageDomainByID(
			Long orderStorageID) {
		SimpleResultVO<OrderStorageVO> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(orderStorageDomainService
					.getOrderStorageDomainByID(orderStorageID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> deleteOrderStorageDomain(Long orderStorageID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(orderStorageDomainService
					.deleteOrderStorageDomain(orderStorageID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> addOrderStorageVO(
			OrderStorageVO orderStorageVO) {
		return SimpleResultVO.buildSuccessResult(orderStorageDomainService
				.addOrderStorageVO(orderStorageVO));
	}

	public SimpleResultVO<QueryResultVO<OrderStorageVO>> queryOrderStorageVOList(
			OrderStorageQueryVO orderStorageQueryVO) {
		return SimpleResultVO.buildSuccessResult(orderStorageDomainService
				.queryOrderStorageVOList(orderStorageQueryVO));
	}

	public SimpleResultVO<OrderStorageVO> getOrderStorageVOByID(Long id) {
		return SimpleResultVO.buildSuccessResult(orderStorageDomainService
				.getOrderStorageVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyOrderStorageVO(
			OrderStorageVO orderStorageVO) {
		return SimpleResultVO.buildSuccessResult(orderStorageDomainService
				.modifyOrderStorageVO(orderStorageVO));
	}

	public SimpleResultVO<Boolean> deleteOrderStorageVO(Long id) {
		return SimpleResultVO.buildSuccessResult(orderStorageDomainService
				.deleteOrderStorageVO(id));
	}

	public SimpleResultVO<Boolean> createOrderStorageDetailVO(
			OrderStorageDetailVO orderStorageDetailVO) {
		return SimpleResultVO.buildSuccessResult(orderStorageDomainService
				.createOrderStorageDetailVO(orderStorageDetailVO));
	}

	public SimpleResultVO<Boolean> batchCreateOrderStorageDetailVO(
			List<OrderStorageDetailVO> orderStorageDetailVOs) {
		return SimpleResultVO.buildSuccessResult(orderStorageDomainService
				.batchCreateOrderStorageDetailVO(orderStorageDetailVOs));
	}

	public SimpleResultVO<List<OrderStorageDetailVO>> queryOrderStorageDetailVOList(
			OrderStorageDetailQueryVO orderStorageDetailQueryVO) {
		return SimpleResultVO.buildSuccessResult(orderStorageDomainService
				.queryOrderStorageDetailVOList(orderStorageDetailQueryVO));
	}

	public SimpleResultVO<List<OrderStorageDetailVO>> getOrderStorageDetailVOListByOrderStorageID(
			Long orderStorageID) {
		return SimpleResultVO.buildSuccessResult(orderStorageDomainService
				.getOrderStorageDetailVOListByOrderStorageID(orderStorageID));
	}

	public SimpleResultVO<OrderStorageDetailQueryVO> getOrderStorageDetailVOByID(
			Long id) {
		return SimpleResultVO.buildSuccessResult(orderStorageDomainService
				.getOrderStorageDetailVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyOrderStorageDetailVO(
			OrderStorageDetailVO orderStorageDetailVO) {
		return SimpleResultVO.buildSuccessResult(orderStorageDomainService
				.modifyOrderStorageDetailVO(orderStorageDetailVO));
	}

	public SimpleResultVO<Boolean> batchModifyOrderStorageDetailVOs(
			List<OrderStorageDetailVO> orderStorageDetailVOs) {
		return SimpleResultVO.buildSuccessResult(orderStorageDomainService
				.batchModifyOrderStorageDetailVOs(orderStorageDetailVOs));
	}

	public SimpleResultVO<Boolean> deleteOrderStorageDetailVO(Long id) {
		return SimpleResultVO.buildSuccessResult(orderStorageDomainService
				.deleteOrderStorageDetailVO(id));
	}

	public SimpleResultVO<Boolean> deleteOrderStorageDetailVOByOrderStorageID(
			Long orderStorageID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO
					.buildSuccessResult(orderStorageDomainService
							.deleteOrderStorageDetailVOByOrderStorageID(orderStorageID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> batchDeleteOrderStorageDetailVOByOrderStorageID(
			List<Long> storyDetailIDs) {
		return SimpleResultVO
				.buildSuccessResult(orderStorageDomainService
						.batchDeleteOrderStorageDetailVOByOrderStorageID(storyDetailIDs));
	}

	public IOrderStorageDomainService getOrderStorageDomainService() {
		return orderStorageDomainService;
	}

	public void setOrderStorageDomainService(
			IOrderStorageDomainService orderStorageDomainService) {
		this.orderStorageDomainService = orderStorageDomainService;
	}

	public SimpleResultVO<Boolean> submit(Long id) {
		return SimpleResultVO.buildSuccessResult(orderStorageDomainService
				.submit(id));
	}

	public SimpleResultVO<Boolean> audit(Long id, Long auditor) {
		return SimpleResultVO.buildSuccessResult(orderStorageDomainService
				.audit(id, auditor));
	}

	public SimpleResultVO<Boolean> accounted(Long id) {
		return SimpleResultVO.buildSuccessResult(orderStorageDomainService
				.accounted(id));
	}
}
