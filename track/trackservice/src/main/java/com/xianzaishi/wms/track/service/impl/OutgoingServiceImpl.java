package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.vo.OutgoingVO;
import com.xianzaishi.wms.track.vo.OutgoingQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.service.itf.IOutgoingService;
import com.xianzaishi.wms.track.manage.itf.IOutgoingManage;

public class OutgoingServiceImpl implements IOutgoingService {
	@Autowired
	private IOutgoingManage outgoingManage = null;

	public IOutgoingManage getOutgoingManage() {
		return outgoingManage;
	}

	public void setOutgoingManage(IOutgoingManage outgoingManage) {
		this.outgoingManage = outgoingManage;
	}

	public Long addOutgoingVO(OutgoingVO outgoingVO) {
		return outgoingManage.addOutgoingVO(outgoingVO);
	}

	public List<OutgoingVO> queryOutgoingVOList(OutgoingQueryVO outgoingQueryVO) {
		return outgoingManage.queryOutgoingVOList(outgoingQueryVO);
	}

	public Integer queryOutgoingVOCount(OutgoingQueryVO outgoingQueryVO) {
		return outgoingManage.queryOutgoingVOCount(outgoingQueryVO);
	}

	public OutgoingVO getOutgoingVOByID(Long id) {
		return (OutgoingVO) outgoingManage.getOutgoingVOByID(id);
	}

	public Boolean modifyOutgoingVO(OutgoingVO outgoingVO) {
		return outgoingManage.modifyOutgoingVO(outgoingVO);
	}

	public Boolean deleteOutgoingVOByID(Long id) {
		return outgoingManage.deleteOutgoingVOByID(id);
	}

	public Boolean submit(Long id) {
		return outgoingManage.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return outgoingManage.audit(id, auditor);
	}

	public Boolean accounted(Long id) {
		return outgoingManage.accounted(id);
	}
}
