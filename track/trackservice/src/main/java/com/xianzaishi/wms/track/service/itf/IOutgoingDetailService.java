package com.xianzaishi.wms.track.service.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.OutgoingDetailQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingDetailVO;

public interface IOutgoingDetailService {

	public Boolean addOutgoingDetailVO(OutgoingDetailVO outgoingDetailVO);

	public List<OutgoingDetailVO> queryOutgoingDetailVOList(
			OutgoingDetailQueryVO outgoingDetailQueryVO);

	public OutgoingDetailVO getOutgoingDetailVOByID(Long id);

	public Boolean modifyOutgoingDetailVO(OutgoingDetailVO outgoingDetailVO);

	public Boolean deleteOutgoingDetailVOByID(Long id);

	public List<OutgoingDetailVO> getOutgoingDetailVOByOutgoingID(Long id);

	public Boolean batchAddOutgoingDetailVO(
			List<OutgoingDetailVO> outgoingDetailVOs);

	public Boolean batchModifyOutgoingDetailVO(
			List<OutgoingDetailVO> outgoingDetailVOs);

	public Boolean batchDeleteOutgoingDetailVO(
			List<OutgoingDetailVO> outgoingDetailVOs);

	public Boolean batchDeleteOutgoingDetailVOByID(List<Long> storyDetailIDs);

}