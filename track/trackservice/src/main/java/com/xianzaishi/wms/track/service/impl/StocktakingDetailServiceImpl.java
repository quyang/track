package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.manage.itf.IStocktakingDetailManage;
import com.xianzaishi.wms.track.service.itf.IStocktakingDetailService;
import com.xianzaishi.wms.track.vo.StocktakingDetailQueryVO;
import com.xianzaishi.wms.track.vo.StocktakingDetailVO;

public class StocktakingDetailServiceImpl implements IStocktakingDetailService {
	@Autowired
	private IStocktakingDetailManage stocktakingDetailManage = null;

	public IStocktakingDetailManage getStocktakingDetailManage() {
		return stocktakingDetailManage;
	}

	public void setStocktakingDetailManage(
			IStocktakingDetailManage stocktakingDetailManage) {
		this.stocktakingDetailManage = stocktakingDetailManage;
	}

	public Boolean addStocktakingDetailVO(
			StocktakingDetailVO stocktakingDetailVO) {
		stocktakingDetailManage.addStocktakingDetailVO(stocktakingDetailVO);
		return true;
	}

	public List<StocktakingDetailVO> queryStocktakingDetailVOList(
			StocktakingDetailQueryVO stocktakingDetailQueryVO) {
		return stocktakingDetailManage
				.queryStocktakingDetailVOList(stocktakingDetailQueryVO);
	}

	public StocktakingDetailVO getStocktakingDetailVOByID(Long id) {
		return (StocktakingDetailVO) stocktakingDetailManage
				.getStocktakingDetailVOByID(id);
	}

	public Boolean modifyStocktakingDetailVO(
			StocktakingDetailVO stocktakingDetailVO) {
		return stocktakingDetailManage
				.modifyStocktakingDetailVO(stocktakingDetailVO);
	}

	public Boolean deleteStocktakingDetailVOByID(Long id) {
		return stocktakingDetailManage.deleteStocktakingDetailVOByID(id);
	}

	public List<StocktakingDetailVO> getStocktakingDetailVOByStocktakingID(Long id) {
		return stocktakingDetailManage.getStocktakingDetailVOByStocktakingID(id);
	}

	public Boolean batchAddStocktakingDetailVO(
			List<StocktakingDetailVO> stocktakingDetailVOs) {
		return stocktakingDetailManage.batchAddStocktakingDetailVO(stocktakingDetailVOs);
	}

	public Boolean batchModifyStocktakingDetailVO(
			List<StocktakingDetailVO> stocktakingDetailVOs) {
		return stocktakingDetailManage.batchModifyStocktakingDetailVO(stocktakingDetailVOs);
	}

	public Boolean batchDeleteStocktakingDetailVO(
			List<StocktakingDetailVO> stocktakingDetailVOs) {
		return stocktakingDetailManage.batchDeleteStocktakingDetailVO(stocktakingDetailVOs);
	}

	public Boolean batchDeleteStocktakingDetailVOByID(List<Long> storyDetailIDs) {
		return stocktakingDetailManage
				.batchDeleteStocktakingDetailVOByID(storyDetailIDs);
	}
}
