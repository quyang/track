package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.IInspectionDetailDao;
import com.xianzaishi.wms.track.manage.itf.IInspectionDetailManage;
import com.xianzaishi.wms.track.vo.InspectionDetailQueryVO;
import com.xianzaishi.wms.track.vo.InspectionDetailVO;

public class InspectionDetailManageImpl implements IInspectionDetailManage {
	@Autowired
	private IInspectionDetailDao inspectionDetailDao = null;

	private void validate(InspectionDetailVO inspectionDetailVO) {
		if (inspectionDetailVO.getInspectionId() == null
				|| inspectionDetailVO.getInspectionId() <= 0) {
			throw new BizException("inspectionID error："
					+ inspectionDetailVO.getInspectionId());
		}
		if (inspectionDetailVO.getSkuId() == null
				|| inspectionDetailVO.getSkuId() <= 0) {
			throw new BizException("skuID error："
					+ inspectionDetailVO.getSkuId());
		}
		if (inspectionDetailVO.getBookingNoReal() == null
				|| inspectionDetailVO.getBookingNoReal() <= 0) {
			throw new BizException("booking number error："
					+ inspectionDetailVO.getBookingNo());
		}
		if (inspectionDetailVO.getDelivedNoReal() == null
				|| inspectionDetailVO.getDelivedNoReal() <= 0) {
			throw new BizException("delivery number error："
					+ inspectionDetailVO.getDelivedNo());
		}
		if (inspectionDetailVO.getQualifiedNoReal() == null
				|| inspectionDetailVO.getQualifiedNoReal() <= 0) {
			throw new BizException("qualified number error："
					+ inspectionDetailVO.getQualifiedNo());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IInspectionDetailDao getInspectionDetailDao() {
		return inspectionDetailDao;
	}

	public void setInspectionDetailDao(IInspectionDetailDao inspectionDetailDao) {
		this.inspectionDetailDao = inspectionDetailDao;
	}

	public Boolean addInspectionDetailVO(InspectionDetailVO inspectionDetailVO) {
		validate(inspectionDetailVO);
		inspectionDetailDao.addDO(inspectionDetailVO);
		return true;
	}

	public List<InspectionDetailVO> queryInspectionDetailVOList(
			InspectionDetailQueryVO inspectionDetailQueryVO) {
		return inspectionDetailDao.queryDO(inspectionDetailQueryVO);
	}

	public InspectionDetailVO getInspectionDetailVOByID(Long id) {
		return (InspectionDetailVO) inspectionDetailDao.getDOByID(id);
	}

	public Boolean modifyInspectionDetailVO(
			InspectionDetailVO inspectionDetailVO) {
		return inspectionDetailDao.updateDO(inspectionDetailVO);
	}

	public Boolean deleteInspectionDetailVOByID(Long id) {
		return inspectionDetailDao.delDO(id);
	}

	public List<InspectionDetailVO> getInspectionDetailVOByInspectionID(Long id) {
		InspectionDetailQueryVO queryVO = new InspectionDetailQueryVO();
		queryVO.setInspectionId(id);
		queryVO.setSize(Integer.MAX_VALUE);
		return inspectionDetailDao.queryDO(queryVO);
	}

	public Boolean batchAddInspectionDetailVO(
			List<InspectionDetailVO> inspectionDetailVOs) {
		return inspectionDetailDao.batchAddDO(inspectionDetailVOs);
	}

	public Boolean batchModifyInspectionDetailVO(
			List<InspectionDetailVO> inspectionDetailVOs) {
		return inspectionDetailDao.batchModifyDO(inspectionDetailVOs);
	}

	public Boolean batchDeleteInspectionDetailVO(
			List<InspectionDetailVO> inspectionDetailVOs) {
		return inspectionDetailDao.batchDeleteDO(inspectionDetailVOs);
	}

	public Boolean batchDeleteInspectionDetailVOByID(
			List<Long> inspectionDetailIDs) {
		return inspectionDetailDao.batchDeleteDOByID(inspectionDetailIDs);
	}

}
