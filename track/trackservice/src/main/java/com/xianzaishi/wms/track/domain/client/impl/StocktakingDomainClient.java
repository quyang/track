package com.xianzaishi.wms.track.domain.client.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.domain.client.itf.IStocktakingDomainClient;
import com.xianzaishi.wms.track.domain.service.itf.IStocktakingDomainService;
import com.xianzaishi.wms.track.vo.StocktakingDetailQueryVO;
import com.xianzaishi.wms.track.vo.StocktakingDetailVO;
import com.xianzaishi.wms.track.vo.StocktakingQueryVO;
import com.xianzaishi.wms.track.vo.StocktakingVO;

public class StocktakingDomainClient implements IStocktakingDomainClient {
	private static final Logger logger = Logger
			.getLogger(StocktakingDomainClient.class);
	@Autowired
	private IStocktakingDomainService stocktakingDomainService = null;

	public SimpleResultVO<Boolean> createStocktakingDomain(
			StocktakingVO stocktakingVO) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(stocktakingDomainService
					.createStocktakingDomain(stocktakingVO));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<StocktakingVO> getStocktakingDomainByID(
			Long stocktakingID) {
		SimpleResultVO<StocktakingVO> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(stocktakingDomainService
					.getStocktakingDomainByID(stocktakingID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> deleteStocktakingDomain(Long stocktakingID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(stocktakingDomainService
					.deleteStocktakingDomain(stocktakingID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> addStocktakingVO(StocktakingVO stocktakingVO) {
		return SimpleResultVO.buildSuccessResult(stocktakingDomainService
				.addStocktakingVO(stocktakingVO));
	}

	public SimpleResultVO<QueryResultVO<StocktakingVO>> queryStocktakingVOList(
			StocktakingQueryVO stocktakingQueryVO) {
		return SimpleResultVO.buildSuccessResult(stocktakingDomainService
				.queryStocktakingVOList(stocktakingQueryVO));
	}

	public SimpleResultVO<StocktakingVO> getStocktakingVOByID(Long id) {
		return SimpleResultVO.buildSuccessResult(stocktakingDomainService
				.getStocktakingVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyStocktakingVO(
			StocktakingVO stocktakingVO) {
		return SimpleResultVO.buildSuccessResult(stocktakingDomainService
				.modifyStocktakingVO(stocktakingVO));
	}

	public SimpleResultVO<Boolean> deleteStocktakingVO(Long id) {
		return SimpleResultVO.buildSuccessResult(stocktakingDomainService
				.deleteStocktakingVO(id));
	}

	public SimpleResultVO<Boolean> submitStocktakingToCheck(
			StocktakingVO stocktakingVO) {
		return SimpleResultVO.buildSuccessResult(stocktakingDomainService
				.submitStocktakingToCheck(stocktakingVO));
	}

	public SimpleResultVO<Boolean> submitStocktakingToAudit(
			StocktakingVO stocktakingVO) {
		return SimpleResultVO.buildSuccessResult(stocktakingDomainService
				.submitStocktakingToAudit(stocktakingVO));
	}

	public SimpleResultVO<Boolean> createStocktakingDetailVO(
			StocktakingDetailVO stocktakingDetailVO) {
		return SimpleResultVO.buildSuccessResult(stocktakingDomainService
				.createStocktakingDetailVO(stocktakingDetailVO));
	}

	public SimpleResultVO<Boolean> batchCreateStocktakingDetailVO(
			List<StocktakingDetailVO> stocktakingDetailVOs) {
		return SimpleResultVO.buildSuccessResult(stocktakingDomainService
				.batchCreateStocktakingDetailVO(stocktakingDetailVOs));
	}

	public SimpleResultVO<List<StocktakingDetailVO>> queryStocktakingDetailVOList(
			StocktakingDetailQueryVO stocktakingDetailQueryVO) {
		return SimpleResultVO.buildSuccessResult(stocktakingDomainService
				.queryStocktakingDetailVOList(stocktakingDetailQueryVO));
	}

	public SimpleResultVO<List<StocktakingDetailVO>> getStocktakingDetailVOListByStocktakingID(
			Long stocktakingID) {
		return SimpleResultVO.buildSuccessResult(stocktakingDomainService
				.getStocktakingDetailVOListByStocktakingID(stocktakingID));
	}

	public SimpleResultVO<StocktakingDetailQueryVO> getStocktakingDetailVOByID(
			Long id) {
		return SimpleResultVO.buildSuccessResult(stocktakingDomainService
				.getStocktakingDetailVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyStocktakingDetailVO(
			StocktakingDetailVO stocktakingDetailVO) {
		return SimpleResultVO.buildSuccessResult(stocktakingDomainService
				.modifyStocktakingDetailVO(stocktakingDetailVO));
	}

	public SimpleResultVO<Boolean> batchModifyStocktakingDetailVOs(
			List<StocktakingDetailVO> stocktakingDetailVOs) {
		return SimpleResultVO.buildSuccessResult(stocktakingDomainService
				.batchModifyStocktakingDetailVOs(stocktakingDetailVOs));
	}

	public SimpleResultVO<Boolean> deleteStocktakingDetailVO(Long id) {
		return SimpleResultVO.buildSuccessResult(stocktakingDomainService
				.deleteStocktakingDetailVO(id));
	}

	public SimpleResultVO<Boolean> deleteStocktakingDetailVOByStocktakingID(
			Long stocktakingID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(stocktakingDomainService
					.deleteStocktakingDetailVOByStocktakingID(stocktakingID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> batchDeleteStocktakingDetailVOByStocktakingID(
			List<Long> storyDetailIDs) {
		return SimpleResultVO.buildSuccessResult(stocktakingDomainService
				.batchDeleteStocktakingDetailVOByStocktakingID(storyDetailIDs));
	}

	public IStocktakingDomainService getStocktakingDomainService() {
		return stocktakingDomainService;
	}

	public void setStocktakingDomainService(
			IStocktakingDomainService stocktakingDomainService) {
		this.stocktakingDomainService = stocktakingDomainService;
	}

	public SimpleResultVO<Boolean> submit(Long id) {
		return SimpleResultVO.buildSuccessResult(stocktakingDomainService
				.submit(id));
	}

	public SimpleResultVO<Boolean> audit(Long id, Long auditor) {
		return SimpleResultVO.buildSuccessResult(stocktakingDomainService
				.audit(id, auditor));
	}

	public SimpleResultVO<Boolean> accounted(Long id) {
		return SimpleResultVO.buildSuccessResult(stocktakingDomainService
				.accounted(id));
	}
}
