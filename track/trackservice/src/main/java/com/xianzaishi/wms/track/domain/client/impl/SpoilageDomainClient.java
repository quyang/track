package com.xianzaishi.wms.track.domain.client.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.domain.client.itf.ISpoilageDomainClient;
import com.xianzaishi.wms.track.domain.service.itf.ISpoilageDomainService;
import com.xianzaishi.wms.track.vo.SpoilageDetailQueryVO;
import com.xianzaishi.wms.track.vo.SpoilageDetailVO;
import com.xianzaishi.wms.track.vo.SpoilageQueryVO;
import com.xianzaishi.wms.track.vo.SpoilageVO;

public class SpoilageDomainClient implements ISpoilageDomainClient {
	private static final Logger logger = Logger
			.getLogger(SpoilageDomainClient.class);
	@Autowired
	private ISpoilageDomainService spoilageDomainService = null;

	public SimpleResultVO<Boolean> createSpoilageDomain(SpoilageVO spoilageVO) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(spoilageDomainService
					.createSpoilageDomain(spoilageVO));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<SpoilageVO> getSpoilageDomainByID(Long spoilageID) {
		SimpleResultVO<SpoilageVO> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(spoilageDomainService
					.getSpoilageDomainByID(spoilageID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> deleteSpoilageDomain(Long spoilageID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(spoilageDomainService
					.deleteSpoilageDomain(spoilageID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> addSpoilageVO(SpoilageVO spoilageVO) {
		return SimpleResultVO.buildSuccessResult(spoilageDomainService
				.addSpoilageVO(spoilageVO));
	}

	public SimpleResultVO<QueryResultVO<SpoilageVO>> querySpoilageVOList(
			SpoilageQueryVO spoilageQueryVO) {
		return SimpleResultVO.buildSuccessResult(spoilageDomainService
				.querySpoilageVOList(spoilageQueryVO));
	}

	public SimpleResultVO<SpoilageVO> getSpoilageVOByID(Long id) {
		return SimpleResultVO.buildSuccessResult(spoilageDomainService
				.getSpoilageVOByID(id));
	}

	public SimpleResultVO<Boolean> modifySpoilageVO(SpoilageVO spoilageVO) {
		return SimpleResultVO.buildSuccessResult(spoilageDomainService
				.modifySpoilageVO(spoilageVO));
	}

	public SimpleResultVO<Boolean> deleteSpoilageVO(Long id) {
		return SimpleResultVO.buildSuccessResult(spoilageDomainService
				.deleteSpoilageVO(id));
	}

	public SimpleResultVO<Boolean> createSpoilageDetailVO(
			SpoilageDetailVO spoilageDetailVO) {
		return SimpleResultVO.buildSuccessResult(spoilageDomainService
				.createSpoilageDetailVO(spoilageDetailVO));
	}

	public SimpleResultVO<Boolean> batchCreateSpoilageDetailVO(
			List<SpoilageDetailVO> spoilageDetailVOs) {
		return SimpleResultVO.buildSuccessResult(spoilageDomainService
				.batchCreateSpoilageDetailVO(spoilageDetailVOs));
	}

	public SimpleResultVO<List<SpoilageDetailVO>> querySpoilageDetailVOList(
			SpoilageDetailQueryVO spoilageDetailQueryVO) {
		return SimpleResultVO.buildSuccessResult(spoilageDomainService
				.querySpoilageDetailVOList(spoilageDetailQueryVO));
	}

	public SimpleResultVO<List<SpoilageDetailVO>> getSpoilageDetailVOListBySpoilageID(
			Long spoilageID) {
		return SimpleResultVO.buildSuccessResult(spoilageDomainService
				.getSpoilageDetailVOListBySpoilageID(spoilageID));
	}

	public SimpleResultVO<SpoilageDetailQueryVO> getSpoilageDetailVOByID(Long id) {
		return SimpleResultVO.buildSuccessResult(spoilageDomainService
				.getSpoilageDetailVOByID(id));
	}

	public SimpleResultVO<Boolean> modifySpoilageDetailVO(
			SpoilageDetailVO spoilageDetailVO) {
		return SimpleResultVO.buildSuccessResult(spoilageDomainService
				.modifySpoilageDetailVO(spoilageDetailVO));
	}

	public SimpleResultVO<Boolean> batchModifySpoilageDetailVOs(
			List<SpoilageDetailVO> spoilageDetailVOs) {
		return SimpleResultVO.buildSuccessResult(spoilageDomainService
				.batchModifySpoilageDetailVOs(spoilageDetailVOs));
	}

	public SimpleResultVO<Boolean> deleteSpoilageDetailVO(Long id) {
		return SimpleResultVO.buildSuccessResult(spoilageDomainService
				.deleteSpoilageDetailVO(id));
	}

	public SimpleResultVO<Boolean> deleteSpoilageDetailVOBySpoilageID(
			Long spoilageID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(spoilageDomainService
					.deleteSpoilageDetailVOBySpoilageID(spoilageID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> batchDeleteSpoilageDetailVOBySpoilageID(
			List<Long> storyDetailIDs) {
		return SimpleResultVO.buildSuccessResult(spoilageDomainService
				.batchDeleteSpoilageDetailVOBySpoilageID(storyDetailIDs));
	}

	public ISpoilageDomainService getSpoilageDomainService() {
		return spoilageDomainService;
	}

	public void setSpoilageDomainService(
			ISpoilageDomainService spoilageDomainService) {
		this.spoilageDomainService = spoilageDomainService;
	}

	public SimpleResultVO<Boolean> submit(Long id) {
		return SimpleResultVO.buildSuccessResult(spoilageDomainService
				.submit(id));
	}

	public SimpleResultVO<Boolean> audit(Long id, Long auditor) {
		return SimpleResultVO.buildSuccessResult(spoilageDomainService.audit(
				id, auditor));
	}

	public SimpleResultVO<Boolean> accounted(Long id) {
		return SimpleResultVO.buildSuccessResult(spoilageDomainService
				.accounted(id));
	}
}
