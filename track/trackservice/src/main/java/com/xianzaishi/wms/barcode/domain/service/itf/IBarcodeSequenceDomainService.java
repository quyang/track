package com.xianzaishi.wms.barcode.domain.service.itf;

import java.util.List;

public interface IBarcodeSequenceDomainService {
	public String getBarcode(Integer type);

	public List<String> getBarcodes(Integer type, Integer no);
}
