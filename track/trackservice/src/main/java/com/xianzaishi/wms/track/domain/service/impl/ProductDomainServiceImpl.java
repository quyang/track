package com.xianzaishi.wms.track.domain.service.impl;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryManageDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IPositionDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryManageDetailVO;
import com.xianzaishi.wms.hive.vo.InventoryManageVO;
import com.xianzaishi.wms.hive.vo.PositionVO;
import com.xianzaishi.wms.track.constants.TrackConstants;
import com.xianzaishi.wms.track.domain.service.itf.IProductDomainService;
import com.xianzaishi.wms.track.service.itf.IProductDetailService;
import com.xianzaishi.wms.track.service.itf.IProductService;
import com.xianzaishi.wms.track.vo.ProductDetailQueryVO;
import com.xianzaishi.wms.track.vo.ProductDetailVO;
import com.xianzaishi.wms.track.vo.ProductQueryVO;
import com.xianzaishi.wms.track.vo.ProductVO;

public class ProductDomainServiceImpl implements IProductDomainService {
	@Autowired
	private IProductService productService = null;
	@Autowired
	private IProductDetailService productDetailService = null;
	@Autowired
	private IInventoryManageDomainClient inventoryManageDomainClient = null;
	@Autowired
	private SkuService skuService = null;
	@Autowired
	private ValueService valueService = null;
	@Autowired
	private IPositionDomainClient positionDomainClient = null;

	public Boolean createProductDomain(ProductVO productVO) {
		if (productVO.getDetails() == null || productVO.getDetails().isEmpty()) {
			throw new BizException("请填充入库明细！");
		}
		productVO.setStatu(0);
		Long productID = productService.addProductVO(productVO);

		initDetails(productID, productVO.getDetails());

		productDetailService.batchAddProductDetailVO(productVO.getDetails());

		return true;
	}

	public ProductVO getProductDomainByID(Long productID) {
		if (productID == null || productID <= 0) {
			throw new BizException("id error");
		}

		ProductVO productVO = productService.getProductVOByID(productID);

		productVO.setDetails(productDetailService
				.getProductDetailVOByProductID(productID));

		return productVO;
	}

	public Boolean deleteProductDomain(Long productID) {
		if (productID == null || productID <= 0) {
			throw new BizException("id error");
		}

		List<ProductDetailVO> productDetailVOs = productDetailService
				.getProductDetailVOByProductID(productID);

		productDetailService.batchDeleteProductDetailVO(productDetailVOs);

		productService.deleteProductVOByID(productID);

		return true;
	}

	public Boolean addProductVO(ProductVO productVO) {
		productService.addProductVO(productVO);
		return true;
	}

	public QueryResultVO<ProductVO> queryProductVOList(
			ProductQueryVO productQueryVO) {
		QueryResultVO<ProductVO> queryResultVO = new QueryResultVO<>();
		queryResultVO.setPage(productQueryVO.getPage());
		queryResultVO.setSize(productQueryVO.getSize());
		queryResultVO.setItems(productService
				.queryProductVOList(productQueryVO));
		queryResultVO.setTotalCount(productService
				.queryProductVOCount(productQueryVO));
		return queryResultVO;
	}

	public ProductVO getProductVOByID(Long id) {
		return productService.getProductVOByID(id);
	}

	public Boolean modifyProductVO(ProductVO productVO) {
		return productService.modifyProductVO(productVO);
	}

	public Boolean deleteProductVO(Long id) {
		return productService.deleteProductVOByID(id);
	}

	public Boolean createProductDetailVO(ProductDetailVO productDetailVO) {
		initInfo(productDetailVO);
		return productDetailService.addProductDetailVO(productDetailVO);
	}

	public Boolean batchCreateProductDetailVO(
			List<ProductDetailVO> productDetailVOs) {
		for (ProductDetailVO productDetailVO : productDetailVOs) {
			initInfo(productDetailVO);
		}
		return productDetailService.batchAddProductDetailVO(productDetailVOs);
	}

	public List<ProductDetailVO> queryProductDetailVOList(
			ProductDetailQueryVO productDetailQueryVO) {
		return productDetailService
				.queryProductDetailVOList(productDetailQueryVO);
	}

	public List<ProductDetailVO> getProductDetailVOListByProductID(
			Long productID) {
		return productDetailService.getProductDetailVOByProductID(productID);
	}

	public ProductDetailVO getProductDetailVOByID(Long id) {
		return productDetailService.getProductDetailVOByID(id);
	}

	public Boolean modifyProductDetailVO(ProductDetailVO productDetailVO) {
		initInfo(productDetailVO);
		return productDetailService.modifyProductDetailVO(productDetailVO);
	}

	public Boolean batchModifyProductDetailVOs(
			List<ProductDetailVO> productDetailVOs) {
		for (ProductDetailVO productDetailVO : productDetailVOs) {
			initInfo(productDetailVO);
		}
		return productDetailService
				.batchModifyProductDetailVO(productDetailVOs);
	}

	public Boolean deleteProductDetailVO(Long id) {
		return productDetailService.deleteProductDetailVOByID(id);
	}

	public Boolean deleteProductDetailVOByProductID(Long productID) {
		if (productID == null || productID <= 0) {
			throw new BizException("id error");
		}

		List<ProductDetailVO> productDetailVOs = productDetailService
				.getProductDetailVOByProductID(productID);

		productDetailService.batchDeleteProductDetailVO(productDetailVOs);

		return true;
	}

	public Boolean batchDeleteProductDetailVOByProductID(
			List<Long> storyDetailIDs) {
		return productDetailService
				.batchDeleteProductDetailVOByID(storyDetailIDs);
	}

	private void initDetails(Long id, List<ProductDetailVO> productDetailVOs) {
		for (int i = 0; i < productDetailVOs.size(); i++) {
			productDetailVOs.get(i).setProductId(id);
			initInfo(productDetailVOs.get(i));
		}
	}

	private void initInfo(ProductDetailVO productDetailVO) {
		ItemCommoditySkuDTO itemCommoditySkuDTO = skuService.queryItemSku(
				SkuQuery.querySkuById(productDetailVO.getSkuId())).getModule();
		if (itemCommoditySkuDTO == null) {
			throw new BizException("skuID error:" + productDetailVO.getSkuId());
		}
		productDetailVO.setSkuName(itemCommoditySkuDTO.getTitle());
		productDetailVO.setSaleUnit(valueService.queryValueNameByValueId(
				itemCommoditySkuDTO.getSkuUnit()).getModule());
		productDetailVO
				.setSaleUnitId(new Long(itemCommoditySkuDTO.getSkuUnit()));
		productDetailVO.setSaleUnitType(new Integer(itemCommoditySkuDTO
				.querySaleUnitType()));
		// productDetailVO.setSpec(itemCommoditySkuDTO.getSkuSpecificationNum());
		// productDetailVO.setSpecUnit(valueService.queryValueNameByValueId(
		// itemCommoditySkuDTO.getSkuSpecificationUnit()).getModule());
		// productDetailVO.setSpecUnitId(new Long(itemCommoditySkuDTO
		// .getSkuSpecificationUnit()));
		PositionVO positionVO = positionDomainClient.getPositionVOByID(
				productDetailVO.getPositionId()).getTarget();
		if (positionVO == null) {
			throw new BizException("positionID error:"
					+ productDetailVO.getPositionId());
		}
		productDetailVO.setPositionCode(positionVO.getCode());

		if (productDetailVO.getNumberReal() == null
				&& productDetailVO.getNumber() != null
				&& !productDetailVO.getNumber().isEmpty()) {
			productDetailVO.setNumberReal(new BigDecimal(productDetailVO
					.getNumber()).multiply(new BigDecimal(1000)).intValue());
		}
	}

	public IProductService getProductService() {
		return productService;
	}

	public void setProductService(IProductService productService) {
		this.productService = productService;
	}

	public IProductDetailService getProductDetailService() {
		return productDetailService;
	}

	public void setProductDetailService(
			IProductDetailService productDetailService) {
		this.productDetailService = productDetailService;
	}

	public Boolean submit(Long id) {
		return productService.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		Boolean flag = false;
		if (productService.audit(id, auditor)) {
			flag = accounted(id);
		}
		return flag;
	}

	public Boolean accounted(Long id) {
		Boolean flag = false;
		ProductVO productVO = getProductDomainByID(id);
		InventoryManageVO inventoryManageVO = initInventoryManage(productVO);
		SimpleResultVO<Boolean> result = inventoryManageDomainClient
				.createInventoryManageDomain(inventoryManageVO);
		if (result.isSuccess() && result.getTarget()) {
			flag = productService.accounted(id);
		}
		return flag;
	}

	private InventoryManageVO initInventoryManage(ProductVO productVO) {
		InventoryManageVO inventoryManageVO = new InventoryManageVO();
		inventoryManageVO.setAgencyId(productVO.getAgencyId());
		inventoryManageVO.setAuditor(productVO.getAuditor());
		inventoryManageVO.setAuditTime(productVO.getAuditTime());
		inventoryManageVO.setOperator(productVO.getOperate());
		inventoryManageVO.setOpReason(TrackConstants.IM_REASON_IN_PRODUCT);
		inventoryManageVO.setOpTime(productVO.getOpTime());
		inventoryManageVO.setOpType(2);
		List<InventoryManageDetailVO> details = new LinkedList<InventoryManageDetailVO>();
		for (int i = 0; i < productVO.getDetails().size(); i++) {
			InventoryManageDetailVO inventoryManageDetailVO = new InventoryManageDetailVO();
			inventoryManageDetailVO.setNumber(productVO.getDetails().get(i)
					.getNumberReal());
			inventoryManageDetailVO.setSaleUnit(productVO.getDetails().get(i)
					.getSaleUnit());
			inventoryManageDetailVO.setSaleUnitId(productVO.getDetails().get(i)
					.getSaleUnitId());
			inventoryManageDetailVO.setSpec(productVO.getDetails().get(i)
					.getSpec());
			inventoryManageDetailVO.setSpecUnit(productVO.getDetails().get(i)
					.getSpecUnit());
			inventoryManageDetailVO.setSpecUnitId(productVO.getDetails().get(i)
					.getSpecUnitId());
			inventoryManageDetailVO.setSaleUnitType(productVO.getDetails()
					.get(i).getSaleUnitType());
			inventoryManageDetailVO.setSkuName(productVO.getDetails().get(i)
					.getSkuName());
			inventoryManageDetailVO.setPositionCode(productVO.getDetails()
					.get(i).getPositionCode());
			inventoryManageDetailVO.setSkuId(productVO.getDetails().get(i)
					.getSkuId());
			inventoryManageDetailVO.setPositionId(productVO.getDetails().get(i)
					.getPositionId());
			details.add(inventoryManageDetailVO);
		}
		inventoryManageVO.setDetails(details);
		return inventoryManageVO;
	}

	public IInventoryManageDomainClient getInventoryManageDomainClient() {
		return inventoryManageDomainClient;
	}

	public void setInventoryManageDomainClient(
			IInventoryManageDomainClient inventoryManageDomainClient) {
		this.inventoryManageDomainClient = inventoryManageDomainClient;
	}

	public SkuService getSkuService() {
		return skuService;
	}

	public void setSkuService(SkuService skuService) {
		this.skuService = skuService;
	}

	public ValueService getValueService() {
		return valueService;
	}

	public void setValueService(ValueService valueService) {
		this.valueService = valueService;
	}

	public IPositionDomainClient getPositionDomainClient() {
		return positionDomainClient;
	}

	public void setPositionDomainClient(
			IPositionDomainClient positionDomainClient) {
		this.positionDomainClient = positionDomainClient;
	}
}
