package com.xianzaishi.wms.track.domain.service.impl;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryManageDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IPositionDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryManageDetailVO;
import com.xianzaishi.wms.hive.vo.InventoryManageVO;
import com.xianzaishi.wms.hive.vo.PositionVO;
import com.xianzaishi.wms.track.constants.TrackConstants;
import com.xianzaishi.wms.track.domain.service.itf.IRequisitionsDomainService;
import com.xianzaishi.wms.track.service.itf.IRequisitionsDetailService;
import com.xianzaishi.wms.track.service.itf.IRequisitionsService;
import com.xianzaishi.wms.track.vo.RequisitionsDetailQueryVO;
import com.xianzaishi.wms.track.vo.RequisitionsDetailVO;
import com.xianzaishi.wms.track.vo.RequisitionsQueryVO;
import com.xianzaishi.wms.track.vo.RequisitionsVO;

public class RequisitionsDomainServiceImpl implements
		IRequisitionsDomainService {
	@Autowired
	private IRequisitionsService requisitionsService = null;
	@Autowired
	private IRequisitionsDetailService requisitionsDetailService = null;
	@Autowired
	private IInventoryManageDomainClient inventoryManageDomainClient = null;
	@Autowired
	private SkuService skuService = null;
	@Autowired
	private ValueService valueService = null;
	@Autowired
	private IPositionDomainClient positionDomainClient = null;

	public Boolean createRequisitionsDomain(RequisitionsVO requisitionsVO) {
		if (requisitionsVO.getDetails() == null
				|| requisitionsVO.getDetails().isEmpty()) {
			throw new BizException("请填充入库明细！");
		}
		requisitionsVO.setStatu(new Short("0"));
		Long requisitionsID = requisitionsService
				.addRequisitionsVO(requisitionsVO);

		initDetails(requisitionsID, requisitionsVO.getDetails());

		requisitionsDetailService.batchAddRequisitionsDetailVO(requisitionsVO
				.getDetails());

		return true;
	}

	public RequisitionsVO getRequisitionsDomainByID(Long requisitionsID) {
		if (requisitionsID == null || requisitionsID <= 0) {
			throw new BizException("id error");
		}

		RequisitionsVO requisitionsVO = requisitionsService
				.getRequisitionsVOByID(requisitionsID);

		requisitionsVO.setDetails(requisitionsDetailService
				.getRequisitionsDetailVOByRequisitionsID(requisitionsID));

		return requisitionsVO;
	}

	public Boolean deleteRequisitionsDomain(Long requisitionsID) {
		if (requisitionsID == null || requisitionsID <= 0) {
			throw new BizException("id error");
		}

		List<RequisitionsDetailVO> requisitionsDetailVOs = requisitionsDetailService
				.getRequisitionsDetailVOByRequisitionsID(requisitionsID);

		requisitionsDetailService
				.batchDeleteRequisitionsDetailVO(requisitionsDetailVOs);

		requisitionsService.deleteRequisitionsVOByID(requisitionsID);

		return true;
	}

	public Boolean addRequisitionsVO(RequisitionsVO requisitionsVO) {
		requisitionsService.addRequisitionsVO(requisitionsVO);
		return true;
	}

	public QueryResultVO<RequisitionsVO> queryRequisitionsVOList(
			RequisitionsQueryVO requisitionsQueryVO) {
		QueryResultVO<RequisitionsVO> queryResultVO = new QueryResultVO<>();
		queryResultVO.setPage(requisitionsQueryVO.getPage());
		queryResultVO.setSize(requisitionsQueryVO.getSize());
		queryResultVO.setItems(requisitionsService
				.queryRequisitionsVOList(requisitionsQueryVO));
		queryResultVO.setTotalCount(requisitionsService
				.queryRequisitionsVOCount(requisitionsQueryVO));
		return queryResultVO;
	}

	public RequisitionsVO getRequisitionsVOByID(Long id) {
		return requisitionsService.getRequisitionsVOByID(id);
	}

	public Boolean modifyRequisitionsVO(RequisitionsVO requisitionsVO) {
		return requisitionsService.modifyRequisitionsVO(requisitionsVO);
	}

	public Boolean deleteRequisitionsVO(Long id) {
		return requisitionsService.deleteRequisitionsVOByID(id);
	}

	public Boolean createRequisitionsDetailVO(
			RequisitionsDetailVO requisitionsDetailVO) {
		initInfo(requisitionsDetailVO);
		return requisitionsDetailService
				.addRequisitionsDetailVO(requisitionsDetailVO);
	}

	public Boolean batchCreateRequisitionsDetailVO(
			List<RequisitionsDetailVO> requisitionsDetailVOs) {
		for (RequisitionsDetailVO requisitionsDetailVO : requisitionsDetailVOs) {
			initInfo(requisitionsDetailVO);
		}
		return requisitionsDetailService
				.batchAddRequisitionsDetailVO(requisitionsDetailVOs);
	}

	public List<RequisitionsDetailVO> queryRequisitionsDetailVOList(
			RequisitionsDetailQueryVO requisitionsDetailQueryVO) {
		return requisitionsDetailService
				.queryRequisitionsDetailVOList(requisitionsDetailQueryVO);
	}

	public List<RequisitionsDetailVO> getRequisitionsDetailVOListByRequisitionsID(
			Long requisitionsID) {
		return requisitionsDetailService
				.getRequisitionsDetailVOByRequisitionsID(requisitionsID);
	}

	public RequisitionsDetailVO getRequisitionsDetailVOByID(Long id) {
		return requisitionsDetailService.getRequisitionsDetailVOByID(id);
	}

	public Boolean modifyRequisitionsDetailVO(
			RequisitionsDetailVO requisitionsDetailVO) {
		initInfo(requisitionsDetailVO);
		return requisitionsDetailService
				.modifyRequisitionsDetailVO(requisitionsDetailVO);
	}

	public Boolean batchModifyRequisitionsDetailVOs(
			List<RequisitionsDetailVO> requisitionsDetailVOs) {
		for (RequisitionsDetailVO requisitionsDetailVO : requisitionsDetailVOs) {
			initInfo(requisitionsDetailVO);
		}
		return requisitionsDetailService
				.batchModifyRequisitionsDetailVO(requisitionsDetailVOs);
	}

	public Boolean deleteRequisitionsDetailVO(Long id) {
		return requisitionsDetailService.deleteRequisitionsDetailVOByID(id);
	}

	public Boolean deleteRequisitionsDetailVOByRequisitionsID(
			Long requisitionsID) {
		if (requisitionsID == null || requisitionsID <= 0) {
			throw new BizException("id error");
		}

		List<RequisitionsDetailVO> requisitionsDetailVOs = requisitionsDetailService
				.getRequisitionsDetailVOByRequisitionsID(requisitionsID);

		requisitionsDetailService
				.batchDeleteRequisitionsDetailVO(requisitionsDetailVOs);

		return true;
	}

	public Boolean batchDeleteRequisitionsDetailVOByRequisitionsID(
			List<Long> storyDetailIDs) {
		return requisitionsDetailService
				.batchDeleteRequisitionsDetailVOByID(storyDetailIDs);
	}

	private void initDetails(Long id,
			List<RequisitionsDetailVO> requisitionsDetailVOs) {
		for (int i = 0; i < requisitionsDetailVOs.size(); i++) {
			requisitionsDetailVOs.get(i).setRequisitionsId(id);
			initInfo(requisitionsDetailVOs.get(i));
		}
	}

	private void initInfo(RequisitionsDetailVO requisitionsDetailVO) {
		ItemCommoditySkuDTO itemCommoditySkuDTO = skuService.queryItemSku(
				SkuQuery.querySkuById(requisitionsDetailVO.getSkuId()))
				.getModule();
		if (itemCommoditySkuDTO == null) {
			throw new BizException("skuID error:"
					+ requisitionsDetailVO.getSkuId());
		}
		requisitionsDetailVO.setSkuName(itemCommoditySkuDTO.getTitle());
		requisitionsDetailVO.setSaleUnit(valueService.queryValueNameByValueId(
				itemCommoditySkuDTO.getSkuUnit()).getModule());
		requisitionsDetailVO.setSaleUnitId(new Long(itemCommoditySkuDTO
				.getSkuUnit()));
		requisitionsDetailVO.setSaleUnitType(new Integer(itemCommoditySkuDTO
				.querySaleUnitType()));
		// requisitionsDetailVO.setSpec(itemCommoditySkuDTO
		// .getSkuSpecificationNum());
		// requisitionsDetailVO.setSpecUnit(valueService.queryValueNameByValueId(
		// itemCommoditySkuDTO.getSkuSpecificationUnit()).getModule());
		// requisitionsDetailVO.setSpecUnitId(new Long(itemCommoditySkuDTO
		// .getSkuSpecificationUnit()));
		PositionVO positionVO = positionDomainClient.getPositionVOByID(
				requisitionsDetailVO.getPositionId()).getTarget();
		if (positionVO == null) {
			throw new BizException("positionID error:"
					+ requisitionsDetailVO.getPositionId());
		}
		requisitionsDetailVO.setPositionCode(positionVO.getCode());

		if (requisitionsDetailVO.getNumberReal() == null
				&& requisitionsDetailVO.getNumber() != null
				&& !requisitionsDetailVO.getNumber().isEmpty()) {
			requisitionsDetailVO.setNumberReal(new BigDecimal(
					requisitionsDetailVO.getNumber()).multiply(
					new BigDecimal(1000)).intValue());
		}
	}

	public IRequisitionsService getRequisitionsService() {
		return requisitionsService;
	}

	public void setRequisitionsService(IRequisitionsService requisitionsService) {
		this.requisitionsService = requisitionsService;
	}

	public IRequisitionsDetailService getRequisitionsDetailService() {
		return requisitionsDetailService;
	}

	public void setRequisitionsDetailService(
			IRequisitionsDetailService requisitionsDetailService) {
		this.requisitionsDetailService = requisitionsDetailService;
	}

	public Boolean submit(Long id) {
		return requisitionsService.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return requisitionsService.audit(id, auditor);
	}

	public Boolean outgoing(Long id, Long auditor) {
		return requisitionsService.outgoing(id, auditor);
	}

	public Boolean outgoingAudit(Long id, Long auditor) {
		Boolean flag = false;
		if (requisitionsService.audit(id, auditor)) {
			flag = accounted(id);
		}
		return flag;
	}

	public Boolean accounted(Long id) {
		Boolean flag = false;
		RequisitionsVO requisitionsVO = getRequisitionsDomainByID(id);
		InventoryManageVO inventoryManageVO = initInventoryManage(requisitionsVO);
		SimpleResultVO<Boolean> result = inventoryManageDomainClient
				.createInventoryManageDomain(inventoryManageVO);
		if (result.isSuccess() && result.getTarget()) {
			flag = requisitionsService.accounted(id);
		}
		return flag;
	}

	private InventoryManageVO initInventoryManage(RequisitionsVO requisitionsVO) {
		InventoryManageVO inventoryManageVO = new InventoryManageVO();
		inventoryManageVO.setAgencyId(requisitionsVO.getAgencyId());
		inventoryManageVO.setAuditor(requisitionsVO.getAuditor());
		inventoryManageVO.setAuditTime(requisitionsVO.getAuditTime());
		inventoryManageVO.setOperator(requisitionsVO.getOperate());
		inventoryManageVO
				.setOpReason(TrackConstants.IM_REASON_OUT_REQUISITIONS);
		inventoryManageVO.setOpTime(requisitionsVO.getOpTime());
		inventoryManageVO.setOpType(1);
		List<InventoryManageDetailVO> details = new LinkedList<InventoryManageDetailVO>();
		for (int i = 0; i < requisitionsVO.getDetails().size(); i++) {
			InventoryManageDetailVO inventoryManageDetailVO = new InventoryManageDetailVO();
			inventoryManageDetailVO.setNumber(-requisitionsVO.getDetails()
					.get(i).getNumberReal());
			inventoryManageDetailVO.setSaleUnit(requisitionsVO.getDetails()
					.get(i).getSaleUnit());
			inventoryManageDetailVO.setSaleUnitId(requisitionsVO.getDetails()
					.get(i).getSaleUnitId());
			inventoryManageDetailVO.setSpec(requisitionsVO.getDetails().get(i)
					.getSpec());
			inventoryManageDetailVO.setSpecUnit(requisitionsVO.getDetails()
					.get(i).getSpecUnit());
			inventoryManageDetailVO.setSpecUnitId(requisitionsVO.getDetails()
					.get(i).getSpecUnitId());
			inventoryManageDetailVO.setSaleUnitType(requisitionsVO.getDetails()
					.get(i).getSaleUnitType());
			inventoryManageDetailVO.setSkuName(requisitionsVO.getDetails()
					.get(i).getSkuName());
			inventoryManageDetailVO.setPositionCode(requisitionsVO.getDetails()
					.get(i).getPositionCode());
			inventoryManageDetailVO.setSkuId(requisitionsVO.getDetails().get(i)
					.getSkuId());
			inventoryManageDetailVO.setPositionId(requisitionsVO.getDetails()
					.get(i).getPositionId());
			details.add(inventoryManageDetailVO);
		}
		inventoryManageVO.setDetails(details);
		return inventoryManageVO;
	}

	public IInventoryManageDomainClient getInventoryManageDomainClient() {
		return inventoryManageDomainClient;
	}

	public void setInventoryManageDomainClient(
			IInventoryManageDomainClient inventoryManageDomainClient) {
		this.inventoryManageDomainClient = inventoryManageDomainClient;
	}

	public SkuService getSkuService() {
		return skuService;
	}

	public void setSkuService(SkuService skuService) {
		this.skuService = skuService;
	}

	public ValueService getValueService() {
		return valueService;
	}

	public void setValueService(ValueService valueService) {
		this.valueService = valueService;
	}

	public IPositionDomainClient getPositionDomainClient() {
		return positionDomainClient;
	}

	public void setPositionDomainClient(
			IPositionDomainClient positionDomainClient) {
		this.positionDomainClient = positionDomainClient;
	}
}
