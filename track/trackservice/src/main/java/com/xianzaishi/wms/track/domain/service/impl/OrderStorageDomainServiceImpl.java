package com.xianzaishi.wms.track.domain.service.impl;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryManageDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IPositionDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryManageDetailVO;
import com.xianzaishi.wms.hive.vo.InventoryManageVO;
import com.xianzaishi.wms.hive.vo.PositionVO;
import com.xianzaishi.wms.track.constants.TrackConstants;
import com.xianzaishi.wms.track.domain.service.itf.IOrderStorageDomainService;
import com.xianzaishi.wms.track.service.itf.IOrderStorageDetailService;
import com.xianzaishi.wms.track.service.itf.IOrderStorageService;
import com.xianzaishi.wms.track.vo.OrderStorageDetailQueryVO;
import com.xianzaishi.wms.track.vo.OrderStorageDetailVO;
import com.xianzaishi.wms.track.vo.OrderStorageQueryVO;
import com.xianzaishi.wms.track.vo.OrderStorageVO;

public class OrderStorageDomainServiceImpl implements
		IOrderStorageDomainService {
	@Autowired
	private IOrderStorageService orderStorageService = null;
	@Autowired
	private IOrderStorageDetailService orderStorageDetailService = null;
	@Autowired
	private IInventoryManageDomainClient inventoryManageDomainClient = null;
	@Autowired
	private SkuService skuService = null;
	@Autowired
	private ValueService valueService = null;
	@Autowired
	private IPositionDomainClient positionDomainClient = null;

	public Boolean createOrderStorageDomain(OrderStorageVO orderStorageVO) {
		if (orderStorageVO.getDetails() == null
				|| orderStorageVO.getDetails().isEmpty()) {
			throw new BizException("请填充入库明细！");
		}

		orderStorageVO.setStatu(0);
		Long orderStorageID = orderStorageService
				.addOrderStorageVO(orderStorageVO);

		initDetails(orderStorageID, orderStorageVO.getDetails());

		orderStorageDetailService.batchAddOrderStorageDetailVO(orderStorageVO
				.getDetails());

		return true;
	}

	public OrderStorageVO getOrderStorageDomainByID(Long orderStorageID) {
		if (orderStorageID == null || orderStorageID <= 0) {
			throw new BizException("id error");
		}

		OrderStorageVO orderStorageVO = orderStorageService
				.getOrderStorageVOByID(orderStorageID);

		orderStorageVO.setDetails(orderStorageDetailService
				.getOrderStorageDetailVOByOrderStorageID(orderStorageID));

		return orderStorageVO;
	}

	public Boolean deleteOrderStorageDomain(Long orderStorageID) {
		if (orderStorageID == null || orderStorageID <= 0) {
			throw new BizException("id error");
		}

		List<OrderStorageDetailVO> orderStorageDetailVOs = orderStorageDetailService
				.getOrderStorageDetailVOByOrderStorageID(orderStorageID);

		orderStorageDetailService
				.batchDeleteOrderStorageDetailVO(orderStorageDetailVOs);

		orderStorageService.deleteOrderStorageVOByID(orderStorageID);

		return true;
	}

	public Boolean addOrderStorageVO(OrderStorageVO orderStorageVO) {
		orderStorageService.addOrderStorageVO(orderStorageVO);
		return true;
	}

	public QueryResultVO<OrderStorageVO> queryOrderStorageVOList(
			OrderStorageQueryVO orderStorageQueryVO) {
		QueryResultVO<OrderStorageVO> queryResultVO = new QueryResultVO<>();
		queryResultVO.setPage(orderStorageQueryVO.getPage());
		queryResultVO.setSize(orderStorageQueryVO.getSize());
		queryResultVO.setItems(orderStorageService
				.queryOrderStorageVOList(orderStorageQueryVO));
		queryResultVO.setTotalCount(orderStorageService
				.queryOrderStorageVOCount(orderStorageQueryVO));
		return queryResultVO;
	}

	public OrderStorageVO getOrderStorageVOByID(Long id) {
		return orderStorageService.getOrderStorageVOByID(id);
	}

	public Boolean modifyOrderStorageVO(OrderStorageVO orderStorageVO) {
		return orderStorageService.modifyOrderStorageVO(orderStorageVO);
	}

	public Boolean deleteOrderStorageVO(Long id) {
		return orderStorageService.deleteOrderStorageVOByID(id);
	}

	public Boolean createOrderStorageDetailVO(
			OrderStorageDetailVO orderStorageDetailVO) {
		initInfo(orderStorageDetailVO);
		return orderStorageDetailService
				.addOrderStorageDetailVO(orderStorageDetailVO);
	}

	public Boolean batchCreateOrderStorageDetailVO(
			List<OrderStorageDetailVO> orderStorageDetailVOs) {
		for (OrderStorageDetailVO orderStorageDetailVO : orderStorageDetailVOs) {
			initInfo(orderStorageDetailVO);
		}
		return orderStorageDetailService
				.batchAddOrderStorageDetailVO(orderStorageDetailVOs);
	}

	public List<OrderStorageDetailVO> queryOrderStorageDetailVOList(
			OrderStorageDetailQueryVO orderStorageDetailQueryVO) {
		return orderStorageDetailService
				.queryOrderStorageDetailVOList(orderStorageDetailQueryVO);
	}

	public List<OrderStorageDetailVO> getOrderStorageDetailVOListByOrderStorageID(
			Long orderStorageID) {
		return orderStorageDetailService
				.getOrderStorageDetailVOByOrderStorageID(orderStorageID);
	}

	public OrderStorageDetailVO getOrderStorageDetailVOByID(Long id) {
		return orderStorageDetailService.getOrderStorageDetailVOByID(id);
	}

	public Boolean modifyOrderStorageDetailVO(
			OrderStorageDetailVO orderStorageDetailVO) {
		initInfo(orderStorageDetailVO);
		return orderStorageDetailService
				.modifyOrderStorageDetailVO(orderStorageDetailVO);
	}

	public Boolean batchModifyOrderStorageDetailVOs(
			List<OrderStorageDetailVO> orderStorageDetailVOs) {
		for (OrderStorageDetailVO orderStorageDetailVO : orderStorageDetailVOs) {
			initInfo(orderStorageDetailVO);
		}
		return orderStorageDetailService
				.batchModifyOrderStorageDetailVO(orderStorageDetailVOs);
	}

	public Boolean deleteOrderStorageDetailVO(Long id) {
		return orderStorageDetailService.deleteOrderStorageDetailVOByID(id);
	}

	public Boolean deleteOrderStorageDetailVOByOrderStorageID(
			Long orderStorageID) {
		if (orderStorageID == null || orderStorageID <= 0) {
			throw new BizException("id error");
		}

		List<OrderStorageDetailVO> orderStorageDetailVOs = orderStorageDetailService
				.getOrderStorageDetailVOByOrderStorageID(orderStorageID);

		orderStorageDetailService
				.batchDeleteOrderStorageDetailVO(orderStorageDetailVOs);

		return true;
	}

	public Boolean batchDeleteOrderStorageDetailVOByOrderStorageID(
			List<Long> storyDetailIDs) {
		return orderStorageDetailService
				.batchDeleteOrderStorageDetailVOByID(storyDetailIDs);
	}

	private void initDetails(Long id,
			List<OrderStorageDetailVO> orderStorageDetailVOs) {
		for (int i = 0; i < orderStorageDetailVOs.size(); i++) {
			orderStorageDetailVOs.get(i).setStorageId(id);
			initInfo(orderStorageDetailVOs.get(i));
		}
	}

	private void initInfo(OrderStorageDetailVO orderStorageDetailVO) {
		ItemCommoditySkuDTO itemCommoditySkuDTO = skuService.queryItemSku(
				SkuQuery.querySkuById(orderStorageDetailVO.getSkuId()))
				.getModule();
		if (itemCommoditySkuDTO == null) {
			throw new BizException("skuID error:"
					+ orderStorageDetailVO.getSkuId());
		}
		orderStorageDetailVO.setSkuName(itemCommoditySkuDTO.getTitle());
		orderStorageDetailVO.setSaleUnit(valueService.queryValueNameByValueId(
				itemCommoditySkuDTO.getSkuUnit()).getModule());
		orderStorageDetailVO.setSaleUnitId(new Long(itemCommoditySkuDTO
				.getSkuUnit()));
		orderStorageDetailVO.setSaleUnitType(new Integer(itemCommoditySkuDTO
				.querySaleUnitType()));
		// orderStorageDetailVO.setSpec(itemCommoditySkuDTO
		// .getSkuSpecificationNum());
		// orderStorageDetailVO.setSpecUnit(valueService.queryValueNameByValueId(
		// itemCommoditySkuDTO.getSkuSpecificationUnit()).getModule());
		// orderStorageDetailVO.setSpecUnitId(new Long(itemCommoditySkuDTO
		// .getSkuSpecificationUnit()));
		PositionVO positionVO = positionDomainClient.getPositionVOByID(
				orderStorageDetailVO.getPositionId()).getTarget();
		if (positionVO == null) {
			throw new BizException("positionID error:"
					+ orderStorageDetailVO.getPositionId());
		}
		orderStorageDetailVO.setPositionCode(positionVO.getCode());

		if (orderStorageDetailVO.getNumberReal() == null
				&& orderStorageDetailVO.getNumber() != null
				&& !orderStorageDetailVO.getNumber().isEmpty()) {
			orderStorageDetailVO.setNumberReal(new BigDecimal(
					orderStorageDetailVO.getNumber()).multiply(
					new BigDecimal(1000)).intValue());
		}
	}

	public IOrderStorageService getOrderStorageService() {
		return orderStorageService;
	}

	public void setOrderStorageService(IOrderStorageService orderStorageService) {
		this.orderStorageService = orderStorageService;
	}

	public IOrderStorageDetailService getOrderStorageDetailService() {
		return orderStorageDetailService;
	}

	public void setOrderStorageDetailService(
			IOrderStorageDetailService orderStorageDetailService) {
		this.orderStorageDetailService = orderStorageDetailService;
	}

	public Boolean submit(Long id) {
		return orderStorageService.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		Boolean flag = false;
		if (orderStorageService.audit(id, auditor)) {
			flag = accounted(id);
		}
		return flag;
	}

	public Boolean accounted(Long id) {
		Boolean flag = false;
		OrderStorageVO orderStorageVO = getOrderStorageDomainByID(id);
		InventoryManageVO inventoryManageVO = initInventoryManage(orderStorageVO);
		SimpleResultVO<Boolean> result = inventoryManageDomainClient
				.createInventoryManageDomain(inventoryManageVO);
		if (result.isSuccess() && result.getTarget()) {
			flag = orderStorageService.accounted(id);
		}
		return flag;
	}

	private InventoryManageVO initInventoryManage(OrderStorageVO orderStorageVO) {
		InventoryManageVO inventoryManageVO = new InventoryManageVO();
		inventoryManageVO.setAgencyId(orderStorageVO.getAgencyId());
		inventoryManageVO.setAuditor(orderStorageVO.getAuditor());
		inventoryManageVO.setAuditTime(orderStorageVO.getAuditTime());
		inventoryManageVO.setOperator(orderStorageVO.getOperate());
		inventoryManageVO.setOpReason(TrackConstants.IM_REASON_IN_ORDER);
		inventoryManageVO.setOpTime(orderStorageVO.getOpTime());
		inventoryManageVO.setOpType(2);
		List<InventoryManageDetailVO> details = new LinkedList<InventoryManageDetailVO>();
		for (int i = 0; i < orderStorageVO.getDetails().size(); i++) {
			InventoryManageDetailVO inventoryManageDetailVO = new InventoryManageDetailVO();
			inventoryManageDetailVO.setNumber(orderStorageVO.getDetails()
					.get(i).getNumberReal());
			inventoryManageDetailVO.setSaleUnit(orderStorageVO.getDetails()
					.get(i).getSaleUnit());
			inventoryManageDetailVO.setSaleUnitId(orderStorageVO.getDetails()
					.get(i).getSaleUnitId());
			inventoryManageDetailVO.setSpec(orderStorageVO.getDetails().get(i)
					.getSpec());
			inventoryManageDetailVO.setSpecUnit(orderStorageVO.getDetails()
					.get(i).getSpecUnit());
			inventoryManageDetailVO.setSpecUnitId(orderStorageVO.getDetails()
					.get(i).getSpecUnitId());
			inventoryManageDetailVO.setSaleUnitType(orderStorageVO.getDetails()
					.get(i).getSaleUnitType());
			inventoryManageDetailVO.setSkuName(orderStorageVO.getDetails()
					.get(i).getSkuName());
			inventoryManageDetailVO.setPositionCode(orderStorageVO.getDetails()
					.get(i).getPositionCode());
			inventoryManageDetailVO.setSkuId(orderStorageVO.getDetails().get(i)
					.getSkuId());
			inventoryManageDetailVO.setPositionId(orderStorageVO.getDetails()
					.get(i).getPositionId());
			details.add(inventoryManageDetailVO);
		}
		inventoryManageVO.setDetails(details);
		return inventoryManageVO;
	}

	public IInventoryManageDomainClient getInventoryManageDomainClient() {
		return inventoryManageDomainClient;
	}

	public void setInventoryManageDomainClient(
			IInventoryManageDomainClient inventoryManageDomainClient) {
		this.inventoryManageDomainClient = inventoryManageDomainClient;
	}

	public SkuService getSkuService() {
		return skuService;
	}

	public void setSkuService(SkuService skuService) {
		this.skuService = skuService;
	}

	public ValueService getValueService() {
		return valueService;
	}

	public void setValueService(ValueService valueService) {
		this.valueService = valueService;
	}

	public IPositionDomainClient getPositionDomainClient() {
		return positionDomainClient;
	}

	public void setPositionDomainClient(
			IPositionDomainClient positionDomainClient) {
		this.positionDomainClient = positionDomainClient;
	}
}
