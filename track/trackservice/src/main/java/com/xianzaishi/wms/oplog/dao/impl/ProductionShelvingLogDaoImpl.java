package com.xianzaishi.wms.oplog.dao.impl;

import java.util.List;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.oplog.dao.itf.IProductionShelvingLogDao;
import com.xianzaishi.wms.oplog.vo.ProductionShelvingLogVO;

public class ProductionShelvingLogDaoImpl extends BaseDaoAdapter implements
		IProductionShelvingLogDao {
	public String getVOClassName() {
		return "ProductionShelvingLogDO";
	}

	public Boolean batchAddProductionShelvingLog(
			List<ProductionShelvingLogVO> productionShelvingLogVOs) {
		for (int i = 0; i < productionShelvingLogVOs.size(); i++) {
			addDO(productionShelvingLogVOs.get(i));
		}
		return true;
	}
}
