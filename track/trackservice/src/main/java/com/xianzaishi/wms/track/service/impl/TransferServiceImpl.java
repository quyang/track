package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.vo.TransferVO;
import com.xianzaishi.wms.track.vo.TransferQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.service.itf.ITransferService;
import com.xianzaishi.wms.track.manage.itf.ITransferManage;

public class TransferServiceImpl implements ITransferService {
	@Autowired
	private ITransferManage transferManage = null;

	public ITransferManage getTransferManage() {
		return transferManage;
	}

	public void setTransferManage(ITransferManage transferManage) {
		this.transferManage = transferManage;
	}

	public Long addTransferVO(TransferVO transferVO) {
		return transferManage.addTransferVO(transferVO);
	}

	public List<TransferVO> queryTransferVOList(TransferQueryVO transferQueryVO) {
		return transferManage.queryTransferVOList(transferQueryVO);
	}

	public TransferVO getTransferVOByID(Long id) {
		return (TransferVO) transferManage.getTransferVOByID(id);
	}

	public Boolean modifyTransferVO(TransferVO transferVO) {
		return transferManage.modifyTransferVO(transferVO);
	}

	public Boolean deleteTransferVOByID(Long id) {
		return transferManage.deleteTransferVOByID(id);
	}

	public Boolean submit(Long id) {
		return transferManage.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return transferManage.audit(id, auditor);
	}

	public Boolean accounted(Long id) {
		return transferManage.accounted(id);
	}

	public Integer queryTransferVOCount(TransferQueryVO transferQueryVO) {
		return transferManage.queryTransferVOCount(transferQueryVO);
	}
}
