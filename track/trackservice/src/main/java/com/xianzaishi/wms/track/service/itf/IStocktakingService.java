package com.xianzaishi.wms.track.service.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.StocktakingVO;
import com.xianzaishi.wms.track.vo.StocktakingQueryVO;

public interface IStocktakingService {

	public Long addStocktakingVO(StocktakingVO stocktakingVO);

	public List<StocktakingVO> queryStocktakingVOList(
			StocktakingQueryVO stocktakingQueryVO);

	public Integer queryStocktakingVOCount(
			StocktakingQueryVO stocktakingQueryVO);

	public StocktakingVO getStocktakingVOByID(Long id);

	public Boolean modifyStocktakingVO(StocktakingVO stocktakingVO);

	public Boolean deleteStocktakingVOByID(Long id);

	public Boolean submitStocktakingToCheck(StocktakingVO stocktakingVO);

	public Boolean submitStocktakingToAudit(StocktakingVO stocktakingVO);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

	public Boolean accounted(Long id);

}