package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.manage.itf.IBookingDeliveryOrderManage;
import com.xianzaishi.wms.track.service.itf.IBookingDeliveryOrderService;
import com.xianzaishi.wms.track.vo.BookingDeliveryOrderQueryVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryOrderVO;

public class BookingDeliveryOrderServiceImpl implements
		IBookingDeliveryOrderService {
	@Autowired
	private IBookingDeliveryOrderManage bookingDeliveryOrderManage = null;

	public IBookingDeliveryOrderManage getBookingDeliveryOrderManage() {
		return bookingDeliveryOrderManage;
	}

	public void setBookingDeliveryOrderManage(
			IBookingDeliveryOrderManage bookingDeliveryOrderManage) {
		this.bookingDeliveryOrderManage = bookingDeliveryOrderManage;
	}

	public Boolean addBookingDeliveryOrderVO(
			BookingDeliveryOrderVO bookingDeliveryOrderVO) {
		bookingDeliveryOrderManage
				.addBookingDeliveryOrderVO(bookingDeliveryOrderVO);
		return true;
	}

	public List<BookingDeliveryOrderVO> queryBookingDeliveryOrderVOList(
			BookingDeliveryOrderQueryVO bookingDeliveryOrderQueryVO) {
		return bookingDeliveryOrderManage
				.queryBookingDeliveryOrderVOList(bookingDeliveryOrderQueryVO);
	}

	public BookingDeliveryOrderVO getBookingDeliveryOrderVOByID(Long id) {
		return (BookingDeliveryOrderVO) bookingDeliveryOrderManage
				.getBookingDeliveryOrderVOByID(id);
	}

	public Boolean modifyBookingDeliveryOrderVO(
			BookingDeliveryOrderVO bookingDeliveryOrderVO) {
		return bookingDeliveryOrderManage
				.modifyBookingDeliveryOrderVO(bookingDeliveryOrderVO);
	}

	public Boolean deleteBookingDeliveryOrderVOByID(Long id) {
		return bookingDeliveryOrderManage.deleteBookingDeliveryOrderVOByID(id);
	}

	public List<BookingDeliveryOrderVO> getBookingDeliveryOrderVOByOrderStorageID(
			Long id) {
		return bookingDeliveryOrderManage
				.getBookingDeliveryOrderVOByOrderStorageID(id);
	}

	public Boolean batchAddBookingDeliveryOrderVO(
			List<BookingDeliveryOrderVO> bookingDeliveryOrderVOs) {
		return bookingDeliveryOrderManage
				.batchAddBookingDeliveryOrderVO(bookingDeliveryOrderVOs);
	}

	public Boolean batchModifyBookingDeliveryOrderVO(
			List<BookingDeliveryOrderVO> bookingDeliveryOrderVOs) {
		return bookingDeliveryOrderManage
				.batchModifyBookingDeliveryOrderVO(bookingDeliveryOrderVOs);
	}

	public Boolean batchDeleteBookingDeliveryOrderVO(
			List<BookingDeliveryOrderVO> bookingDeliveryOrderVOs) {
		return bookingDeliveryOrderManage
				.batchDeleteBookingDeliveryOrderVO(bookingDeliveryOrderVOs);
	}

	public Boolean batchDeleteBookingDeliveryOrderVOByID(
			List<Long> storyDetailIDs) {
		return bookingDeliveryOrderManage
				.batchDeleteBookingDeliveryOrderVOByID(storyDetailIDs);
	}
}
