package com.xianzaishi.wms.oplog.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.oplog.vo.TradeServiceShelvingLogVO;
import com.xianzaishi.wms.oplog.vo.TradeServiceShelvingLogDO;
import com.xianzaishi.wms.oplog.vo.TradeServiceShelvingLogQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.oplog.manage.itf.ITradeServiceShelvingLogManage;
import com.xianzaishi.wms.oplog.dao.itf.ITradeServiceShelvingLogDao;

public class TradeServiceShelvingLogManageImpl implements
		ITradeServiceShelvingLogManage {
	@Autowired
	private ITradeServiceShelvingLogDao tradeServiceShelvingLogDao = null;

	private void validate(TradeServiceShelvingLogDO tradeServiceShelvingLogDO) {
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public ITradeServiceShelvingLogDao getTradeServiceShelvingLogDao() {
		return tradeServiceShelvingLogDao;
	}

	public void setTradeServiceShelvingLogDao(
			ITradeServiceShelvingLogDao tradeServiceShelvingLogDao) {
		this.tradeServiceShelvingLogDao = tradeServiceShelvingLogDao;
	}

	public Boolean addTradeServiceShelvingLogVO(
			TradeServiceShelvingLogVO tradeServiceShelvingLogVO) {
		tradeServiceShelvingLogDao.addDO(tradeServiceShelvingLogVO);
		return true;
	}

	public List<TradeServiceShelvingLogVO> queryTradeServiceShelvingLogVOList(
			TradeServiceShelvingLogQueryVO tradeServiceShelvingLogQueryVO) {
		return tradeServiceShelvingLogDao
				.queryDO(tradeServiceShelvingLogQueryVO);
	}

	public TradeServiceShelvingLogVO getTradeServiceShelvingLogVOByID(Long id) {
		return (TradeServiceShelvingLogVO) tradeServiceShelvingLogDao
				.getDOByID(id);
	}

	public Boolean modifyTradeServiceShelvingLogVO(
			TradeServiceShelvingLogVO tradeServiceShelvingLogVO) {
		return tradeServiceShelvingLogDao.updateDO(tradeServiceShelvingLogVO);
	}

	public Boolean deleteTradeServiceShelvingLogVOByID(Long id) {
		return tradeServiceShelvingLogDao.delDO(id);
	}

	public Boolean batchAddTradeServiceShelvingLog(
			List<TradeServiceShelvingLogVO> tradeServiceShelvingLogVOs) {
		return tradeServiceShelvingLogDao
				.batchAddTradeServiceShelvingLog(tradeServiceShelvingLogVOs);
	}
}
