package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.vo.StocktakingVO;
import com.xianzaishi.wms.track.vo.StocktakingQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.service.itf.IStocktakingService;
import com.xianzaishi.wms.track.manage.itf.IStocktakingManage;

public class StocktakingServiceImpl implements IStocktakingService {
	@Autowired
	private IStocktakingManage stocktakingManage = null;

	public IStocktakingManage getStocktakingManage() {
		return stocktakingManage;
	}

	public void setStocktakingManage(IStocktakingManage stocktakingManage) {
		this.stocktakingManage = stocktakingManage;
	}

	public Long addStocktakingVO(StocktakingVO stocktakingVO) {
		return stocktakingManage.addStocktakingVO(stocktakingVO);
	}

	public List<StocktakingVO> queryStocktakingVOList(
			StocktakingQueryVO stocktakingQueryVO) {
		return stocktakingManage.queryStocktakingVOList(stocktakingQueryVO);
	}

	public Integer queryStocktakingVOCount(StocktakingQueryVO stocktakingQueryVO) {
		return stocktakingManage.queryStocktakingVOCount(stocktakingQueryVO);
	}

	public StocktakingVO getStocktakingVOByID(Long id) {
		return (StocktakingVO) stocktakingManage.getStocktakingVOByID(id);
	}

	public Boolean modifyStocktakingVO(StocktakingVO stocktakingVO) {
		return stocktakingManage.modifyStocktakingVO(stocktakingVO);
	}

	public Boolean deleteStocktakingVOByID(Long id) {
		return stocktakingManage.deleteStocktakingVOByID(id);
	}

	public Boolean submitStocktakingToCheck(StocktakingVO stocktakingVO) {
		return stocktakingManage.submitStocktakingToCheck(stocktakingVO);
	}

	public Boolean submitStocktakingToAudit(StocktakingVO stocktakingVO) {
		return stocktakingManage.submitStocktakingToAudit(stocktakingVO);
	}

	public Boolean submit(Long id) {
		return stocktakingManage.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return stocktakingManage.audit(id, auditor);
	}

	public Boolean accounted(Long id) {
		return stocktakingManage.accounted(id);
	}
}
