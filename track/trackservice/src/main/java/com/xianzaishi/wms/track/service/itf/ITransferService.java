package com.xianzaishi.wms.track.service.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.TransferVO;
import com.xianzaishi.wms.track.vo.TransferQueryVO;

public interface ITransferService {

	public Long addTransferVO(TransferVO transferVO);

	public List<TransferVO> queryTransferVOList(TransferQueryVO transferQueryVO);

	public Integer queryTransferVOCount(TransferQueryVO transferQueryVO);

	public TransferVO getTransferVOByID(Long id);

	public Boolean modifyTransferVO(TransferVO transferVO);

	public Boolean deleteTransferVOByID(Long id);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

	public Boolean accounted(Long id);

}