package com.xianzaishi.wms.track.domain.service.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.track.vo.TransferDetailQueryVO;
import com.xianzaishi.wms.track.vo.TransferDetailVO;
import com.xianzaishi.wms.track.vo.TransferQueryVO;
import com.xianzaishi.wms.track.vo.TransferVO;

public interface ITransferDomainService {

	// domain
	public Boolean createTransferDomain(TransferVO transferVO);

	public TransferVO getTransferDomainByID(Long transferID);

	public Boolean deleteTransferDomain(Long id);

	public Boolean createAndAccount(TransferVO transferVO);

	// head
	public Boolean addTransferVO(TransferVO transferVO);

	public QueryResultVO<TransferVO> queryTransferVOList(
			TransferQueryVO transferQueryVO);

	public TransferVO getTransferVOByID(Long id);

	public Boolean modifyTransferVO(TransferVO transferVO);

	public Boolean deleteTransferVO(Long transferID);

	// body
	public Boolean createTransferDetailVO(TransferDetailVO transferDetailVO);

	public Boolean batchCreateTransferDetailVO(
			List<TransferDetailVO> transferDetailVOs);

	public List<TransferDetailVO> queryTransferDetailVOList(
			TransferDetailQueryVO transferDetailQueryVO);

	public List<TransferDetailVO> getTransferDetailVOListByTransferID(
			Long transferID);

	public TransferDetailVO getTransferDetailVOByID(Long id);

	public Boolean modifyTransferDetailVO(TransferDetailVO transferDetailVO);

	public Boolean batchModifyTransferDetailVOs(
			List<TransferDetailVO> transferDetailVOs);

	public Boolean deleteTransferDetailVO(Long id);

	public Boolean deleteTransferDetailVOByTransferID(Long transferID);

	public Boolean batchDeleteTransferDetailVOByTransferID(
			List<Long> storyDetailIDs);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

	public Boolean accounted(Long id);

}
