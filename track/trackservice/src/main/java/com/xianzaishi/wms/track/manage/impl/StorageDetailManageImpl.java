package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.IStorageDetailDao;
import com.xianzaishi.wms.track.manage.itf.IStorageDetailManage;
import com.xianzaishi.wms.track.vo.StorageDetailQueryVO;
import com.xianzaishi.wms.track.vo.StorageDetailVO;

public class StorageDetailManageImpl implements IStorageDetailManage {
	@Autowired
	private IStorageDetailDao storageDetailDao = null;

	private void validate(StorageDetailVO storageDetailVO) {
		if (storageDetailVO.getStorageId() == null
				|| storageDetailVO.getStorageId() <= 0) {
			throw new BizException("storageID error："
					+ storageDetailVO.getStorageId());
		}
		if (storageDetailVO.getSkuId() == null
				|| storageDetailVO.getSkuId() <= 0) {
			throw new BizException("skuID error：" + storageDetailVO.getSkuId());
		}
		if (storageDetailVO.getPositionId() == null
				|| storageDetailVO.getPositionId() <= 0) {
			throw new BizException("positionID error："
					+ storageDetailVO.getPositionId());
		}
		if (storageDetailVO.getNumberReal() == null
				|| storageDetailVO.getNumberReal() <= 0) {
			throw new BizException("number error："
					+ storageDetailVO.getNumberReal());
		}
		if (storageDetailVO.getSkuName() == null
				|| storageDetailVO.getSkuName().isEmpty()) {
			throw new BizException("skuName error："
					+ storageDetailVO.getSkuName());
		}
		if (storageDetailVO.getPositionCode() == null
				|| storageDetailVO.getPositionCode().isEmpty()) {
			throw new BizException("positionCode error："
					+ storageDetailVO.getPositionCode());
		}
		if (storageDetailVO.getSaleUnit() == null
				|| storageDetailVO.getSaleUnit().isEmpty()) {
			throw new BizException("saleUnit error："
					+ storageDetailVO.getSaleUnit());
		}
		if (storageDetailVO.getSaleUnitId() == null
				|| storageDetailVO.getSaleUnitId() <= 0) {
			throw new BizException("saleUnitId error："
					+ storageDetailVO.getSaleUnitId());
		}
		if (storageDetailVO.getSaleUnitType() == null) {
			throw new BizException("saleUnitType error："
					+ storageDetailVO.getSaleUnitType());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IStorageDetailDao getStorageDetailDao() {
		return storageDetailDao;
	}

	public void setStorageDetailDao(IStorageDetailDao storageDetailDao) {
		this.storageDetailDao = storageDetailDao;
	}

	public Boolean addStorageDetailVO(StorageDetailVO storageDetailVO) {
		validate(storageDetailVO);
		storageDetailDao.addDO(storageDetailVO);
		return true;
	}

	public List<StorageDetailVO> queryStorageDetailVOList(
			StorageDetailQueryVO storageDetailQueryVO) {
		return storageDetailDao.queryDO(storageDetailQueryVO);
	}

	public StorageDetailVO getStorageDetailVOByID(Long id) {
		return (StorageDetailVO) storageDetailDao.getDOByID(id);
	}

	public Boolean modifyStorageDetailVO(StorageDetailVO storageDetailVO) {
		return storageDetailDao.updateDO(storageDetailVO);
	}

	public Boolean deleteStorageDetailVOByID(Long id) {
		return storageDetailDao.delDO(id);
	}

	public List<StorageDetailVO> getStorageDetailVOByStorageID(Long id) {
		StorageDetailQueryVO queryVO = new StorageDetailQueryVO();
		queryVO.setStorageId(id);
		queryVO.setSize(Integer.MAX_VALUE);
		return storageDetailDao.queryDO(queryVO);
	}

	public Boolean batchAddStorageDetailVO(
			List<StorageDetailVO> storageDetailVOs) {
		return storageDetailDao.batchAddDO(storageDetailVOs);
	}

	public Boolean batchModifyStorageDetailVO(
			List<StorageDetailVO> storageDetailVOs) {
		return storageDetailDao.batchModifyDO(storageDetailVOs);
	}

	public Boolean batchDeleteStorageDetailVO(
			List<StorageDetailVO> storageDetailVOs) {
		return storageDetailDao.batchDeleteDO(storageDetailVOs);
	}

	public Boolean batchDeleteStorageDetailVOByID(List<Long> storageDetailIDs) {
		return storageDetailDao.batchDeleteDOByID(storageDetailIDs);
	}
}
