package com.xianzaishi.wms.track.domain.service.impl;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryManageDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IPositionDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryManageDetailVO;
import com.xianzaishi.wms.hive.vo.InventoryManageVO;
import com.xianzaishi.wms.hive.vo.PositionVO;
import com.xianzaishi.wms.track.constants.TrackConstants;
import com.xianzaishi.wms.track.domain.service.itf.ITradeServiceShelvingDomainService;
import com.xianzaishi.wms.track.service.itf.ITradeServiceShelvingDetailService;
import com.xianzaishi.wms.track.service.itf.ITradeServiceShelvingService;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingDetailQueryVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingDetailVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingQueryVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingVO;

public class TradeServiceShelvingDomainServiceImpl implements
		ITradeServiceShelvingDomainService {
	@Autowired
	private ITradeServiceShelvingService tradeServiceShelvingService = null;
	@Autowired
	private ITradeServiceShelvingDetailService tradeServiceShelvingDetailService = null;
	@Autowired
	private IInventoryManageDomainClient inventoryManageDomainClient = null;
	@Autowired
	private SkuService skuService = null;
	@Autowired
	private ValueService valueService = null;
	@Autowired
	private IPositionDomainClient positionDomainClient = null;

	public Boolean createTradeServiceShelvingDomain(
			TradeServiceShelvingVO tradeServiceShelvingVO) {
		if (tradeServiceShelvingVO.getDetails() == null
				|| tradeServiceShelvingVO.getDetails().isEmpty()) {
			throw new BizException("请填充入库明细！");
		}
		tradeServiceShelvingVO.setStatu(0);
		Long tradeServiceShelvingID = tradeServiceShelvingService
				.addTradeServiceShelvingVO(tradeServiceShelvingVO);

		initDetails(tradeServiceShelvingID, tradeServiceShelvingVO.getDetails());

		tradeServiceShelvingDetailService
				.batchAddTradeServiceShelvingDetailVO(tradeServiceShelvingVO
						.getDetails());
		return true;
	}

	public Boolean createAndAccount(
			TradeServiceShelvingVO tradeServiceShelvingVO) {
		if (tradeServiceShelvingVO.getDetails() == null
				|| tradeServiceShelvingVO.getDetails().isEmpty()) {
			throw new BizException("请填充入库明细！");
		}
		tradeServiceShelvingVO.setStatu(0);
		Long tradeServiceShelvingID = tradeServiceShelvingService
				.addTradeServiceShelvingVO(tradeServiceShelvingVO);

		initDetails(tradeServiceShelvingID, tradeServiceShelvingVO.getDetails());

		tradeServiceShelvingDetailService
				.batchAddTradeServiceShelvingDetailVO(tradeServiceShelvingVO
						.getDetails());
		submit(tradeServiceShelvingID);
		audit(tradeServiceShelvingID, tradeServiceShelvingVO.getOperate());
		return true;
	}

	public TradeServiceShelvingVO getTradeServiceShelvingDomainByID(
			Long tradeServiceShelvingID) {
		if (tradeServiceShelvingID == null || tradeServiceShelvingID <= 0) {
			throw new BizException("id error");
		}

		TradeServiceShelvingVO tradeServiceShelvingVO = tradeServiceShelvingService
				.getTradeServiceShelvingVOByID(tradeServiceShelvingID);

		tradeServiceShelvingVO
				.setDetails(tradeServiceShelvingDetailService
						.getTradeServiceShelvingDetailVOByTradeServiceShelvingID(tradeServiceShelvingID));

		return tradeServiceShelvingVO;
	}

	public Boolean deleteTradeServiceShelvingDomain(Long tradeServiceShelvingID) {
		if (tradeServiceShelvingID == null || tradeServiceShelvingID <= 0) {
			throw new BizException("id error");
		}

		List<TradeServiceShelvingDetailVO> tradeServiceShelvingDetailVOs = tradeServiceShelvingDetailService
				.getTradeServiceShelvingDetailVOByTradeServiceShelvingID(tradeServiceShelvingID);

		tradeServiceShelvingDetailService
				.batchDeleteTradeServiceShelvingDetailVO(tradeServiceShelvingDetailVOs);

		tradeServiceShelvingService
				.deleteTradeServiceShelvingVOByID(tradeServiceShelvingID);

		return true;
	}

	public Boolean addTradeServiceShelvingVO(
			TradeServiceShelvingVO tradeServiceShelvingVO) {
		tradeServiceShelvingService
				.addTradeServiceShelvingVO(tradeServiceShelvingVO);
		return true;
	}

	public QueryResultVO<TradeServiceShelvingVO> queryTradeServiceShelvingVOList(
			TradeServiceShelvingQueryVO tradeServiceShelvingQueryVO) {
		QueryResultVO<TradeServiceShelvingVO> queryResultVO = new QueryResultVO<>();
		queryResultVO.setPage(tradeServiceShelvingQueryVO.getPage());
		queryResultVO.setSize(tradeServiceShelvingQueryVO.getSize());
		queryResultVO.setItems(tradeServiceShelvingService
				.queryTradeServiceShelvingVOList(tradeServiceShelvingQueryVO));
		queryResultVO.setTotalCount(tradeServiceShelvingService
				.queryTradeServiceShelvingVOCount(tradeServiceShelvingQueryVO));
		return queryResultVO;

	}

	public TradeServiceShelvingVO getTradeServiceShelvingVOByID(Long id) {
		return tradeServiceShelvingService.getTradeServiceShelvingVOByID(id);
	}

	public Boolean modifyTradeServiceShelvingVO(
			TradeServiceShelvingVO tradeServiceShelvingVO) {
		return tradeServiceShelvingService
				.modifyTradeServiceShelvingVO(tradeServiceShelvingVO);
	}

	public Boolean deleteTradeServiceShelvingVO(Long id) {
		return tradeServiceShelvingService.deleteTradeServiceShelvingVOByID(id);
	}

	public Boolean createTradeServiceShelvingDetailVO(
			TradeServiceShelvingDetailVO tradeServiceShelvingDetailVO) {
		initInfo(tradeServiceShelvingDetailVO);
		return tradeServiceShelvingDetailService
				.addTradeServiceShelvingDetailVO(tradeServiceShelvingDetailVO);
	}

	public Boolean batchCreateTradeServiceShelvingDetailVO(
			List<TradeServiceShelvingDetailVO> tradeServiceShelvingDetailVOs) {
		for (TradeServiceShelvingDetailVO tradeServiceShelvingDetailVO : tradeServiceShelvingDetailVOs) {
			initInfo(tradeServiceShelvingDetailVO);
		}
		return tradeServiceShelvingDetailService
				.batchAddTradeServiceShelvingDetailVO(tradeServiceShelvingDetailVOs);
	}

	public List<TradeServiceShelvingDetailVO> queryTradeServiceShelvingDetailVOList(
			TradeServiceShelvingDetailQueryVO tradeServiceShelvingDetailQueryVO) {
		return tradeServiceShelvingDetailService
				.queryTradeServiceShelvingDetailVOList(tradeServiceShelvingDetailQueryVO);
	}

	public List<TradeServiceShelvingDetailVO> getTradeServiceShelvingDetailVOListByTradeServiceShelvingID(
			Long tradeServiceShelvingID) {
		return tradeServiceShelvingDetailService
				.getTradeServiceShelvingDetailVOByTradeServiceShelvingID(tradeServiceShelvingID);
	}

	public TradeServiceShelvingDetailVO getTradeServiceShelvingDetailVOByID(
			Long id) {
		return tradeServiceShelvingDetailService
				.getTradeServiceShelvingDetailVOByID(id);
	}

	public Boolean modifyTradeServiceShelvingDetailVO(
			TradeServiceShelvingDetailVO tradeServiceShelvingDetailVO) {
		initInfo(tradeServiceShelvingDetailVO);
		return tradeServiceShelvingDetailService
				.modifyTradeServiceShelvingDetailVO(tradeServiceShelvingDetailVO);
	}

	public Boolean batchModifyTradeServiceShelvingDetailVOs(
			List<TradeServiceShelvingDetailVO> tradeServiceShelvingDetailVOs) {
		for (TradeServiceShelvingDetailVO tradeServiceShelvingDetailVO : tradeServiceShelvingDetailVOs) {
			initInfo(tradeServiceShelvingDetailVO);
		}
		return tradeServiceShelvingDetailService
				.batchModifyTradeServiceShelvingDetailVO(tradeServiceShelvingDetailVOs);
	}

	public Boolean deleteTradeServiceShelvingDetailVO(Long id) {
		return tradeServiceShelvingDetailService
				.deleteTradeServiceShelvingDetailVOByID(id);
	}

	public Boolean deleteTradeServiceShelvingDetailVOByTradeServiceShelvingID(
			Long tradeServiceShelvingID) {
		if (tradeServiceShelvingID == null || tradeServiceShelvingID <= 0) {
			throw new BizException("id error");
		}

		List<TradeServiceShelvingDetailVO> tradeServiceShelvingDetailVOs = tradeServiceShelvingDetailService
				.getTradeServiceShelvingDetailVOByTradeServiceShelvingID(tradeServiceShelvingID);

		tradeServiceShelvingDetailService
				.batchDeleteTradeServiceShelvingDetailVO(tradeServiceShelvingDetailVOs);

		return true;
	}

	public Boolean batchDeleteTradeServiceShelvingDetailVOByTradeServiceShelvingID(
			List<Long> storyDetailIDs) {
		return tradeServiceShelvingDetailService
				.batchDeleteTradeServiceShelvingDetailVOByID(storyDetailIDs);
	}

	private void initDetails(Long id,
			List<TradeServiceShelvingDetailVO> tradeServiceShelvingDetailVOs) {
		for (int i = 0; i < tradeServiceShelvingDetailVOs.size(); i++) {
			tradeServiceShelvingDetailVOs.get(i).setShelveId(id);
			initInfo(tradeServiceShelvingDetailVOs.get(i));
		}
	}

	private void initInfo(
			TradeServiceShelvingDetailVO tradeServiceShelvingDetailVO) {
		ItemCommoditySkuDTO itemCommoditySkuDTO = skuService.queryItemSku(
				SkuQuery.querySkuById(tradeServiceShelvingDetailVO.getSkuId()))
				.getModule();
		if (itemCommoditySkuDTO == null) {
			throw new BizException("skuID error:"
					+ tradeServiceShelvingDetailVO.getSkuId());
		}
		tradeServiceShelvingDetailVO.setSkuName(itemCommoditySkuDTO.getTitle());
		tradeServiceShelvingDetailVO.setSaleUnit(valueService
				.queryValueNameByValueId(itemCommoditySkuDTO.getSkuUnit())
				.getModule());
		tradeServiceShelvingDetailVO.setSaleUnitId(new Long(itemCommoditySkuDTO
				.getSkuUnit()));
		tradeServiceShelvingDetailVO.setSaleUnitType(new Integer(
				itemCommoditySkuDTO.querySaleUnitType()));
		// tradeServiceShelvingDetailVO.setSpec(itemCommoditySkuDTO
		// .getSkuSpecificationNum());
		// tradeServiceShelvingDetailVO.setSpecUnit(valueService
		// .queryValueNameByValueId(
		// itemCommoditySkuDTO.getSkuSpecificationUnit())
		// .getModule());
		// tradeServiceShelvingDetailVO.setSpecUnitId(new
		// Long(itemCommoditySkuDTO
		// .getSkuSpecificationUnit()));
		PositionVO positionVO = positionDomainClient.getPositionVOByID(
				tradeServiceShelvingDetailVO.getPositionId()).getTarget();
		if (positionVO == null) {
			throw new BizException("positionID error:"
					+ tradeServiceShelvingDetailVO.getPositionId());
		}
		tradeServiceShelvingDetailVO.setPositionCode(positionVO.getCode());

		if (tradeServiceShelvingDetailVO.getShelveNoReal() == null
				&& tradeServiceShelvingDetailVO.getShelveNo() != null
				&& !tradeServiceShelvingDetailVO.getShelveNo().isEmpty()) {
			tradeServiceShelvingDetailVO.setShelveNoReal(new BigDecimal(
					tradeServiceShelvingDetailVO.getShelveNo()).multiply(
					new BigDecimal(1000)).intValue());
		}
	}

	public ITradeServiceShelvingService getTradeServiceShelvingService() {
		return tradeServiceShelvingService;
	}

	public void setTradeServiceShelvingService(
			ITradeServiceShelvingService tradeServiceShelvingService) {
		this.tradeServiceShelvingService = tradeServiceShelvingService;
	}

	public ITradeServiceShelvingDetailService getTradeServiceShelvingDetailService() {
		return tradeServiceShelvingDetailService;
	}

	public void setTradeServiceShelvingDetailService(
			ITradeServiceShelvingDetailService tradeServiceShelvingDetailService) {
		this.tradeServiceShelvingDetailService = tradeServiceShelvingDetailService;
	}

	public Boolean submit(Long id) {
		return tradeServiceShelvingService.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		Boolean flag = false;
		if (tradeServiceShelvingService.audit(id, auditor)) {
			flag = accounted(id);
		}
		return flag;
	}

	public Boolean accounted(Long id) {
		Boolean flag = false;
		TradeServiceShelvingVO tradeServiceShelvingVO = getTradeServiceShelvingDomainByID(id);
		InventoryManageVO inventoryManageVO = initInventoryManage(tradeServiceShelvingVO);
		SimpleResultVO<Boolean> result = inventoryManageDomainClient
				.createInventoryManageDomain(inventoryManageVO);
		if (result.isSuccess() && result.getTarget()) {
			flag = tradeServiceShelvingService.accounted(id);
		}
		return flag;
	}

	private InventoryManageVO initInventoryManage(
			TradeServiceShelvingVO tradeServiceShelvingVO) {
		InventoryManageVO inventoryManageVO = new InventoryManageVO();
		inventoryManageVO.setAgencyId(tradeServiceShelvingVO.getAgencyId());
		inventoryManageVO.setAuditor(tradeServiceShelvingVO.getAuditor());
		inventoryManageVO.setAuditTime(tradeServiceShelvingVO.getAuditTime());
		inventoryManageVO.setOperator(tradeServiceShelvingVO.getOperate());
		inventoryManageVO.setOpReason(TrackConstants.IM_REASON_IN_TRADESERVICE);
		inventoryManageVO.setOpTime(tradeServiceShelvingVO.getOpTime());
		inventoryManageVO.setOpType(2);
		List<InventoryManageDetailVO> details = new LinkedList<InventoryManageDetailVO>();
		for (int i = 0; i < tradeServiceShelvingVO.getDetails().size(); i++) {
			InventoryManageDetailVO inventoryManageDetailVO = new InventoryManageDetailVO();
			inventoryManageDetailVO.setNumber(tradeServiceShelvingVO
					.getDetails().get(i).getShelveNoReal());
			inventoryManageDetailVO.setSaleUnit(tradeServiceShelvingVO
					.getDetails().get(i).getSaleUnit());
			inventoryManageDetailVO.setSaleUnitId(tradeServiceShelvingVO
					.getDetails().get(i).getSaleUnitId());
			inventoryManageDetailVO.setSpec(tradeServiceShelvingVO.getDetails()
					.get(i).getSpec());
			inventoryManageDetailVO.setSpecUnit(tradeServiceShelvingVO
					.getDetails().get(i).getSpecUnit());
			inventoryManageDetailVO.setSpecUnitId(tradeServiceShelvingVO
					.getDetails().get(i).getSpecUnitId());
			inventoryManageDetailVO.setSaleUnitType(tradeServiceShelvingVO
					.getDetails().get(i).getSaleUnitType());
			inventoryManageDetailVO.setSkuName(tradeServiceShelvingVO
					.getDetails().get(i).getSkuName());
			inventoryManageDetailVO.setPositionCode(tradeServiceShelvingVO
					.getDetails().get(i).getPositionCode());
			inventoryManageDetailVO.setSkuId(tradeServiceShelvingVO
					.getDetails().get(i).getSkuId());
			inventoryManageDetailVO.setPositionId(tradeServiceShelvingVO
					.getDetails().get(i).getPositionId());
			details.add(inventoryManageDetailVO);
		}
		inventoryManageVO.setDetails(details);
		return inventoryManageVO;
	}

	public IInventoryManageDomainClient getInventoryManageDomainClient() {
		return inventoryManageDomainClient;
	}

	public void setInventoryManageDomainClient(
			IInventoryManageDomainClient inventoryManageDomainClient) {
		this.inventoryManageDomainClient = inventoryManageDomainClient;
	}

	public SkuService getSkuService() {
		return skuService;
	}

	public void setSkuService(SkuService skuService) {
		this.skuService = skuService;
	}

	public ValueService getValueService() {
		return valueService;
	}

	public void setValueService(ValueService valueService) {
		this.valueService = valueService;
	}

	public IPositionDomainClient getPositionDomainClient() {
		return positionDomainClient;
	}

	public void setPositionDomainClient(
			IPositionDomainClient positionDomainClient) {
		this.positionDomainClient = positionDomainClient;
	}
}
