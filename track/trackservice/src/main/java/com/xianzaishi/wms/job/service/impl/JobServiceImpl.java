package com.xianzaishi.wms.job.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.job.manage.itf.IJobManage;
import com.xianzaishi.wms.job.service.itf.IJobService;
import com.xianzaishi.wms.job.vo.JobQueryVO;
import com.xianzaishi.wms.job.vo.JobVO;

public class JobServiceImpl implements IJobService {
	@Autowired
	private IJobManage jobManage = null;

	public IJobManage getJobManage() {
		return jobManage;
	}

	public void setJobManage(IJobManage jobManage) {
		this.jobManage = jobManage;
	}

	public Boolean addJobVO(JobVO jobVO) {
		jobManage.addJobVO(jobVO);
		return true;
	}

	public List<JobVO> queryJobVOList(JobQueryVO jobQueryVO) {
		return jobManage.queryJobVOList(jobQueryVO);
	}

	public JobVO getJobVOByID(Long id) {
		return (JobVO) jobManage.getJobVOByID(id);
	}

	public Boolean modifyJobVO(JobVO jobVO) {
		return jobManage.modifyJobVO(jobVO);
	}

	public Boolean deleteJobVOByID(Long id) {
		return jobManage.deleteJobVOByID(id);
	}

	public Integer queryJobVOCount(JobQueryVO jobQueryVO) {
		return jobManage.queryJobVOCount(jobQueryVO);
	}

	public Boolean processed(Long jobID) {
		JobVO jobVO = new JobVO();
		jobVO.setId(jobID);
		jobVO.setStatu(2);
		return jobManage.modifyJobVO(jobVO);
	}
}
