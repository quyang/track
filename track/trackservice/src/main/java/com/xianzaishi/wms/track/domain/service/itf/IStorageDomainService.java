package com.xianzaishi.wms.track.domain.service.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.track.vo.StorageDetailQueryVO;
import com.xianzaishi.wms.track.vo.StorageDetailVO;
import com.xianzaishi.wms.track.vo.StorageQueryVO;
import com.xianzaishi.wms.track.vo.StorageVO;

public interface IStorageDomainService {

	// domain
	public Boolean auditStorage(Long storageID, Long operate);

	public Boolean createStorageDomain(StorageVO storageVO);

	public StorageVO getStorageDomainByID(Long storageID);

	public Boolean deleteStorageDomain(Long id);

	public Boolean createAndAccount(StorageVO storageVO);

	// head
	public Boolean addStorageVO(StorageVO storageVO);

	public QueryResultVO<StorageVO> queryStorageVOList(
			StorageQueryVO storageQueryVO);

	public StorageVO getStorageVOByID(Long id);

	public Boolean modifyStorageVO(StorageVO storageVO);

	public Boolean deleteStorageVO(Long storageID);

	// body
	public Boolean createStorageDetailVO(StorageDetailVO storageDetailVO);

	public Boolean batchCreateStorageDetailVO(
			List<StorageDetailVO> storageDetailVOs);

	public List<StorageDetailVO> queryStorageDetailVOList(
			StorageDetailQueryVO storageDetailQueryVO);

	public List<StorageDetailVO> getStorageDetailVOListByStorageID(
			Long storageID);

	public StorageDetailVO getStorageDetailVOByID(Long id);

	public Boolean modifyStorageDetailVO(StorageDetailVO storageDetailVO);

	public Boolean batchModifyStorageDetailVOs(
			List<StorageDetailVO> storageDetailVOs);

	public Boolean deleteStorageDetailVO(Long id);

	public Boolean deleteStorageDetailVOByStorageID(Long storageID);

	public Boolean batchDeleteStorageDetailVOByStorageID(
			List<Long> storyDetailIDs);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

	public Boolean accounted(Long id);

	/**
	 * 和createAndAccount方法一样，只不过返回的是某次入库id
	 * @param storageVO
	 * @return
	 */
	public Long createAndAccountGetStoreId(StorageVO storageVO);



}
