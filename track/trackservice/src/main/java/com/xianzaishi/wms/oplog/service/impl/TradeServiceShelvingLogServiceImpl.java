package com.xianzaishi.wms.oplog.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.oplog.manage.itf.ITradeServiceShelvingLogManage;
import com.xianzaishi.wms.oplog.service.itf.ITradeServiceShelvingLogService;
import com.xianzaishi.wms.oplog.vo.TradeServiceShelvingLogQueryVO;
import com.xianzaishi.wms.oplog.vo.TradeServiceShelvingLogVO;

public class TradeServiceShelvingLogServiceImpl implements
		ITradeServiceShelvingLogService {
	@Autowired
	private ITradeServiceShelvingLogManage tradeServiceShelvingLogManage = null;

	public ITradeServiceShelvingLogManage getTradeServiceShelvingLogManage() {
		return tradeServiceShelvingLogManage;
	}

	public void setTradeServiceShelvingLogManage(
			ITradeServiceShelvingLogManage tradeServiceShelvingLogManage) {
		this.tradeServiceShelvingLogManage = tradeServiceShelvingLogManage;
	}

	public Boolean addTradeServiceShelvingLogVO(
			TradeServiceShelvingLogVO tradeServiceShelvingLogVO) {
		tradeServiceShelvingLogManage
				.addTradeServiceShelvingLogVO(tradeServiceShelvingLogVO);
		return true;
	}

	public List<TradeServiceShelvingLogVO> queryTradeServiceShelvingLogVOList(
			TradeServiceShelvingLogQueryVO tradeServiceShelvingLogQueryVO) {
		return tradeServiceShelvingLogManage
				.queryTradeServiceShelvingLogVOList(tradeServiceShelvingLogQueryVO);
	}

	public TradeServiceShelvingLogVO getTradeServiceShelvingLogVOByID(Long id) {
		return (TradeServiceShelvingLogVO) tradeServiceShelvingLogManage
				.getTradeServiceShelvingLogVOByID(id);
	}

	public Boolean modifyTradeServiceShelvingLogVO(
			TradeServiceShelvingLogVO tradeServiceShelvingLogVO) {
		return tradeServiceShelvingLogManage
				.modifyTradeServiceShelvingLogVO(tradeServiceShelvingLogVO);
	}

	public Boolean deleteTradeServiceShelvingLogVOByID(Long id) {
		return tradeServiceShelvingLogManage
				.deleteTradeServiceShelvingLogVOByID(id);
	}

	public Boolean batchAddTradeServiceShelvingLog(
			List<TradeServiceShelvingLogVO> tradeServiceShelvingLogVOs) {
		return tradeServiceShelvingLogManage
				.batchAddTradeServiceShelvingLog(tradeServiceShelvingLogVOs);
	}
}
