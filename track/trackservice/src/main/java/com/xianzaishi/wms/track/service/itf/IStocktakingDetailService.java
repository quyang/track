package com.xianzaishi.wms.track.service.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.StocktakingDetailQueryVO;
import com.xianzaishi.wms.track.vo.StocktakingDetailVO;

public interface IStocktakingDetailService {

	public Boolean addStocktakingDetailVO(
			StocktakingDetailVO stocktakingDetailVO);

	public List<StocktakingDetailVO> queryStocktakingDetailVOList(
			StocktakingDetailQueryVO stocktakingDetailQueryVO);

	public StocktakingDetailVO getStocktakingDetailVOByID(Long id);

	public Boolean modifyStocktakingDetailVO(
			StocktakingDetailVO stocktakingDetailVO);

	public Boolean deleteStocktakingDetailVOByID(Long id);

	public List<StocktakingDetailVO> getStocktakingDetailVOByStocktakingID(
			Long id);

	public Boolean batchAddStocktakingDetailVO(
			List<StocktakingDetailVO> stocktakingDetailVOs);

	public Boolean batchModifyStocktakingDetailVO(
			List<StocktakingDetailVO> stocktakingDetailVOs);

	public Boolean batchDeleteStocktakingDetailVO(
			List<StocktakingDetailVO> stocktakingDetailVOs);

	public Boolean batchDeleteStocktakingDetailVOByID(List<Long> storyDetailIDs);
}