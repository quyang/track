package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.IAdjustDao;
import com.xianzaishi.wms.track.manage.itf.IAdjustManage;
import com.xianzaishi.wms.track.vo.AdjustQueryVO;
import com.xianzaishi.wms.track.vo.AdjustVO;

public class AdjustManageImpl implements IAdjustManage {
	@Autowired
	private IAdjustDao adjustDao = null;

	private void validate(AdjustVO adjustVO) {
		if (adjustVO.getAgencyId() == null || adjustVO.getAgencyId() <= 0) {
			throw new BizException("agencyID error：" + adjustVO.getAgencyId());
		}
		if (adjustVO.getOperate() == null || adjustVO.getOperate() <= 0) {
			throw new BizException("operator error：" + adjustVO.getOperate());
		}
		if (adjustVO.getOpReason() == null || adjustVO.getOpReason() <= 0) {
			adjustVO.setOpReason(12);
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IAdjustDao getAdjustDao() {
		return adjustDao;
	}

	public void setAdjustDao(IAdjustDao adjustDao) {
		this.adjustDao = adjustDao;
	}

	public Long addAdjustVO(AdjustVO adjustVO) {
		validate(adjustVO);
		return (Long) adjustDao.addDO(adjustVO);
	}

	public List<AdjustVO> queryAdjustVOList(AdjustQueryVO adjustQueryVO) {
		return adjustDao.queryDO(adjustQueryVO);
	}

	public AdjustVO getAdjustVOByID(Long id) {
		return (AdjustVO) adjustDao.getDOByID(id);
	}

	public Boolean modifyAdjustVO(AdjustVO adjustVO) {
		return adjustDao.updateDO(adjustVO);
	}

	public Boolean deleteAdjustVOByID(Long id) {
		return adjustDao.delDO(id);
	}

	public Boolean submit(Long id) {
		return adjustDao.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return adjustDao.audit(id, auditor);
	}

	public Boolean accounted(Long id) {
		return adjustDao.accounted(id);
	}

	public Integer queryAdjustVOCount(AdjustQueryVO adjustQueryVO) {
		return adjustDao.queryCount(adjustQueryVO);
	}
}
