package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.IBookingDeliveryOrderDao;
import com.xianzaishi.wms.track.manage.itf.IBookingDeliveryOrderManage;
import com.xianzaishi.wms.track.vo.BookingDeliveryOrderQueryVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryOrderVO;

public class BookingDeliveryOrderManageImpl implements
		IBookingDeliveryOrderManage {
	@Autowired
	private IBookingDeliveryOrderDao bookingDeliveryOrderDao = null;

	private void validate(BookingDeliveryOrderVO bookingDeliveryOrderVO) {
		if (bookingDeliveryOrderVO.getOrderId() == null
				|| bookingDeliveryOrderVO.getOrderId() <= 0) {
			throw new BizException("deliveryID error："
					+ bookingDeliveryOrderVO.getOrderId());
		}
		if (bookingDeliveryOrderVO.getDeliveryId() == null
				|| bookingDeliveryOrderVO.getDeliveryId() <= 0) {
			throw new BizException("orderID error："
					+ bookingDeliveryOrderVO.getOrderId());
		}
		if (bookingDeliveryOrderVO.getAllRemain() != null
				&& bookingDeliveryOrderVO.getAllRemain() != 0) {
			throw new BizException("allRemain error："
					+ bookingDeliveryOrderVO.getAllRemain());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IBookingDeliveryOrderDao getBookingDeliveryOrderDao() {
		return bookingDeliveryOrderDao;
	}

	public void setBookingDeliveryOrderDao(
			IBookingDeliveryOrderDao bookingDeliveryOrderDao) {
		this.bookingDeliveryOrderDao = bookingDeliveryOrderDao;
	}

	public Boolean addBookingDeliveryOrderVO(
			BookingDeliveryOrderVO bookingDeliveryOrderVO) {
		validate(bookingDeliveryOrderVO);
		bookingDeliveryOrderDao.addDO(bookingDeliveryOrderVO);
		return true;
	}

	public List<BookingDeliveryOrderVO> queryBookingDeliveryOrderVOList(
			BookingDeliveryOrderQueryVO bookingDeliveryOrderQueryVO) {
		return bookingDeliveryOrderDao.queryDO(bookingDeliveryOrderQueryVO);
	}

	public BookingDeliveryOrderVO getBookingDeliveryOrderVOByID(Long id) {
		return (BookingDeliveryOrderVO) bookingDeliveryOrderDao.getDOByID(id);
	}

	public Boolean modifyBookingDeliveryOrderVO(
			BookingDeliveryOrderVO bookingDeliveryOrderVO) {
		return bookingDeliveryOrderDao.updateDO(bookingDeliveryOrderVO);
	}

	public Boolean deleteBookingDeliveryOrderVOByID(Long id) {
		return bookingDeliveryOrderDao.delDO(id);
	}

	public List<BookingDeliveryOrderVO> getBookingDeliveryOrderVOByOrderStorageID(
			Long id) {
		BookingDeliveryOrderQueryVO queryVO = new BookingDeliveryOrderQueryVO();
		queryVO.setDeliveryId(id);
		queryVO.setSize(Integer.MAX_VALUE);
		return bookingDeliveryOrderDao.queryDO(queryVO);
	}

	public Boolean batchAddBookingDeliveryOrderVO(
			List<BookingDeliveryOrderVO> bookingDeliveryOrderVOs) {
		return bookingDeliveryOrderDao.batchAddDO(bookingDeliveryOrderVOs);
	}

	public Boolean batchModifyBookingDeliveryOrderVO(
			List<BookingDeliveryOrderVO> bookingDeliveryOrderVOs) {
		return bookingDeliveryOrderDao.batchModifyDO(bookingDeliveryOrderVOs);
	}

	public Boolean batchDeleteBookingDeliveryOrderVO(
			List<BookingDeliveryOrderVO> bookingDeliveryOrderVOs) {
		return bookingDeliveryOrderDao.batchDeleteDO(bookingDeliveryOrderVOs);
	}

	public Boolean batchDeleteBookingDeliveryOrderVOByID(
			List<Long> bookingDeliveryOrderIDs) {
		return bookingDeliveryOrderDao
				.batchDeleteDOByID(bookingDeliveryOrderIDs);
	}
}
