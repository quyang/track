package com.xianzaishi.wms.track.manage.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.SpoilageDetailQueryVO;
import com.xianzaishi.wms.track.vo.SpoilageDetailVO;

public interface ISpoilageDetailManage {

	public Boolean addSpoilageDetailVO(SpoilageDetailVO spoilageDetailVO);

	public List<SpoilageDetailVO> querySpoilageDetailVOList(
			SpoilageDetailQueryVO spoilageDetailQueryVO);

	public SpoilageDetailVO getSpoilageDetailVOByID(Long id);

	public Boolean modifySpoilageDetailVO(SpoilageDetailVO spoilageDetailVO);

	public Boolean deleteSpoilageDetailVOByID(Long id);

	public List<SpoilageDetailVO> getSpoilageDetailVOBySpoilageID(Long id);

	public Boolean batchAddSpoilageDetailVO(
			List<SpoilageDetailVO> spoilageDetailVOs);

	public Boolean batchModifySpoilageDetailVO(
			List<SpoilageDetailVO> spoilageDetailVOs);

	public Boolean batchDeleteSpoilageDetailVO(
			List<SpoilageDetailVO> spoilageDetailVOs);

	public Boolean batchDeleteSpoilageDetailVOByID(List<Long> storyDetailIDs);

}