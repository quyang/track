package com.xianzaishi.wms.track.manage.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.OrderStorageDetailQueryVO;
import com.xianzaishi.wms.track.vo.OrderStorageDetailVO;

public interface IOrderStorageDetailManage {

	public Boolean addOrderStorageDetailVO(
			OrderStorageDetailVO orderStorageDetailVO);

	public List<OrderStorageDetailVO> queryOrderStorageDetailVOList(
			OrderStorageDetailQueryVO orderStorageDetailQueryVO);

	public OrderStorageDetailVO getOrderStorageDetailVOByID(Long id);

	public Boolean modifyOrderStorageDetailVO(
			OrderStorageDetailVO orderStorageDetailVO);

	public Boolean deleteOrderStorageDetailVOByID(Long id);

	public List<OrderStorageDetailVO> getOrderStorageDetailVOByOrderStorageID(
			Long id);

	public Boolean batchAddOrderStorageDetailVO(
			List<OrderStorageDetailVO> orderStorageDetailVOs);

	public Boolean batchModifyOrderStorageDetailVO(
			List<OrderStorageDetailVO> orderStorageDetailVOs);

	public Boolean batchDeleteOrderStorageDetailVO(
			List<OrderStorageDetailVO> orderStorageDetailVOs);

	public Boolean batchDeleteOrderStorageDetailVOByID(
			List<Long> orderStorageDetailIDs);

}