package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.manage.itf.IBookingDeliveryManage;
import com.xianzaishi.wms.track.service.itf.IBookingDeliveryService;
import com.xianzaishi.wms.track.vo.BookingDeliveryQueryVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryVO;

public class BookingDeliveryServiceImpl implements IBookingDeliveryService {
	@Autowired
	private IBookingDeliveryManage bookingDeliveryManage = null;

	public IBookingDeliveryManage getBookingDeliveryManage() {
		return bookingDeliveryManage;
	}

	public void setBookingDeliveryManage(
			IBookingDeliveryManage bookingDeliveryManage) {
		this.bookingDeliveryManage = bookingDeliveryManage;
	}

	public Long addBookingDeliveryVO(BookingDeliveryVO bookingDeliveryVO) {
		return bookingDeliveryManage.addBookingDeliveryVO(bookingDeliveryVO);
	}

	public List<BookingDeliveryVO> queryBookingDeliveryVOList(
			BookingDeliveryQueryVO bookingDeliveryQueryVO) {
		return bookingDeliveryManage
				.queryBookingDeliveryVOList(bookingDeliveryQueryVO);
	}

	public Integer queryBookingDeliveryVOCount(
			BookingDeliveryQueryVO bookingDeliveryQueryVO) {
		return bookingDeliveryManage
				.queryBookingDeliveryVOCount(bookingDeliveryQueryVO);
	}

	public BookingDeliveryVO getBookingDeliveryVOByID(Long id) {
		return (BookingDeliveryVO) bookingDeliveryManage
				.getBookingDeliveryVOByID(id);
	}

	public Boolean modifyBookingDeliveryVO(BookingDeliveryVO bookingDeliveryVO) {
		return bookingDeliveryManage.modifyBookingDeliveryVO(bookingDeliveryVO);
	}

	public Boolean deleteBookingDeliveryVOByID(Long id) {
		return bookingDeliveryManage.deleteBookingDeliveryVOByID(id);
	}

	public Boolean submit(Long id) {
		return bookingDeliveryManage.submit(id);
	}

	public Boolean confirm(Long id, Long auditor) {
		return bookingDeliveryManage.confirm(id, auditor);
	}
}
