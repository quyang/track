package com.xianzaishi.wms.oplog.domain.client.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.oplog.domain.client.itf.ITradeServiceShelvingLogDomainClient;
import com.xianzaishi.wms.oplog.domain.service.itf.ITradeServiceShelvingLogDomainService;
import com.xianzaishi.wms.oplog.vo.TradeServiceShelvingLogVO;

public class TradeServiceShelvingLogDomainClient implements
		ITradeServiceShelvingLogDomainClient {

	@Autowired
	private ITradeServiceShelvingLogDomainService tradeServiceShelvingLogDomainService = null;

	public SimpleResultVO<Boolean> batchAddTradeServiceShelvingLog(
			List<TradeServiceShelvingLogVO> tradeServiceShelvingLogVOs) {
		return SimpleResultVO
				.buildSuccessResult(tradeServiceShelvingLogDomainService
						.batchAddTradeServiceShelvingLog(tradeServiceShelvingLogVOs));
	}

	public ITradeServiceShelvingLogDomainService getTradeServiceShelvingLogDomainService() {
		return tradeServiceShelvingLogDomainService;
	}

	public void setTradeServiceShelvingLogDomainService(
			ITradeServiceShelvingLogDomainService tradeServiceShelvingLogDomainService) {
		this.tradeServiceShelvingLogDomainService = tradeServiceShelvingLogDomainService;
	}

}
