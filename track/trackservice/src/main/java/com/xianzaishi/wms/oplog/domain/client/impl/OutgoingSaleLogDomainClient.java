package com.xianzaishi.wms.oplog.domain.client.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.oplog.domain.client.itf.IOutgoingSaleLogDomainClient;
import com.xianzaishi.wms.oplog.domain.service.itf.IOutgoingSaleLogDomainService;
import com.xianzaishi.wms.oplog.vo.OutgoingSaleLogVO;

public class OutgoingSaleLogDomainClient implements
		IOutgoingSaleLogDomainClient {

	@Autowired
	private IOutgoingSaleLogDomainService outgoingSaleLogDomainService = null;

	public SimpleResultVO<Boolean> batchAddOutgoingSaleLog(
			List<OutgoingSaleLogVO> outgoingSaleLogVOs) {
		return SimpleResultVO.buildSuccessResult(outgoingSaleLogDomainService
				.batchAddOutgoingSaleLog(outgoingSaleLogVOs));
	}

	public IOutgoingSaleLogDomainService getOutgoingSaleLogDomainService() {
		return outgoingSaleLogDomainService;
	}

	public void setOutgoingSaleLogDomainService(
			IOutgoingSaleLogDomainService outgoingSaleLogDomainService) {
		this.outgoingSaleLogDomainService = outgoingSaleLogDomainService;
	}

}
