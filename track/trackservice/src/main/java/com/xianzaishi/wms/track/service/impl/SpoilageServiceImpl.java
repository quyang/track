package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.vo.SpoilageVO;
import com.xianzaishi.wms.track.vo.SpoilageQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.service.itf.ISpoilageService;
import com.xianzaishi.wms.track.manage.itf.ISpoilageManage;

public class SpoilageServiceImpl implements ISpoilageService {
	@Autowired
	private ISpoilageManage spoilageManage = null;

	public ISpoilageManage getSpoilageManage() {
		return spoilageManage;
	}

	public void setSpoilageManage(ISpoilageManage spoilageManage) {
		this.spoilageManage = spoilageManage;
	}

	public Long addSpoilageVO(SpoilageVO spoilageVO) {
		return spoilageManage.addSpoilageVO(spoilageVO);
	}

	public List<SpoilageVO> querySpoilageVOList(SpoilageQueryVO spoilageQueryVO) {
		return spoilageManage.querySpoilageVOList(spoilageQueryVO);
	}

	public Integer querySpoilageVOCount(SpoilageQueryVO spoilageQueryVO) {
		return spoilageManage.querySpoilageVOCount(spoilageQueryVO);
	}

	public SpoilageVO getSpoilageVOByID(Long id) {
		return (SpoilageVO) spoilageManage.getSpoilageVOByID(id);
	}

	public Boolean modifySpoilageVO(SpoilageVO spoilageVO) {
		return spoilageManage.modifySpoilageVO(spoilageVO);
	}

	public Boolean deleteSpoilageVOByID(Long id) {
		return spoilageManage.deleteSpoilageVOByID(id);
	}

	public Boolean submit(Long id) {
		return spoilageManage.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return spoilageManage.audit(id, auditor);
	}

	public Boolean accounted(Long id) {
		return spoilageManage.accounted(id);
	}
}
