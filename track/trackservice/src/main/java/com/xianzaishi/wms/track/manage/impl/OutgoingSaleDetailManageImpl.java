package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.IOutgoingSaleDetailDao;
import com.xianzaishi.wms.track.manage.itf.IOutgoingSaleDetailManage;
import com.xianzaishi.wms.track.vo.OutgoingSaleDetailQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingSaleDetailVO;

public class OutgoingSaleDetailManageImpl implements IOutgoingSaleDetailManage {
	@Autowired
	private IOutgoingSaleDetailDao outgoingSaleDetailDao = null;

	private void validate(OutgoingSaleDetailVO outgoingSaleDetailVO) {
		if (outgoingSaleDetailVO.getOutgoingSaleId() == null
				|| outgoingSaleDetailVO.getOutgoingSaleId() <= 0) {
			throw new BizException("outgoingSaleID error："
					+ outgoingSaleDetailVO.getOutgoingSaleId());
		}
		if (outgoingSaleDetailVO.getPositionId() == null
				|| outgoingSaleDetailVO.getPositionId() <= 0) {
			throw new BizException("positionID error："
					+ outgoingSaleDetailVO.getPositionId());
		}
		if (outgoingSaleDetailVO.getSkuId() == null
				|| outgoingSaleDetailVO.getSkuId() <= 0) {
			throw new BizException("outgoingSaleID error："
					+ outgoingSaleDetailVO.getSkuId());
		}
		if (outgoingSaleDetailVO.getNumberReal() == null
				|| outgoingSaleDetailVO.getNumberReal() <= 0) {
			throw new BizException("number error："
					+ outgoingSaleDetailVO.getNumberReal());
		}
		if (outgoingSaleDetailVO.getSkuName() == null
				|| outgoingSaleDetailVO.getSkuName().isEmpty()) {
			throw new BizException("skuName error："
					+ outgoingSaleDetailVO.getSkuName());
		}
		if (outgoingSaleDetailVO.getPositionCode() == null
				|| outgoingSaleDetailVO.getPositionCode().isEmpty()) {
			throw new BizException("positionCode error："
					+ outgoingSaleDetailVO.getPositionCode());
		}
		if (outgoingSaleDetailVO.getSaleUnit() == null
				|| outgoingSaleDetailVO.getSaleUnit().isEmpty()) {
			throw new BizException("saleUnit error："
					+ outgoingSaleDetailVO.getSaleUnit());
		}
		if (outgoingSaleDetailVO.getSaleUnitId() == null
				|| outgoingSaleDetailVO.getSaleUnitId() <= 0) {
			throw new BizException("saleUnitId error："
					+ outgoingSaleDetailVO.getSaleUnitId());
		}
		if (outgoingSaleDetailVO.getSaleUnitType() == null) {
			throw new BizException("saleUnitType error："
					+ outgoingSaleDetailVO.getSaleUnitType());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IOutgoingSaleDetailDao getOutgoingSaleDetailDao() {
		return outgoingSaleDetailDao;
	}

	public void setOutgoingSaleDetailDao(
			IOutgoingSaleDetailDao outgoingSaleDetailDao) {
		this.outgoingSaleDetailDao = outgoingSaleDetailDao;
	}

	public Boolean addOutgoingSaleDetailVO(
			OutgoingSaleDetailVO outgoingSaleDetailVO) {
		validate(outgoingSaleDetailVO);
		outgoingSaleDetailDao.addDO(outgoingSaleDetailVO);
		return true;
	}

	public List<OutgoingSaleDetailVO> queryOutgoingSaleDetailVOList(
			OutgoingSaleDetailQueryVO outgoingSaleDetailQueryVO) {
		return outgoingSaleDetailDao.queryDO(outgoingSaleDetailQueryVO);
	}

	public OutgoingSaleDetailVO getOutgoingSaleDetailVOByID(Long id) {
		return (OutgoingSaleDetailVO) outgoingSaleDetailDao.getDOByID(id);
	}

	public Boolean modifyOutgoingSaleDetailVO(
			OutgoingSaleDetailVO outgoingSaleDetailVO) {
		return outgoingSaleDetailDao.updateDO(outgoingSaleDetailVO);
	}

	public Boolean deleteOutgoingSaleDetailVOByID(Long id) {
		return outgoingSaleDetailDao.delDO(id);
	}

	public List<OutgoingSaleDetailVO> getOutgoingSaleDetailVOByOutgoingSaleID(
			Long id) {
		OutgoingSaleDetailQueryVO queryVO = new OutgoingSaleDetailQueryVO();
		queryVO.setOutgoingSaleId(id);
		queryVO.setSize(Integer.MAX_VALUE);
		return outgoingSaleDetailDao.queryDO(queryVO);
	}

	public Boolean batchAddOutgoingSaleDetailVO(
			List<OutgoingSaleDetailVO> outgoingSaleDetailVOs) {
		return outgoingSaleDetailDao.batchAddDO(outgoingSaleDetailVOs);
	}

	public Boolean batchModifyOutgoingSaleDetailVO(
			List<OutgoingSaleDetailVO> outgoingSaleDetailVOs) {
		return outgoingSaleDetailDao.batchModifyDO(outgoingSaleDetailVOs);
	}

	public Boolean batchDeleteOutgoingSaleDetailVO(
			List<OutgoingSaleDetailVO> outgoingSaleDetailVOs) {
		return outgoingSaleDetailDao.batchDeleteDO(outgoingSaleDetailVOs);
	}

	public Boolean batchDeleteOutgoingSaleDetailVOByID(
			List<Long> outgoingSaleDetailIDs) {
		return outgoingSaleDetailDao.batchDeleteDOByID(outgoingSaleDetailIDs);
	}
}
