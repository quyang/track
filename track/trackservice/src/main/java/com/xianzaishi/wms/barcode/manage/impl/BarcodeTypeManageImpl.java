package com.xianzaishi.wms.barcode.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.barcode.vo.BarcodeTypeVO;
import com.xianzaishi.wms.barcode.vo.BarcodeTypeDO;
import com.xianzaishi.wms.barcode.vo.BarcodeTypeQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.barcode.manage.itf.IBarcodeTypeManage;

import com.xianzaishi.wms.barcode.dao.itf.IBarcodeTypeDao;

public class BarcodeTypeManageImpl implements IBarcodeTypeManage {
	@Autowired
	private IBarcodeTypeDao barcodeTypeDao = null;

	private void validate(BarcodeTypeDO barcodeTypeDO) {
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}
	
	public IBarcodeTypeDao getBarcodeTypeDao() {
		return barcodeTypeDao;
	}

	public void setBarcodeTypeDao(IBarcodeTypeDao barcodeTypeDao) {
		this.barcodeTypeDao = barcodeTypeDao;
	}
	
	public Boolean addBarcodeTypeVO(BarcodeTypeVO barcodeTypeVO) {
		barcodeTypeDao.addDO(barcodeTypeVO);
		return true;
	}

	public List<BarcodeTypeVO> queryBarcodeTypeVOList(BarcodeTypeQueryVO barcodeTypeQueryVO) {
		return barcodeTypeDao.queryDO(barcodeTypeQueryVO);
	}

	public BarcodeTypeVO getBarcodeTypeVOByID(Long id) {
		return (BarcodeTypeVO) barcodeTypeDao.getDOByID(id);
	}

	public Boolean modifyBarcodeTypeVO(BarcodeTypeVO barcodeTypeVO) {
		return barcodeTypeDao.updateDO(barcodeTypeVO);
	}

	public Boolean deleteBarcodeTypeVOByID(Long id) {
		return barcodeTypeDao.delDO(id);
	}
}
