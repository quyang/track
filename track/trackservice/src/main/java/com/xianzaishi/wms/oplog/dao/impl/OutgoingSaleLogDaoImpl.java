package com.xianzaishi.wms.oplog.dao.impl;

import java.util.List;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.oplog.dao.itf.IOutgoingSaleLogDao;
import com.xianzaishi.wms.oplog.vo.OutgoingSaleLogVO;

public class OutgoingSaleLogDaoImpl extends BaseDaoAdapter implements
		IOutgoingSaleLogDao {
	public String getVOClassName() {
		return "OutgoingSaleLogDO";
	}

	public Boolean batchAddOutgoingSaleLog(
			List<OutgoingSaleLogVO> outgoingSaleLogVOs) {
		for (int i = 0; i < outgoingSaleLogVOs.size(); i++) {
			addDO(outgoingSaleLogVOs.get(i));
		}
		return true;
	}
}
