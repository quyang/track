package com.xianzaishi.wms.track.domain.client.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.domain.client.itf.IInspectionDomainClient;
import com.xianzaishi.wms.track.domain.service.itf.IInspectionDomainService;
import com.xianzaishi.wms.track.vo.InspectionDetailQueryVO;
import com.xianzaishi.wms.track.vo.InspectionDetailVO;
import com.xianzaishi.wms.track.vo.InspectionQueryVO;
import com.xianzaishi.wms.track.vo.InspectionVO;

public class InspectionDomainClient implements IInspectionDomainClient {
	private static final Logger logger = Logger
			.getLogger(InspectionDomainClient.class);
	@Autowired
	private IInspectionDomainService inspectionDomainService = null;

	public SimpleResultVO<Boolean> createInspectionDomain(
			InspectionVO inspectionVO) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(inspectionDomainService
					.createInspectionDomain(inspectionVO));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<InspectionVO> getInspectionDomainByID(
			Long inspectionID) {
		SimpleResultVO<InspectionVO> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(inspectionDomainService
					.getInspectionDomainByID(inspectionID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> deleteInspectionDomain(Long inspectionID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(inspectionDomainService
					.deleteInspectionDomain(inspectionID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> addInspectionVO(InspectionVO inspectionVO) {
		return SimpleResultVO.buildSuccessResult(inspectionDomainService
				.addInspectionVO(inspectionVO));
	}

	public SimpleResultVO<QueryResultVO<InspectionVO>> queryInspectionVOList(
			InspectionQueryVO inspectionQueryVO) {
		return SimpleResultVO.buildSuccessResult(inspectionDomainService
				.queryInspectionVOList(inspectionQueryVO));
	}

	public SimpleResultVO<InspectionVO> getInspectionVOByID(Long id) {
		return SimpleResultVO.buildSuccessResult(inspectionDomainService
				.getInspectionVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyInspectionVO(InspectionVO inspectionVO) {
		return SimpleResultVO.buildSuccessResult(inspectionDomainService
				.modifyInspectionVO(inspectionVO));
	}

	public SimpleResultVO<Boolean> deleteInspectionVO(Long id) {
		return SimpleResultVO.buildSuccessResult(inspectionDomainService
				.deleteInspectionVO(id));
	}

	public SimpleResultVO<Boolean> createInspectionDetailVO(
			InspectionDetailVO inspectionDetailVO) {
		return SimpleResultVO.buildSuccessResult(inspectionDomainService
				.createInspectionDetailVO(inspectionDetailVO));
	}

	public SimpleResultVO<Boolean> batchCreateInspectionDetailVO(
			List<InspectionDetailVO> inspectionDetailVOs) {
		return SimpleResultVO.buildSuccessResult(inspectionDomainService
				.batchCreateInspectionDetailVO(inspectionDetailVOs));
	}

	public SimpleResultVO<List<InspectionDetailVO>> queryInspectionDetailVOList(
			InspectionDetailQueryVO inspectionDetailQueryVO) {
		return SimpleResultVO.buildSuccessResult(inspectionDomainService
				.queryInspectionDetailVOList(inspectionDetailQueryVO));
	}

	public SimpleResultVO<List<InspectionDetailVO>> getInspectionDetailVOListByInspectionID(
			Long inspectionID) {
		return SimpleResultVO.buildSuccessResult(inspectionDomainService
				.getInspectionDetailVOListByInspectionID(inspectionID));
	}

	public SimpleResultVO<InspectionDetailQueryVO> getInspectionDetailVOByID(
			Long id) {
		return SimpleResultVO.buildSuccessResult(inspectionDomainService
				.getInspectionDetailVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyInspectionDetailVO(
			InspectionDetailVO inspectionDetailVO) {
		return SimpleResultVO.buildSuccessResult(inspectionDomainService
				.modifyInspectionDetailVO(inspectionDetailVO));
	}

	public SimpleResultVO<Boolean> batchModifyInspectionDetailVOs(
			List<InspectionDetailVO> inspectionDetailVOs) {
		return SimpleResultVO.buildSuccessResult(inspectionDomainService
				.batchModifyInspectionDetailVOs(inspectionDetailVOs));
	}

	public SimpleResultVO<Boolean> deleteInspectionDetailVO(Long id) {
		return SimpleResultVO.buildSuccessResult(inspectionDomainService
				.deleteInspectionDetailVO(id));
	}

	public SimpleResultVO<Boolean> deleteInspectionDetailVOByInspectionID(
			Long inspectionID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(inspectionDomainService
					.deleteInspectionDetailVOByInspectionID(inspectionID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> batchDeleteInspectionDetailVOByInspectionID(
			List<Long> storyDetailIDs) {
		return SimpleResultVO.buildSuccessResult(inspectionDomainService
				.batchDeleteInspectionDetailVOByInspectionID(storyDetailIDs));
	}

	public IInspectionDomainService getInspectionDomainService() {
		return inspectionDomainService;
	}

	public void setInspectionDomainService(
			IInspectionDomainService inspectionDomainService) {
		this.inspectionDomainService = inspectionDomainService;
	}

	public SimpleResultVO<Boolean> submit(Long id) {
		return SimpleResultVO.buildSuccessResult(inspectionDomainService
				.submit(id));
	}

	public SimpleResultVO<Boolean> audit(Long id, Long auditor) {
		return SimpleResultVO.buildSuccessResult(inspectionDomainService.audit(
				id, auditor));
	}

}
