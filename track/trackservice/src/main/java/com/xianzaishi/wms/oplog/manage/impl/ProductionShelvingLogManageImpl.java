package com.xianzaishi.wms.oplog.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.oplog.dao.itf.IProductionShelvingLogDao;
import com.xianzaishi.wms.oplog.manage.itf.IProductionShelvingLogManage;
import com.xianzaishi.wms.oplog.vo.ProductionShelvingLogDO;
import com.xianzaishi.wms.oplog.vo.ProductionShelvingLogQueryVO;
import com.xianzaishi.wms.oplog.vo.ProductionShelvingLogVO;

public class ProductionShelvingLogManageImpl implements
		IProductionShelvingLogManage {
	@Autowired
	private IProductionShelvingLogDao productionShelvingLogDao = null;

	private void validate(ProductionShelvingLogDO productionShelvingLogDO) {
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IProductionShelvingLogDao getProductionShelvingLogDao() {
		return productionShelvingLogDao;
	}

	public void setProductionShelvingLogDao(
			IProductionShelvingLogDao productionShelvingLogDao) {
		this.productionShelvingLogDao = productionShelvingLogDao;
	}

	public Boolean addProductionShelvingLogVO(
			ProductionShelvingLogVO productionShelvingLogVO) {
		productionShelvingLogDao.addDO(productionShelvingLogVO);
		return true;
	}

	public List<ProductionShelvingLogVO> queryProductionShelvingLogVOList(
			ProductionShelvingLogQueryVO productionShelvingLogQueryVO) {
		return productionShelvingLogDao.queryDO(productionShelvingLogQueryVO);
	}

	public ProductionShelvingLogVO getProductionShelvingLogVOByID(Long id) {
		return (ProductionShelvingLogVO) productionShelvingLogDao.getDOByID(id);
	}

	public Boolean modifyProductionShelvingLogVO(
			ProductionShelvingLogVO productionShelvingLogVO) {
		return productionShelvingLogDao.updateDO(productionShelvingLogVO);
	}

	public Boolean deleteProductionShelvingLogVOByID(Long id) {
		return productionShelvingLogDao.delDO(id);
	}

	public Boolean batchAddProductionShelvingLog(
			List<ProductionShelvingLogVO> productionShelvingLogVOs) {
		return productionShelvingLogDao
				.batchAddProductionShelvingLog(productionShelvingLogVOs);
	}
}
