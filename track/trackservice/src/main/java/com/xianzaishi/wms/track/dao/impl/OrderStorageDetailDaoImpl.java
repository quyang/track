package com.xianzaishi.wms.track.dao.impl;

import com.xianzaishi.wms.track.dao.itf.IOrderStorageDetailDao;
import com.xianzaishi.wms.track.vo.OrderStorageDetailVO;
import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;

public class OrderStorageDetailDaoImpl extends BaseDaoAdapter implements IOrderStorageDetailDao {
public String getVOClassName() {
		return "OrderStorageDetailDO";
	}
}
