package com.xianzaishi.wms.track.manage.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.OutgoingSaleQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingSaleVO;

public interface IOutgoingSaleManage {

	public Long addOutgoingSaleVO(OutgoingSaleVO outgoingSaleVO);

	public List<OutgoingSaleVO> queryOutgoingSaleVOList(
			OutgoingSaleQueryVO outgoingSaleQueryVO);

	public Integer queryOutgoingSaleVOCount(
			OutgoingSaleQueryVO outgoingSaleQueryVO);

	public OutgoingSaleVO getOutgoingSaleVOByID(Long id);

	public Boolean modifyOutgoingSaleVO(OutgoingSaleVO outgoingSaleVO);

	public Boolean deleteOutgoingSaleVOByID(Long id);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

	public Boolean accounted(Long id);

}