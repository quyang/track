package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.IOrderStorageDao;
import com.xianzaishi.wms.track.manage.itf.IOrderStorageManage;
import com.xianzaishi.wms.track.vo.OrderStorageQueryVO;
import com.xianzaishi.wms.track.vo.OrderStorageVO;

public class OrderStorageManageImpl implements IOrderStorageManage {
	@Autowired
	private IOrderStorageDao orderStorageDao = null;

	private void validate(OrderStorageVO orderStorageVO) {
		if (orderStorageVO.getDeliveryId() == null
				|| orderStorageVO.getDeliveryId() <= 0) {
			throw new BizException("deliveryID error："
					+ orderStorageVO.getDeliveryId());
		}
		if (orderStorageVO.getAgencyId() == null
				|| orderStorageVO.getAgencyId() <= 0) {
			throw new BizException("agencyID error："
					+ orderStorageVO.getAgencyId());
		}
		if (orderStorageVO.getOperate() == null
				|| orderStorageVO.getOperate() <= 0) {
			throw new BizException("operator error："
					+ orderStorageVO.getOperate());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IOrderStorageDao getOrderStorageDao() {
		return orderStorageDao;
	}

	public void setOrderStorageDao(IOrderStorageDao orderStorageDao) {
		this.orderStorageDao = orderStorageDao;
	}

	public Long addOrderStorageVO(OrderStorageVO orderStorageVO) {
		validate(orderStorageVO);
		return (Long) orderStorageDao.addDO(orderStorageVO);
	}

	public List<OrderStorageVO> queryOrderStorageVOList(
			OrderStorageQueryVO orderStorageQueryVO) {
		return orderStorageDao.queryDO(orderStorageQueryVO);
	}

	public OrderStorageVO getOrderStorageVOByID(Long id) {
		return (OrderStorageVO) orderStorageDao.getDOByID(id);
	}

	public Boolean modifyOrderStorageVO(OrderStorageVO orderStorageVO) {
		return orderStorageDao.updateDO(orderStorageVO);
	}

	public Boolean deleteOrderStorageVOByID(Long id) {
		return orderStorageDao.delDO(id);
	}

	public Boolean submit(Long id) {
		return orderStorageDao.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return orderStorageDao.audit(id, auditor);
	}

	public Boolean accounted(Long id) {
		return orderStorageDao.accounted(id);
	}

	public Integer queryOrderStorageVOCount(
			OrderStorageQueryVO orderStorageQueryVO) {
		return orderStorageDao.queryCount(orderStorageQueryVO);
	}
}
