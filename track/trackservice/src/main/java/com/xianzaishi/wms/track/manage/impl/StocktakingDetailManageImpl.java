package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.IStocktakingDetailDao;
import com.xianzaishi.wms.track.manage.itf.IStocktakingDetailManage;
import com.xianzaishi.wms.track.vo.StocktakingDetailQueryVO;
import com.xianzaishi.wms.track.vo.StocktakingDetailVO;

public class StocktakingDetailManageImpl implements IStocktakingDetailManage {
	@Autowired
	private IStocktakingDetailDao stocktakingDetailDao = null;

	private void validate(StocktakingDetailVO stocktakingDetailVO) {
		if (stocktakingDetailVO.getStocktakingId() == null
				|| stocktakingDetailVO.getStocktakingId() <= 0) {
			throw new BizException("requisitionsID error："
					+ stocktakingDetailVO.getStocktakingId());
		}
		if (stocktakingDetailVO.getSkuId() == null
				|| stocktakingDetailVO.getSkuId() <= 0) {
			throw new BizException("skuID error："
					+ stocktakingDetailVO.getSkuId());
		}
		if (stocktakingDetailVO.getPositionId() == null
				|| stocktakingDetailVO.getPositionId() <= 0) {
			throw new BizException("positionID error："
					+ stocktakingDetailVO.getPositionId());
		}
		if (stocktakingDetailVO.getFirstNoReal() == null
				|| stocktakingDetailVO.getFirstNoReal() <= 0) {
			throw new BizException("FirstNo error："
					+ stocktakingDetailVO.getFirstNoReal());
		}
		if (stocktakingDetailVO.getSkuName() == null
				|| stocktakingDetailVO.getSkuName().isEmpty()) {
			throw new BizException("skuName error："
					+ stocktakingDetailVO.getSkuName());
		}
		if (stocktakingDetailVO.getPositionCode() == null
				|| stocktakingDetailVO.getPositionCode().isEmpty()) {
			throw new BizException("positionCode error："
					+ stocktakingDetailVO.getPositionCode());
		}
		if (stocktakingDetailVO.getSaleUnit() == null
				|| stocktakingDetailVO.getSaleUnit().isEmpty()) {
			throw new BizException("saleUnit error："
					+ stocktakingDetailVO.getSaleUnit());
		}
		if (stocktakingDetailVO.getSaleUnitId() == null
				|| stocktakingDetailVO.getSaleUnitId() <= 0) {
			throw new BizException("saleUnitId error："
					+ stocktakingDetailVO.getSaleUnitId());
		}
		if (stocktakingDetailVO.getSaleUnitType() == null) {
			throw new BizException("saleUnitType error："
					+ stocktakingDetailVO.getSaleUnitType());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IStocktakingDetailDao getStocktakingDetailDao() {
		return stocktakingDetailDao;
	}

	public void setStocktakingDetailDao(
			IStocktakingDetailDao stocktakingDetailDao) {
		this.stocktakingDetailDao = stocktakingDetailDao;
	}

	public Long addStocktakingDetailVO(StocktakingDetailVO stocktakingDetailVO) {
		validate(stocktakingDetailVO);
		return (Long) stocktakingDetailDao.addDO(stocktakingDetailVO);
	}

	public List<StocktakingDetailVO> queryStocktakingDetailVOList(
			StocktakingDetailQueryVO stocktakingDetailQueryVO) {
		return stocktakingDetailDao.queryDO(stocktakingDetailQueryVO);
	}

	public StocktakingDetailVO getStocktakingDetailVOByID(Long id) {
		return (StocktakingDetailVO) stocktakingDetailDao.getDOByID(id);
	}

	public Boolean modifyStocktakingDetailVO(
			StocktakingDetailVO stocktakingDetailVO) {
		return stocktakingDetailDao.updateDO(stocktakingDetailVO);
	}

	public Boolean deleteStocktakingDetailVOByID(Long id) {
		return stocktakingDetailDao.delDO(id);
	}

	public List<StocktakingDetailVO> getStocktakingDetailVOByStocktakingID(
			Long id) {
		StocktakingDetailQueryVO queryVO = new StocktakingDetailQueryVO();
		queryVO.setStocktakingId(id);
		queryVO.setSize(Integer.MAX_VALUE);
		return stocktakingDetailDao.queryDO(queryVO);
	}

	public Boolean batchAddStocktakingDetailVO(
			List<StocktakingDetailVO> stocktakingDetailVOs) {
		return stocktakingDetailDao.batchAddDO(stocktakingDetailVOs);
	}

	public Boolean batchModifyStocktakingDetailVO(
			List<StocktakingDetailVO> stocktakingDetailVOs) {
		return stocktakingDetailDao.batchModifyDO(stocktakingDetailVOs);
	}

	public Boolean batchDeleteStocktakingDetailVO(
			List<StocktakingDetailVO> stocktakingDetailVOs) {
		return stocktakingDetailDao.batchDeleteDO(stocktakingDetailVOs);
	}

	public Boolean batchDeleteStocktakingDetailVOByID(
			List<Long> stocktakingDetailIDs) {
		return stocktakingDetailDao.batchDeleteDOByID(stocktakingDetailIDs);
	}
}
