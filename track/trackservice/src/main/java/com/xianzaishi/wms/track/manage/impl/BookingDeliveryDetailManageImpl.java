package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.IBookingDeliveryDetailDao;
import com.xianzaishi.wms.track.manage.itf.IBookingDeliveryDetailManage;
import com.xianzaishi.wms.track.vo.BookingDeliveryDetailQueryVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryDetailVO;

public class BookingDeliveryDetailManageImpl implements
		IBookingDeliveryDetailManage {
	@Autowired
	private IBookingDeliveryDetailDao bookingDeliveryDetailDao = null;

	private void validate(BookingDeliveryDetailVO bookingDeliveryDetailVO) {
		if (bookingDeliveryDetailVO.getDeliveryId() == null
				|| bookingDeliveryDetailVO.getDeliveryId() <= 0) {
			throw new BizException("deliveryID error："
					+ bookingDeliveryDetailVO.getDeliveryId());
		}

		if (bookingDeliveryDetailVO.getAllNoReal() == null
				|| bookingDeliveryDetailVO.getAllNoReal() <= 0) {
			throw new BizException("all number error："
					+ bookingDeliveryDetailVO.getAllNo());
		}

		if (bookingDeliveryDetailVO.getDelivNoReal() == null
				|| bookingDeliveryDetailVO.getDelivNoReal() <= 0) {
			throw new BizException("delivery number error："
					+ bookingDeliveryDetailVO.getDelivNo());
		}

		if (bookingDeliveryDetailVO.getOrderId() == null
				|| bookingDeliveryDetailVO.getOrderId() <= 0) {
			throw new BizException("orderID error："
					+ bookingDeliveryDetailVO.getOrderId());
		}
		if (bookingDeliveryDetailVO.getSkuId() == null
				|| bookingDeliveryDetailVO.getSkuId() <= 0) {
			throw new BizException("skuID error："
					+ bookingDeliveryDetailVO.getSkuId());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IBookingDeliveryDetailDao getBookingDeliveryDetailDao() {
		return bookingDeliveryDetailDao;
	}

	public void setBookingDeliveryDetailDao(
			IBookingDeliveryDetailDao bookingDeliveryDetailDao) {
		this.bookingDeliveryDetailDao = bookingDeliveryDetailDao;
	}

	public Boolean addBookingDeliveryDetailVO(
			BookingDeliveryDetailVO bookingDeliveryDetailVO) {
		validate(bookingDeliveryDetailVO);
		bookingDeliveryDetailDao.addDO(bookingDeliveryDetailVO);
		return true;
	}

	public List<BookingDeliveryDetailVO> queryBookingDeliveryDetailVOList(
			BookingDeliveryDetailQueryVO bookingDeliveryDetailQueryVO) {
		return bookingDeliveryDetailDao.queryDO(bookingDeliveryDetailQueryVO);
	}

	public BookingDeliveryDetailVO getBookingDeliveryDetailVOByID(Long id) {
		return (BookingDeliveryDetailVO) bookingDeliveryDetailDao.getDOByID(id);
	}

	public Boolean modifyBookingDeliveryDetailVO(
			BookingDeliveryDetailVO bookingDeliveryDetailVO) {
		return bookingDeliveryDetailDao.updateDO(bookingDeliveryDetailVO);
	}

	public Boolean deleteBookingDeliveryDetailVOByID(Long id) {
		return bookingDeliveryDetailDao.delDO(id);
	}

	public List<BookingDeliveryDetailVO> getBookingDeliveryDetailVOByBookingDeliveryID(
			Long id) {
		BookingDeliveryDetailQueryVO queryVO = new BookingDeliveryDetailQueryVO();
		queryVO.setDeliveryId(id);
		queryVO.setSize(Integer.MAX_VALUE);
		return bookingDeliveryDetailDao.queryDO(queryVO);
	}

	public Boolean batchAddBookingDeliveryDetailVO(
			List<BookingDeliveryDetailVO> bookingDeliveryDetailVOs) {
		return bookingDeliveryDetailDao.batchAddDO(bookingDeliveryDetailVOs);
	}

	public Boolean batchModifyBookingDeliveryDetailVO(
			List<BookingDeliveryDetailVO> bookingDeliveryDetailVOs) {
		return bookingDeliveryDetailDao.batchModifyDO(bookingDeliveryDetailVOs);
	}

	public Boolean batchDeleteBookingDeliveryDetailVO(
			List<BookingDeliveryDetailVO> bookingDeliveryDetailVOs) {
		return bookingDeliveryDetailDao.batchDeleteDO(bookingDeliveryDetailVOs);
	}

	public Boolean batchDeleteBookingDeliveryDetailVOByID(
			List<Long> bookingDeliveryDetailIDs) {
		return bookingDeliveryDetailDao
				.batchDeleteDOByID(bookingDeliveryDetailIDs);
	}
}
