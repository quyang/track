package com.xianzaishi.wms.track.dao.itf;

import com.xianzaishi.wms.common.dao.itf.IBaseDao;

public interface IAdjustDao extends IBaseDao {
	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

	public Boolean accounted(Long id);

}