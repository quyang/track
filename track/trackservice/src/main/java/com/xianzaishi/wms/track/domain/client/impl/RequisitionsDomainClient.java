package com.xianzaishi.wms.track.domain.client.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.domain.client.itf.IRequisitionsDomainClient;
import com.xianzaishi.wms.track.domain.service.itf.IRequisitionsDomainService;
import com.xianzaishi.wms.track.vo.RequisitionsDetailQueryVO;
import com.xianzaishi.wms.track.vo.RequisitionsDetailVO;
import com.xianzaishi.wms.track.vo.RequisitionsQueryVO;
import com.xianzaishi.wms.track.vo.RequisitionsVO;

public class RequisitionsDomainClient implements IRequisitionsDomainClient {
	private static final Logger logger = Logger
			.getLogger(RequisitionsDomainClient.class);
	@Autowired
	private IRequisitionsDomainService requisitionsDomainService = null;

	public SimpleResultVO<Boolean> createRequisitionsDomain(
			RequisitionsVO requisitionsVO) {
		SimpleResultVO<Boolean> flag = null;
		try {

			flag = SimpleResultVO.buildSuccessResult(requisitionsDomainService
					.createRequisitionsDomain(requisitionsVO));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<RequisitionsVO> getRequisitionsDomainByID(
			Long requisitionsID) {
		SimpleResultVO<RequisitionsVO> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(requisitionsDomainService
					.getRequisitionsDomainByID(requisitionsID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> deleteRequisitionsDomain(Long requisitionsID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(requisitionsDomainService
					.deleteRequisitionsDomain(requisitionsID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> addRequisitionsVO(
			RequisitionsVO requisitionsVO) {
		return SimpleResultVO.buildSuccessResult(requisitionsDomainService
				.addRequisitionsVO(requisitionsVO));
	}

	public SimpleResultVO<QueryResultVO<RequisitionsVO>> queryRequisitionsVOList(
			RequisitionsQueryVO requisitionsQueryVO) {
		return SimpleResultVO.buildSuccessResult(requisitionsDomainService
				.queryRequisitionsVOList(requisitionsQueryVO));
	}

	public SimpleResultVO<RequisitionsVO> getRequisitionsVOByID(Long id) {
		return SimpleResultVO.buildSuccessResult(requisitionsDomainService
				.getRequisitionsVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyRequisitionsVO(
			RequisitionsVO requisitionsVO) {
		return SimpleResultVO.buildSuccessResult(requisitionsDomainService
				.modifyRequisitionsVO(requisitionsVO));
	}

	public SimpleResultVO<Boolean> deleteRequisitionsVO(Long id) {
		return SimpleResultVO.buildSuccessResult(requisitionsDomainService
				.deleteRequisitionsVO(id));
	}

	public SimpleResultVO<Boolean> createRequisitionsDetailVO(
			RequisitionsDetailVO requisitionsDetailVO) {
		return SimpleResultVO.buildSuccessResult(requisitionsDomainService
				.createRequisitionsDetailVO(requisitionsDetailVO));
	}

	public SimpleResultVO<Boolean> batchCreateRequisitionsDetailVO(
			List<RequisitionsDetailVO> requisitionsDetailVOs) {
		return SimpleResultVO.buildSuccessResult(requisitionsDomainService
				.batchCreateRequisitionsDetailVO(requisitionsDetailVOs));
	}

	public SimpleResultVO<List<RequisitionsDetailVO>> queryRequisitionsDetailVOList(
			RequisitionsDetailQueryVO requisitionsDetailQueryVO) {
		return SimpleResultVO.buildSuccessResult(requisitionsDomainService
				.queryRequisitionsDetailVOList(requisitionsDetailQueryVO));
	}

	public SimpleResultVO<List<RequisitionsDetailVO>> getRequisitionsDetailVOListByRequisitionsID(
			Long requisitionsID) {
		return SimpleResultVO.buildSuccessResult(requisitionsDomainService
				.getRequisitionsDetailVOListByRequisitionsID(requisitionsID));
	}

	public SimpleResultVO<RequisitionsDetailQueryVO> getRequisitionsDetailVOByID(
			Long id) {
		return SimpleResultVO.buildSuccessResult(requisitionsDomainService
				.getRequisitionsDetailVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyRequisitionsDetailVO(
			RequisitionsDetailVO requisitionsDetailVO) {
		return SimpleResultVO.buildSuccessResult(requisitionsDomainService
				.modifyRequisitionsDetailVO(requisitionsDetailVO));
	}

	public SimpleResultVO<Boolean> batchModifyRequisitionsDetailVOs(
			List<RequisitionsDetailVO> requisitionsDetailVOs) {
		return SimpleResultVO.buildSuccessResult(requisitionsDomainService
				.batchModifyRequisitionsDetailVOs(requisitionsDetailVOs));
	}

	public SimpleResultVO<Boolean> deleteRequisitionsDetailVO(Long id) {
		return SimpleResultVO.buildSuccessResult(requisitionsDomainService
				.deleteRequisitionsDetailVO(id));
	}

	public SimpleResultVO<Boolean> deleteRequisitionsDetailVOByRequisitionsID(
			Long requisitionsID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO
					.buildSuccessResult(requisitionsDomainService
							.deleteRequisitionsDetailVOByRequisitionsID(requisitionsID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> batchDeleteRequisitionsDetailVOByRequisitionsID(
			List<Long> storyDetailIDs) {
		return SimpleResultVO
				.buildSuccessResult(requisitionsDomainService
						.batchDeleteRequisitionsDetailVOByRequisitionsID(storyDetailIDs));
	}

	public IRequisitionsDomainService getRequisitionsDomainService() {
		return requisitionsDomainService;
	}

	public void setRequisitionsDomainService(
			IRequisitionsDomainService requisitionsDomainService) {
		this.requisitionsDomainService = requisitionsDomainService;
	}

	public SimpleResultVO<Boolean> submit(Long id) {
		return SimpleResultVO.buildSuccessResult(requisitionsDomainService
				.submit(id));
	}

	public SimpleResultVO<Boolean> audit(Long id, Long auditor) {
		return SimpleResultVO.buildSuccessResult(requisitionsDomainService
				.audit(id, auditor));
	}

	public SimpleResultVO<Boolean> outgoing(Long id, Long auditor) {
		return SimpleResultVO.buildSuccessResult(requisitionsDomainService
				.outgoing(id, auditor));
	}

	public SimpleResultVO<Boolean> outgoingAudit(Long id, Long auditor) {
		return SimpleResultVO.buildSuccessResult(requisitionsDomainService
				.outgoingAudit(id, auditor));
	}

	public SimpleResultVO<Boolean> accounted(Long id) {
		return SimpleResultVO.buildSuccessResult(requisitionsDomainService
				.accounted(id));
	}

}
