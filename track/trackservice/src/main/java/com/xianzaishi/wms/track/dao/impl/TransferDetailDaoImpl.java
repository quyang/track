package com.xianzaishi.wms.track.dao.impl;

import java.util.List;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.track.dao.itf.ITransferDetailDao;
import com.xianzaishi.wms.track.vo.TransferDetailQueryVO;
import com.xianzaishi.wms.track.vo.TransferDetailVO;

public class TransferDetailDaoImpl extends BaseDaoAdapter implements
		ITransferDetailDao {
	public String getVOClassName() {
		return "TransferDetailDO";
	}

}
