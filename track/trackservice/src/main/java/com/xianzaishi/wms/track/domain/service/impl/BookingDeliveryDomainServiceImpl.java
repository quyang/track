package com.xianzaishi.wms.track.domain.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.track.domain.service.itf.IBookingDeliveryDomainService;
import com.xianzaishi.wms.track.service.itf.IBookingDeliveryDetailService;
import com.xianzaishi.wms.track.service.itf.IBookingDeliveryService;
import com.xianzaishi.wms.track.vo.BookingDeliveryDetailQueryVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryDetailVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryQueryVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryVO;

public class BookingDeliveryDomainServiceImpl implements
		IBookingDeliveryDomainService {
	@Autowired
	private IBookingDeliveryService bookingDeliveryService = null;
	@Autowired
	private IBookingDeliveryDetailService bookingDeliveryDetailService = null;
	@Autowired
	private SkuService skuService = null;
	@Autowired
	private ValueService valueService = null;

	public Boolean createBookingDeliveryDomain(
			BookingDeliveryVO bookingDeliveryVO) {
		if (bookingDeliveryVO.getDetails() == null
				|| bookingDeliveryVO.getDetails().isEmpty()) {
			throw new BizException("请填充入库明细！");
		}

		Long bookingDeliveryID = bookingDeliveryService
				.addBookingDeliveryVO(bookingDeliveryVO);

		initDetails(bookingDeliveryID, bookingDeliveryVO.getDetails());

		bookingDeliveryDetailService
				.batchAddBookingDeliveryDetailVO(bookingDeliveryVO.getDetails());

		return true;
	}

	public BookingDeliveryVO getBookingDeliveryDomainByID(Long bookingDeliveryID) {
		if (bookingDeliveryID == null || bookingDeliveryID <= 0) {
			throw new BizException("id error");
		}

		BookingDeliveryVO bookingDeliveryVO = bookingDeliveryService
				.getBookingDeliveryVOByID(bookingDeliveryID);

		bookingDeliveryVO
				.setDetails(bookingDeliveryDetailService
						.getBookingDeliveryDetailVOByBookingDeliveryID(bookingDeliveryID));
		return bookingDeliveryVO;
	}

	public Boolean deleteBookingDeliveryDomain(Long bookingDeliveryID) {
		if (bookingDeliveryID == null || bookingDeliveryID <= 0) {
			throw new BizException("id error");
		}

		List<BookingDeliveryDetailVO> bookingDeliveryDetailVOs = bookingDeliveryDetailService
				.getBookingDeliveryDetailVOByBookingDeliveryID(bookingDeliveryID);

		bookingDeliveryDetailService
				.batchDeleteBookingDeliveryDetailVO(bookingDeliveryDetailVOs);

		bookingDeliveryService.deleteBookingDeliveryVOByID(bookingDeliveryID);

		return true;
	}

	public Boolean addBookingDeliveryVO(BookingDeliveryVO bookingDeliveryVO) {
		bookingDeliveryService.addBookingDeliveryVO(bookingDeliveryVO);
		return true;
	}

	public QueryResultVO<BookingDeliveryVO> queryBookingDeliveryVOList(
			BookingDeliveryQueryVO bookingDeliveryQueryVO) {
		QueryResultVO<BookingDeliveryVO> queryResultVO = new QueryResultVO<>();
		queryResultVO.setPage(bookingDeliveryQueryVO.getPage());
		queryResultVO.setSize(bookingDeliveryQueryVO.getSize());
		queryResultVO.setItems(bookingDeliveryService
				.queryBookingDeliveryVOList(bookingDeliveryQueryVO));
		queryResultVO.setTotalCount(bookingDeliveryService
				.queryBookingDeliveryVOCount(bookingDeliveryQueryVO));
		return queryResultVO;
	}

	public BookingDeliveryVO getBookingDeliveryVOByID(Long id) {
		return bookingDeliveryService.getBookingDeliveryVOByID(id);
	}

	public Boolean modifyBookingDeliveryVO(BookingDeliveryVO bookingDeliveryVO) {
		return bookingDeliveryService
				.modifyBookingDeliveryVO(bookingDeliveryVO);
	}

	public Boolean deleteBookingDeliveryVO(Long id) {
		return bookingDeliveryService.deleteBookingDeliveryVOByID(id);
	}

	public Boolean createBookingDeliveryDetailVO(
			BookingDeliveryDetailVO bookingDeliveryDetailVO) {
		initInfo(bookingDeliveryDetailVO);
		return bookingDeliveryDetailService
				.addBookingDeliveryDetailVO(bookingDeliveryDetailVO);
	}

	public Boolean batchCreateBookingDeliveryDetailVO(
			List<BookingDeliveryDetailVO> bookingDeliveryDetailVOs) {
		for (BookingDeliveryDetailVO bookingDeliveryDetailVO : bookingDeliveryDetailVOs) {
			initInfo(bookingDeliveryDetailVO);
		}
		return bookingDeliveryDetailService
				.batchAddBookingDeliveryDetailVO(bookingDeliveryDetailVOs);
	}

	public List<BookingDeliveryDetailVO> queryBookingDeliveryDetailVOList(
			BookingDeliveryDetailQueryVO bookingDeliveryDetailQueryVO) {
		return bookingDeliveryDetailService
				.queryBookingDeliveryDetailVOList(bookingDeliveryDetailQueryVO);
	}

	public List<BookingDeliveryDetailVO> getBookingDeliveryDetailVOListByBookingDeliveryID(
			Long bookingDeliveryID) {
		return bookingDeliveryDetailService
				.getBookingDeliveryDetailVOByBookingDeliveryID(bookingDeliveryID);
	}

	public BookingDeliveryDetailVO getBookingDeliveryDetailVOByID(Long id) {
		return bookingDeliveryDetailService.getBookingDeliveryDetailVOByID(id);
	}

	public Boolean modifyBookingDeliveryDetailVO(
			BookingDeliveryDetailVO bookingDeliveryDetailVO) {
		initInfo(bookingDeliveryDetailVO);
		return bookingDeliveryDetailService
				.modifyBookingDeliveryDetailVO(bookingDeliveryDetailVO);
	}

	public Boolean batchModifyBookingDeliveryDetailVOs(
			List<BookingDeliveryDetailVO> bookingDeliveryDetailVOs) {
		for (BookingDeliveryDetailVO bookingDeliveryDetailVO : bookingDeliveryDetailVOs) {
			initInfo(bookingDeliveryDetailVO);
		}
		return bookingDeliveryDetailService
				.batchModifyBookingDeliveryDetailVO(bookingDeliveryDetailVOs);
	}

	public Boolean deleteBookingDeliveryDetailVO(Long id) {
		return bookingDeliveryDetailService
				.deleteBookingDeliveryDetailVOByID(id);
	}

	public Boolean deleteBookingDeliveryDetailVOByBookingDeliveryID(
			Long bookingDeliveryID) {
		if (bookingDeliveryID == null || bookingDeliveryID <= 0) {
			throw new BizException("id error");
		}

		List<BookingDeliveryDetailVO> bookingDeliveryDetailVOs = bookingDeliveryDetailService
				.getBookingDeliveryDetailVOByBookingDeliveryID(bookingDeliveryID);

		bookingDeliveryDetailService
				.batchDeleteBookingDeliveryDetailVO(bookingDeliveryDetailVOs);
		return true;
	}

	public Boolean batchDeleteBookingDeliveryDetailVOByBookingDeliveryID(
			List<Long> storyDetailIDs) {
		return bookingDeliveryDetailService
				.batchDeleteBookingDeliveryDetailVOByID(storyDetailIDs);
	}

	private void initDetails(Long id,
			List<BookingDeliveryDetailVO> bookingDeliveryDetailVOs) {
		for (int i = 0; i < bookingDeliveryDetailVOs.size(); i++) {
			bookingDeliveryDetailVOs.get(i).setDeliveryId(id);
			initInfo(bookingDeliveryDetailVOs.get(i));
		}
	}

	private void initInfo(BookingDeliveryDetailVO bookingDeliveryDetailVO) {
		ItemCommoditySkuDTO itemCommoditySkuDTO = skuService.queryItemSku(
				SkuQuery.querySkuById(bookingDeliveryDetailVO.getSkuId()))
				.getModule();
		if (itemCommoditySkuDTO == null) {
			throw new BizException("skuID error:"
					+ bookingDeliveryDetailVO.getSkuId());
		}
		bookingDeliveryDetailVO.setSkuName(itemCommoditySkuDTO.getTitle());
		bookingDeliveryDetailVO
				.setSaleUnit(valueService.queryValueNameByValueId(
						itemCommoditySkuDTO.getSkuUnit()).getModule());
		bookingDeliveryDetailVO.setSaleUnitId(new Long(itemCommoditySkuDTO
				.getSkuUnit()));
		bookingDeliveryDetailVO.setSaleUnitType(new Integer(itemCommoditySkuDTO
				.querySaleUnitType()));
		// bookingDeliveryDetailVO.setSpec(itemCommoditySkuDTO
		// .getSkuSpecificationNum());
		// bookingDeliveryDetailVO.setSpecUnit(valueService.queryValueNameByValueId(
		// itemCommoditySkuDTO.getSkuSpecificationUnit()).getModule());
		// bookingDeliveryDetailVO.setSpecUnitId(new Long(itemCommoditySkuDTO
		// .getSkuSpecificationUnit()));

		if (bookingDeliveryDetailVO.getAllNoReal() == null
				&& bookingDeliveryDetailVO.getAllNo() != null
				&& !bookingDeliveryDetailVO.getAllNo().isEmpty()) {
				bookingDeliveryDetailVO.setAllNoReal(new BigDecimal(
						bookingDeliveryDetailVO.getAllNo()).multiply(
						new BigDecimal(1000)).intValue());
		}

		if (bookingDeliveryDetailVO.getDelivedNoReal() == null
				&& bookingDeliveryDetailVO.getDelivedNo() != null
				&& !bookingDeliveryDetailVO.getDelivedNo().isEmpty()) {
				bookingDeliveryDetailVO.setDelivedNoReal(new BigDecimal(
						bookingDeliveryDetailVO.getDelivedNo()).multiply(
						new BigDecimal(1000)).intValue());
		}

		if (bookingDeliveryDetailVO.getDelivNoReal() == null
				&& bookingDeliveryDetailVO.getDelivNo() != null
				&& !bookingDeliveryDetailVO.getDelivNo().isEmpty()) {
				bookingDeliveryDetailVO.setDelivNoReal(new BigDecimal(
						bookingDeliveryDetailVO.getDelivNo()).multiply(
						new BigDecimal(1000)).intValue());
		}
	}

	public IBookingDeliveryService getBookingDeliveryService() {
		return bookingDeliveryService;
	}

	public void setBookingDeliveryService(
			IBookingDeliveryService bookingDeliveryService) {
		this.bookingDeliveryService = bookingDeliveryService;
	}

	public IBookingDeliveryDetailService getBookingDeliveryDetailService() {
		return bookingDeliveryDetailService;
	}

	public void setBookingDeliveryDetailService(
			IBookingDeliveryDetailService bookingDeliveryDetailService) {
		this.bookingDeliveryDetailService = bookingDeliveryDetailService;
	}

	public Boolean submit(Long id) {
		return bookingDeliveryService.submit(id);
	}

	public Boolean confirm(Long id, Long auditor) {
		return bookingDeliveryService.confirm(id, auditor);
	}

	public SkuService getSkuService() {
		return skuService;
	}

	public void setSkuService(SkuService skuService) {
		this.skuService = skuService;
	}

	public ValueService getValueService() {
		return valueService;
	}

	public void setValueService(ValueService valueService) {
		this.valueService = valueService;
	}

}
