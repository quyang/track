package com.xianzaishi.wms.track.dao.impl;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.IBookingDeliveryDao;
import com.xianzaishi.wms.track.vo.BookingDeliveryVO;
import com.xianzaishi.wms.track.vo.OutgoingVO;

public class BookingDeliveryDaoImpl extends BaseDaoAdapter implements
		IBookingDeliveryDao {
	public String getVOClassName() {
		return "BookingDeliveryDO";
	}

	public Boolean submit(Long id) {
		Boolean flag = false;
		try {
			if (id != null && id > 0) {
				int no = simpleSqlMapClientTemplate.update(getVOClassName()
						+ ".submit", id);
				if (no == 1) {
					flag = true;
				} else if (no > 1) {
					throw new BizException("影响的数据大于1条。");
				} else {
					throw new BizException("请检查单据状态。");
				}
			} else {
				throw new BizException("更新数据失败，ID错误：ID = " + id);
			}
		} catch (Exception e) {
			throw new BizException("更新数据失败。", e);
		}
		return flag;
	}

	public Boolean confirm(Long id, Long auditor) {
		Boolean flag = false;
		try {
			if (id != null && id > 0 && auditor != null && auditor > 0) {
				BookingDeliveryVO bookingDeliveryVO = new BookingDeliveryVO();
				bookingDeliveryVO.setId(id);
				bookingDeliveryVO.setAuditor(auditor);
				int no = simpleSqlMapClientTemplate.update(getVOClassName()
						+ ".confirm", bookingDeliveryVO);
				if (no == 1) {
					flag = true;
				} else if (no > 1) {
					throw new BizException("影响的数据大于1条。");
				} else {
					throw new BizException("请检查单据状态。");
				}
			} else {
				throw new BizException("更新数据失败，ID错误：ID = " + id);
			}
		} catch (Exception e) {
			throw new BizException("更新数据失败。", e);
		}
		return flag;
	}
}
