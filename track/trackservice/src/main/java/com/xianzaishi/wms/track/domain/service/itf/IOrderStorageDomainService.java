package com.xianzaishi.wms.track.domain.service.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.track.vo.OrderStorageDetailQueryVO;
import com.xianzaishi.wms.track.vo.OrderStorageDetailVO;
import com.xianzaishi.wms.track.vo.OrderStorageQueryVO;
import com.xianzaishi.wms.track.vo.OrderStorageVO;

public interface IOrderStorageDomainService {

	// domain
	public Boolean createOrderStorageDomain(OrderStorageVO orderStorageVO);

	public OrderStorageVO getOrderStorageDomainByID(Long orderStorageID);

	public Boolean deleteOrderStorageDomain(Long id);

	// head
	public Boolean addOrderStorageVO(OrderStorageVO orderStorageVO);

	public QueryResultVO<OrderStorageVO> queryOrderStorageVOList(
			OrderStorageQueryVO orderStorageQueryVO);

	public OrderStorageVO getOrderStorageVOByID(Long id);

	public Boolean modifyOrderStorageVO(OrderStorageVO orderStorageVO);

	public Boolean deleteOrderStorageVO(Long orderStorageID);

	// body
	public Boolean createOrderStorageDetailVO(
			OrderStorageDetailVO orderStorageDetailVO);

	public Boolean batchCreateOrderStorageDetailVO(
			List<OrderStorageDetailVO> orderStorageDetailVOs);

	public List<OrderStorageDetailVO> queryOrderStorageDetailVOList(
			OrderStorageDetailQueryVO orderStorageDetailQueryVO);

	public List<OrderStorageDetailVO> getOrderStorageDetailVOListByOrderStorageID(
			Long orderStorageID);

	public OrderStorageDetailVO getOrderStorageDetailVOByID(Long id);

	public Boolean modifyOrderStorageDetailVO(
			OrderStorageDetailVO orderStorageDetailVO);

	public Boolean batchModifyOrderStorageDetailVOs(
			List<OrderStorageDetailVO> orderStorageDetailVOs);

	public Boolean deleteOrderStorageDetailVO(Long id);

	public Boolean deleteOrderStorageDetailVOByOrderStorageID(
			Long orderStorageID);

	public Boolean batchDeleteOrderStorageDetailVOByOrderStorageID(
			List<Long> storyDetailIDs);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

	public Boolean accounted(Long id);

}
