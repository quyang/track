package com.xianzaishi.wms.track.domain.service.impl;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryManageDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IPositionDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryManageDetailVO;
import com.xianzaishi.wms.hive.vo.InventoryManageVO;
import com.xianzaishi.wms.hive.vo.PositionVO;
import com.xianzaishi.wms.track.constants.TrackConstants;
import com.xianzaishi.wms.track.domain.service.itf.ISpoilageDomainService;
import com.xianzaishi.wms.track.service.itf.ISpoilageDetailService;
import com.xianzaishi.wms.track.service.itf.ISpoilageService;
import com.xianzaishi.wms.track.vo.SpoilageDetailQueryVO;
import com.xianzaishi.wms.track.vo.SpoilageDetailVO;
import com.xianzaishi.wms.track.vo.SpoilageQueryVO;
import com.xianzaishi.wms.track.vo.SpoilageVO;

public class SpoilageDomainServiceImpl implements ISpoilageDomainService {
	@Autowired
	private ISpoilageService spoilageService = null;
	@Autowired
	private ISpoilageDetailService spoilageDetailService = null;
	@Autowired
	private IInventoryManageDomainClient inventoryManageDomainClient = null;
	@Autowired
	private SkuService skuService = null;
	@Autowired
	private ValueService valueService = null;
	@Autowired
	private IPositionDomainClient positionDomainClient = null;

	public Boolean createSpoilageDomain(SpoilageVO spoilageVO) {
		if (spoilageVO.getDetails() == null
				|| spoilageVO.getDetails().isEmpty()) {
			throw new BizException("请填充入库明细！");
		}
		spoilageVO.setStatu(0);
		Long spoilageID = spoilageService.addSpoilageVO(spoilageVO);

		initDetails(spoilageID, spoilageVO.getDetails());

		spoilageDetailService.batchAddSpoilageDetailVO(spoilageVO.getDetails());

		return true;
	}

	public SpoilageVO getSpoilageDomainByID(Long spoilageID) {
		if (spoilageID == null || spoilageID <= 0) {
			throw new BizException("id error");
		}

		SpoilageVO spoilageVO = spoilageService.getSpoilageVOByID(spoilageID);

		spoilageVO.setDetails(spoilageDetailService
				.getSpoilageDetailVOBySpoilageID(spoilageID));

		return spoilageVO;
	}

	public Boolean deleteSpoilageDomain(Long spoilageID) {
		if (spoilageID == null || spoilageID <= 0) {
			throw new BizException("id error");
		}

		List<SpoilageDetailVO> spoilageDetailVOs = spoilageDetailService
				.getSpoilageDetailVOBySpoilageID(spoilageID);

		spoilageDetailService.batchDeleteSpoilageDetailVO(spoilageDetailVOs);

		spoilageService.deleteSpoilageVOByID(spoilageID);

		return true;
	}

	public Boolean addSpoilageVO(SpoilageVO spoilageVO) {
		spoilageService.addSpoilageVO(spoilageVO);
		return true;
	}

	public QueryResultVO<SpoilageVO> querySpoilageVOList(
			SpoilageQueryVO spoilageQueryVO) {
		QueryResultVO<SpoilageVO> queryResultVO = new QueryResultVO<>();
		queryResultVO.setPage(spoilageQueryVO.getPage());
		queryResultVO.setSize(spoilageQueryVO.getSize());
		queryResultVO.setItems(spoilageService
				.querySpoilageVOList(spoilageQueryVO));
		queryResultVO.setTotalCount(spoilageService
				.querySpoilageVOCount(spoilageQueryVO));
		return queryResultVO;
	}

	public SpoilageVO getSpoilageVOByID(Long id) {
		return spoilageService.getSpoilageVOByID(id);
	}

	public Boolean modifySpoilageVO(SpoilageVO spoilageVO) {
		return spoilageService.modifySpoilageVO(spoilageVO);
	}

	public Boolean deleteSpoilageVO(Long id) {
		return spoilageService.deleteSpoilageVOByID(id);
	}

	public Boolean createSpoilageDetailVO(SpoilageDetailVO spoilageDetailVO) {
		initInfo(spoilageDetailVO);
		return spoilageDetailService.addSpoilageDetailVO(spoilageDetailVO);
	}

	public Boolean batchCreateSpoilageDetailVO(
			List<SpoilageDetailVO> spoilageDetailVOs) {
		for (SpoilageDetailVO spoilageDetailVO : spoilageDetailVOs) {
			initInfo(spoilageDetailVO);
		}
		return spoilageDetailService
				.batchAddSpoilageDetailVO(spoilageDetailVOs);
	}

	public List<SpoilageDetailVO> querySpoilageDetailVOList(
			SpoilageDetailQueryVO spoilageDetailQueryVO) {
		return spoilageDetailService
				.querySpoilageDetailVOList(spoilageDetailQueryVO);
	}

	public List<SpoilageDetailVO> getSpoilageDetailVOListBySpoilageID(
			Long spoilageID) {
		return spoilageDetailService
				.getSpoilageDetailVOBySpoilageID(spoilageID);
	}

	public SpoilageDetailVO getSpoilageDetailVOByID(Long id) {
		return spoilageDetailService.getSpoilageDetailVOByID(id);
	}

	public Boolean modifySpoilageDetailVO(SpoilageDetailVO spoilageDetailVO) {
		initInfo(spoilageDetailVO);
		return spoilageDetailService.modifySpoilageDetailVO(spoilageDetailVO);
	}

	public Boolean batchModifySpoilageDetailVOs(
			List<SpoilageDetailVO> spoilageDetailVOs) {
		for (SpoilageDetailVO spoilageDetailVO : spoilageDetailVOs) {
			initInfo(spoilageDetailVO);
		}
		return spoilageDetailService
				.batchModifySpoilageDetailVO(spoilageDetailVOs);
	}

	public Boolean deleteSpoilageDetailVO(Long id) {
		return spoilageDetailService.deleteSpoilageDetailVOByID(id);
	}

	public Boolean deleteSpoilageDetailVOBySpoilageID(Long spoilageID) {
		if (spoilageID == null || spoilageID <= 0) {
			throw new BizException("id error");
		}

		List<SpoilageDetailVO> spoilageDetailVOs = spoilageDetailService
				.getSpoilageDetailVOBySpoilageID(spoilageID);

		spoilageDetailService.batchDeleteSpoilageDetailVO(spoilageDetailVOs);

		return true;
	}

	public Boolean batchDeleteSpoilageDetailVOBySpoilageID(
			List<Long> storyDetailIDs) {
		return spoilageDetailService
				.batchDeleteSpoilageDetailVOByID(storyDetailIDs);
	}

	private void initDetails(Long id, List<SpoilageDetailVO> spoilageDetailVOs) {
		for (int i = 0; i < spoilageDetailVOs.size(); i++) {
			spoilageDetailVOs.get(i).setSpoilageId(id);
			initInfo(spoilageDetailVOs.get(i));
		}
	}

	private void initInfo(SpoilageDetailVO spoilageDetailVO) {
		ItemCommoditySkuDTO itemCommoditySkuDTO = skuService.queryItemSku(
				SkuQuery.querySkuById(spoilageDetailVO.getSkuId())).getModule();
		if (itemCommoditySkuDTO == null) {
			throw new BizException("skuID error:" + spoilageDetailVO.getSkuId());
		}
		spoilageDetailVO.setSkuName(itemCommoditySkuDTO.getTitle());
		spoilageDetailVO.setSaleUnit(valueService.queryValueNameByValueId(
				itemCommoditySkuDTO.getSkuUnit()).getModule());
		spoilageDetailVO.setSaleUnitId(new Long(itemCommoditySkuDTO
				.getSkuUnit()));
		spoilageDetailVO.setSaleUnitType(new Integer(itemCommoditySkuDTO
				.querySaleUnitType()));
		// spoilageDetailVO.setSpec(itemCommoditySkuDTO.getSkuSpecificationNum());
		// spoilageDetailVO.setSpecUnit(valueService.queryValueNameByValueId(
		// itemCommoditySkuDTO.getSkuSpecificationUnit()).getModule());
		// spoilageDetailVO.setSpecUnitId(new Long(itemCommoditySkuDTO
		// .getSkuSpecificationUnit()));
		PositionVO positionVO = positionDomainClient.getPositionVOByID(
				spoilageDetailVO.getPositionId()).getTarget();
		if (positionVO == null) {
			throw new BizException("positionID error:"
					+ spoilageDetailVO.getPositionId());
		}
		spoilageDetailVO.setPositionCode(positionVO.getCode());

		if (spoilageDetailVO.getSpoilageNoReal() == null
				&& spoilageDetailVO.getSpoilageNo() != null
				&& !spoilageDetailVO.getSpoilageNo().isEmpty()) {
			spoilageDetailVO
					.setSpoilageNoReal(new BigDecimal(spoilageDetailVO
							.getSpoilageNo()).multiply(new BigDecimal(1000))
							.intValue());
		}
	}

	public ISpoilageService getSpoilageService() {
		return spoilageService;
	}

	public void setSpoilageService(ISpoilageService spoilageService) {
		this.spoilageService = spoilageService;
	}

	public ISpoilageDetailService getSpoilageDetailService() {
		return spoilageDetailService;
	}

	public void setSpoilageDetailService(
			ISpoilageDetailService spoilageDetailService) {
		this.spoilageDetailService = spoilageDetailService;
	}

	public Boolean submit(Long id) {
		return spoilageService.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		Boolean flag = false;
		if (spoilageService.audit(id, auditor)) {
			flag = accounted(id);
		}
		return flag;
	}

	public Boolean accounted(Long id) {
		Boolean flag = false;
		SpoilageVO spoilageVO = getSpoilageDomainByID(id);
		InventoryManageVO inventoryManageVO = initInventoryManage(spoilageVO);
		SimpleResultVO<Boolean> result = inventoryManageDomainClient
				.createInventoryManageDomain(inventoryManageVO);
		if (result.isSuccess() && result.getTarget()) {
			flag = spoilageService.accounted(id);
		}
		return flag;
	}

	private InventoryManageVO initInventoryManage(SpoilageVO spoilageVO) {
		InventoryManageVO inventoryManageVO = new InventoryManageVO();
		inventoryManageVO.setAgencyId(spoilageVO.getAgencyId());
		inventoryManageVO.setAuditor(spoilageVO.getAuditor());
		inventoryManageVO.setAuditTime(spoilageVO.getAuditTime());
		inventoryManageVO.setOperator(spoilageVO.getOperate());
		inventoryManageVO.setOpReason(TrackConstants.IM_REASON_OUT_SPOILAGE);
		inventoryManageVO.setOpTime(spoilageVO.getOpTime());
		inventoryManageVO.setOpType(1);
		List<InventoryManageDetailVO> details = new LinkedList<InventoryManageDetailVO>();
		for (int i = 0; i < spoilageVO.getDetails().size(); i++) {
			InventoryManageDetailVO inventoryManageDetailVO = new InventoryManageDetailVO();
			inventoryManageDetailVO.setNumber(-spoilageVO.getDetails().get(i)
					.getSpoilageNoReal());
			inventoryManageDetailVO.setSaleUnit(spoilageVO.getDetails().get(i)
					.getSaleUnit());
			inventoryManageDetailVO.setSaleUnitId(spoilageVO.getDetails()
					.get(i).getSaleUnitId());
			inventoryManageDetailVO.setSpec(spoilageVO.getDetails().get(i)
					.getSpec());
			inventoryManageDetailVO.setSpecUnit(spoilageVO.getDetails().get(i)
					.getSpecUnit());
			inventoryManageDetailVO.setSpecUnitId(spoilageVO.getDetails()
					.get(i).getSpecUnitId());
			inventoryManageDetailVO.setSaleUnitType(spoilageVO.getDetails()
					.get(i).getSaleUnitType());
			inventoryManageDetailVO.setSkuName(spoilageVO.getDetails().get(i)
					.getSkuName());
			inventoryManageDetailVO.setPositionCode(spoilageVO.getDetails()
					.get(i).getPositionCode());
			inventoryManageDetailVO.setSkuId(spoilageVO.getDetails().get(i)
					.getSkuId());
			inventoryManageDetailVO.setPositionId(spoilageVO.getDetails()
					.get(i).getPositionId());
			details.add(inventoryManageDetailVO);
		}
		inventoryManageVO.setDetails(details);
		return inventoryManageVO;
	}

	public IInventoryManageDomainClient getInventoryManageDomainClient() {
		return inventoryManageDomainClient;
	}

	public void setInventoryManageDomainClient(
			IInventoryManageDomainClient inventoryManageDomainClient) {
		this.inventoryManageDomainClient = inventoryManageDomainClient;
	}

	public SkuService getSkuService() {
		return skuService;
	}

	public void setSkuService(SkuService skuService) {
		this.skuService = skuService;
	}

	public ValueService getValueService() {
		return valueService;
	}

	public void setValueService(ValueService valueService) {
		this.valueService = valueService;
	}

	public IPositionDomainClient getPositionDomainClient() {
		return positionDomainClient;
	}

	public void setPositionDomainClient(
			IPositionDomainClient positionDomainClient) {
		this.positionDomainClient = positionDomainClient;
	}
}
