package com.xianzaishi.wms.track.domain.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.customercenter.client.customerservice.CustomerService;
import com.xianzaishi.customercenter.client.customerservice.dto.CustomerServiceTaskDTO;
import com.xianzaishi.customercenter.client.customerservice.dto.CustomerServiceTaskDTO.CustomerServiceTaskStatusConstants;
import com.xianzaishi.customercenter.client.customerservice.dto.ProofDataDTO;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.user.BackGroundUserService;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryManageDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IPositionDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryManageDetailVO;
import com.xianzaishi.wms.hive.vo.InventoryManageVO;
import com.xianzaishi.wms.hive.vo.PositionVO;
import com.xianzaishi.wms.track.domain.service.itf.IOutgoingDomainService;
import com.xianzaishi.wms.track.service.itf.IOutgoingDetailService;
import com.xianzaishi.wms.track.service.itf.IOutgoingService;
import com.xianzaishi.wms.track.vo.OutgoingDetailQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingDetailVO;
import com.xianzaishi.wms.track.vo.OutgoingQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingVO;

public class OutgoingDomainServiceImpl implements IOutgoingDomainService {
	@Autowired
	private IOutgoingService outgoingService = null;
	@Autowired
	private IOutgoingDetailService outgoingDetailService = null;
	@Autowired
	private IInventoryManageDomainClient inventoryManageDomainClient = null;
	@Autowired
	private IPositionDomainClient positionDomainClient = null;
	@Autowired
	private SkuService skuService = null;
	@Autowired
	private ValueService valueService = null;
	@Autowired
	private CustomerService customerService = null;
	@Autowired
	private BackGroundUserService backgrounduserservice = null;

	public Boolean createOutgoingDomain(OutgoingVO outgoingVO) {
		if (outgoingVO.getDetails() == null
				|| outgoingVO.getDetails().isEmpty()) {
			throw new BizException("请填充入库明细！");
		}
		outgoingVO.setStatu(0);
		Long outgoingID = outgoingService.addOutgoingVO(outgoingVO);
		outgoingVO.setId(outgoingID);
		initDetails(outgoingVO, outgoingVO.getDetails());

		outgoingDetailService.batchAddOutgoingDetailVO(outgoingVO.getDetails());

		return true;
	}

	public Boolean createAndAccount(OutgoingVO outgoingVO) {
		if (outgoingVO.getDetails() == null
				|| outgoingVO.getDetails().isEmpty()) {
			throw new BizException("请填充入库明细！");
		}
		outgoingVO.setStatu(0);
		Long outgoingID = outgoingService.addOutgoingVO(outgoingVO);
		outgoingVO.setId(outgoingID);
		initDetails(outgoingVO, outgoingVO.getDetails());

		outgoingDetailService.batchAddOutgoingDetailVO(outgoingVO.getDetails());
		submit(outgoingID);
		if(outgoingVO.getOpReason() != 2 && outgoingVO.getOpReason() != 3){
		  auditOutgoing(outgoingID, outgoingVO.getOperate());
		}else{
		  customerService.insertCustomerServiceTask(getCustomerTask(outgoingVO));
		}
		return true;
	}

	/**
	 * 获取客服任务
	 * @param outgoingVO
	 */
	private CustomerServiceTaskDTO getCustomerTask(OutgoingVO outgoingVO) {
  	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        CustomerServiceTaskDTO customerServiceTask = new CustomerServiceTaskDTO();
        customerServiceTask.setBizId(String.valueOf(outgoingVO.getId()));
        customerServiceTask.setCustomerUser(outgoingVO.getOperate());
        customerServiceTask.setProcessType((short) 1);
        customerServiceTask.setStatus(CustomerServiceTaskStatusConstants.WAIT_DEAL);
        if(StringUtils.isEmpty(outgoingVO.getOperateName()) && null != outgoingVO.getOperate()){
          Result<BackGroundUserDTO> backGroundUserResult = backgrounduserservice.queryUserDTOById(outgoingVO.getOperate().intValue());
          if(null != backGroundUserResult && backGroundUserResult.getSuccess() && null != backGroundUserResult.getModule()){
            BackGroundUserDTO backGroundUserDTO = backGroundUserResult.getModule();
            outgoingVO.setOperateName(backGroundUserDTO.getName());
          }
        }
        if(StringUtils.isEmpty(outgoingVO.getOperateName())){
          if(null == outgoingVO.getOperate()){
            outgoingVO.setOperate(0L);
          }
          outgoingVO.setOperateName(String.valueOf(outgoingVO.getOperate()));
        }
        customerServiceTask.setTitle(new StringBuilder(outgoingVO.getOperateName()).append("_").append(sdf.format(new Date())).toString());
        customerServiceTask.setType((short) 2);
        ProofDataDTO proofDataDTO = new ProofDataDTO();
        proofDataDTO.setId(1);
        if(outgoingVO.getOpReason() == 2){
          proofDataDTO.setSource("领用出库");
        }else if(outgoingVO.getOpReason() == 3){
          proofDataDTO.setSource("报损出库");
        }
        proofDataDTO.setType(1);
        customerServiceTask.setCustomerProofList(Arrays.asList(proofDataDTO));
        return customerServiceTask;
    }

  public OutgoingVO getOutgoingDomainByID(Long outgoingID) {
		if (outgoingID == null || outgoingID <= 0) {
			throw new BizException("id error");
		}

		OutgoingVO outgoingVO = outgoingService.getOutgoingVOByID(outgoingID);

		outgoingVO.setDetails(outgoingDetailService
				.getOutgoingDetailVOByOutgoingID(outgoingID));

		return outgoingVO;
	}

	public Boolean deleteOutgoingDomain(Long outgoingID) {
		if (outgoingID == null || outgoingID <= 0) {
			throw new BizException("id error");
		}

		List<OutgoingDetailVO> outgoingDetailVOs = outgoingDetailService
				.getOutgoingDetailVOByOutgoingID(outgoingID);

		outgoingDetailService.batchDeleteOutgoingDetailVO(outgoingDetailVOs);

		outgoingService.deleteOutgoingVOByID(outgoingID);

		return true;
	}

	public Boolean addOutgoingVO(OutgoingVO outgoingVO) {
		outgoingService.addOutgoingVO(outgoingVO);
		return true;
	}

	public QueryResultVO<OutgoingVO> queryOutgoingVOList(
			OutgoingQueryVO outgoingQueryVO) {
		QueryResultVO<OutgoingVO> queryResultVO = new QueryResultVO<>();
		queryResultVO.setPage(outgoingQueryVO.getPage());
		queryResultVO.setSize(outgoingQueryVO.getSize());
		queryResultVO.setItems(outgoingService
				.queryOutgoingVOList(outgoingQueryVO));
		queryResultVO.setTotalCount(outgoingService
				.queryOutgoingVOCount(outgoingQueryVO));
		return queryResultVO;
	}

	public OutgoingVO getOutgoingVOByID(Long id) {
		return outgoingService.getOutgoingVOByID(id);
	}

	public Boolean modifyOutgoingVO(OutgoingVO outgoingVO) {
		return outgoingService.modifyOutgoingVO(outgoingVO);
	}

	public Boolean deleteOutgoingVO(Long id) {
		return outgoingService.deleteOutgoingVOByID(id);
	}

	public Boolean createOutgoingDetailVO(OutgoingDetailVO outgoingDetailVO) {
		initInfo(outgoingDetailVO);
		return outgoingDetailService.addOutgoingDetailVO(outgoingDetailVO);
	}

	public Boolean batchCreateOutgoingDetailVO(
			List<OutgoingDetailVO> outgoingDetailVOs) {
		for (OutgoingDetailVO outgoingDetailVO : outgoingDetailVOs) {
			initInfo(outgoingDetailVO);
		}
		return outgoingDetailService
				.batchAddOutgoingDetailVO(outgoingDetailVOs);
	}

	public List<OutgoingDetailVO> queryOutgoingDetailVOList(
			OutgoingDetailQueryVO outgoingDetailQueryVO) {
		return outgoingDetailService
				.queryOutgoingDetailVOList(outgoingDetailQueryVO);
	}

	public List<OutgoingDetailVO> getOutgoingDetailVOListByOutgoingID(
			Long outgoingID) {
		return outgoingDetailService
				.getOutgoingDetailVOByOutgoingID(outgoingID);
	}

	public OutgoingDetailVO getOutgoingDetailVOByID(Long id) {
		return outgoingDetailService.getOutgoingDetailVOByID(id);
	}

	public Boolean modifyOutgoingDetailVO(OutgoingDetailVO outgoingDetailVO) {
		initInfo(outgoingDetailVO);
		return outgoingDetailService.modifyOutgoingDetailVO(outgoingDetailVO);
	}

	public Boolean batchModifyOutgoingDetailVOs(
			List<OutgoingDetailVO> outgoingDetailVOs) {
		for (OutgoingDetailVO outgoingDetailVO : outgoingDetailVOs) {
			initInfo(outgoingDetailVO);
		}
		return outgoingDetailService
				.batchModifyOutgoingDetailVO(outgoingDetailVOs);
	}

	public Boolean deleteOutgoingDetailVO(Long id) {
		return outgoingDetailService.deleteOutgoingDetailVOByID(id);
	}

	public Boolean deleteOutgoingDetailVOByOutgoingID(Long outgoingID) {
		if (outgoingID == null || outgoingID <= 0) {
			throw new BizException("id error");
		}

		List<OutgoingDetailVO> outgoingDetailVOs = outgoingDetailService
				.getOutgoingDetailVOByOutgoingID(outgoingID);

		outgoingDetailService.batchDeleteOutgoingDetailVO(outgoingDetailVOs);

		return true;
	}

	public Boolean batchDeleteOutgoingDetailVOByOutgoingID(
			List<Long> storyDetailIDs) {
		return outgoingDetailService
				.batchDeleteOutgoingDetailVOByID(storyDetailIDs);
	}

	private void initDetails(OutgoingVO outgoingVO,
			List<OutgoingDetailVO> outgoingDetailVOs) {
		for (int i = 0; i < outgoingDetailVOs.size(); i++) {
			outgoingDetailVOs.get(i).setOutgoingId(outgoingVO.getId());
			initInfo(outgoingDetailVOs.get(i));
		}
	}

	private void initInfo(OutgoingDetailVO outgoingDetailVO) {
		ItemCommoditySkuDTO itemCommoditySkuDTO = skuService.queryItemSku(
				SkuQuery.querySkuById(outgoingDetailVO.getSkuId())).getModule();
		if (itemCommoditySkuDTO == null) {
			throw new BizException("skuID error:" + outgoingDetailVO.getSkuId());
		}
		outgoingDetailVO.setSkuName(itemCommoditySkuDTO.getTitle());
		outgoingDetailVO.setSaleUnit(valueService.queryValueNameByValueId(
				itemCommoditySkuDTO.getSkuUnit()).getModule());
		outgoingDetailVO.setSaleUnitId(new Long(itemCommoditySkuDTO
				.getSkuUnit()));
		outgoingDetailVO.setSaleUnitType(new Integer(itemCommoditySkuDTO
				.querySaleUnitType()));
		// outgoingDetailVO.setSpec(itemCommoditySkuDTO.getSkuSpecificationNum());
		// outgoingDetailVO.setSpecUnit(valueService.queryValueNameByValueId(
		// itemCommoditySkuDTO.getSkuSpecificationUnit()).getModule());
		// outgoingDetailVO.setSpecUnitId(new Long(itemCommoditySkuDTO
		// .getSkuSpecificationUnit()));
		PositionVO positionVO = positionDomainClient.getPositionVOByID(
				outgoingDetailVO.getPositionId()).getTarget();
		if (positionVO == null) {
			throw new BizException("positionID error:"
					+ outgoingDetailVO.getPositionId());
		}
		outgoingDetailVO.setPositionCode(positionVO.getCode());

		if (outgoingDetailVO.getNumberReal() == null
				&& outgoingDetailVO.getNumber() != null
				&& !outgoingDetailVO.getNumber().isEmpty()) {
			outgoingDetailVO.setNumberReal(new BigDecimal(outgoingDetailVO
					.getNumber()).multiply(new BigDecimal(1000)).intValue());
		}
	}

	public IOutgoingService getOutgoingService() {
		return outgoingService;
	}

	public void setOutgoingService(IOutgoingService outgoingService) {
		this.outgoingService = outgoingService;
	}

	public IOutgoingDetailService getOutgoingDetailService() {
		return outgoingDetailService;
	}

	public void setOutgoingDetailService(
			IOutgoingDetailService outgoingDetailService) {
		this.outgoingDetailService = outgoingDetailService;
	}

	public Boolean auditOutgoing(Long outgoingID, Long operate) {
		Boolean flag = false;
		if (outgoingService.audit(outgoingID, operate)) {
			flag = accounted(outgoingID);
		}
		return flag;
	}

	public Boolean accounted(Long id) {
		Boolean flag = false;
		OutgoingVO outgoingVO = getOutgoingDomainByID(id);
		InventoryManageVO inventoryManageVO = initInventoryManage(outgoingVO);
		SimpleResultVO<Boolean> result = inventoryManageDomainClient
				.createInventoryManageDomain(inventoryManageVO);
		if (result.isSuccess() && result.getTarget()) {
			flag = outgoingService.accounted(id);
		}
		return flag;
	}

	private InventoryManageVO initInventoryManage(OutgoingVO outgoingVO) {
		InventoryManageVO inventoryManageVO = new InventoryManageVO();
		inventoryManageVO.setAgencyId(outgoingVO.getAgencyId());
		inventoryManageVO.setAuditor(outgoingVO.getAuditor());
		inventoryManageVO.setAuditTime(outgoingVO.getAuditTime());
		inventoryManageVO.setOperator(outgoingVO.getOperate());
		inventoryManageVO.setOpReason(outgoingVO.getOpReason());
		inventoryManageVO.setOpTime(outgoingVO.getOpTime());
		inventoryManageVO.setOpType(1);
		inventoryManageVO.setInventoryId(outgoingVO.getId());
		List<InventoryManageDetailVO> details = new LinkedList<InventoryManageDetailVO>();
		for (int i = 0; i < outgoingVO.getDetails().size(); i++) {
			InventoryManageDetailVO inventoryManageDetailVO = new InventoryManageDetailVO();
			inventoryManageDetailVO.setNumber(-outgoingVO.getDetails().get(i)
					.getNumberReal());
			inventoryManageDetailVO.setSaleUnit(outgoingVO.getDetails().get(i)
					.getSaleUnit());
			inventoryManageDetailVO.setSaleUnitId(outgoingVO.getDetails()
					.get(i).getSaleUnitId());
			inventoryManageDetailVO.setSpec(outgoingVO.getDetails().get(i)
					.getSpec());
			inventoryManageDetailVO.setSpecUnit(outgoingVO.getDetails().get(i)
					.getSpecUnit());
			inventoryManageDetailVO.setSpecUnitId(outgoingVO.getDetails()
					.get(i).getSpecUnitId());
			inventoryManageDetailVO.setSaleUnitType(outgoingVO.getDetails()
					.get(i).getSaleUnitType());
			inventoryManageDetailVO.setSkuName(outgoingVO.getDetails().get(i)
					.getSkuName());
			inventoryManageDetailVO.setPositionCode(outgoingVO.getDetails()
					.get(i).getPositionCode());
			inventoryManageDetailVO.setSkuId(outgoingVO.getDetails().get(i)
					.getSkuId());
			inventoryManageDetailVO.setPositionId(outgoingVO.getDetails()
					.get(i).getPositionId());
			inventoryManageDetailVO.setInventoryId(outgoingVO.getDetails()
                    .get(i).getId());
			details.add(inventoryManageDetailVO);
		}
		inventoryManageVO.setDetails(details);
		return inventoryManageVO;
	}

	public IInventoryManageDomainClient getInventoryManageDomainClient() {
		return inventoryManageDomainClient;
	}

	public void setInventoryManageDomainClient(
			IInventoryManageDomainClient inventoryManageDomainClient) {
		this.inventoryManageDomainClient = inventoryManageDomainClient;
	}

	public Boolean submit(Long id) {
		return outgoingService.submit(id);
	}

	public IPositionDomainClient getPositionDomainClient() {
		return positionDomainClient;
	}

	public void setPositionDomainClient(
			IPositionDomainClient positionDomainClient) {
		this.positionDomainClient = positionDomainClient;
	}

	public SkuService getSkuService() {
		return skuService;
	}

	public void setSkuService(SkuService skuService) {
		this.skuService = skuService;
	}

	public ValueService getValueService() {
		return valueService;
	}

	public void setValueService(ValueService valueService) {
		this.valueService = valueService;
	}

    public CustomerService getCustomerService() {
      return customerService;
    }
  
    public void setCustomerService(CustomerService customerService) {
      this.customerService = customerService;
    }

    public BackGroundUserService getBackgrounduserservice() {
      return backgrounduserservice;
    }

    public void setBackgrounduserservice(BackGroundUserService backgrounduserservice) {
      this.backgrounduserservice = backgrounduserservice;
    }

}
