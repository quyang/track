package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.vo.ProductVO;
import com.xianzaishi.wms.track.vo.ProductQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.service.itf.IProductService;
import com.xianzaishi.wms.track.manage.itf.IProductManage;

public class ProductServiceImpl implements IProductService {
	@Autowired
	private IProductManage productManage = null;

	public IProductManage getProductManage() {
		return productManage;
	}

	public void setProductManage(IProductManage productManage) {
		this.productManage = productManage;
	}

	public Long addProductVO(ProductVO productVO) {
		return productManage.addProductVO(productVO);
	}

	public List<ProductVO> queryProductVOList(ProductQueryVO productQueryVO) {
		return productManage.queryProductVOList(productQueryVO);
	}

	public Integer queryProductVOCount(ProductQueryVO productQueryVO) {
		return productManage.queryProductVOCount(productQueryVO);
	}

	public ProductVO getProductVOByID(Long id) {
		return (ProductVO) productManage.getProductVOByID(id);
	}

	public Boolean modifyProductVO(ProductVO productVO) {
		return productManage.modifyProductVO(productVO);
	}

	public Boolean deleteProductVOByID(Long id) {
		return productManage.deleteProductVOByID(id);
	}

	public Boolean submit(Long id) {
		return productManage.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return productManage.audit(id, auditor);
	}

	public Boolean accounted(Long id) {
		return productManage.accounted(id);
	}
}
