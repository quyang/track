package com.xianzaishi.wms.job.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.job.domain.service.itf.IJobDomainService;
import com.xianzaishi.wms.job.service.itf.IJobService;
import com.xianzaishi.wms.job.vo.JobQueryVO;
import com.xianzaishi.wms.job.vo.JobVO;

public class JobDomainServiceImpl implements IJobDomainService {

	@Autowired
	private IJobService jobService = null;

	public Boolean addJob(JobVO jobVO) {
		return jobService.addJobVO(jobVO);
	}

	public IJobService getJobService() {
		return jobService;
	}

	public void setJobService(IJobService jobService) {
		this.jobService = jobService;
	}

	public JobVO getJob(Long jobID) {
		return jobService.getJobVOByID(jobID);
	}

	public QueryResultVO<JobVO> getJobPending(Long jobID) {
		JobQueryVO jobQueryVO = new JobQueryVO();
		jobQueryVO.setStatu(0);
		QueryResultVO<JobVO> queryResult = new QueryResultVO<>();
		queryResult.setItems(jobService.queryJobVOList(jobQueryVO));
		queryResult.setTotalCount(jobService.queryJobVOCount(jobQueryVO));
		return queryResult;
	}

	public Boolean processed(Long jobID) {
		return jobService.processed(jobID);
	}

	public List<JobVO> queryJobVOList(JobQueryVO jobQueryVO) {
		return jobService.queryJobVOList(jobQueryVO);
	}
}
