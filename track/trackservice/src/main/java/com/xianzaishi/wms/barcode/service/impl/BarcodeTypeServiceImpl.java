package com.xianzaishi.wms.barcode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.barcode.vo.BarcodeTypeVO;
import com.xianzaishi.wms.barcode.vo.BarcodeTypeQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.barcode.service.itf.IBarcodeTypeService;
import com.xianzaishi.wms.barcode.manage.itf.IBarcodeTypeManage;

public class BarcodeTypeServiceImpl implements IBarcodeTypeService {
	@Autowired
	private IBarcodeTypeManage barcodeTypeManage = null;

	public IBarcodeTypeManage getBarcodeTypeManage() {
		return barcodeTypeManage;
	}

	public void setBarcodeTypeManage(IBarcodeTypeManage barcodeTypeManage) {
		this.barcodeTypeManage = barcodeTypeManage;
	}
	
	public Boolean addBarcodeTypeVO(BarcodeTypeVO barcodeTypeVO) {
		barcodeTypeManage.addBarcodeTypeVO(barcodeTypeVO);
		return true;
	}

	public List<BarcodeTypeVO> queryBarcodeTypeVOList(BarcodeTypeQueryVO barcodeTypeQueryVO) {
		return barcodeTypeManage.queryBarcodeTypeVOList(barcodeTypeQueryVO);
	}

	public BarcodeTypeVO getBarcodeTypeVOByID(Long id) {
		return (BarcodeTypeVO) barcodeTypeManage.getBarcodeTypeVOByID(id);
	}

	public Boolean modifyBarcodeTypeVO(BarcodeTypeVO barcodeTypeVO) {
		return barcodeTypeManage.modifyBarcodeTypeVO(barcodeTypeVO);
	}

	public Boolean deleteBarcodeTypeVOByID(Long id) {
		return barcodeTypeManage.deleteBarcodeTypeVOByID(id);
	}
}
