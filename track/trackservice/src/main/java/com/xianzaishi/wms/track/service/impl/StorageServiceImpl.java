package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.manage.itf.IStorageManage;
import com.xianzaishi.wms.track.service.itf.IStorageService;
import com.xianzaishi.wms.track.vo.StorageQueryVO;
import com.xianzaishi.wms.track.vo.StorageVO;

public class StorageServiceImpl implements IStorageService {
	@Autowired
	private IStorageManage storageManage = null;

	public IStorageManage getStorageManage() {
		return storageManage;
	}

	public void setStorageManage(IStorageManage storageManage) {
		this.storageManage = storageManage;
	}

	public Long addStorageVO(StorageVO storageVO) {
		return storageManage.addStorageVO(storageVO);
	}

	public List<StorageVO> queryStorageVOList(StorageQueryVO storageQueryVO) {
		return storageManage.queryStorageVOList(storageQueryVO);
	}

	public Integer queryStorageVOCount(StorageQueryVO storageQueryVO) {
		return storageManage.queryStorageVOCount(storageQueryVO);
	}

	public StorageVO getStorageVOByID(Long id) {
		return (StorageVO) storageManage.getStorageVOByID(id);
	}

	public Boolean modifyStorageVO(StorageVO storageVO) {
		return storageManage.modifyStorageVO(storageVO);
	}

	public Boolean deleteStorageVOByID(Long id) {
		return storageManage.deleteStorageVOByID(id);
	}

	public Boolean submit(Long id) {
		return storageManage.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return storageManage.audit(id, auditor);
	}

	public Boolean accounted(Long id) {
		return storageManage.accounted(id);
	}
}
