package com.xianzaishi.wms.track.domain.service.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.track.vo.OutgoingSaleDetailQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingSaleDetailVO;
import com.xianzaishi.wms.track.vo.OutgoingSaleQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingSaleVO;

public interface IOutgoingSaleDomainService {

	// domain
	public Boolean createOutgoingSaleDomain(OutgoingSaleVO outgoingSaleVO);

	public Boolean createAndAccountedOutgoingSaleDomain(
			OutgoingSaleVO outgoingSaleVO);

	public OutgoingSaleVO getOutgoingSaleDomainByID(Long outgoingSaleID);

	public Boolean deleteOutgoingSaleDomain(Long id);

	// head
	public Boolean addOutgoingSaleVO(OutgoingSaleVO outgoingSaleVO);

	public QueryResultVO<OutgoingSaleVO> queryOutgoingSaleVOList(
			OutgoingSaleQueryVO outgoingSaleQueryVO);

	public OutgoingSaleVO getOutgoingSaleVOByID(Long id);

	public Boolean modifyOutgoingSaleVO(OutgoingSaleVO outgoingSaleVO);

	public Boolean deleteOutgoingSaleVO(Long outgoingSaleID);

	// body
	public Boolean createOutgoingSaleDetailVO(
			OutgoingSaleDetailVO outgoingSaleDetailVO);

	public Boolean batchCreateOutgoingSaleDetailVO(
			List<OutgoingSaleDetailVO> outgoingSaleDetailVOs);

	public List<OutgoingSaleDetailVO> queryOutgoingSaleDetailVOList(
			OutgoingSaleDetailQueryVO outgoingSaleDetailQueryVO);

	public List<OutgoingSaleDetailVO> getOutgoingSaleDetailVOListByOutgoingSaleID(
			Long outgoingSaleID);

	public OutgoingSaleDetailVO getOutgoingSaleDetailVOByID(Long id);

	public Boolean modifyOutgoingSaleDetailVO(
			OutgoingSaleDetailVO outgoingSaleDetailVO);

	public Boolean batchModifyOutgoingSaleDetailVOs(
			List<OutgoingSaleDetailVO> outgoingSaleDetailVOs);

	public Boolean deleteOutgoingSaleDetailVO(Long id);

	public Boolean deleteOutgoingSaleDetailVOByOutgoingSaleID(
			Long outgoingSaleID);

	public Boolean batchDeleteOutgoingSaleDetailVOByOutgoingSaleID(
			List<Long> storyDetailIDs);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

	public Boolean accounted(Long id);

}
