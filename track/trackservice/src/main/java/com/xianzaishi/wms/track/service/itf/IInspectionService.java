package com.xianzaishi.wms.track.service.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.InspectionQueryVO;
import com.xianzaishi.wms.track.vo.InspectionVO;

public interface IInspectionService {

	public Long addInspectionVO(InspectionVO inspectionVO);

	public List<InspectionVO> queryInspectionVOList(
			InspectionQueryVO inspectionQueryVO);

	public Integer queryInspectionVOCount(InspectionQueryVO inspectionQueryVO);

	public InspectionVO getInspectionVOByID(Long id);

	public Boolean modifyInspectionVO(InspectionVO inspectionVO);

	public Boolean deleteInspectionVOByID(Long id);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

}