package com.xianzaishi.wms.oplog.domain.client.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.oplog.domain.client.itf.IProductionShelvingLogDomainClient;
import com.xianzaishi.wms.oplog.domain.service.itf.IProductionShelvingLogDomainService;
import com.xianzaishi.wms.oplog.vo.ProductionShelvingLogVO;

public class ProductionShelvingLogDomainClient implements
		IProductionShelvingLogDomainClient {

	@Autowired
	private IProductionShelvingLogDomainService productionShelvingLogDomainService = null;

	public SimpleResultVO<Boolean> batchAddProductionShelvingLog(
			List<ProductionShelvingLogVO> productionShelvingLogVOs) {
		return SimpleResultVO
				.buildSuccessResult(productionShelvingLogDomainService
						.batchAddProductionShelvingLog(productionShelvingLogVOs));
	}

	public IProductionShelvingLogDomainService getProductionShelvingLogDomainService() {
		return productionShelvingLogDomainService;
	}

	public void setProductionShelvingLogDomainService(
			IProductionShelvingLogDomainService productionShelvingLogDomainService) {
		this.productionShelvingLogDomainService = productionShelvingLogDomainService;
	}

}
