package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.vo.StorageVO;
import com.xianzaishi.wms.track.vo.StorageDO;
import com.xianzaishi.wms.track.vo.StorageQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.manage.itf.IStorageManage;
import com.xianzaishi.wms.track.dao.itf.IStorageDao;

public class StorageManageImpl implements IStorageManage {
	@Autowired
	private IStorageDao storageDao = null;

	private void validate(StorageVO storageVO) {
		if (storageVO.getAgencyId() == null || storageVO.getAgencyId() <= 0) {
			throw new BizException("agencyID error：" + storageVO.getAgencyId());
		}
		if (storageVO.getOperate() == null || storageVO.getOperate() <= 0) {
			throw new BizException("operator error：" + storageVO.getOperate());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IStorageDao getStorageDao() {
		return storageDao;
	}

	public void setStorageDao(IStorageDao storageDao) {
		this.storageDao = storageDao;
	}

	public Long addStorageVO(StorageVO storageVO) {
		validate(storageVO);
		return (Long) storageDao.addDO(storageVO);
	}

	public List<StorageVO> queryStorageVOList(StorageQueryVO storageQueryVO) {
		return storageDao.queryDO(storageQueryVO);
	}

	public StorageVO getStorageVOByID(Long id) {
		return (StorageVO) storageDao.getDOByID(id);
	}

	public Boolean modifyStorageVO(StorageVO storageVO) {
		return storageDao.updateDO(storageVO);
	}

	public Boolean deleteStorageVOByID(Long id) {
		return storageDao.delDO(id);
	}

	public Boolean submit(Long id) {
		return storageDao.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return storageDao.audit(id, auditor);
	}

	public Boolean accounted(Long id) {
		return storageDao.accounted(id);
	}

	public Integer queryStorageVOCount(StorageQueryVO storageQueryVO) {
		return storageDao.queryCount(storageQueryVO);
	}
}
