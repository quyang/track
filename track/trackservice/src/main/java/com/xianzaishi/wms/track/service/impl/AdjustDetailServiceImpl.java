package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.manage.itf.IAdjustDetailManage;
import com.xianzaishi.wms.track.service.itf.IAdjustDetailService;
import com.xianzaishi.wms.track.vo.AdjustDetailQueryVO;
import com.xianzaishi.wms.track.vo.AdjustDetailVO;

public class AdjustDetailServiceImpl implements IAdjustDetailService {
	@Autowired
	private IAdjustDetailManage adjustDetailManage = null;

	public IAdjustDetailManage getAdjustDetailManage() {
		return adjustDetailManage;
	}

	public void setAdjustDetailManage(IAdjustDetailManage adjustDetailManage) {
		this.adjustDetailManage = adjustDetailManage;
	}

	public Boolean addAdjustDetailVO(AdjustDetailVO adjustDetailVO) {
		adjustDetailManage.addAdjustDetailVO(adjustDetailVO);
		return true;
	}

	public List<AdjustDetailVO> queryAdjustDetailVOList(
			AdjustDetailQueryVO adjustDetailQueryVO) {
		return adjustDetailManage.queryAdjustDetailVOList(adjustDetailQueryVO);
	}

	public AdjustDetailVO getAdjustDetailVOByID(Long id) {
		return (AdjustDetailVO) adjustDetailManage.getAdjustDetailVOByID(id);
	}

	public Boolean modifyAdjustDetailVO(AdjustDetailVO adjustDetailVO) {
		return adjustDetailManage.modifyAdjustDetailVO(adjustDetailVO);
	}

	public Boolean deleteAdjustDetailVOByID(Long id) {
		return adjustDetailManage.deleteAdjustDetailVOByID(id);
	}

	public List<AdjustDetailVO> getAdjustDetailVOByAdjustID(Long id) {
		return adjustDetailManage.getAdjustDetailVOByAdjustID(id);
	}

	public Boolean batchAddAdjustDetailVO(List<AdjustDetailVO> adjustDetailVOs) {
		return adjustDetailManage.batchAddAdjustDetailVO(adjustDetailVOs);
	}

	public Boolean batchModifyAdjustDetailVO(
			List<AdjustDetailVO> adjustDetailVOs) {
		return adjustDetailManage.batchModifyAdjustDetailVO(adjustDetailVOs);
	}

	public Boolean batchDeleteAdjustDetailVO(
			List<AdjustDetailVO> adjustDetailVOs) {
		return adjustDetailManage.batchDeleteAdjustDetailVO(adjustDetailVOs);
	}

	public Boolean batchDeleteAdjustDetailVOByID(List<Long> storyDetailIDs) {
		return adjustDetailManage.batchDeleteAdjustDetailVOByID(storyDetailIDs);
	}
}
