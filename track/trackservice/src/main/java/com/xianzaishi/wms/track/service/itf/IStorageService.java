package com.xianzaishi.wms.track.service.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.StorageQueryVO;
import com.xianzaishi.wms.track.vo.StorageVO;

public interface IStorageService {

	public Long addStorageVO(StorageVO storageVO);

	public List<StorageVO> queryStorageVOList(StorageQueryVO storageQueryVO);
	
	public Integer queryStorageVOCount(StorageQueryVO storageQueryVO);

	public StorageVO getStorageVOByID(Long id);

	public Boolean modifyStorageVO(StorageVO storageVO);

	public Boolean deleteStorageVOByID(Long id);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

	public Boolean accounted(Long id);

}