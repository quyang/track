package com.xianzaishi.wms.track.manage.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.user.BackGroundUserService;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.IOutgoingDao;
import com.xianzaishi.wms.track.manage.itf.IOutgoingManage;
import com.xianzaishi.wms.track.vo.OutgoingQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingVO;

public class OutgoingManageImpl implements IOutgoingManage {
	@Autowired
	private IOutgoingDao outgoingDao = null;
	
	private BackGroundUserService backGroundUserService = null;
	private void validate(OutgoingVO outgoingVO) {
		if (outgoingVO.getAgencyId() == null || outgoingVO.getAgencyId() <= 0) {
			throw new BizException("agencyID error：" + outgoingVO.getAgencyId());
		}
		if (outgoingVO.getOperate() == null || outgoingVO.getOperate() <= 0) {
			throw new BizException("operator error：" + outgoingVO.getOperate());
		}
		if (outgoingVO.getOpReason() == null || outgoingVO.getOpReason() <= 0) {
			throw new BizException("opReason error：" + outgoingVO.getOpReason());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IOutgoingDao getOutgoingDao() {
		return outgoingDao;
	}

	public void setOutgoingDao(IOutgoingDao outgoingDao) {
		this.outgoingDao = outgoingDao;
	}

	public Long addOutgoingVO(OutgoingVO outgoingVO) {
		validate(outgoingVO);
		return (Long) outgoingDao.addDO(outgoingVO);
	}

	public List<OutgoingVO> queryOutgoingVOList(OutgoingQueryVO outgoingQueryVO) {
  	    List<OutgoingVO> outgoingVOs = outgoingDao.queryDO(outgoingQueryVO);
  	    List<Integer> userIdList = Lists.newArrayList();
        for(OutgoingVO outgoingVO : outgoingVOs){
          Integer operateId = null;
          Integer auditorId = null;
          if(null != outgoingVO.getOperate()){
            operateId = outgoingVO.getOperate().intValue();
            userIdList.add(operateId);
          }
          if(null != outgoingVO.getAuditor()){
            auditorId = outgoingVO.getAuditor().intValue();
            userIdList.add(auditorId);
          }
        }
        Result<List<BackGroundUserDTO>> backGroundUserResult = backGroundUserService.queryUserDTOByIdList(userIdList);
        Map<Integer,String> userNameCache = Maps.newHashMap();
        if(null != backGroundUserResult && backGroundUserResult.getSuccess() && null != backGroundUserResult.getModule()){
          List<BackGroundUserDTO> backGroundUserDTOs = backGroundUserResult.getModule();
          for (BackGroundUserDTO backGroundUserDTO : backGroundUserDTOs) {
            userNameCache.put(backGroundUserDTO.getUserId(),backGroundUserDTO.getName());
          }
        }
        for(OutgoingVO outgoingVO : outgoingVOs){
          if(null != outgoingVO.getAuditor()){
            outgoingVO.setAuditorName(userNameCache.get(outgoingVO.getAuditor().intValue()));
          }
          if(null != outgoingVO.getOperate()){
            outgoingVO.setOperateName(userNameCache.get(outgoingVO.getOperate().intValue()));
          }
        }
		return outgoingVOs;
	}

	public OutgoingVO getOutgoingVOByID(Long id) {
	    OutgoingVO outgoingVO = (OutgoingVO) outgoingDao.getDOByID(id);
	    List<Integer> userIdList = Lists.newArrayList();
	    Integer operateId = null;
	    Integer auditorId = null;
	    if(null != outgoingVO.getOperate()){
	      operateId = outgoingVO.getOperate().intValue();
	      userIdList.add(operateId);
	    }
	    if(null != outgoingVO.getAuditor()){
	      auditorId = outgoingVO.getAuditor().intValue();
	      userIdList.add(outgoingVO.getAuditor().intValue());
	    }
	    Result<List<BackGroundUserDTO>> backGroundUserResult = backGroundUserService.queryUserDTOByIdList(userIdList);
	    Map<Integer,String> userNameCache = Maps.newHashMap();
	    if(null != backGroundUserResult && backGroundUserResult.getSuccess() && null != backGroundUserResult.getModule()){
	      List<BackGroundUserDTO> backGroundUserDTOs = backGroundUserResult.getModule();
	      for (BackGroundUserDTO backGroundUserDTO : backGroundUserDTOs) {
            userNameCache.put(backGroundUserDTO.getUserId(),backGroundUserDTO.getName());
          }
	    }
	    outgoingVO.setAuditorName(userNameCache.get(auditorId));
	    outgoingVO.setOperateName(userNameCache.get(operateId));
		return outgoingVO;
	}

	public Boolean modifyOutgoingVO(OutgoingVO outgoingVO) {
		return outgoingDao.updateDO(outgoingVO);
	}

	public Boolean deleteOutgoingVOByID(Long id) {
		return outgoingDao.delDO(id);
	}

	public Boolean submit(Long id) {
		return outgoingDao.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return outgoingDao.audit(id, auditor);
	}

	public Boolean accounted(Long id) {
		return outgoingDao.accounted(id);
	}

	public Integer queryOutgoingVOCount(OutgoingQueryVO outgoingQueryVO) {
		return outgoingDao.queryCount(outgoingQueryVO);
	}

    public BackGroundUserService getBackGroundUserService() {
      return backGroundUserService;
    }
  
    public void setBackGroundUserService(BackGroundUserService backGroundUserService) {
      this.backGroundUserService = backGroundUserService;
    }
	
}
