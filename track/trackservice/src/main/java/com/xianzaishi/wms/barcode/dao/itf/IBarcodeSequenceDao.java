package com.xianzaishi.wms.barcode.dao.itf;

import com.xianzaishi.wms.barcode.vo.BarcodeSequenceVO;
import com.xianzaishi.wms.common.dao.itf.IBaseDao;

public interface IBarcodeSequenceDao extends IBaseDao {
	public Boolean order(BarcodeSequenceVO barcodeSequenceVO);
}