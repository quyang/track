package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.manage.itf.ISpoilageDetailManage;
import com.xianzaishi.wms.track.service.itf.ISpoilageDetailService;
import com.xianzaishi.wms.track.vo.SpoilageDetailQueryVO;
import com.xianzaishi.wms.track.vo.SpoilageDetailVO;

public class SpoilageDetailServiceImpl implements ISpoilageDetailService {
	@Autowired
	private ISpoilageDetailManage spoilageDetailManage = null;

	public ISpoilageDetailManage getSpoilageDetailManage() {
		return spoilageDetailManage;
	}

	public void setSpoilageDetailManage(
			ISpoilageDetailManage spoilageDetailManage) {
		this.spoilageDetailManage = spoilageDetailManage;
	}

	public Boolean addSpoilageDetailVO(SpoilageDetailVO spoilageDetailVO) {
		spoilageDetailManage.addSpoilageDetailVO(spoilageDetailVO);
		return true;
	}

	public List<SpoilageDetailVO> querySpoilageDetailVOList(
			SpoilageDetailQueryVO spoilageDetailQueryVO) {
		return spoilageDetailManage
				.querySpoilageDetailVOList(spoilageDetailQueryVO);
	}

	public SpoilageDetailVO getSpoilageDetailVOByID(Long id) {
		return (SpoilageDetailVO) spoilageDetailManage
				.getSpoilageDetailVOByID(id);
	}

	public Boolean modifySpoilageDetailVO(SpoilageDetailVO spoilageDetailVO) {
		return spoilageDetailManage.modifySpoilageDetailVO(spoilageDetailVO);
	}

	public Boolean deleteSpoilageDetailVOByID(Long id) {
		return spoilageDetailManage.deleteSpoilageDetailVOByID(id);
	}

	public List<SpoilageDetailVO> getSpoilageDetailVOBySpoilageID(Long id) {
		return spoilageDetailManage.getSpoilageDetailVOBySpoilageID(id);
	}

	public Boolean batchAddSpoilageDetailVO(
			List<SpoilageDetailVO> spoilageDetailVOs) {
		return spoilageDetailManage.batchAddSpoilageDetailVO(spoilageDetailVOs);
	}

	public Boolean batchModifySpoilageDetailVO(
			List<SpoilageDetailVO> spoilageDetailVOs) {
		return spoilageDetailManage.batchModifySpoilageDetailVO(spoilageDetailVOs);
	}

	public Boolean batchDeleteSpoilageDetailVO(
			List<SpoilageDetailVO> spoilageDetailVOs) {
		return spoilageDetailManage.batchDeleteSpoilageDetailVO(spoilageDetailVOs);
	}

	public Boolean batchDeleteSpoilageDetailVOByID(List<Long> storyDetailIDs) {
		return spoilageDetailManage
				.batchDeleteSpoilageDetailVOByID(storyDetailIDs);
	}
}
