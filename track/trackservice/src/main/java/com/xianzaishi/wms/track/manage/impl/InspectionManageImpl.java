package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.IInspectionDao;
import com.xianzaishi.wms.track.manage.itf.IInspectionManage;
import com.xianzaishi.wms.track.vo.InspectionQueryVO;
import com.xianzaishi.wms.track.vo.InspectionVO;

public class InspectionManageImpl implements IInspectionManage {
	@Autowired
	private IInspectionDao inspectionDao = null;

	private void validate(InspectionVO inspectionVO) {
		if (inspectionVO.getDeliveryId() == null
				|| inspectionVO.getDeliveryId() <= 0) {
			throw new BizException("deliveryID error："
					+ inspectionVO.getDeliveryId());
		}
		if (inspectionVO.getOperate() == null || inspectionVO.getOperate() <= 0) {
			throw new BizException("inspectionID error："
					+ inspectionVO.getOperate());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IInspectionDao getInspectionDao() {
		return inspectionDao;
	}

	public void setInspectionDao(IInspectionDao inspectionDao) {
		this.inspectionDao = inspectionDao;
	}

	public Long addInspectionVO(InspectionVO inspectionVO) {
		validate(inspectionVO);
		return (Long) inspectionDao.addDO(inspectionVO);
	}

	public List<InspectionVO> queryInspectionVOList(
			InspectionQueryVO inspectionQueryVO) {
		return inspectionDao.queryDO(inspectionQueryVO);
	}

	public InspectionVO getInspectionVOByID(Long id) {
		return (InspectionVO) inspectionDao.getDOByID(id);
	}

	public Boolean modifyInspectionVO(InspectionVO inspectionVO) {
		return inspectionDao.updateDO(inspectionVO);
	}

	public Boolean deleteInspectionVOByID(Long id) {
		return inspectionDao.delDO(id);
	}

	public Boolean submit(Long id) {
		return inspectionDao.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return inspectionDao.audit(id, auditor);
	}

	public Integer queryInspectionVOCount(InspectionQueryVO inspectionQueryVO) {
		return inspectionDao.queryCount(inspectionQueryVO);
	}
}
