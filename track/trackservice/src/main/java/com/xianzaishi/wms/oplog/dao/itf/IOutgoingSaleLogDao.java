package com.xianzaishi.wms.oplog.dao.itf;

import java.util.List;

import com.xianzaishi.wms.common.dao.itf.IBaseDao;
import com.xianzaishi.wms.oplog.vo.OutgoingSaleLogVO;

public interface IOutgoingSaleLogDao extends IBaseDao {
	public Boolean batchAddOutgoingSaleLog(
			List<OutgoingSaleLogVO> outgoingSaleLogVOs);
}