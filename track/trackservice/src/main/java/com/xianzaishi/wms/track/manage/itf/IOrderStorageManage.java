package com.xianzaishi.wms.track.manage.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.OrderStorageQueryVO;
import com.xianzaishi.wms.track.vo.OrderStorageVO;

public interface IOrderStorageManage {

	public Long addOrderStorageVO(OrderStorageVO orderStorageVO);

	public List<OrderStorageVO> queryOrderStorageVOList(
			OrderStorageQueryVO orderStorageQueryVO);

	public Integer queryOrderStorageVOCount(
			OrderStorageQueryVO orderStorageQueryVO);

	public OrderStorageVO getOrderStorageVOByID(Long id);

	public Boolean modifyOrderStorageVO(OrderStorageVO orderStorageVO);

	public Boolean deleteOrderStorageVOByID(Long id);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

	public Boolean accounted(Long id);

}