package com.xianzaishi.wms.track.domain.client.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.domain.client.itf.IAdjustDomainClient;
import com.xianzaishi.wms.track.domain.service.itf.IAdjustDomainService;
import com.xianzaishi.wms.track.vo.AdjustDetailQueryVO;
import com.xianzaishi.wms.track.vo.AdjustDetailVO;
import com.xianzaishi.wms.track.vo.AdjustQueryVO;
import com.xianzaishi.wms.track.vo.AdjustVO;

public class AdjustDomainClient implements IAdjustDomainClient {
	private static final Logger logger = Logger
			.getLogger(AdjustDomainClient.class);
	@Autowired
	private IAdjustDomainService adjustDomainService = null;

	public SimpleResultVO<Boolean> auditAdjust(Long adjustID, Long operate) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(adjustDomainService
					.auditAdjust(adjustID, operate));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> createAdjustDomain(AdjustVO adjustVO) {
		SimpleResultVO<Boolean> flag = null;
		try {
			if (adjustVO.getOpReason() == null) {
				adjustVO.setOpReason(12);
			}
			flag = SimpleResultVO.buildSuccessResult(adjustDomainService
					.createAdjustDomain(adjustVO));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<AdjustVO> getAdjustDomainByID(Long adjustID) {
		SimpleResultVO<AdjustVO> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(adjustDomainService
					.getAdjustDomainByID(adjustID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> deleteAdjustDomain(Long adjustID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(adjustDomainService
					.deleteAdjustDomain(adjustID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> addAdjustVO(AdjustVO adjustVO) {
		if (adjustVO.getOpReason() == null) {
			adjustVO.setOpReason(12);
		}
		return SimpleResultVO.buildSuccessResult(adjustDomainService
				.addAdjustVO(adjustVO));
	}

	public SimpleResultVO<QueryResultVO<AdjustVO>> queryAdjustVOList(
			AdjustQueryVO adjustQueryVO) {
		return SimpleResultVO.buildSuccessResult(adjustDomainService
				.queryAdjustVOList(adjustQueryVO));
	}

	public SimpleResultVO<AdjustVO> getAdjustVOByID(Long id) {
		return SimpleResultVO.buildSuccessResult(adjustDomainService
				.getAdjustVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyAdjustVO(AdjustVO adjustVO) {
		return SimpleResultVO.buildSuccessResult(adjustDomainService
				.modifyAdjustVO(adjustVO));
	}

	public SimpleResultVO<Boolean> deleteAdjustVO(Long id) {
		return SimpleResultVO.buildSuccessResult(adjustDomainService
				.deleteAdjustVO(id));
	}

	public SimpleResultVO<Boolean> createAdjustDetailVO(
			AdjustDetailVO adjustDetailVO) {
		return SimpleResultVO.buildSuccessResult(adjustDomainService
				.createAdjustDetailVO(adjustDetailVO));
	}

	public SimpleResultVO<Boolean> batchCreateAdjustDetailVO(
			List<AdjustDetailVO> adjustDetailVOs) {
		return SimpleResultVO.buildSuccessResult(adjustDomainService
				.batchCreateAdjustDetailVO(adjustDetailVOs));
	}

	public SimpleResultVO<List<AdjustDetailVO>> queryAdjustDetailVOList(
			AdjustDetailQueryVO adjustDetailQueryVO) {
		return SimpleResultVO.buildSuccessResult(adjustDomainService
				.queryAdjustDetailVOList(adjustDetailQueryVO));
	}

	public SimpleResultVO<List<AdjustDetailVO>> getAdjustDetailVOListByAdjustID(
			Long adjustID) {
		return SimpleResultVO.buildSuccessResult(adjustDomainService
				.getAdjustDetailVOListByAdjustID(adjustID));
	}

	public SimpleResultVO<AdjustDetailQueryVO> getAdjustDetailVOByID(Long id) {
		return SimpleResultVO.buildSuccessResult(adjustDomainService
				.getAdjustDetailVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyAdjustDetailVO(
			AdjustDetailVO adjustDetailVO) {
		return SimpleResultVO.buildSuccessResult(adjustDomainService
				.modifyAdjustDetailVO(adjustDetailVO));
	}

	public SimpleResultVO<Boolean> batchModifyAdjustDetailVOs(
			List<AdjustDetailVO> adjustDetailVOs) {
		return SimpleResultVO.buildSuccessResult(adjustDomainService
				.batchModifyAdjustDetailVOs(adjustDetailVOs));
	}

	public SimpleResultVO<Boolean> deleteAdjustDetailVO(Long id) {
		return SimpleResultVO.buildSuccessResult(adjustDomainService
				.deleteAdjustDetailVO(id));
	}

	public SimpleResultVO<Boolean> deleteAdjustDetailVOByAdjustID(Long adjustID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(adjustDomainService
					.deleteAdjustDetailVOByAdjustID(adjustID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> batchDeleteAdjustDetailVOByAdjustID(
			List<Long> storyDetailIDs) {
		return SimpleResultVO.buildSuccessResult(adjustDomainService
				.batchDeleteAdjustDetailVOByAdjustID(storyDetailIDs));
	}

	public IAdjustDomainService getAdjustDomainService() {
		return adjustDomainService;
	}

	public void setAdjustDomainService(IAdjustDomainService adjustDomainService) {
		this.adjustDomainService = adjustDomainService;
	}

	public SimpleResultVO<Boolean> submit(Long id) {
		return SimpleResultVO
				.buildSuccessResult(adjustDomainService.submit(id));
	}

	public SimpleResultVO<Boolean> accounted(Long id) {
		return SimpleResultVO.buildSuccessResult(adjustDomainService
				.accounted(id));
	}

	public SimpleResultVO<Boolean> createAndAccount(AdjustVO adjustVO) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(adjustDomainService
					.createAndAccount(adjustVO));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

}
