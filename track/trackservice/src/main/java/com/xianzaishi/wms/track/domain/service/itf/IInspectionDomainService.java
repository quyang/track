package com.xianzaishi.wms.track.domain.service.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.track.vo.InspectionDetailQueryVO;
import com.xianzaishi.wms.track.vo.InspectionDetailVO;
import com.xianzaishi.wms.track.vo.InspectionQueryVO;
import com.xianzaishi.wms.track.vo.InspectionVO;

public interface IInspectionDomainService {

	// domain
	public Boolean createInspectionDomain(InspectionVO inspectionVO);

	public InspectionVO getInspectionDomainByID(Long inspectionID);

	public Boolean deleteInspectionDomain(Long id);

	// head
	public Boolean addInspectionVO(InspectionVO inspectionVO);

	public QueryResultVO<InspectionVO> queryInspectionVOList(
			InspectionQueryVO inspectionQueryVO);

	public InspectionVO getInspectionVOByID(Long id);

	public Boolean modifyInspectionVO(InspectionVO inspectionVO);

	public Boolean deleteInspectionVO(Long inspectionID);

	// body
	public Boolean createInspectionDetailVO(
			InspectionDetailVO inspectionDetailVO);

	public Boolean batchCreateInspectionDetailVO(
			List<InspectionDetailVO> inspectionDetailVOs);

	public List<InspectionDetailVO> queryInspectionDetailVOList(
			InspectionDetailQueryVO inspectionDetailQueryVO);

	public List<InspectionDetailVO> getInspectionDetailVOListByInspectionID(
			Long inspectionID);

	public InspectionDetailVO getInspectionDetailVOByID(Long id);

	public Boolean modifyInspectionDetailVO(
			InspectionDetailVO inspectionDetailVO);

	public Boolean batchModifyInspectionDetailVOs(
			List<InspectionDetailVO> inspectionDetailVOs);

	public Boolean deleteInspectionDetailVO(Long id);

	public Boolean deleteInspectionDetailVOByInspectionID(Long inspectionID);

	public Boolean batchDeleteInspectionDetailVOByInspectionID(
			List<Long> storyDetailIDs);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

}
