package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.manage.itf.IStorageDetailManage;
import com.xianzaishi.wms.track.service.itf.IStorageDetailService;
import com.xianzaishi.wms.track.vo.StorageDetailQueryVO;
import com.xianzaishi.wms.track.vo.StorageDetailVO;

public class StorageDetailServiceImpl implements IStorageDetailService {
	@Autowired
	private IStorageDetailManage storageDetailManage = null;

	public IStorageDetailManage getStorageDetailManage() {
		return storageDetailManage;
	}

	public void setStorageDetailManage(IStorageDetailManage storageDetailManage) {
		this.storageDetailManage = storageDetailManage;
	}

	public Boolean addStorageDetailVO(StorageDetailVO storageDetailVO) {
		storageDetailManage.addStorageDetailVO(storageDetailVO);
		return true;
	}

	public List<StorageDetailVO> queryStorageDetailVOList(
			StorageDetailQueryVO storageDetailQueryVO) {
		return storageDetailManage
				.queryStorageDetailVOList(storageDetailQueryVO);
	}

	public StorageDetailVO getStorageDetailVOByID(Long id) {
		return (StorageDetailVO) storageDetailManage.getStorageDetailVOByID(id);
	}

	public Boolean modifyStorageDetailVO(StorageDetailVO storageDetailVO) {
		return storageDetailManage.modifyStorageDetailVO(storageDetailVO);
	}

	public Boolean deleteStorageDetailVOByID(Long id) {
		return storageDetailManage.deleteStorageDetailVOByID(id);
	}

	public List<StorageDetailVO> getStorageDetailVOByStorageID(Long id) {
		return storageDetailManage.getStorageDetailVOByStorageID(id);
	}

	public Boolean batchAddStorageDetailVO(
			List<StorageDetailVO> storageDetailVOs) {
		return storageDetailManage.batchAddStorageDetailVO(storageDetailVOs);
	}

	public Boolean batchModifyStorageDetailVO(
			List<StorageDetailVO> storageDetailVOs) {
		return storageDetailManage.batchModifyStorageDetailVO(storageDetailVOs);
	}

	public Boolean batchDeleteStorageDetailVO(
			List<StorageDetailVO> storageDetailVOs) {
		return storageDetailManage.batchDeleteStorageDetailVO(storageDetailVOs);
	}

	public Boolean batchDeleteStorageDetailVOByID(List<Long> storyDetailIDs) {
		return storageDetailManage
				.batchDeleteStorageDetailVOByID(storyDetailIDs);
	}
}
