package com.xianzaishi.wms.oplog.domain.service.itf;

import java.util.List;

import com.xianzaishi.wms.oplog.vo.OutgoingSaleLogVO;

public interface IOutgoingSaleLogDomainService {
	public Boolean batchAddOutgoingSaleLog(
			List<OutgoingSaleLogVO> outgoingSaleLogVOs);
}
