package com.xianzaishi.wms.track.manage.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.BookingDeliveryQueryVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryVO;

public interface IBookingDeliveryManage {

	public Long addBookingDeliveryVO(BookingDeliveryVO bookingDeliveryVO);

	public List<BookingDeliveryVO> queryBookingDeliveryVOList(
			BookingDeliveryQueryVO bookingDeliveryQueryVO);

	public Integer queryBookingDeliveryVOCount(
			BookingDeliveryQueryVO bookingDeliveryQueryVO);

	public BookingDeliveryVO getBookingDeliveryVOByID(Long id);

	public Boolean modifyBookingDeliveryVO(BookingDeliveryVO bookingDeliveryVO);

	public Boolean deleteBookingDeliveryVOByID(Long id);

	public Boolean submit(Long id);

	public Boolean confirm(Long id, Long auditor);

}