package com.xianzaishi.wms.track.service.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.StorageDetailQueryVO;
import com.xianzaishi.wms.track.vo.StorageDetailVO;

public interface IStorageDetailService {

	public Boolean addStorageDetailVO(StorageDetailVO storageDetailVO);

	public List<StorageDetailVO> queryStorageDetailVOList(
			StorageDetailQueryVO storageDetailQueryVO);

	public StorageDetailVO getStorageDetailVOByID(Long id);

	public Boolean modifyStorageDetailVO(StorageDetailVO storageDetailVO);

	public Boolean deleteStorageDetailVOByID(Long id);

	public List<StorageDetailVO> getStorageDetailVOByStorageID(Long id);

	public Boolean batchAddStorageDetailVO(
			List<StorageDetailVO> storageDetailVOs);

	public Boolean batchModifyStorageDetailVO(
			List<StorageDetailVO> storageDetailVOs);

	public Boolean batchDeleteStorageDetailVO(
			List<StorageDetailVO> storageDetailVOs);

	public Boolean batchDeleteStorageDetailVOByID(List<Long> storyDetailIDs);
}