package com.xianzaishi.wms.track.dao.impl;

import com.xianzaishi.wms.track.dao.itf.IOrderStorageDao;
import com.xianzaishi.wms.track.vo.InspectionVO;
import com.xianzaishi.wms.track.vo.OrderStorageVO;
import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.common.exception.BizException;

public class OrderStorageDaoImpl extends BaseDaoAdapter implements
		IOrderStorageDao {
	public String getVOClassName() {
		return "OrderStorageDO";
	}

	public Boolean submit(Long id) {
		Boolean flag = false;
		try {
			if (id != null && id > 0) {
				int no = simpleSqlMapClientTemplate.update(getVOClassName()
						+ ".submit", id);
				if (no == 1) {
					flag = true;
				} else if (no > 1) {
					throw new BizException("影响的数据大于1条。");
				} else {
					throw new BizException("请检查单据状态。");
				}
			} else {
				throw new BizException("更新数据失败，ID错误：ID = " + id);
			}
		} catch (Exception e) {
			throw new BizException("更新数据失败。", e);
		}
		return flag;
	}

	public Boolean audit(Long id, Long auditor) {
		Boolean flag = false;
		try {
			if (id != null && id > 0 && auditor != null && auditor > 0) {
				OrderStorageVO orderStorageVO = new OrderStorageVO();
				orderStorageVO.setId(id);
				orderStorageVO.setAuditor(auditor);
				int no = simpleSqlMapClientTemplate.update(getVOClassName()
						+ ".audit", orderStorageVO);
				if (no == 1) {
					flag = true;
				} else if (no > 1) {
					throw new BizException("影响的数据大于1条。");
				} else {
					throw new BizException("请检查单据状态。");
				}
			} else {
				throw new BizException("更新数据失败，ID错误：ID = " + id);
			}
		} catch (Exception e) {
			throw new BizException("更新数据失败。", e);
		}
		return flag;
	}

	public Boolean accounted(Long id) {
		Boolean flag = false;
		try {
			if (id != null && id > 0) {
				int no = simpleSqlMapClientTemplate.update(getVOClassName()
						+ ".accounted", id);
				if (no == 1) {
					flag = true;
				} else if (no > 1) {
					throw new BizException("影响的数据大于1条。");
				} else {
					throw new BizException("请检查单据状态。");
				}
			} else {
				throw new BizException("更新数据失败，ID错误：ID = " + id);
			}
		} catch (Exception e) {
			throw new BizException("更新数据失败。", e);
		}
		return flag;
	}
}
