package com.xianzaishi.wms.job.manage.itf;

import java.util.List;

import com.xianzaishi.wms.job.vo.JobTypeVO;
import com.xianzaishi.wms.job.vo.JobTypeDO;
import com.xianzaishi.wms.job.vo.JobTypeQueryVO;

public interface IJobTypeManage {

	public Boolean addJobTypeVO(JobTypeVO jobTypeVO);

	public List<JobTypeVO> queryJobTypeVOList(JobTypeQueryVO jobTypeQueryVO);

	public JobTypeVO getJobTypeVOByID(Long id);

	public Boolean modifyJobTypeVO(JobTypeVO jobTypeVO);

	public Boolean deleteJobTypeVOByID(Long id);

}