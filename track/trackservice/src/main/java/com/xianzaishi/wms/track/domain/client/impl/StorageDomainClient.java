package com.xianzaishi.wms.track.domain.client.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.domain.client.itf.IStorageDomainClient;
import com.xianzaishi.wms.track.domain.service.itf.IStorageDomainService;
import com.xianzaishi.wms.track.vo.StorageDetailQueryVO;
import com.xianzaishi.wms.track.vo.StorageDetailVO;
import com.xianzaishi.wms.track.vo.StorageQueryVO;
import com.xianzaishi.wms.track.vo.StorageVO;

public class StorageDomainClient implements IStorageDomainClient {
	private static final Logger logger = Logger
			.getLogger(StorageDomainClient.class);
	@Autowired
	private IStorageDomainService storageDomainService = null;

	public SimpleResultVO<Boolean> auditStorage(Long storageID, Long operate) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(storageDomainService
					.auditStorage(storageID, operate));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> createStorageDomain(StorageVO storageVO) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(storageDomainService
					.createStorageDomain(storageVO));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<StorageVO> getStorageDomainByID(Long storageID) {
		SimpleResultVO<StorageVO> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(storageDomainService
					.getStorageDomainByID(storageID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> deleteStorageDomain(Long storageID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(storageDomainService
					.deleteStorageDomain(storageID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> addStorageVO(StorageVO storageVO) {
		return SimpleResultVO.buildSuccessResult(storageDomainService
				.addStorageVO(storageVO));
	}

	public SimpleResultVO<QueryResultVO<StorageVO>> queryStorageVOList(
			StorageQueryVO storageQueryVO) {
		return SimpleResultVO.buildSuccessResult(storageDomainService
				.queryStorageVOList(storageQueryVO));
	}

	public SimpleResultVO<StorageVO> getStorageVOByID(Long id) {
		return SimpleResultVO.buildSuccessResult(storageDomainService
				.getStorageVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyStorageVO(StorageVO storageVO) {
		return SimpleResultVO.buildSuccessResult(storageDomainService
				.modifyStorageVO(storageVO));
	}

	public SimpleResultVO<Boolean> deleteStorageVO(Long id) {
		return SimpleResultVO.buildSuccessResult(storageDomainService
				.deleteStorageVO(id));
	}

	public SimpleResultVO<Boolean> createStorageDetailVO(
			StorageDetailVO storageDetailVO) {
		return SimpleResultVO.buildSuccessResult(storageDomainService
				.createStorageDetailVO(storageDetailVO));
	}

	public SimpleResultVO<Boolean> batchCreateStorageDetailVO(
			List<StorageDetailVO> storageDetailVOs) {
		return SimpleResultVO.buildSuccessResult(storageDomainService
				.batchCreateStorageDetailVO(storageDetailVOs));
	}

	public SimpleResultVO<List<StorageDetailVO>> queryStorageDetailVOList(
			StorageDetailQueryVO storageDetailQueryVO) {
		return SimpleResultVO.buildSuccessResult(storageDomainService
				.queryStorageDetailVOList(storageDetailQueryVO));
	}

	public SimpleResultVO<List<StorageDetailVO>> getStorageDetailVOListByStorageID(
			Long storageID) {
		return SimpleResultVO.buildSuccessResult(storageDomainService
				.getStorageDetailVOListByStorageID(storageID));
	}

	public SimpleResultVO<StorageDetailQueryVO> getStorageDetailVOByID(Long id) {
		return SimpleResultVO.buildSuccessResult(storageDomainService
				.getStorageDetailVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyStorageDetailVO(
			StorageDetailVO storageDetailVO) {
		return SimpleResultVO.buildSuccessResult(storageDomainService
				.modifyStorageDetailVO(storageDetailVO));
	}

	public SimpleResultVO<Boolean> batchModifyStorageDetailVOs(
			List<StorageDetailVO> storageDetailVOs) {
		return SimpleResultVO.buildSuccessResult(storageDomainService
				.batchModifyStorageDetailVOs(storageDetailVOs));
	}

	public SimpleResultVO<Boolean> deleteStorageDetailVO(Long id) {
		return SimpleResultVO.buildSuccessResult(storageDomainService
				.deleteStorageDetailVO(id));
	}

	public SimpleResultVO<Boolean> deleteStorageDetailVOByStorageID(
			Long storageID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(storageDomainService
					.deleteStorageDetailVOByStorageID(storageID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> batchDeleteStorageDetailVOByStorageID(
			List<Long> storyDetailIDs) {
		return SimpleResultVO.buildSuccessResult(storageDomainService
				.batchDeleteStorageDetailVOByStorageID(storyDetailIDs));
	}

	public IStorageDomainService getStorageDomainService() {
		return storageDomainService;
	}

	public void setStorageDomainService(
			IStorageDomainService storageDomainService) {
		this.storageDomainService = storageDomainService;
	}

	public SimpleResultVO<Boolean> submit(Long id) {
		return SimpleResultVO.buildSuccessResult(storageDomainService
				.submit(id));
	}

	public SimpleResultVO<Boolean> audit(Long id, Long auditor) {
		return SimpleResultVO.buildSuccessResult(storageDomainService.audit(id,
				auditor));
	}

	public SimpleResultVO<Boolean> accounted(Long id) {
		return SimpleResultVO.buildSuccessResult(storageDomainService
				.accounted(id));
	}

	public SimpleResultVO<Boolean> createAndAccount(StorageVO storageVO) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(storageDomainService
					.createAndAccount(storageVO));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	/**
	 * 和入库方法一样，只不过返回某次入库id而已
	 * @param storageVO
	 * @return
	 */
	public SimpleResultVO<Long> getStoreIdCreateAndAccount(StorageVO storageVO) {
		SimpleResultVO<Long> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(storageDomainService
					.createAndAccountGetStoreId(storageVO));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}
}
