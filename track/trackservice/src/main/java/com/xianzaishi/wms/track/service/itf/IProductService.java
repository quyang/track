package com.xianzaishi.wms.track.service.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.ProductVO;
import com.xianzaishi.wms.track.vo.ProductQueryVO;

public interface IProductService {

	public Long addProductVO(ProductVO productVO);

	public List<ProductVO> queryProductVOList(ProductQueryVO productQueryVO);

	public Integer queryProductVOCount(ProductQueryVO productQueryVO);

	public ProductVO getProductVOByID(Long id);

	public Boolean modifyProductVO(ProductVO productVO);

	public Boolean deleteProductVOByID(Long id);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

	public Boolean accounted(Long id);
}