package com.xianzaishi.wms.track.domain.client.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.domain.client.itf.IOutgoingDomainClient;
import com.xianzaishi.wms.track.domain.service.itf.IOutgoingDomainService;
import com.xianzaishi.wms.track.vo.OutgoingDetailQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingDetailVO;
import com.xianzaishi.wms.track.vo.OutgoingQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingVO;

public class OutgoingDomainClient implements IOutgoingDomainClient {
	private static final Logger logger = Logger
			.getLogger(OutgoingDomainClient.class);
	@Autowired
	private IOutgoingDomainService outgoingDomainService = null;

	public SimpleResultVO<Boolean> auditOutgoing(Long outgoingID, Long operate) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(outgoingDomainService
					.auditOutgoing(outgoingID, operate));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> createOutgoingDomain(OutgoingVO outgoingVO) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(outgoingDomainService
					.createOutgoingDomain(outgoingVO));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<OutgoingVO> getOutgoingDomainByID(Long outgoingID) {
		SimpleResultVO<OutgoingVO> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(outgoingDomainService
					.getOutgoingDomainByID(outgoingID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> deleteOutgoingDomain(Long outgoingID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(outgoingDomainService
					.deleteOutgoingDomain(outgoingID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> addOutgoingVO(OutgoingVO outgoingVO) {
		return SimpleResultVO.buildSuccessResult(outgoingDomainService
				.addOutgoingVO(outgoingVO));
	}

	public SimpleResultVO<QueryResultVO<OutgoingVO>> queryOutgoingVOList(
			OutgoingQueryVO outgoingQueryVO) {
		return SimpleResultVO.buildSuccessResult(outgoingDomainService
				.queryOutgoingVOList(outgoingQueryVO));
	}

	public SimpleResultVO<OutgoingVO> getOutgoingVOByID(Long id) {
		return SimpleResultVO.buildSuccessResult(outgoingDomainService
				.getOutgoingVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyOutgoingVO(OutgoingVO outgoingVO) {
		return SimpleResultVO.buildSuccessResult(outgoingDomainService
				.modifyOutgoingVO(outgoingVO));
	}

	public SimpleResultVO<Boolean> deleteOutgoingVO(Long id) {
		return SimpleResultVO.buildSuccessResult(outgoingDomainService
				.deleteOutgoingVO(id));
	}

	public SimpleResultVO<Boolean> createOutgoingDetailVO(
			OutgoingDetailVO outgoingDetailVO) {
		return SimpleResultVO.buildSuccessResult(outgoingDomainService
				.createOutgoingDetailVO(outgoingDetailVO));
	}

	public SimpleResultVO<Boolean> batchCreateOutgoingDetailVO(
			List<OutgoingDetailVO> outgoingDetailVOs) {
		return SimpleResultVO.buildSuccessResult(outgoingDomainService
				.batchCreateOutgoingDetailVO(outgoingDetailVOs));
	}

	public SimpleResultVO<List<OutgoingDetailVO>> queryOutgoingDetailVOList(
			OutgoingDetailQueryVO outgoingDetailQueryVO) {
		return SimpleResultVO.buildSuccessResult(outgoingDomainService
				.queryOutgoingDetailVOList(outgoingDetailQueryVO));
	}

	public SimpleResultVO<List<OutgoingDetailVO>> getOutgoingDetailVOListByOutgoingID(
			Long outgoingID) {
		return SimpleResultVO.buildSuccessResult(outgoingDomainService
				.getOutgoingDetailVOListByOutgoingID(outgoingID));
	}

	public SimpleResultVO<OutgoingDetailQueryVO> getOutgoingDetailVOByID(Long id) {
		return SimpleResultVO.buildSuccessResult(outgoingDomainService
				.getOutgoingDetailVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyOutgoingDetailVO(
			OutgoingDetailVO outgoingDetailVO) {
		return SimpleResultVO.buildSuccessResult(outgoingDomainService
				.modifyOutgoingDetailVO(outgoingDetailVO));
	}

	public SimpleResultVO<Boolean> batchModifyOutgoingDetailVOs(
			List<OutgoingDetailVO> outgoingDetailVOs) {
		return SimpleResultVO.buildSuccessResult(outgoingDomainService
				.batchModifyOutgoingDetailVOs(outgoingDetailVOs));
	}

	public SimpleResultVO<Boolean> deleteOutgoingDetailVO(Long id) {
		return SimpleResultVO.buildSuccessResult(outgoingDomainService
				.deleteOutgoingDetailVO(id));
	}

	public SimpleResultVO<Boolean> deleteOutgoingDetailVOByOutgoingID(
			Long outgoingID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(outgoingDomainService
					.deleteOutgoingDetailVOByOutgoingID(outgoingID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> batchDeleteOutgoingDetailVOByOutgoingID(
			List<Long> storyDetailIDs) {
		return SimpleResultVO.buildSuccessResult(outgoingDomainService
				.batchDeleteOutgoingDetailVOByOutgoingID(storyDetailIDs));
	}

	public IOutgoingDomainService getOutgoingDomainService() {
		return outgoingDomainService;
	}

	public void setOutgoingDomainService(
			IOutgoingDomainService outgoingDomainService) {
		this.outgoingDomainService = outgoingDomainService;
	}

	public SimpleResultVO<Boolean> submit(Long id) {
		return SimpleResultVO.buildSuccessResult(outgoingDomainService
				.submit(id));
	}

	public SimpleResultVO<Boolean> accounted(Long id) {
		return SimpleResultVO.buildSuccessResult(outgoingDomainService
				.accounted(id));
	}

	public SimpleResultVO<Boolean> createAndAccount(OutgoingVO outgoingVO) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(outgoingDomainService
					.createAndAccount(outgoingVO));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

}
