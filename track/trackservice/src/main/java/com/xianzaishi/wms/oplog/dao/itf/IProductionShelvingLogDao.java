package com.xianzaishi.wms.oplog.dao.itf;

import java.util.List;

import com.xianzaishi.wms.common.dao.itf.IBaseDao;
import com.xianzaishi.wms.oplog.vo.ProductionShelvingLogVO;

public interface IProductionShelvingLogDao extends IBaseDao {
	public Boolean batchAddProductionShelvingLog(
			List<ProductionShelvingLogVO> productionShelvingLogVOs);
}