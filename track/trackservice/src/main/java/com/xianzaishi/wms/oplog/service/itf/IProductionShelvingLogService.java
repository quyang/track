package com.xianzaishi.wms.oplog.service.itf;

import java.util.List;

import com.xianzaishi.wms.oplog.vo.ProductionShelvingLogQueryVO;
import com.xianzaishi.wms.oplog.vo.ProductionShelvingLogVO;

public interface IProductionShelvingLogService {

	public Boolean addProductionShelvingLogVO(
			ProductionShelvingLogVO productionShelvingLogVO);

	public List<ProductionShelvingLogVO> queryProductionShelvingLogVOList(
			ProductionShelvingLogQueryVO productionShelvingLogQueryVO);

	public ProductionShelvingLogVO getProductionShelvingLogVOByID(Long id);

	public Boolean modifyProductionShelvingLogVO(
			ProductionShelvingLogVO productionShelvingLogVO);

	public Boolean deleteProductionShelvingLogVOByID(Long id);

	public Boolean batchAddProductionShelvingLog(
			List<ProductionShelvingLogVO> productionShelvingLogVOs);
}