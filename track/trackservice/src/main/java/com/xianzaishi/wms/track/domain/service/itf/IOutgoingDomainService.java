package com.xianzaishi.wms.track.domain.service.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.track.vo.OutgoingDetailQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingDetailVO;
import com.xianzaishi.wms.track.vo.OutgoingQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingVO;

public interface IOutgoingDomainService {

	// domain
	public Boolean auditOutgoing(Long outgoingID, Long operate);

	public Boolean createOutgoingDomain(OutgoingVO outgoingVO);

	public OutgoingVO getOutgoingDomainByID(Long outgoingID);

	public Boolean deleteOutgoingDomain(Long id);

	public Boolean createAndAccount(OutgoingVO outgoingVO);

	// head
	public Boolean addOutgoingVO(OutgoingVO outgoingVO);

	public QueryResultVO<OutgoingVO> queryOutgoingVOList(
			OutgoingQueryVO outgoingQueryVO);

	public OutgoingVO getOutgoingVOByID(Long id);

	public Boolean modifyOutgoingVO(OutgoingVO outgoingVO);

	public Boolean deleteOutgoingVO(Long outgoingID);

	// body
	public Boolean createOutgoingDetailVO(OutgoingDetailVO outgoingDetailVO);

	public Boolean batchCreateOutgoingDetailVO(
			List<OutgoingDetailVO> outgoingDetailVOs);

	public List<OutgoingDetailVO> queryOutgoingDetailVOList(
			OutgoingDetailQueryVO outgoingDetailQueryVO);

	public List<OutgoingDetailVO> getOutgoingDetailVOListByOutgoingID(
			Long outgoingID);

	public OutgoingDetailVO getOutgoingDetailVOByID(Long id);

	public Boolean modifyOutgoingDetailVO(OutgoingDetailVO outgoingDetailVO);

	public Boolean batchModifyOutgoingDetailVOs(
			List<OutgoingDetailVO> outgoingDetailVOs);

	public Boolean deleteOutgoingDetailVO(Long id);

	public Boolean deleteOutgoingDetailVOByOutgoingID(Long outgoingID);

	public Boolean batchDeleteOutgoingDetailVOByOutgoingID(
			List<Long> storyDetailIDs);

	public Boolean submit(Long id);

	public Boolean accounted(Long id);

}
