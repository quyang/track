package com.xianzaishi.wms.track.dao.itf;

import com.xianzaishi.wms.common.dao.itf.IBaseDao;
import com.xianzaishi.wms.track.vo.InspectionVO;

public interface IInspectionDao extends IBaseDao {
	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);
}