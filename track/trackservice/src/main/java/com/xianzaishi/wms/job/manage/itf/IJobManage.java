package com.xianzaishi.wms.job.manage.itf;

import java.util.List;

import com.xianzaishi.wms.job.vo.JobQueryVO;
import com.xianzaishi.wms.job.vo.JobVO;

public interface IJobManage {

	public Boolean addJobVO(JobVO jobVO);

	public List<JobVO> queryJobVOList(JobQueryVO jobQueryVO);

	public Integer queryJobVOCount(JobQueryVO jobQueryVO);

	public JobVO getJobVOByID(Long id);

	public Boolean modifyJobVO(JobVO jobVO);

	public Boolean deleteJobVOByID(Long id);

}