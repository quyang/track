package com.xianzaishi.wms.track.dao.itf;

import com.xianzaishi.wms.common.dao.itf.IBaseDao;
import com.xianzaishi.wms.track.vo.StocktakingVO;

public interface IStocktakingDao extends IBaseDao {

	public Boolean updateStocktakingStatu(StocktakingVO stocktakingVO);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

	public Boolean accounted(Long id);
}