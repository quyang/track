package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.ITransferDetailDao;
import com.xianzaishi.wms.track.manage.itf.ITransferDetailManage;
import com.xianzaishi.wms.track.vo.TransferDetailQueryVO;
import com.xianzaishi.wms.track.vo.TransferDetailVO;

public class TransferDetailManageImpl implements ITransferDetailManage {
	@Autowired
	private ITransferDetailDao transferDetailDao = null;

	private void validate(TransferDetailVO transferDetailVO) {
		if (transferDetailVO.getTransferId() == null
				|| transferDetailVO.getTransferId() <= 0) {
			throw new BizException("transferId error："
					+ transferDetailVO.getTransferId());
		}
		if (transferDetailVO.getSkuId() == null
				|| transferDetailVO.getSkuId() <= 0) {
			throw new BizException("skuID error：" + transferDetailVO.getSkuId());
		}
		if (transferDetailVO.getPositionNew() == null
				|| transferDetailVO.getPositionNew() <= 0) {
			throw new BizException("positionNew error："
					+ transferDetailVO.getPositionNew());
		}
		if (transferDetailVO.getPositionOld() == null
				|| transferDetailVO.getPositionOld() <= 0) {
			throw new BizException("positionOld error："
					+ transferDetailVO.getPositionOld());
		}
		if (transferDetailVO.getNumberReal() == null
				|| transferDetailVO.getNumberReal() <= 0) {
			throw new BizException("number error："
					+ transferDetailVO.getNumberReal());
		}
		if (transferDetailVO.getSkuName() == null
				|| transferDetailVO.getSkuName().isEmpty()) {
			throw new BizException("skuName error："
					+ transferDetailVO.getSkuName());
		}
		if (transferDetailVO.getPositionNewCode() == null
				|| transferDetailVO.getPositionNewCode().isEmpty()) {
			throw new BizException("positionNewCode error："
					+ transferDetailVO.getPositionNewCode());
		}
		if (transferDetailVO.getPositionOldCode() == null
				|| transferDetailVO.getPositionOldCode().isEmpty()) {
			throw new BizException("positionOldCode error："
					+ transferDetailVO.getPositionOldCode());
		}
		if (transferDetailVO.getSaleUnit() == null
				|| transferDetailVO.getSaleUnit().isEmpty()) {
			throw new BizException("saleUnit error："
					+ transferDetailVO.getSaleUnit());
		}
		if (transferDetailVO.getSaleUnitId() == null
				|| transferDetailVO.getSaleUnitId() <= 0) {
			throw new BizException("saleUnitId error："
					+ transferDetailVO.getSaleUnitId());
		}
		if (transferDetailVO.getSaleUnitType() == null) {
			throw new BizException("saleUnitType error："
					+ transferDetailVO.getSaleUnitType());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public ITransferDetailDao getTransferDetailDao() {
		return transferDetailDao;
	}

	public void setTransferDetailDao(ITransferDetailDao transferDetailDao) {
		this.transferDetailDao = transferDetailDao;
	}

	public Boolean addTransferDetailVO(TransferDetailVO transferDetailVO) {
		validate(transferDetailVO);
		transferDetailDao.addDO(transferDetailVO);
		return true;
	}

	public List<TransferDetailVO> queryTransferDetailVOList(
			TransferDetailQueryVO transferDetailQueryVO) {
		return transferDetailDao.queryDO(transferDetailQueryVO);
	}

	public TransferDetailVO getTransferDetailVOByID(Long id) {
		return (TransferDetailVO) transferDetailDao.getDOByID(id);
	}

	public Boolean modifyTransferDetailVO(TransferDetailVO transferDetailVO) {
		return transferDetailDao.updateDO(transferDetailVO);
	}

	public Boolean deleteTransferDetailVOByID(Long id) {
		return transferDetailDao.delDO(id);
	}

	public List<TransferDetailVO> getTransferDetailVOByTransferID(Long id) {
		TransferDetailQueryVO queryVO = new TransferDetailQueryVO();
		queryVO.setTransferId(id);
		queryVO.setSize(Integer.MAX_VALUE);
		return transferDetailDao.queryDO(queryVO);
	}

	public Boolean batchAddTransferDetailVO(
			List<TransferDetailVO> transferDetailVOs) {
		return transferDetailDao.batchAddDO(transferDetailVOs);
	}

	public Boolean batchModifyTransferDetailVO(
			List<TransferDetailVO> transferDetailVOs) {
		return transferDetailDao.batchModifyDO(transferDetailVOs);
	}

	public Boolean batchDeleteTransferDetailVO(
			List<TransferDetailVO> transferDetailVOs) {
		return transferDetailDao.batchDeleteDO(transferDetailVOs);
	}

	public Boolean batchDeleteTransferDetailVOByID(List<Long> transferDetailIDs) {
		return transferDetailDao.batchDeleteDOByID(transferDetailIDs);
	}
}
