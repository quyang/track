package com.xianzaishi.wms.barcode.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.barcode.vo.BarcodeSequenceVO;
import com.xianzaishi.wms.barcode.vo.BarcodeSequenceDO;
import com.xianzaishi.wms.barcode.vo.BarcodeSequenceQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.barcode.manage.itf.IBarcodeSequenceManage;
import com.xianzaishi.wms.barcode.dao.itf.IBarcodeSequenceDao;

public class BarcodeSequenceManageImpl implements IBarcodeSequenceManage {
	@Autowired
	private IBarcodeSequenceDao barcodeSequenceDao = null;

	private void validate(BarcodeSequenceDO barcodeSequenceDO) {
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IBarcodeSequenceDao getBarcodeSequenceDao() {
		return barcodeSequenceDao;
	}

	public void setBarcodeSequenceDao(IBarcodeSequenceDao barcodeSequenceDao) {
		this.barcodeSequenceDao = barcodeSequenceDao;
	}

	public Boolean addBarcodeSequenceVO(BarcodeSequenceVO barcodeSequenceVO) {
		barcodeSequenceDao.addDO(barcodeSequenceVO);
		return true;
	}

	public List<BarcodeSequenceVO> queryBarcodeSequenceVOList(
			BarcodeSequenceQueryVO barcodeSequenceQueryVO) {
		return barcodeSequenceDao.queryDO(barcodeSequenceQueryVO);
	}

	public BarcodeSequenceVO getBarcodeSequenceVOByID(Long id) {
		return (BarcodeSequenceVO) barcodeSequenceDao.getDOByID(id);
	}

	public Boolean modifyBarcodeSequenceVO(BarcodeSequenceVO barcodeSequenceVO) {
		return barcodeSequenceDao.updateDO(barcodeSequenceVO);
	}

	public Boolean deleteBarcodeSequenceVOByID(Long id) {
		return barcodeSequenceDao.delDO(id);
	}

	public Boolean order(BarcodeSequenceVO barcodeSequenceVO) {
		return barcodeSequenceDao.order(barcodeSequenceVO);
	}
}
