package com.xianzaishi.wms.track.domain.service.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryDetailQueryVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryDetailVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryQueryVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryVO;

public interface IBookingDeliveryDomainService {

	// domain
	public Boolean createBookingDeliveryDomain(
			BookingDeliveryVO bookingDeliveryVO);

	public BookingDeliveryVO getBookingDeliveryDomainByID(Long bookingDeliveryID);

	public Boolean deleteBookingDeliveryDomain(Long id);

	// head
	public Boolean addBookingDeliveryVO(BookingDeliveryVO bookingDeliveryVO);

	public QueryResultVO<BookingDeliveryVO> queryBookingDeliveryVOList(
			BookingDeliveryQueryVO bookingDeliveryQueryVO);

	public BookingDeliveryVO getBookingDeliveryVOByID(Long id);

	public Boolean modifyBookingDeliveryVO(BookingDeliveryVO bookingDeliveryVO);

	public Boolean deleteBookingDeliveryVO(Long bookingDeliveryID);

	// body
	public Boolean createBookingDeliveryDetailVO(
			BookingDeliveryDetailVO bookingDeliveryDetailVO);

	public Boolean batchCreateBookingDeliveryDetailVO(
			List<BookingDeliveryDetailVO> bookingDeliveryDetailVOs);

	public List<BookingDeliveryDetailVO> queryBookingDeliveryDetailVOList(
			BookingDeliveryDetailQueryVO bookingDeliveryDetailQueryVO);

	public List<BookingDeliveryDetailVO> getBookingDeliveryDetailVOListByBookingDeliveryID(
			Long bookingDeliveryID);

	public BookingDeliveryDetailVO getBookingDeliveryDetailVOByID(Long id);

	public Boolean modifyBookingDeliveryDetailVO(
			BookingDeliveryDetailVO bookingDeliveryDetailVO);

	public Boolean batchModifyBookingDeliveryDetailVOs(
			List<BookingDeliveryDetailVO> bookingDeliveryDetailVOs);

	public Boolean deleteBookingDeliveryDetailVO(Long id);

	public Boolean deleteBookingDeliveryDetailVOByBookingDeliveryID(
			Long bookingDeliveryID);

	public Boolean batchDeleteBookingDeliveryDetailVOByBookingDeliveryID(
			List<Long> storyDetailIDs);

	public Boolean submit(Long id);

	public Boolean confirm(Long id, Long auditor);

}
