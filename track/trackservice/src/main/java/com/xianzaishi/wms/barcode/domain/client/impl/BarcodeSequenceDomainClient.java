package com.xianzaishi.wms.barcode.domain.client.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.barcode.domain.client.itf.IBarcodeSequenceDomainClient;
import com.xianzaishi.wms.barcode.domain.service.itf.IBarcodeSequenceDomainService;
import com.xianzaishi.wms.common.vo.SimpleResultVO;

public class BarcodeSequenceDomainClient implements
		IBarcodeSequenceDomainClient {

	@Autowired
	private IBarcodeSequenceDomainService barcodeSequenceDomainService = null;

	public SimpleResultVO<String> getBarcode(Integer type) {
		return SimpleResultVO.buildSuccessResult(barcodeSequenceDomainService
				.getBarcode(type));
	}

	public SimpleResultVO<List<String>> getBarcodes(Integer type, Integer no) {
		return SimpleResultVO.buildSuccessResult(barcodeSequenceDomainService
				.getBarcodes(type, no));
	}

	public IBarcodeSequenceDomainService getBarcodeSequenceDomainService() {
		return barcodeSequenceDomainService;
	}

	public void setBarcodeSequenceDomainService(
			IBarcodeSequenceDomainService barcodeSequenceDomainService) {
		this.barcodeSequenceDomainService = barcodeSequenceDomainService;
	}

}
