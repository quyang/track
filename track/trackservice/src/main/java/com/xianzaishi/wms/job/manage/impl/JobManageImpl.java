package com.xianzaishi.wms.job.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.job.vo.JobVO;
import com.xianzaishi.wms.job.vo.JobDO;
import com.xianzaishi.wms.job.vo.JobQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.job.manage.itf.IJobManage;
import com.xianzaishi.wms.job.dao.itf.IJobDao;

public class JobManageImpl implements IJobManage {
	@Autowired
	private IJobDao jobDao = null;

	private void validate(JobDO jobDO) {
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IJobDao getJobDao() {
		return jobDao;
	}

	public void setJobDao(IJobDao jobDao) {
		this.jobDao = jobDao;
	}

	public Boolean addJobVO(JobVO jobVO) {
		jobDao.addDO(jobVO);
		return true;
	}

	public List<JobVO> queryJobVOList(JobQueryVO jobQueryVO) {
		return jobDao.queryDO(jobQueryVO);
	}

	public JobVO getJobVOByID(Long id) {
		return (JobVO) jobDao.getDOByID(id);
	}

	public Boolean modifyJobVO(JobVO jobVO) {
		return jobDao.updateDO(jobVO);
	}

	public Boolean deleteJobVOByID(Long id) {
		return jobDao.delDO(id);
	}

	public Integer queryJobVOCount(JobQueryVO jobQueryVO) {
		return jobDao.queryCount(jobQueryVO);
	}
}
