package com.xianzaishi.wms.track.dao.itf;

import com.xianzaishi.wms.common.dao.itf.IBaseDao;
import com.xianzaishi.wms.track.vo.BookingDeliveryVO;

public interface IBookingDeliveryDao extends IBaseDao {
	public Boolean submit(Long id);

	public Boolean confirm(Long id, Long auditor);
}