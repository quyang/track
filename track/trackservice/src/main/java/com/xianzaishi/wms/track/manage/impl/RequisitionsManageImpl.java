package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.IRequisitionsDao;
import com.xianzaishi.wms.track.manage.itf.IRequisitionsManage;
import com.xianzaishi.wms.track.vo.RequisitionsQueryVO;
import com.xianzaishi.wms.track.vo.RequisitionsVO;

public class RequisitionsManageImpl implements IRequisitionsManage {
	@Autowired
	private IRequisitionsDao requisitionsDao = null;

	private void validate(RequisitionsVO requisitionsVO) {
		if (requisitionsVO.getAgencyId() == null
				|| requisitionsVO.getAgencyId() <= 0) {
			throw new BizException("agencyID error："
					+ requisitionsVO.getAgencyId());
		}
		if (requisitionsVO.getReceiver() == null
				|| requisitionsVO.getReceiver() <= 0) {
			throw new BizException("receiver error："
					+ requisitionsVO.getReceiver());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IRequisitionsDao getRequisitionsDao() {
		return requisitionsDao;
	}

	public void setRequisitionsDao(IRequisitionsDao requisitionsDao) {
		this.requisitionsDao = requisitionsDao;
	}

	public Long addRequisitionsVO(RequisitionsVO requisitionsVO) {
		validate(requisitionsVO);
		return (Long) requisitionsDao.addDO(requisitionsVO);
	}

	public List<RequisitionsVO> queryRequisitionsVOList(
			RequisitionsQueryVO requisitionsQueryVO) {
		return requisitionsDao.queryDO(requisitionsQueryVO);
	}

	public RequisitionsVO getRequisitionsVOByID(Long id) {
		return (RequisitionsVO) requisitionsDao.getDOByID(id);
	}

	public Boolean modifyRequisitionsVO(RequisitionsVO requisitionsVO) {
		return requisitionsDao.updateDO(requisitionsVO);
	}

	public Boolean deleteRequisitionsVOByID(Long id) {
		return requisitionsDao.delDO(id);
	}

	public Boolean submit(Long id) {
		return requisitionsDao.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return requisitionsDao.audit(id, auditor);
	}

	public Boolean outgoing(Long id, Long auditor) {
		return requisitionsDao.outgoing(id, auditor);
	}

	public Boolean outgoingAudit(Long id, Long auditor) {
		return requisitionsDao.outgoingAudit(id, auditor);
	}

	public Boolean accounted(Long id) {
		return requisitionsDao.accounted(id);
	}

	public Integer queryRequisitionsVOCount(
			RequisitionsQueryVO requisitionsQueryVO) {
		return requisitionsDao.queryCount(requisitionsQueryVO);
	}
}
