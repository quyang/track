package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.IRequisitionsDetailDao;
import com.xianzaishi.wms.track.manage.itf.IRequisitionsDetailManage;
import com.xianzaishi.wms.track.vo.RequisitionsDetailQueryVO;
import com.xianzaishi.wms.track.vo.RequisitionsDetailVO;

public class RequisitionsDetailManageImpl implements IRequisitionsDetailManage {
	@Autowired
	private IRequisitionsDetailDao requisitionsDetailDao = null;

	private void validate(RequisitionsDetailVO requisitionsDetailVO) {
		if (requisitionsDetailVO.getRequisitionsId() == null
				|| requisitionsDetailVO.getRequisitionsId() <= 0) {
			throw new BizException("requisitionsID error："
					+ requisitionsDetailVO.getRequisitionsId());
		}
		if (requisitionsDetailVO.getSkuId() == null
				|| requisitionsDetailVO.getSkuId() <= 0) {
			throw new BizException("skuID error："
					+ requisitionsDetailVO.getSkuId());
		}
		if (requisitionsDetailVO.getNumberReal() == null
				|| requisitionsDetailVO.getNumberReal() <= 0) {
			throw new BizException("number error："
					+ requisitionsDetailVO.getNumberReal());
		}
		if (requisitionsDetailVO.getSkuName() == null
				|| requisitionsDetailVO.getSkuName().isEmpty()) {
			throw new BizException("skuName error："
					+ requisitionsDetailVO.getSkuName());
		}
		if (requisitionsDetailVO.getPositionCode() == null
				|| requisitionsDetailVO.getPositionCode().isEmpty()) {
			throw new BizException("positionCode error："
					+ requisitionsDetailVO.getPositionCode());
		}
		if (requisitionsDetailVO.getSaleUnit() == null
				|| requisitionsDetailVO.getSaleUnit().isEmpty()) {
			throw new BizException("saleUnit error："
					+ requisitionsDetailVO.getSaleUnit());
		}
		if (requisitionsDetailVO.getSaleUnitId() == null
				|| requisitionsDetailVO.getSaleUnitId() <= 0) {
			throw new BizException("saleUnitId error："
					+ requisitionsDetailVO.getSaleUnitId());
		}
		if (requisitionsDetailVO.getSaleUnitType() == null) {
			throw new BizException("saleUnitType error："
					+ requisitionsDetailVO.getSaleUnitType());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IRequisitionsDetailDao getRequisitionsDetailDao() {
		return requisitionsDetailDao;
	}

	public void setRequisitionsDetailDao(
			IRequisitionsDetailDao requisitionsDetailDao) {
		this.requisitionsDetailDao = requisitionsDetailDao;
	}

	public Boolean addRequisitionsDetailVO(
			RequisitionsDetailVO requisitionsDetailVO) {
		validate(requisitionsDetailVO);
		requisitionsDetailDao.addDO(requisitionsDetailVO);
		return true;
	}

	public List<RequisitionsDetailVO> queryRequisitionsDetailVOList(
			RequisitionsDetailQueryVO requisitionsDetailQueryVO) {
		return requisitionsDetailDao.queryDO(requisitionsDetailQueryVO);
	}

	public RequisitionsDetailVO getRequisitionsDetailVOByID(Long id) {
		return (RequisitionsDetailVO) requisitionsDetailDao.getDOByID(id);
	}

	public Boolean modifyRequisitionsDetailVO(
			RequisitionsDetailVO requisitionsDetailVO) {
		return requisitionsDetailDao.updateDO(requisitionsDetailVO);
	}

	public Boolean deleteRequisitionsDetailVOByID(Long id) {
		return requisitionsDetailDao.delDO(id);
	}

	public List<RequisitionsDetailVO> getRequisitionsDetailVOByRequisitionsID(
			Long id) {
		RequisitionsDetailQueryVO queryVO = new RequisitionsDetailQueryVO();
		queryVO.setRequisitionsId(id);
		queryVO.setSize(Integer.MAX_VALUE);
		return requisitionsDetailDao.queryDO(queryVO);
	}

	public Boolean batchAddRequisitionsDetailVO(
			List<RequisitionsDetailVO> requisitionsDetailVOs) {
		return requisitionsDetailDao.batchAddDO(requisitionsDetailVOs);
	}

	public Boolean batchModifyRequisitionsDetailVO(
			List<RequisitionsDetailVO> requisitionsDetailVOs) {
		return requisitionsDetailDao.batchModifyDO(requisitionsDetailVOs);
	}

	public Boolean batchDeleteRequisitionsDetailVO(
			List<RequisitionsDetailVO> requisitionsDetailVOs) {
		return requisitionsDetailDao.batchDeleteDO(requisitionsDetailVOs);
	}

	public Boolean batchDeleteRequisitionsDetailVOByID(
			List<Long> requisitionsDetailIDs) {
		return requisitionsDetailDao.batchDeleteDOByID(requisitionsDetailIDs);
	}
}
