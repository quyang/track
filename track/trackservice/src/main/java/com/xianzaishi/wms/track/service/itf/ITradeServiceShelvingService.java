package com.xianzaishi.wms.track.service.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.TradeServiceShelvingVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingQueryVO;

public interface ITradeServiceShelvingService {

	public Long addTradeServiceShelvingVO(
			TradeServiceShelvingVO tradeServiceShelvingVO);

	public List<TradeServiceShelvingVO> queryTradeServiceShelvingVOList(
			TradeServiceShelvingQueryVO tradeServiceShelvingQueryVO);

	public Integer queryTradeServiceShelvingVOCount(
			TradeServiceShelvingQueryVO tradeServiceShelvingQueryVO);

	public TradeServiceShelvingVO getTradeServiceShelvingVOByID(Long id);

	public Boolean modifyTradeServiceShelvingVO(
			TradeServiceShelvingVO tradeServiceShelvingVO);

	public Boolean deleteTradeServiceShelvingVOByID(Long id);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

	public Boolean accounted(Long id);

}