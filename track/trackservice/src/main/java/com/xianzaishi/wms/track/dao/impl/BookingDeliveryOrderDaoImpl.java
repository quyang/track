package com.xianzaishi.wms.track.dao.impl;

import com.xianzaishi.wms.track.dao.itf.IBookingDeliveryOrderDao;
import com.xianzaishi.wms.track.vo.BookingDeliveryOrderVO;
import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;

public class BookingDeliveryOrderDaoImpl extends BaseDaoAdapter implements IBookingDeliveryOrderDao {
public String getVOClassName() {
		return "BookingDeliveryOrderDO";
	}
}
