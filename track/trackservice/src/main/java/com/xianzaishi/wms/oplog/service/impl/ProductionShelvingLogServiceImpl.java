package com.xianzaishi.wms.oplog.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.oplog.manage.itf.IProductionShelvingLogManage;
import com.xianzaishi.wms.oplog.service.itf.IProductionShelvingLogService;
import com.xianzaishi.wms.oplog.vo.ProductionShelvingLogQueryVO;
import com.xianzaishi.wms.oplog.vo.ProductionShelvingLogVO;

public class ProductionShelvingLogServiceImpl implements
		IProductionShelvingLogService {
	@Autowired
	private IProductionShelvingLogManage productionShelvingLogManage = null;

	public IProductionShelvingLogManage getProductionShelvingLogManage() {
		return productionShelvingLogManage;
	}

	public void setProductionShelvingLogManage(
			IProductionShelvingLogManage productionShelvingLogManage) {
		this.productionShelvingLogManage = productionShelvingLogManage;
	}

	public Boolean addProductionShelvingLogVO(
			ProductionShelvingLogVO productionShelvingLogVO) {
		productionShelvingLogManage
				.addProductionShelvingLogVO(productionShelvingLogVO);
		return true;
	}

	public List<ProductionShelvingLogVO> queryProductionShelvingLogVOList(
			ProductionShelvingLogQueryVO productionShelvingLogQueryVO) {
		return productionShelvingLogManage
				.queryProductionShelvingLogVOList(productionShelvingLogQueryVO);
	}

	public ProductionShelvingLogVO getProductionShelvingLogVOByID(Long id) {
		return (ProductionShelvingLogVO) productionShelvingLogManage
				.getProductionShelvingLogVOByID(id);
	}

	public Boolean modifyProductionShelvingLogVO(
			ProductionShelvingLogVO productionShelvingLogVO) {
		return productionShelvingLogManage
				.modifyProductionShelvingLogVO(productionShelvingLogVO);
	}

	public Boolean deleteProductionShelvingLogVOByID(Long id) {
		return productionShelvingLogManage
				.deleteProductionShelvingLogVOByID(id);
	}

	public Boolean batchAddProductionShelvingLog(
			List<ProductionShelvingLogVO> productionShelvingLogVOs) {
		return productionShelvingLogManage
				.batchAddProductionShelvingLog(productionShelvingLogVOs);
	}
}
