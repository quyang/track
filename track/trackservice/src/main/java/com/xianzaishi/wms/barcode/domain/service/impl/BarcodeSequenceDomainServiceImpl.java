package com.xianzaishi.wms.barcode.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.barcode.domain.service.itf.IBarcodeSequenceDomainService;
import com.xianzaishi.wms.barcode.service.itf.IBarcodeSequenceService;

public class BarcodeSequenceDomainServiceImpl implements
		IBarcodeSequenceDomainService {

	@Autowired
	private IBarcodeSequenceService barcodeSequenceService = null;

	public String getBarcode(Integer type) {
		return barcodeSequenceService.getBarcode(type);
	}

	public List<String> getBarcodes(Integer type, Integer no) {
		return barcodeSequenceService.getBarcodes(type, no);
	}

	public IBarcodeSequenceService getBarcodeSequenceService() {
		return barcodeSequenceService;
	}

	public void setBarcodeSequenceService(
			IBarcodeSequenceService barcodeSequenceService) {
		this.barcodeSequenceService = barcodeSequenceService;
	}

}
