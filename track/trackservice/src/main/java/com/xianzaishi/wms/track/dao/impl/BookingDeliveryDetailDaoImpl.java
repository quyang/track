package com.xianzaishi.wms.track.dao.impl;

import com.xianzaishi.wms.track.dao.itf.IBookingDeliveryDetailDao;
import com.xianzaishi.wms.track.vo.BookingDeliveryDetailVO;
import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;

public class BookingDeliveryDetailDaoImpl extends BaseDaoAdapter implements IBookingDeliveryDetailDao {
public String getVOClassName() {
		return "BookingDeliveryDetailDO";
	}
}
