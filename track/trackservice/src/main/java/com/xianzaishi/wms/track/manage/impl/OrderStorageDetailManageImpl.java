package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.IOrderStorageDetailDao;
import com.xianzaishi.wms.track.manage.itf.IOrderStorageDetailManage;
import com.xianzaishi.wms.track.vo.OrderStorageDetailDO;
import com.xianzaishi.wms.track.vo.OrderStorageDetailQueryVO;
import com.xianzaishi.wms.track.vo.OrderStorageDetailVO;

public class OrderStorageDetailManageImpl implements IOrderStorageDetailManage {
	@Autowired
	private IOrderStorageDetailDao orderStorageDetailDao = null;

	private void validate(OrderStorageDetailVO orderStorageDetailVO) {
		if (orderStorageDetailVO.getStorageId() == null
				|| orderStorageDetailVO.getStorageId() <= 0) {
			throw new BizException("storageID error："
					+ orderStorageDetailVO.getStorageId());
		}
		if (orderStorageDetailVO.getPositionId() == null
				|| orderStorageDetailVO.getPositionId() <= 0) {
			throw new BizException("positionID error："
					+ orderStorageDetailVO.getPositionId());
		}
		if (orderStorageDetailVO.getSkuId() == null
				|| orderStorageDetailVO.getSkuId() <= 0) {
			throw new BizException("skuID error："
					+ orderStorageDetailVO.getSkuId());
		}
		if (orderStorageDetailVO.getNumberReal() == null
				|| orderStorageDetailVO.getNumberReal() <= 0) {
			throw new BizException("number error："
					+ orderStorageDetailVO.getNumberReal());
		}
		if (orderStorageDetailVO.getSkuName() == null
				|| orderStorageDetailVO.getSkuName().isEmpty()) {
			throw new BizException("skuName error："
					+ orderStorageDetailVO.getSkuName());
		}
		if (orderStorageDetailVO.getPositionCode() == null
				|| orderStorageDetailVO.getPositionCode().isEmpty()) {
			throw new BizException("positionCode error："
					+ orderStorageDetailVO.getPositionCode());
		}
		if (orderStorageDetailVO.getSaleUnit() == null
				|| orderStorageDetailVO.getSaleUnit().isEmpty()) {
			throw new BizException("saleUnit error："
					+ orderStorageDetailVO.getSaleUnit());
		}
		if (orderStorageDetailVO.getSaleUnitId() == null
				|| orderStorageDetailVO.getSaleUnitId() <= 0) {
			throw new BizException("saleUnitId error："
					+ orderStorageDetailVO.getSaleUnitId());
		}
		if (orderStorageDetailVO.getSaleUnitType() == null) {
			throw new BizException("saleUnitType error："
					+ orderStorageDetailVO.getSaleUnitType());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IOrderStorageDetailDao getOrderStorageDetailDao() {
		return orderStorageDetailDao;
	}

	public void setOrderStorageDetailDao(
			IOrderStorageDetailDao orderStorageDetailDao) {
		this.orderStorageDetailDao = orderStorageDetailDao;
	}

	public Boolean addOrderStorageDetailVO(
			OrderStorageDetailVO orderStorageDetailVO) {
		validate(orderStorageDetailVO);
		orderStorageDetailDao.addDO(orderStorageDetailVO);
		return true;
	}

	public List<OrderStorageDetailVO> queryOrderStorageDetailVOList(
			OrderStorageDetailQueryVO orderStorageDetailQueryVO) {
		return orderStorageDetailDao.queryDO(orderStorageDetailQueryVO);
	}

	public OrderStorageDetailVO getOrderStorageDetailVOByID(Long id) {
		return (OrderStorageDetailVO) orderStorageDetailDao.getDOByID(id);
	}

	public Boolean modifyOrderStorageDetailVO(
			OrderStorageDetailVO orderStorageDetailVO) {
		return orderStorageDetailDao.updateDO(orderStorageDetailVO);
	}

	public Boolean deleteOrderStorageDetailVOByID(Long id) {
		return orderStorageDetailDao.delDO(id);
	}

	public List<OrderStorageDetailVO> getOrderStorageDetailVOByOrderStorageID(
			Long id) {
		OrderStorageDetailQueryVO queryVO = new OrderStorageDetailQueryVO();
		queryVO.setStorageId(id);
		queryVO.setSize(Integer.MAX_VALUE);
		return orderStorageDetailDao.queryDO(queryVO);
	}

	public Boolean batchAddOrderStorageDetailVO(
			List<OrderStorageDetailVO> orderStorageDetailVOs) {
		return orderStorageDetailDao.batchAddDO(orderStorageDetailVOs);
	}

	public Boolean batchModifyOrderStorageDetailVO(
			List<OrderStorageDetailVO> orderStorageDetailVOs) {
		return orderStorageDetailDao.batchModifyDO(orderStorageDetailVOs);
	}

	public Boolean batchDeleteOrderStorageDetailVO(
			List<OrderStorageDetailVO> orderStorageDetailVOs) {
		return orderStorageDetailDao.batchDeleteDO(orderStorageDetailVOs);
	}

	public Boolean batchDeleteOrderStorageDetailVOByID(
			List<Long> orderStorageDetailIDs) {
		return orderStorageDetailDao.batchDeleteDOByID(orderStorageDetailIDs);
	}

}
