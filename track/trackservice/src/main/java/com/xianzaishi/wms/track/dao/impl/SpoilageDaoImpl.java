package com.xianzaishi.wms.track.dao.impl;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.ISpoilageDao;
import com.xianzaishi.wms.track.vo.SpoilageVO;

public class SpoilageDaoImpl extends BaseDaoAdapter implements ISpoilageDao {
	public String getVOClassName() {
		return "SpoilageDO";
	}

	public Boolean submit(Long id) {
		Boolean flag = false;
		try {
			if (id != null && id > 0) {
				int no = simpleSqlMapClientTemplate.update(getVOClassName()
						+ ".submit", id);
				if (no == 1) {
					flag = true;
				} else if (no > 1) {
					throw new BizException("影响的数据大于1条。");
				} else {
					throw new BizException("请检查单据状态。");
				}
			} else {
				throw new BizException("更新数据失败，ID错误：ID = " + id);
			}
		} catch (Exception e) {
			throw new BizException("更新数据失败。", e);
		}
		return flag;
	}

	public Boolean audit(Long id, Long auditor) {
		Boolean flag = false;
		try {
			if (id != null && id > 0 && auditor != null && auditor > 0) {
				SpoilageVO spoilageVO = new SpoilageVO();
				spoilageVO.setId(id);
				spoilageVO.setAuditor(auditor);
				int no = simpleSqlMapClientTemplate.update(getVOClassName()
						+ ".audit", spoilageVO);
				if (no == 1) {
					flag = true;
				} else if (no > 1) {
					throw new BizException("影响的数据大于1条。");
				} else {
					throw new BizException("请检查单据状态。");
				}
			} else {
				throw new BizException("更新数据失败，ID错误：ID = " + id);
			}
		} catch (Exception e) {
			throw new BizException("更新数据失败。", e);
		}
		return flag;
	}

	public Boolean accounted(Long id) {
		Boolean flag = false;
		try {
			if (id != null && id > 0) {
				int no = simpleSqlMapClientTemplate.update(getVOClassName()
						+ ".accounted", id);
				if (no == 1) {
					flag = true;
				} else if (no > 1) {
					throw new BizException("影响的数据大于1条。");
				} else {
					throw new BizException("请检查单据状态。");
				}
			} else {
				throw new BizException("更新数据失败，ID错误：ID = " + id);
			}
		} catch (Exception e) {
			throw new BizException("更新数据失败。", e);
		}
		return flag;
	}
}
