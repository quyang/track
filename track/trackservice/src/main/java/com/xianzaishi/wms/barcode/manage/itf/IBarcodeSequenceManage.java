package com.xianzaishi.wms.barcode.manage.itf;

import java.util.List;

import com.xianzaishi.wms.barcode.vo.BarcodeSequenceQueryVO;
import com.xianzaishi.wms.barcode.vo.BarcodeSequenceVO;

public interface IBarcodeSequenceManage {

	public Boolean addBarcodeSequenceVO(BarcodeSequenceVO barcodeSequenceVO);

	public List<BarcodeSequenceVO> queryBarcodeSequenceVOList(
			BarcodeSequenceQueryVO barcodeSequenceQueryVO);

	public BarcodeSequenceVO getBarcodeSequenceVOByID(Long id);

	public Boolean modifyBarcodeSequenceVO(BarcodeSequenceVO barcodeSequenceVO);

	public Boolean deleteBarcodeSequenceVOByID(Long id);

	public Boolean order(BarcodeSequenceVO barcodeSequenceVO);

}