package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.ITradeServiceShelvingDao;
import com.xianzaishi.wms.track.manage.itf.ITradeServiceShelvingManage;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingQueryVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingVO;

public class TradeServiceShelvingManageImpl implements
		ITradeServiceShelvingManage {
	@Autowired
	private ITradeServiceShelvingDao tradeServiceShelvingDao = null;

	private void validate(TradeServiceShelvingVO tradeServiceShelvingVO) {
		if (tradeServiceShelvingVO.getAgencyId() == null
				|| tradeServiceShelvingVO.getAgencyId() <= 0) {
			throw new BizException("agencyID error："
					+ tradeServiceShelvingVO.getAgencyId());
		}
		if (tradeServiceShelvingVO.getOperate() == null
				|| tradeServiceShelvingVO.getOperate() <= 0) {
			throw new BizException("operator error："
					+ tradeServiceShelvingVO.getOperate());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public ITradeServiceShelvingDao getTradeServiceShelvingDao() {
		return tradeServiceShelvingDao;
	}

	public void setTradeServiceShelvingDao(
			ITradeServiceShelvingDao tradeServiceShelvingDao) {
		this.tradeServiceShelvingDao = tradeServiceShelvingDao;
	}

	public Long addTradeServiceShelvingVO(
			TradeServiceShelvingVO tradeServiceShelvingVO) {
		validate(tradeServiceShelvingVO);
		return (Long) tradeServiceShelvingDao.addDO(tradeServiceShelvingVO);
	}

	public List<TradeServiceShelvingVO> queryTradeServiceShelvingVOList(
			TradeServiceShelvingQueryVO tradeServiceShelvingQueryVO) {
		return tradeServiceShelvingDao.queryDO(tradeServiceShelvingQueryVO);
	}

	public TradeServiceShelvingVO getTradeServiceShelvingVOByID(Long id) {
		return (TradeServiceShelvingVO) tradeServiceShelvingDao.getDOByID(id);
	}

	public Boolean modifyTradeServiceShelvingVO(
			TradeServiceShelvingVO tradeServiceShelvingVO) {
		return tradeServiceShelvingDao.updateDO(tradeServiceShelvingVO);
	}

	public Boolean deleteTradeServiceShelvingVOByID(Long id) {
		return tradeServiceShelvingDao.delDO(id);
	}

	public Boolean submit(Long id) {
		return tradeServiceShelvingDao.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return tradeServiceShelvingDao.audit(id, auditor);
	}

	public Boolean accounted(Long id) {
		return tradeServiceShelvingDao.accounted(id);
	}

	public Integer queryTradeServiceShelvingVOCount(
			TradeServiceShelvingQueryVO tradeServiceShelvingQueryVO) {
		return tradeServiceShelvingDao.queryCount(tradeServiceShelvingQueryVO);
	}
}
