package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.vo.OutgoingSaleVO;
import com.xianzaishi.wms.track.vo.OutgoingSaleQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.service.itf.IOutgoingSaleService;
import com.xianzaishi.wms.track.manage.itf.IOutgoingSaleManage;

public class OutgoingSaleServiceImpl implements IOutgoingSaleService {
	@Autowired
	private IOutgoingSaleManage outgoingSaleManage = null;

	public IOutgoingSaleManage getOutgoingSaleManage() {
		return outgoingSaleManage;
	}

	public void setOutgoingSaleManage(IOutgoingSaleManage outgoingSaleManage) {
		this.outgoingSaleManage = outgoingSaleManage;
	}

	public Long addOutgoingSaleVO(OutgoingSaleVO outgoingSaleVO) {
		return outgoingSaleManage.addOutgoingSaleVO(outgoingSaleVO);
	}

	public List<OutgoingSaleVO> queryOutgoingSaleVOList(
			OutgoingSaleQueryVO outgoingSaleQueryVO) {
		return outgoingSaleManage.queryOutgoingSaleVOList(outgoingSaleQueryVO);
	}

	public Integer queryOutgoingSaleVOCount(
			OutgoingSaleQueryVO outgoingSaleQueryVO) {
		return outgoingSaleManage.queryOutgoingSaleVOCount(outgoingSaleQueryVO);
	}

	public OutgoingSaleVO getOutgoingSaleVOByID(Long id) {
		return (OutgoingSaleVO) outgoingSaleManage.getOutgoingSaleVOByID(id);
	}

	public Boolean modifyOutgoingSaleVO(OutgoingSaleVO outgoingSaleVO) {
		return outgoingSaleManage.modifyOutgoingSaleVO(outgoingSaleVO);
	}

	public Boolean deleteOutgoingSaleVOByID(Long id) {
		return outgoingSaleManage.deleteOutgoingSaleVOByID(id);
	}

	public Boolean submit(Long id) {
		return outgoingSaleManage.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return outgoingSaleManage.audit(id, auditor);
	}

	public Boolean accounted(Long id) {
		return outgoingSaleManage.accounted(id);
	}
}
