package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.ISpoilageDao;
import com.xianzaishi.wms.track.manage.itf.ISpoilageManage;
import com.xianzaishi.wms.track.vo.SpoilageQueryVO;
import com.xianzaishi.wms.track.vo.SpoilageVO;

public class SpoilageManageImpl implements ISpoilageManage {
	@Autowired
	private ISpoilageDao spoilageDao = null;

	private void validate(SpoilageVO spoilageVO) {
		if (spoilageVO.getAgencyId() == null || spoilageVO.getAgencyId() <= 0) {
			throw new BizException("agencyID error：" + spoilageVO.getAgencyId());
		}
		if (spoilageVO.getOperate() == null || spoilageVO.getOperate() <= 0) {
			throw new BizException("operator error：" + spoilageVO.getOperate());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public ISpoilageDao getSpoilageDao() {
		return spoilageDao;
	}

	public void setSpoilageDao(ISpoilageDao spoilageDao) {
		this.spoilageDao = spoilageDao;
	}

	public Long addSpoilageVO(SpoilageVO spoilageVO) {
		validate(spoilageVO);
		return (Long) spoilageDao.addDO(spoilageVO);
	}

	public List<SpoilageVO> querySpoilageVOList(SpoilageQueryVO spoilageQueryVO) {
		return spoilageDao.queryDO(spoilageQueryVO);
	}

	public SpoilageVO getSpoilageVOByID(Long id) {
		return (SpoilageVO) spoilageDao.getDOByID(id);
	}

	public Boolean modifySpoilageVO(SpoilageVO spoilageVO) {
		return spoilageDao.updateDO(spoilageVO);
	}

	public Boolean deleteSpoilageVOByID(Long id) {
		return spoilageDao.delDO(id);
	}

	public Boolean submit(Long id) {
		return spoilageDao.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return spoilageDao.audit(id, auditor);
	}

	public Boolean accounted(Long id) {
		return spoilageDao.accounted(id);
	}

	public Integer querySpoilageVOCount(SpoilageQueryVO spoilageQueryVO) {
		return spoilageDao.queryCount(spoilageQueryVO);
	}
}
