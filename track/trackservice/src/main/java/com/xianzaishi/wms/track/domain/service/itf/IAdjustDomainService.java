package com.xianzaishi.wms.track.domain.service.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.track.vo.AdjustDetailQueryVO;
import com.xianzaishi.wms.track.vo.AdjustDetailVO;
import com.xianzaishi.wms.track.vo.AdjustQueryVO;
import com.xianzaishi.wms.track.vo.AdjustVO;

public interface IAdjustDomainService {

	// domain
	public Boolean auditAdjust(Long adjustID, Long operate);

	public Boolean createAdjustDomain(AdjustVO adjustVO);

	public AdjustVO getAdjustDomainByID(Long adjustID);

	public Boolean deleteAdjustDomain(Long id);

	public Boolean createAndAccount(AdjustVO adjustVO);

	// head
	public Boolean addAdjustVO(AdjustVO adjustVO);

	public QueryResultVO<AdjustVO> queryAdjustVOList(AdjustQueryVO adjustQueryVO);

	public AdjustVO getAdjustVOByID(Long id);

	public Boolean modifyAdjustVO(AdjustVO adjustVO);

	public Boolean deleteAdjustVO(Long adjustID);

	// body
	public Boolean createAdjustDetailVO(AdjustDetailVO adjustDetailVO);

	public Boolean batchCreateAdjustDetailVO(
			List<AdjustDetailVO> adjustDetailVOs);

	public List<AdjustDetailVO> queryAdjustDetailVOList(
			AdjustDetailQueryVO adjustDetailQueryVO);

	public List<AdjustDetailVO> getAdjustDetailVOListByAdjustID(Long adjustID);

	public AdjustDetailVO getAdjustDetailVOByID(Long id);

	public Boolean modifyAdjustDetailVO(AdjustDetailVO adjustDetailVO);

	public Boolean batchModifyAdjustDetailVOs(
			List<AdjustDetailVO> adjustDetailVOs);

	public Boolean deleteAdjustDetailVO(Long id);

	public Boolean deleteAdjustDetailVOByAdjustID(Long adjustID);

	public Boolean batchDeleteAdjustDetailVOByAdjustID(List<Long> storyDetailIDs);

	public Boolean submit(Long id);

	public Boolean accounted(Long id);

}
