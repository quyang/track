package com.xianzaishi.wms.track.manage.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.AdjustDetailQueryVO;
import com.xianzaishi.wms.track.vo.AdjustDetailVO;

public interface IAdjustDetailManage {

	public Boolean addAdjustDetailVO(AdjustDetailVO adjustDetailVO);

	public List<AdjustDetailVO> queryAdjustDetailVOList(
			AdjustDetailQueryVO adjustDetailQueryVO);

	public AdjustDetailVO getAdjustDetailVOByID(Long id);

	public Boolean modifyAdjustDetailVO(AdjustDetailVO adjustDetailVO);

	public Boolean deleteAdjustDetailVOByID(Long id);

	public List<AdjustDetailVO> getAdjustDetailVOByAdjustID(Long id);

	public Boolean batchAddAdjustDetailVO(List<AdjustDetailVO> adjustDetailVOs);

	public Boolean batchModifyAdjustDetailVO(
			List<AdjustDetailVO> adjustDetailVOs);

	public Boolean batchDeleteAdjustDetailVO(
			List<AdjustDetailVO> adjustDetailVOs);

	public Boolean batchDeleteAdjustDetailVOByID(List<Long> storyDetailIDs);

}