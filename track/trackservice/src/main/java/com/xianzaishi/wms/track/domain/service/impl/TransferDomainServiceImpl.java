package com.xianzaishi.wms.track.domain.service.impl;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryManageDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IPositionDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryManageDetailVO;
import com.xianzaishi.wms.hive.vo.InventoryManageVO;
import com.xianzaishi.wms.hive.vo.PositionVO;
import com.xianzaishi.wms.track.constants.TrackConstants;
import com.xianzaishi.wms.track.domain.service.itf.ITransferDomainService;
import com.xianzaishi.wms.track.service.itf.ITransferDetailService;
import com.xianzaishi.wms.track.service.itf.ITransferService;
import com.xianzaishi.wms.track.vo.TransferDetailQueryVO;
import com.xianzaishi.wms.track.vo.TransferDetailVO;
import com.xianzaishi.wms.track.vo.TransferQueryVO;
import com.xianzaishi.wms.track.vo.TransferVO;

public class TransferDomainServiceImpl implements ITransferDomainService {
	@Autowired
	private ITransferService transferService = null;
	@Autowired
	private ITransferDetailService transferDetailService = null;
	@Autowired
	private IInventoryManageDomainClient inventoryManageDomainClient = null;
	@Autowired
	private IPositionDomainClient positionDomainClient = null;
	@Autowired
	private SkuService skuService = null;
	@Autowired
	private ValueService valueService = null;

	public Boolean createTransferDomain(TransferVO transferVO) {
		if (transferVO.getDetails() == null
				|| transferVO.getDetails().isEmpty()) {
			throw new BizException("请填充入库明细！");
		}
		transferVO.setStatu(0);
		Long transferID = transferService.addTransferVO(transferVO);
		transferVO.setId(transferID);
		initDetails(transferVO, transferVO.getDetails());

		transferDetailService.batchAddTransferDetailVO(transferVO.getDetails());

		return true;
	}

	public Boolean createAndAccount(TransferVO transferVO) {
		if (transferVO.getDetails() == null
				|| transferVO.getDetails().isEmpty()) {
			throw new BizException("请填充入库明细！");
		}
		transferVO.setStatu(0);
		Long transferID = transferService.addTransferVO(transferVO);
		transferVO.setId(transferID);
		initDetails(transferVO, transferVO.getDetails());

		transferDetailService.batchAddTransferDetailVO(transferVO.getDetails());

		submit(transferID);
		audit(transferID, transferVO.getOperate());
		return true;
	}

	public TransferVO getTransferDomainByID(Long transferID) {
		if (transferID == null || transferID <= 0) {
			throw new BizException("id error");
		}

		TransferVO transferVO = transferService.getTransferVOByID(transferID);

		transferVO.setDetails(transferDetailService
				.getTransferDetailVOByTransferID(transferID));

		return transferVO;
	}

	public Boolean deleteTransferDomain(Long transferID) {
		if (transferID == null || transferID <= 0) {
			throw new BizException("id error");
		}

		List<TransferDetailVO> transferDetailVOs = transferDetailService
				.getTransferDetailVOByTransferID(transferID);

		transferDetailService.batchDeleteTransferDetailVO(transferDetailVOs);

		transferService.deleteTransferVOByID(transferID);

		return true;
	}

	public Boolean addTransferVO(TransferVO transferVO) {
		transferService.addTransferVO(transferVO);
		return true;
	}

	public QueryResultVO<TransferVO> queryTransferVOList(
			TransferQueryVO transferQueryVO) {
		QueryResultVO<TransferVO> queryResultVO = new QueryResultVO<>();
		queryResultVO.setPage(transferQueryVO.getPage());
		queryResultVO.setSize(transferQueryVO.getSize());
		queryResultVO.setItems(transferService
				.queryTransferVOList(transferQueryVO));
		queryResultVO.setTotalCount(transferService
				.queryTransferVOCount(transferQueryVO));
		return queryResultVO;
	}

	public TransferVO getTransferVOByID(Long id) {
		return transferService.getTransferVOByID(id);
	}

	public Boolean modifyTransferVO(TransferVO transferVO) {
		return transferService.modifyTransferVO(transferVO);
	}

	public Boolean deleteTransferVO(Long id) {
		return transferService.deleteTransferVOByID(id);
	}

	public Boolean createTransferDetailVO(TransferDetailVO transferDetailVO) {
		initInfo(transferDetailVO);
		return transferDetailService.addTransferDetailVO(transferDetailVO);
	}

	public Boolean batchCreateTransferDetailVO(
			List<TransferDetailVO> transferDetailVOs) {
		for (TransferDetailVO transferDetailVO : transferDetailVOs) {
			initInfo(transferDetailVO);
		}
		return transferDetailService
				.batchAddTransferDetailVO(transferDetailVOs);
	}

	public List<TransferDetailVO> queryTransferDetailVOList(
			TransferDetailQueryVO transferDetailQueryVO) {
		return transferDetailService
				.queryTransferDetailVOList(transferDetailQueryVO);
	}

	public List<TransferDetailVO> getTransferDetailVOListByTransferID(
			Long transferID) {
		return transferDetailService
				.getTransferDetailVOByTransferID(transferID);
	}

	public TransferDetailVO getTransferDetailVOByID(Long id) {
		return transferDetailService.getTransferDetailVOByID(id);
	}

	public Boolean modifyTransferDetailVO(TransferDetailVO transferDetailVO) {
		initInfo(transferDetailVO);
		return transferDetailService.modifyTransferDetailVO(transferDetailVO);
	}

	public Boolean batchModifyTransferDetailVOs(
			List<TransferDetailVO> transferDetailVOs) {
		for (TransferDetailVO transferDetailVO : transferDetailVOs) {
			initInfo(transferDetailVO);
		}
		return transferDetailService
				.batchModifyTransferDetailVO(transferDetailVOs);
	}

	public Boolean deleteTransferDetailVO(Long id) {
		return transferDetailService.deleteTransferDetailVOByID(id);
	}

	public Boolean deleteTransferDetailVOByTransferID(Long transferID) {
		if (transferID == null || transferID <= 0) {
			throw new BizException("id error");
		}

		List<TransferDetailVO> transferDetailVOs = transferDetailService
				.getTransferDetailVOByTransferID(transferID);

		transferDetailService.batchDeleteTransferDetailVO(transferDetailVOs);

		return true;
	}

	public Boolean batchDeleteTransferDetailVOByTransferID(
			List<Long> storyDetailIDs) {
		return transferDetailService
				.batchDeleteTransferDetailVOByID(storyDetailIDs);
	}

	private void initDetails(TransferVO transferVO,
			List<TransferDetailVO> transferDetailVOs) {
		for (int i = 0; i < transferDetailVOs.size(); i++) {
			transferDetailVOs.get(i).setTransferId(transferVO.getId());
			initInfo(transferDetailVOs.get(i));
		}
	}

	private void initInfo(TransferDetailVO transferDetailVO) {
		ItemCommoditySkuDTO itemCommoditySkuDTO = skuService.queryItemSku(
				SkuQuery.querySkuById(transferDetailVO.getSkuId())).getModule();
		if (itemCommoditySkuDTO == null) {
			throw new BizException("skuID error:" + transferDetailVO.getSkuId());
		}
		transferDetailVO.setSkuName(itemCommoditySkuDTO.getTitle());
		transferDetailVO.setSaleUnit(valueService.queryValueNameByValueId(
				itemCommoditySkuDTO.getSkuUnit()).getModule());
		transferDetailVO.setSaleUnitId(new Long(itemCommoditySkuDTO
				.getSkuUnit()));
		transferDetailVO.setSaleUnitType(new Integer(itemCommoditySkuDTO
				.querySaleUnitType()));
		// transferDetailVO.setSpec(itemCommoditySkuDTO.getSkuSpecificationNum());
		// transferDetailVO.setSpecUnit(valueService.queryValueNameByValueId(
		// itemCommoditySkuDTO.getSkuSpecificationUnit()).getModule());
		// transferDetailVO.setSpecUnitId(new Long(itemCommoditySkuDTO
		// .getSkuSpecificationUnit()));
		PositionVO positionVO = positionDomainClient.getPositionVOByID(
				transferDetailVO.getPositionNew()).getTarget();
		if (positionVO == null) {
			throw new BizException("positionID error:"
					+ transferDetailVO.getPositionNew());
		}
		transferDetailVO.setPositionNewCode(positionVO.getCode());
		positionVO = positionDomainClient.getPositionVOByID(
				transferDetailVO.getPositionOld()).getTarget();
		if (positionVO == null) {
			throw new BizException("positionID error:"
					+ transferDetailVO.getPositionOld());
		}
		transferDetailVO.setPositionOldCode(positionVO.getCode());

		if (transferDetailVO.getNumberReal() == null
				&& transferDetailVO.getNumber() != null
				&& !transferDetailVO.getNumber().isEmpty()) {
			transferDetailVO.setNumberReal(new BigDecimal(transferDetailVO
					.getNumber()).multiply(new BigDecimal(1000)).intValue());
		}
	}

	public ITransferService getTransferService() {
		return transferService;
	}

	public void setTransferService(ITransferService transferService) {
		this.transferService = transferService;
	}

	public ITransferDetailService getTransferDetailService() {
		return transferDetailService;
	}

	public void setTransferDetailService(
			ITransferDetailService transferDetailService) {
		this.transferDetailService = transferDetailService;
	}

	public Boolean submit(Long id) {
		return transferService.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		Boolean flag = false;
		if (transferService.audit(id, auditor)) {
			flag = accounted(id);
		}
		return flag;
	}

	public Boolean accounted(Long id) {
		Boolean flag = false;
		TransferVO transferVO = getTransferDomainByID(id);
		InventoryManageVO inventoryManageVO = initInventoryManage(transferVO);
		SimpleResultVO<Boolean> result = inventoryManageDomainClient
				.createInventoryManageDomain(inventoryManageVO);
		if (result.isSuccess() && result.getTarget()) {
			flag = transferService.accounted(id);
		}
		return flag;
	}

	private InventoryManageVO initInventoryManage(TransferVO transferVO) {
		InventoryManageVO inventoryManageVO = new InventoryManageVO();
		inventoryManageVO.setAgencyId(transferVO.getAgencyId());
		inventoryManageVO.setAuditor(transferVO.getAuditor());
		inventoryManageVO.setAuditTime(transferVO.getAuditTime());
		inventoryManageVO.setOperator(transferVO.getOperate());
		inventoryManageVO.setOpReason(TrackConstants.IM_REASON_ADJ_TRANSFER);
		inventoryManageVO.setOpTime(transferVO.getOpTime());
		inventoryManageVO.setOpType(3);
		List<InventoryManageDetailVO> details = new LinkedList<InventoryManageDetailVO>();
		for (int i = 0; i < transferVO.getDetails().size(); i++) {
			InventoryManageDetailVO inventoryManageDetailVO = new InventoryManageDetailVO();
			inventoryManageDetailVO.setNumber(transferVO.getDetails().get(i)
					.getNumberReal());
			inventoryManageDetailVO.setSaleUnit(transferVO.getDetails().get(i)
					.getSaleUnit());
			inventoryManageDetailVO.setSaleUnitId(transferVO.getDetails()
					.get(i).getSaleUnitId());
			inventoryManageDetailVO.setSpec(transferVO.getDetails().get(i)
					.getSpec());
			inventoryManageDetailVO.setSpecUnit(transferVO.getDetails().get(i)
					.getSpecUnit());
			inventoryManageDetailVO.setSpecUnitId(transferVO.getDetails()
					.get(i).getSpecUnitId());
			inventoryManageDetailVO.setSaleUnitType(transferVO.getDetails()
					.get(i).getSaleUnitType());
			inventoryManageDetailVO.setSkuName(transferVO.getDetails().get(i)
					.getSkuName());
			inventoryManageDetailVO.setPositionCode(transferVO.getDetails()
					.get(i).getPositionNewCode());
			inventoryManageDetailVO.setSkuId(transferVO.getDetails().get(i)
					.getSkuId());
			inventoryManageDetailVO.setPositionId(transferVO.getDetails()
					.get(i).getPositionNew());
			details.add(inventoryManageDetailVO);

			inventoryManageDetailVO = new InventoryManageDetailVO();
			inventoryManageDetailVO.setNumber(-transferVO.getDetails().get(i)
					.getNumberReal());
			inventoryManageDetailVO.setSaleUnit(transferVO.getDetails().get(i)
					.getSaleUnit());
			inventoryManageDetailVO.setSaleUnitId(transferVO.getDetails()
					.get(i).getSaleUnitId());
			inventoryManageDetailVO.setSpec(transferVO.getDetails().get(i)
					.getSpec());
			inventoryManageDetailVO.setSpecUnit(transferVO.getDetails().get(i)
					.getSpecUnit());
			inventoryManageDetailVO.setSpecUnitId(transferVO.getDetails()
					.get(i).getSpecUnitId());
			inventoryManageDetailVO.setSaleUnitType(transferVO.getDetails()
					.get(i).getSaleUnitType());
			inventoryManageDetailVO.setSkuName(transferVO.getDetails().get(i)
					.getSkuName());
			inventoryManageDetailVO.setPositionCode(transferVO.getDetails()
					.get(i).getPositionOldCode());
			inventoryManageDetailVO.setSkuId(transferVO.getDetails().get(i)
					.getSkuId());
			inventoryManageDetailVO.setPositionId(transferVO.getDetails()
					.get(i).getPositionOld());
			details.add(inventoryManageDetailVO);
		}
		inventoryManageVO.setDetails(details);
		return inventoryManageVO;
	}

	public IInventoryManageDomainClient getInventoryManageDomainClient() {
		return inventoryManageDomainClient;
	}

	public void setInventoryManageDomainClient(
			IInventoryManageDomainClient inventoryManageDomainClient) {
		this.inventoryManageDomainClient = inventoryManageDomainClient;
	}

	public IPositionDomainClient getPositionDomainClient() {
		return positionDomainClient;
	}

	public void setPositionDomainClient(
			IPositionDomainClient positionDomainClient) {
		this.positionDomainClient = positionDomainClient;
	}

	public SkuService getSkuService() {
		return skuService;
	}

	public void setSkuService(SkuService skuService) {
		this.skuService = skuService;
	}

	public ValueService getValueService() {
		return valueService;
	}

	public void setValueService(ValueService valueService) {
		this.valueService = valueService;
	}
}
