package com.xianzaishi.wms.track.domain.service.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryManageDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IPositionDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryManageDetailVO;
import com.xianzaishi.wms.hive.vo.InventoryManageVO;
import com.xianzaishi.wms.hive.vo.InventoryVO;
import com.xianzaishi.wms.hive.vo.PositionVO;
import com.xianzaishi.wms.track.constants.TrackConstants;
import com.xianzaishi.wms.track.domain.client.impl.TradeServiceShelvingDomainClient;
import com.xianzaishi.wms.track.domain.service.itf.IStocktakingDomainService;
import com.xianzaishi.wms.track.service.itf.IStocktakingDetailService;
import com.xianzaishi.wms.track.service.itf.IStocktakingService;
import com.xianzaishi.wms.track.vo.StocktakingDetailQueryVO;
import com.xianzaishi.wms.track.vo.StocktakingDetailVO;
import com.xianzaishi.wms.track.vo.StocktakingQueryVO;
import com.xianzaishi.wms.track.vo.StocktakingVO;

public class StocktakingDomainServiceImpl implements IStocktakingDomainService {
	@Autowired
	private IStocktakingService stocktakingService = null;
	@Autowired
	private IStocktakingDetailService stocktakingDetailService = null;
	@Autowired
	private IInventoryManageDomainClient inventoryManageDomainClient = null;
	@Autowired
	private IInventoryDomainClient inventoryDomainClient = null;
	@Autowired
	private SkuService skuService = null;
	@Autowired
	private ValueService valueService = null;
	@Autowired
	private IPositionDomainClient positionDomainClient = null;

	private static final Logger logger = Logger
        .getLogger(StocktakingDomainServiceImpl.class);
	
	public Boolean createStocktakingDomain(StocktakingVO stocktakingVO) {
		stocktakingVO.setStatu(0);
		Long stocktakingID = stocktakingService.addStocktakingVO(stocktakingVO);
		if (stocktakingVO.getDetails() != null
				&& !stocktakingVO.getDetails().isEmpty()) {
			initDetails(stocktakingID, stocktakingVO.getDetails());

			stocktakingDetailService.batchAddStocktakingDetailVO(stocktakingVO
					.getDetails());
		}
		return true;
	}

	public StocktakingVO getStocktakingDomainByID(Long stocktakingID) {
		if (stocktakingID == null || stocktakingID <= 0) {
			throw new BizException("id error");
		}

		StocktakingVO stocktakingVO = stocktakingService
				.getStocktakingVOByID(stocktakingID);

		stocktakingVO.setDetails(stocktakingDetailService
				.getStocktakingDetailVOByStocktakingID(stocktakingID));

		return stocktakingVO;
	}

	public Boolean deleteStocktakingDomain(Long stocktakingID) {
		if (stocktakingID == null || stocktakingID <= 0) {
			throw new BizException("id error");
		}

		List<StocktakingDetailVO> stocktakingDetailVOs = stocktakingDetailService
				.getStocktakingDetailVOByStocktakingID(stocktakingID);

		stocktakingDetailService
				.batchDeleteStocktakingDetailVO(stocktakingDetailVOs);

		stocktakingService.deleteStocktakingVOByID(stocktakingID);

		return true;
	}

	public Boolean addStocktakingVO(StocktakingVO stocktakingVO) {
		stocktakingService.addStocktakingVO(stocktakingVO);
		return true;
	}

	public QueryResultVO<StocktakingVO> queryStocktakingVOList(
			StocktakingQueryVO stocktakingQueryVO) {
		QueryResultVO<StocktakingVO> queryResultVO = new QueryResultVO<>();
		queryResultVO.setPage(stocktakingQueryVO.getPage());
		queryResultVO.setSize(stocktakingQueryVO.getSize());
		queryResultVO.setItems(stocktakingService
				.queryStocktakingVOList(stocktakingQueryVO));
		queryResultVO.setTotalCount(stocktakingService
				.queryStocktakingVOCount(stocktakingQueryVO));
		return queryResultVO;

	}

	public StocktakingVO getStocktakingVOByID(Long id) {
		return stocktakingService.getStocktakingVOByID(id);
	}

	public Boolean modifyStocktakingVO(StocktakingVO stocktakingVO) {
		return stocktakingService.modifyStocktakingVO(stocktakingVO);
	}

	public Boolean deleteStocktakingVO(Long id) {
		return stocktakingService.deleteStocktakingVOByID(id);
	}

	public Boolean submitStocktakingToCheck(StocktakingVO stocktakingVO) {
		return stocktakingService.submitStocktakingToCheck(stocktakingVO);
	}

	public Boolean submitStocktakingToAudit(StocktakingVO stocktakingVO) {
		return stocktakingService.submitStocktakingToAudit(stocktakingVO);
	}

	public Boolean createStocktakingDetailVO(
			StocktakingDetailVO stocktakingDetailVO) {
		if (stocktakingDetailVO.getStocktakingId() == null
				|| stocktakingDetailVO.getSkuId() == null
				|| stocktakingDetailVO.getPositionId() == null) {
			throw new BizException(
					"please check skuId/positionId/stocktakingId");
		}
		initInfo(stocktakingDetailVO);

		StocktakingDetailQueryVO stocktakingDetailQueryVO = new StocktakingDetailQueryVO();
		stocktakingDetailQueryVO.setStocktakingId(stocktakingDetailVO
				.getStocktakingId());
		stocktakingDetailQueryVO.setSkuId(stocktakingDetailVO.getSkuId());
		stocktakingDetailQueryVO.setPositionId(stocktakingDetailVO
				.getPositionId());
		List<StocktakingDetailVO> stocktakingDetailVOs = stocktakingDetailService
				.queryStocktakingDetailVOList(stocktakingDetailQueryVO);
		if (stocktakingDetailVOs == null || stocktakingDetailVOs.isEmpty()) {
			return stocktakingDetailService
					.addStocktakingDetailVO(stocktakingDetailVO);
		} else {
			StocktakingDetailVO tmp = stocktakingDetailVOs.get(0);
			tmp.setFirstNoReal(stocktakingDetailVO.getFirstNoReal());
			tmp.setSecondNoReal(stocktakingDetailVO.getSecondNoReal());
			tmp.setTheoreticalNoReal(stocktakingDetailVO.getTheoreticalNoReal());
			stocktakingDetailService.modifyStocktakingDetailVO(tmp);
		}
		return true;
	}

	public Boolean batchCreateStocktakingDetailVO(
			List<StocktakingDetailVO> stocktakingDetailVOs) {
		for (StocktakingDetailVO stocktakingDetailVO : stocktakingDetailVOs) {
			createStocktakingDetailVO(stocktakingDetailVO);
		}
		return true;
	}

	public List<StocktakingDetailVO> queryStocktakingDetailVOList(
			StocktakingDetailQueryVO stocktakingDetailQueryVO) {
		return stocktakingDetailService
				.queryStocktakingDetailVOList(stocktakingDetailQueryVO);
	}

	public List<StocktakingDetailVO> getStocktakingDetailVOListByStocktakingID(
			Long stocktakingID) {
		return stocktakingDetailService
				.getStocktakingDetailVOByStocktakingID(stocktakingID);
	}

	public StocktakingDetailVO getStocktakingDetailVOByID(Long id) {
		return stocktakingDetailService.getStocktakingDetailVOByID(id);
	}

	public Boolean modifyStocktakingDetailVO(
			StocktakingDetailVO stocktakingDetailVO) {
		if (stocktakingDetailVO.getStocktakingId() == null
				|| stocktakingDetailVO.getSkuId() == null
				|| stocktakingDetailVO.getPositionId() == null
				|| stocktakingDetailVO.getId() == null) {
			throw new BizException(
					"please check skuId/positionId/stocktakingId/id");
		}
		StocktakingDetailQueryVO stocktakingDetailQueryVO = new StocktakingDetailQueryVO();
		stocktakingDetailQueryVO.setStocktakingId(stocktakingDetailVO
				.getStocktakingId());
		stocktakingDetailQueryVO.setSkuId(stocktakingDetailVO.getSkuId());
		stocktakingDetailQueryVO.setPositionId(stocktakingDetailVO
				.getPositionId());
		List<StocktakingDetailVO> stocktakingDetailVOs = stocktakingDetailService
				.queryStocktakingDetailVOList(stocktakingDetailQueryVO);
		if (stocktakingDetailVOs != null
				&& !stocktakingDetailVOs.isEmpty()
				&& !stocktakingDetailVO.getId().equals(
						stocktakingDetailVOs.get(0).getId())) {
			throw new BizException("该盘点任务中目标库位、商品记录已经存在！");
		}
		initInfo(stocktakingDetailVO);
		StocktakingDetailVO tmp = stocktakingDetailVOs.get(0);
		tmp.setFirstNoReal(stocktakingDetailVO.getFirstNoReal());
		tmp.setSecondNoReal(stocktakingDetailVO.getSecondNoReal());
		tmp.setTheoreticalNoReal(stocktakingDetailVO.getTheoreticalNoReal());
		stocktakingDetailService.modifyStocktakingDetailVO(stocktakingDetailVO);
		return stocktakingDetailService
				.modifyStocktakingDetailVO(stocktakingDetailVO);
	}

	public Boolean batchModifyStocktakingDetailVOs(
			List<StocktakingDetailVO> stocktakingDetailVOs) {
		for (StocktakingDetailVO stocktakingDetailVO : stocktakingDetailVOs) {
			modifyStocktakingDetailVO(stocktakingDetailVO);
		}
		return true;
	}

	public Boolean deleteStocktakingDetailVO(Long id) {
		return stocktakingDetailService.deleteStocktakingDetailVOByID(id);
	}

	public Boolean deleteStocktakingDetailVOByStocktakingID(Long stocktakingID) {
		if (stocktakingID == null || stocktakingID <= 0) {
			throw new BizException("id error");
		}

		List<StocktakingDetailVO> stocktakingDetailVOs = stocktakingDetailService
				.getStocktakingDetailVOByStocktakingID(stocktakingID);

		stocktakingDetailService
				.batchDeleteStocktakingDetailVO(stocktakingDetailVOs);

		return true;
	}

	public Boolean batchDeleteStocktakingDetailVOByStocktakingID(
			List<Long> storyDetailIDs) {
		return stocktakingDetailService
				.batchDeleteStocktakingDetailVOByID(storyDetailIDs);
	}

	private void initDetails(Long id,
			List<StocktakingDetailVO> stocktakingDetailVOs) {
		for (int i = 0; i < stocktakingDetailVOs.size(); i++) {
			stocktakingDetailVOs.get(i).setStocktakingId(id);
			initInfo(stocktakingDetailVOs.get(i));
		}
	}

	private void initInfo(StocktakingDetailVO stocktakingDetailVO) {
		ItemCommoditySkuDTO itemCommoditySkuDTO = skuService.queryItemSku(
				SkuQuery.querySkuById(stocktakingDetailVO.getSkuId()))
				.getModule();
		if (itemCommoditySkuDTO == null) {
			throw new BizException("skuID error:"
					+ stocktakingDetailVO.getSkuId());
		}
		stocktakingDetailVO.setSkuName(itemCommoditySkuDTO.getTitle());
		stocktakingDetailVO.setSaleUnit(valueService.queryValueNameByValueId(
				itemCommoditySkuDTO.getSkuUnit()).getModule());
		stocktakingDetailVO.setSaleUnitId(new Long(itemCommoditySkuDTO
				.getSkuUnit()));
		stocktakingDetailVO.setSaleUnitType(new Integer(itemCommoditySkuDTO
				.querySaleUnitType()));
		// stocktakingDetailVO.setSpec(itemCommoditySkuDTO
		// .getSkuSpecificationNum());
		// stocktakingDetailVO.setSpecUnit(valueService.queryValueNameByValueId(
		// itemCommoditySkuDTO.getSkuSpecificationUnit()).getModule());
		// stocktakingDetailVO.setSpecUnitId(new Long(itemCommoditySkuDTO
		// .getSkuSpecificationUnit()));
		PositionVO positionVO = positionDomainClient.getPositionVOByID(
				stocktakingDetailVO.getPositionId()).getTarget();
		if (positionVO == null) {
			throw new BizException("positionID error:"
					+ stocktakingDetailVO.getPositionId());
		}
		stocktakingDetailVO.setPositionCode(positionVO.getCode());

		if (stocktakingDetailVO.getTheoreticalNoReal() == null
				&& stocktakingDetailVO.getTheoreticalNo() != null
				&& !stocktakingDetailVO.getTheoreticalNo().isEmpty()) {
			stocktakingDetailVO.setTheoreticalNoReal(new BigDecimal(
					stocktakingDetailVO.getTheoreticalNo()).multiply(
					new BigDecimal(1000)).intValue());
		}

		if (stocktakingDetailVO.getFirstNoReal() == null
				&& stocktakingDetailVO.getFirstNo() != null
				&& !stocktakingDetailVO.getFirstNo().isEmpty()) {
			stocktakingDetailVO.setFirstNoReal(new BigDecimal(
					stocktakingDetailVO.getFirstNo()).multiply(
					new BigDecimal(1000)).intValue());
		}

		if (stocktakingDetailVO.getSecondNoReal() == null
				&& stocktakingDetailVO.getSecondNo() != null
				&& !stocktakingDetailVO.getSecondNo().isEmpty()) {
			stocktakingDetailVO.setSecondNoReal(new BigDecimal(
					stocktakingDetailVO.getSecondNo()).multiply(
					new BigDecimal(1000)).intValue());
		}
	}

	public IStocktakingService getStocktakingService() {
		return stocktakingService;
	}

	public void setStocktakingService(IStocktakingService stocktakingService) {
		this.stocktakingService = stocktakingService;
	}

	public IStocktakingDetailService getStocktakingDetailService() {
		return stocktakingDetailService;
	}

	public void setStocktakingDetailService(
			IStocktakingDetailService stocktakingDetailService) {
		this.stocktakingDetailService = stocktakingDetailService;
	}

	public Boolean submit(Long id) {
		return stocktakingService.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		Boolean flag = false;
		if (stocktakingService.audit(id, auditor)) {
			flag = accounted(id);
		}
		return flag;
	}

	public Boolean accounted(Long id) {
		Boolean flag = false;
		StocktakingVO stocktakingVO = getStocktakingDomainByID(id);
		InventoryManageVO inventoryManageVO = initInventoryManage(stocktakingVO);
		SimpleResultVO<Boolean> result = inventoryManageDomainClient
				.createInventoryManageDomain(inventoryManageVO);
		if (result.isSuccess() && result.getTarget()) {
			flag = stocktakingService.accounted(id);
		}
		return flag;
	}

  private InventoryManageVO initInventoryManage(StocktakingVO stocktakingVO) {
    InventoryManageVO inventoryManageVO = new InventoryManageVO();
    inventoryManageVO.setAgencyId(stocktakingVO.getAgencyId());
    inventoryManageVO.setAuditor(stocktakingVO.getAuditor());
    inventoryManageVO.setAuditTime(stocktakingVO.getAuditTime());
    inventoryManageVO.setOperator(stocktakingVO.getOperate());
    inventoryManageVO.setOpReason(TrackConstants.IM_REASON_ADJ_STOCKTAKING);
    inventoryManageVO.setOpTime(stocktakingVO.getOpTime());
    inventoryManageVO.setOpType(3);
    List<InventoryManageDetailVO> details = new LinkedList<InventoryManageDetailVO>();
    if (stocktakingVO.getType() != null && stocktakingVO.getType() == 1) {
      Map<Long, Map<Long, StocktakingDetailVO>> newskumaps =
          new HashMap<Long, Map<Long, StocktakingDetailVO>>();
      Map<Long, Map<Long, InventoryVO>> oldskumaps = new HashMap<Long, Map<Long, InventoryVO>>();
      for (int i = 0; i < stocktakingVO.getDetails().size(); i++) {
        StocktakingDetailVO tmpVO = stocktakingVO.getDetails().get(i);
        Long skuId = tmpVO.getSkuId();
        Long positionId = tmpVO.getPositionId();
        if (newskumaps.containsKey(skuId)) {
          if (newskumaps.get(skuId).containsKey(
              positionId)) {
            continue;
          } else {
            newskumaps.get(skuId).put(
                positionId,
                tmpVO);
          }
        } else {
          Map<Long, StocktakingDetailVO> newpositionmaps = new HashMap<Long, StocktakingDetailVO>();
          Map<Long, InventoryVO> oldpositionmaps = new HashMap<Long, InventoryVO>();
          newpositionmaps.put(positionId, stocktakingVO
              .getDetails().get(i));
          newskumaps.put(skuId, newpositionmaps);

          List<InventoryVO> inventories =
              inventoryDomainClient.getAllInventoryBySKUID(stocktakingVO.getAgencyId(),
                  skuId).getTarget();
          if (inventories != null) {
            for (InventoryVO inventoryVO : inventories) {
              oldpositionmaps.put(inventoryVO.getPositionId(), inventoryVO);
            }
          }
          oldskumaps.put(skuId, oldpositionmaps);
        }
      }
      for (int i = 0; i < stocktakingVO.getDetails().size(); i++) {
        StocktakingDetailVO tmpVO = stocktakingVO.getDetails().get(i);
        Long skuId = tmpVO.getSkuId();
        Long positionId = tmpVO.getPositionId();
        InventoryVO inventoryVO =
            oldskumaps.get(skuId).get(
                positionId);

        InventoryManageDetailVO inventoryManageDetailVO = new InventoryManageDetailVO();
        inventoryManageDetailVO.setSkuId(skuId);
        inventoryManageDetailVO.setPositionId(positionId);
        if (inventoryVO == null) {
          inventoryManageDetailVO
              .setNumber(tmpVO.getSecondNoReal() == null ? tmpVO.getFirstNoReal()
                  : tmpVO.getSecondNoReal());
        } else {
          inventoryManageDetailVO
              .setNumber((tmpVO.getSecondNoReal() == null ? tmpVO.getFirstNoReal()
                  : tmpVO.getSecondNoReal())
                  - inventoryVO.getNumberReal());
        }
        inventoryManageDetailVO.setSaleUnit(tmpVO.getSaleUnit());
        inventoryManageDetailVO.setSaleUnitId(tmpVO.getSaleUnitId());
        inventoryManageDetailVO.setSpec(tmpVO.getSpec());
        inventoryManageDetailVO.setSpecUnit(tmpVO.getSpecUnit());
        inventoryManageDetailVO.setSpecUnitId(tmpVO.getSpecUnitId());
        inventoryManageDetailVO
            .setSaleUnitType(tmpVO.getSaleUnitType());
        inventoryManageDetailVO.setSkuName(tmpVO.getSkuName());
        inventoryManageDetailVO
            .setPositionCode(tmpVO.getPositionCode());
        if (inventoryManageDetailVO.getNumber() != 0) {
          details.add(inventoryManageDetailVO);
        }
      }
      for (Map<Long, InventoryVO> oldpositionmaps : oldskumaps.values()) {
        for (InventoryVO inventoryVO : oldpositionmaps.values()) {
          StocktakingDetailVO stocktakingDetailVO =
              newskumaps.get(inventoryVO.getSkuId()).get(inventoryVO.getPositionId());

          if (stocktakingDetailVO == null) {
            InventoryManageDetailVO inventoryManageDetailVO = new InventoryManageDetailVO();
            inventoryManageDetailVO.setSkuId(inventoryVO.getSkuId());
            inventoryManageDetailVO.setPositionId(inventoryVO.getPositionId());
            inventoryManageDetailVO.setNumber(-inventoryVO.getNumberReal());
            inventoryManageDetailVO.setSaleUnit(inventoryVO.getSaleUnit());
            inventoryManageDetailVO.setSaleUnitId(inventoryVO.getSaleUnitId());
            inventoryManageDetailVO.setSpec(inventoryVO.getSpec());
            inventoryManageDetailVO.setSpecUnit(inventoryVO.getSpecUnit());
            inventoryManageDetailVO.setSpecUnitId(inventoryVO.getSpecUnitId());
            inventoryManageDetailVO.setSaleUnitType(inventoryVO.getSaleUnitType());
            inventoryManageDetailVO.setSkuName(inventoryVO.getSkuName());
            inventoryManageDetailVO.setPositionCode(inventoryVO.getPositionCode());
            if (inventoryManageDetailVO.getNumber() != 0) {
              details.add(inventoryManageDetailVO);
            }
          }
        }
      }
    } else {
      Map<Long, Map<Long, StocktakingDetailVO>> newpositionmaps =
          new HashMap<Long, Map<Long, StocktakingDetailVO>>();
      Map<Long, Map<Long, InventoryVO>> oldpositionmaps =
          new HashMap<Long, Map<Long, InventoryVO>>();
      for (int i = 0; i < stocktakingVO.getDetails().size(); i++) {
        StocktakingDetailVO tmpVO = stocktakingVO.getDetails().get(i);
        Long skuId = tmpVO.getSkuId();
        Long positionId = tmpVO.getPositionId();
        if (newpositionmaps.containsKey(positionId)) {
          if (newpositionmaps.get(positionId).containsKey(
              positionId)) {
            continue;
          } else {
            newpositionmaps.get(positionId).put(
                skuId, tmpVO);
          }
        } else {
          Map<Long, StocktakingDetailVO> newskumaps = new HashMap<Long, StocktakingDetailVO>();
          Map<Long, InventoryVO> oldskumaps = new HashMap<Long, InventoryVO>();
          newskumaps.put(skuId, stocktakingVO.getDetails()
              .get(i));
          newpositionmaps.put(positionId, newskumaps);

          List<InventoryVO> inventories = Lists.newArrayList();
          for(int j=0;;j++){
            List<InventoryVO> tmpList =
                inventoryDomainClient.getInventoryByPositionID(stocktakingVO.getAgencyId(),
                    positionId,50,j).getTarget();
            if(CollectionUtils.isEmpty(tmpList)){
              break;
            }else{
              inventories.addAll(tmpList);
            }
          }
          
          if (inventories != null) {
            for (InventoryVO inventoryVO : inventories) {
              oldskumaps.put(inventoryVO.getSkuId(), inventoryVO);
            }
          }
          oldpositionmaps.put(positionId, oldskumaps);
        }
      }
      for (int i = 0; i < stocktakingVO.getDetails().size(); i++) {
        StocktakingDetailVO tmpVO = stocktakingVO.getDetails().get(i);
        Long skuId = tmpVO.getSkuId();
        Long positionId = tmpVO.getPositionId();
        InventoryVO inventoryVO =
            oldpositionmaps.get(positionId).get(
                skuId);

        InventoryManageDetailVO inventoryManageDetailVO = new InventoryManageDetailVO();
        inventoryManageDetailVO.setSkuId(skuId);
        inventoryManageDetailVO.setPositionId(positionId);
        if (inventoryVO == null) {
          inventoryManageDetailVO
              .setNumber(tmpVO.getSecondNoReal() == null ? tmpVO.getFirstNoReal()
                  : tmpVO.getSecondNoReal());
        } else {
          inventoryManageDetailVO
              .setNumber((tmpVO.getSecondNoReal() == null ? tmpVO.getFirstNoReal()
                  : tmpVO.getSecondNoReal())
                  - inventoryVO.getNumberReal());
        }
        inventoryManageDetailVO.setSaleUnit(tmpVO.getSaleUnit());
        inventoryManageDetailVO.setSaleUnitId(tmpVO.getSaleUnitId());
        inventoryManageDetailVO.setSpec(tmpVO.getSpec());
        inventoryManageDetailVO.setSpecUnit(tmpVO.getSpecUnit());
        inventoryManageDetailVO.setSpecUnitId(tmpVO.getSpecUnitId());
        inventoryManageDetailVO
            .setSaleUnitType(tmpVO.getSaleUnitType());
        inventoryManageDetailVO.setSkuName(tmpVO.getSkuName());
        inventoryManageDetailVO
            .setPositionCode(tmpVO.getPositionCode());
        if (inventoryManageDetailVO.getNumber() != 0) {
          details.add(inventoryManageDetailVO);
        }
      }
//      for (Map<Long, InventoryVO> oldskumaps : oldpositionmaps.values()) {
//        for (InventoryVO inventoryVO : oldskumaps.values()) {
//          StocktakingDetailVO stocktakingDetailVO =
//              newpositionmaps.get(inventoryVO.getPositionId()).get(inventoryVO.getSkuId());
//
//          if (stocktakingDetailVO == null) {
//            InventoryManageDetailVO inventoryManageDetailVO = new InventoryManageDetailVO();
//            inventoryManageDetailVO.setSkuId(inventoryVO.getSkuId());
//            inventoryManageDetailVO.setPositionId(inventoryVO.getPositionId());
//            inventoryManageDetailVO.setNumber(-inventoryVO.getNumberReal());
//            inventoryManageDetailVO.setSaleUnit(inventoryVO.getSaleUnit());
//            inventoryManageDetailVO.setSaleUnitId(inventoryVO.getSaleUnitId());
//            inventoryManageDetailVO.setSpec(inventoryVO.getSpec());
//            inventoryManageDetailVO.setSpecUnit(inventoryVO.getSpecUnit());
//            inventoryManageDetailVO.setSpecUnitId(inventoryVO.getSpecUnitId());
//            inventoryManageDetailVO.setSaleUnitType(inventoryVO.getSaleUnitType());
//            inventoryManageDetailVO.setSkuName(inventoryVO.getSkuName());
//            inventoryManageDetailVO.setPositionCode(inventoryVO.getPositionCode());
//            if (inventoryManageDetailVO.getNumber() != 0) {
//              details.add(inventoryManageDetailVO);
//            }
//          }
//        }
//      }
    }
    inventoryManageVO.setDetails(details);
    return inventoryManageVO;
  }

	public IInventoryManageDomainClient getInventoryManageDomainClient() {
		return inventoryManageDomainClient;
	}

	public void setInventoryManageDomainClient(
			IInventoryManageDomainClient inventoryManageDomainClient) {
		this.inventoryManageDomainClient = inventoryManageDomainClient;
	}

	public IInventoryDomainClient getInventoryDomainClient() {
		return inventoryDomainClient;
	}

	public void setInventoryDomainClient(
			IInventoryDomainClient inventoryDomainClient) {
		this.inventoryDomainClient = inventoryDomainClient;
	}

	public SkuService getSkuService() {
		return skuService;
	}

	public void setSkuService(SkuService skuService) {
		this.skuService = skuService;
	}

	public ValueService getValueService() {
		return valueService;
	}

	public void setValueService(ValueService valueService) {
		this.valueService = valueService;
	}

	public IPositionDomainClient getPositionDomainClient() {
		return positionDomainClient;
	}

	public void setPositionDomainClient(
			IPositionDomainClient positionDomainClient) {
		this.positionDomainClient = positionDomainClient;
	}
}
