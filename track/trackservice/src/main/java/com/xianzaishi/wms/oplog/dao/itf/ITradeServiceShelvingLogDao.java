package com.xianzaishi.wms.oplog.dao.itf;

import java.util.List;

import com.xianzaishi.wms.common.dao.itf.IBaseDao;
import com.xianzaishi.wms.oplog.vo.TradeServiceShelvingLogVO;

public interface ITradeServiceShelvingLogDao extends IBaseDao {
	public Boolean batchAddTradeServiceShelvingLog(
			List<TradeServiceShelvingLogVO> tradeServiceShelvingLogVOs);
}