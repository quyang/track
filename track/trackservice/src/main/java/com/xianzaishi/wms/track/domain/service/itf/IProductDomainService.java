package com.xianzaishi.wms.track.domain.service.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.track.vo.ProductDetailQueryVO;
import com.xianzaishi.wms.track.vo.ProductDetailVO;
import com.xianzaishi.wms.track.vo.ProductQueryVO;
import com.xianzaishi.wms.track.vo.ProductVO;

public interface IProductDomainService {

	// domain
	public Boolean createProductDomain(ProductVO productVO);

	public ProductVO getProductDomainByID(Long productID);

	public Boolean deleteProductDomain(Long id);

	// head
	public Boolean addProductVO(ProductVO productVO);

	public QueryResultVO<ProductVO> queryProductVOList(
			ProductQueryVO productQueryVO);

	public ProductVO getProductVOByID(Long id);

	public Boolean modifyProductVO(ProductVO productVO);

	public Boolean deleteProductVO(Long productID);

	// body
	public Boolean createProductDetailVO(ProductDetailVO productDetailVO);

	public Boolean batchCreateProductDetailVO(
			List<ProductDetailVO> productDetailVOs);

	public List<ProductDetailVO> queryProductDetailVOList(
			ProductDetailQueryVO productDetailQueryVO);

	public List<ProductDetailVO> getProductDetailVOListByProductID(
			Long productID);

	public ProductDetailVO getProductDetailVOByID(Long id);

	public Boolean modifyProductDetailVO(ProductDetailVO productDetailVO);

	public Boolean batchModifyProductDetailVOs(
			List<ProductDetailVO> productDetailVOs);

	public Boolean deleteProductDetailVO(Long id);

	public Boolean deleteProductDetailVOByProductID(Long productID);

	public Boolean batchDeleteProductDetailVOByProductID(
			List<Long> storyDetailIDs);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

	public Boolean accounted(Long id);

}
