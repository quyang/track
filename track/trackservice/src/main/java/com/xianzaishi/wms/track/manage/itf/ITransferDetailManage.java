package com.xianzaishi.wms.track.manage.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.TransferDetailQueryVO;
import com.xianzaishi.wms.track.vo.TransferDetailVO;

public interface ITransferDetailManage {

	public Boolean addTransferDetailVO(TransferDetailVO transferDetailVO);

	public List<TransferDetailVO> queryTransferDetailVOList(
			TransferDetailQueryVO transferDetailQueryVO);

	public TransferDetailVO getTransferDetailVOByID(Long id);

	public Boolean modifyTransferDetailVO(TransferDetailVO transferDetailVO);

	public Boolean deleteTransferDetailVOByID(Long id);

	public List<TransferDetailVO> getTransferDetailVOByTransferID(Long id);

	public Boolean batchAddTransferDetailVO(
			List<TransferDetailVO> transferDetailVOs);

	public Boolean batchModifyTransferDetailVO(
			List<TransferDetailVO> transferDetailVOs);

	public Boolean batchDeleteTransferDetailVO(
			List<TransferDetailVO> transferDetailVOs);

	public Boolean batchDeleteTransferDetailVOByID(List<Long> storyDetailIDs);

}