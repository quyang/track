package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.manage.itf.IInspectionDetailManage;
import com.xianzaishi.wms.track.service.itf.IInspectionDetailService;
import com.xianzaishi.wms.track.vo.InspectionDetailQueryVO;
import com.xianzaishi.wms.track.vo.InspectionDetailVO;

public class InspectionDetailServiceImpl implements IInspectionDetailService {
	@Autowired
	private IInspectionDetailManage inspectionDetailManage = null;

	public IInspectionDetailManage getInspectionDetailManage() {
		return inspectionDetailManage;
	}

	public void setInspectionDetailManage(
			IInspectionDetailManage inspectionDetailManage) {
		this.inspectionDetailManage = inspectionDetailManage;
	}

	public Boolean addInspectionDetailVO(InspectionDetailVO inspectionDetailVO) {
		inspectionDetailManage.addInspectionDetailVO(inspectionDetailVO);
		return true;
	}

	public List<InspectionDetailVO> queryInspectionDetailVOList(
			InspectionDetailQueryVO inspectionDetailQueryVO) {
		return inspectionDetailManage
				.queryInspectionDetailVOList(inspectionDetailQueryVO);
	}

	public InspectionDetailVO getInspectionDetailVOByID(Long id) {
		return (InspectionDetailVO) inspectionDetailManage
				.getInspectionDetailVOByID(id);
	}

	public Boolean modifyInspectionDetailVO(
			InspectionDetailVO inspectionDetailVO) {
		return inspectionDetailManage
				.modifyInspectionDetailVO(inspectionDetailVO);
	}

	public Boolean deleteInspectionDetailVOByID(Long id) {
		return inspectionDetailManage.deleteInspectionDetailVOByID(id);
	}

	public List<InspectionDetailVO> getInspectionDetailVOByInspectionID(Long id) {
		return inspectionDetailManage.getInspectionDetailVOByInspectionID(id);
	}

	public Boolean batchAddInspectionDetailVO(
			List<InspectionDetailVO> inspectionDetailVOs) {
		return inspectionDetailManage
				.batchAddInspectionDetailVO(inspectionDetailVOs);
	}

	public Boolean batchModifyInspectionDetailVO(
			List<InspectionDetailVO> inspectionDetailVOs) {
		return inspectionDetailManage
				.batchModifyInspectionDetailVO(inspectionDetailVOs);
	}

	public Boolean batchDeleteInspectionDetailVO(
			List<InspectionDetailVO> inspectionDetailVOs) {
		return inspectionDetailManage
				.batchDeleteInspectionDetailVO(inspectionDetailVOs);
	}

	public Boolean batchDeleteInspectionDetailVOByID(List<Long> storyDetailIDs) {
		return inspectionDetailManage
				.batchDeleteInspectionDetailVOByID(storyDetailIDs);
	}
}
