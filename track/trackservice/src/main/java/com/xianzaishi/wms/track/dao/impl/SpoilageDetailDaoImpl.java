package com.xianzaishi.wms.track.dao.impl;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.track.dao.itf.ISpoilageDetailDao;

public class SpoilageDetailDaoImpl extends BaseDaoAdapter implements
		ISpoilageDetailDao {
	public String getVOClassName() {
		return "SpoilageDetailDO";
	}
}
