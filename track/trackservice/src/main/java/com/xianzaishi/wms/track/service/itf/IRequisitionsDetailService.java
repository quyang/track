package com.xianzaishi.wms.track.service.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.RequisitionsDetailQueryVO;
import com.xianzaishi.wms.track.vo.RequisitionsDetailVO;

public interface IRequisitionsDetailService {

	public Boolean addRequisitionsDetailVO(
			RequisitionsDetailVO requisitionsDetailVO);

	public List<RequisitionsDetailVO> queryRequisitionsDetailVOList(
			RequisitionsDetailQueryVO requisitionsDetailQueryVO);

	public RequisitionsDetailVO getRequisitionsDetailVOByID(Long id);

	public Boolean modifyRequisitionsDetailVO(
			RequisitionsDetailVO requisitionsDetailVO);

	public Boolean deleteRequisitionsDetailVOByID(Long id);

	public List<RequisitionsDetailVO> getRequisitionsDetailVOByRequisitionsID(
			Long id);

	public Boolean batchAddRequisitionsDetailVO(
			List<RequisitionsDetailVO> requisitionsDetailVOs);

	public Boolean batchModifyRequisitionsDetailVO(
			List<RequisitionsDetailVO> requisitionsDetailVOs);

	public Boolean batchDeleteRequisitionsDetailVO(
			List<RequisitionsDetailVO> requisitionsDetailVOs);

	public Boolean batchDeleteRequisitionsDetailVOByID(List<Long> storyDetailIDs);

}