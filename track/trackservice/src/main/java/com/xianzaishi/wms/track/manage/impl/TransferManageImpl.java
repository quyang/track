package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.ITransferDao;
import com.xianzaishi.wms.track.manage.itf.ITransferManage;
import com.xianzaishi.wms.track.vo.TransferQueryVO;
import com.xianzaishi.wms.track.vo.TransferVO;

public class TransferManageImpl implements ITransferManage {
	@Autowired
	private ITransferDao transferDao = null;

	private void validate(TransferVO transferVO) {
		if (transferVO.getAgencyId() == null || transferVO.getAgencyId() <= 0) {
			throw new BizException("agencyID error：" + transferVO.getAgencyId());
		}
		if (transferVO.getOperate() == null || transferVO.getOperate() <= 0) {
			throw new BizException("operator error：" + transferVO.getOperate());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public ITransferDao getTransferDao() {
		return transferDao;
	}

	public void setTransferDao(ITransferDao transferDao) {
		this.transferDao = transferDao;
	}

	public Long addTransferVO(TransferVO transferVO) {
		validate(transferVO);
		return (Long) transferDao.addDO(transferVO);
	}

	public List<TransferVO> queryTransferVOList(TransferQueryVO transferQueryVO) {
		return transferDao.queryDO(transferQueryVO);
	}

	public TransferVO getTransferVOByID(Long id) {
		return (TransferVO) transferDao.getDOByID(id);
	}

	public Boolean modifyTransferVO(TransferVO transferVO) {
		return transferDao.updateDO(transferVO);
	}

	public Boolean deleteTransferVOByID(Long id) {
		return transferDao.delDO(id);
	}

	public Boolean submit(Long id) {
		return transferDao.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return transferDao.audit(id, auditor);
	}

	public Boolean accounted(Long id) {
		return transferDao.accounted(id);
	}

	public Integer queryTransferVOCount(TransferQueryVO transferQueryVO) {
		return transferDao.queryCount(transferQueryVO);
	}
}
