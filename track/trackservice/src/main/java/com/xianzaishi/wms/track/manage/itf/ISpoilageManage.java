package com.xianzaishi.wms.track.manage.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.SpoilageQueryVO;
import com.xianzaishi.wms.track.vo.SpoilageVO;

public interface ISpoilageManage {

	public Long addSpoilageVO(SpoilageVO spoilageVO);

	public List<SpoilageVO> querySpoilageVOList(SpoilageQueryVO spoilageQueryVO);

	public Integer querySpoilageVOCount(SpoilageQueryVO spoilageQueryVO);

	public SpoilageVO getSpoilageVOByID(Long id);

	public Boolean modifySpoilageVO(SpoilageVO spoilageVO);

	public Boolean deleteSpoilageVOByID(Long id);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

	public Boolean accounted(Long id);

}