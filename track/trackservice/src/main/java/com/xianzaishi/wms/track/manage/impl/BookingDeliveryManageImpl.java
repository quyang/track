package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.IBookingDeliveryDao;
import com.xianzaishi.wms.track.manage.itf.IBookingDeliveryManage;
import com.xianzaishi.wms.track.vo.BookingDeliveryDO;
import com.xianzaishi.wms.track.vo.BookingDeliveryQueryVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryVO;

public class BookingDeliveryManageImpl implements IBookingDeliveryManage {
	@Autowired
	private IBookingDeliveryDao bookingDeliveryDao = null;

	private void validate(BookingDeliveryVO bookingDeliveryDO) {
		if (bookingDeliveryDO.getAgencyId() == null
				|| bookingDeliveryDO.getAgencyId() <= 0) {
			throw new BizException("agency error："
					+ bookingDeliveryDO.getAgencyId());
		}
		if (bookingDeliveryDO.getDeliveryTime() == null) {
			throw new BizException("delivery time error："
					+ bookingDeliveryDO.getDeliveryTime());
		}
		if (bookingDeliveryDO.getOperate() == null
				|| bookingDeliveryDO.getOperate() <= 0) {
			throw new BizException("operator error："
					+ bookingDeliveryDO.getOperate());
		}
		if (bookingDeliveryDO.getSupplierId() == null
				|| bookingDeliveryDO.getSupplierId() <= 0) {
			throw new BizException("supplier error："
					+ bookingDeliveryDO.getSupplierId());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IBookingDeliveryDao getBookingDeliveryDao() {
		return bookingDeliveryDao;
	}

	public void setBookingDeliveryDao(IBookingDeliveryDao bookingDeliveryDao) {
		this.bookingDeliveryDao = bookingDeliveryDao;
	}

	public Long addBookingDeliveryVO(BookingDeliveryVO bookingDeliveryVO) {
		validate(bookingDeliveryVO);
		return (Long) bookingDeliveryDao.addDO(bookingDeliveryVO);
	}

	public List<BookingDeliveryVO> queryBookingDeliveryVOList(
			BookingDeliveryQueryVO bookingDeliveryQueryVO) {
		return bookingDeliveryDao.queryDO(bookingDeliveryQueryVO);
	}

	public BookingDeliveryVO getBookingDeliveryVOByID(Long id) {
		return (BookingDeliveryVO) bookingDeliveryDao.getDOByID(id);
	}

	public Boolean modifyBookingDeliveryVO(BookingDeliveryVO bookingDeliveryVO) {
		return bookingDeliveryDao.updateDO(bookingDeliveryVO);
	}

	public Boolean deleteBookingDeliveryVOByID(Long id) {
		return bookingDeliveryDao.delDO(id);
	}

	public Boolean submit(Long id) {
		return bookingDeliveryDao.submit(id);
	}

	public Boolean confirm(Long id, Long auditor) {
		return bookingDeliveryDao.confirm(id, auditor);
	}

	public Integer queryBookingDeliveryVOCount(
			BookingDeliveryQueryVO bookingDeliveryQueryVO) {
		return bookingDeliveryDao.queryCount(bookingDeliveryQueryVO);
	}
}
