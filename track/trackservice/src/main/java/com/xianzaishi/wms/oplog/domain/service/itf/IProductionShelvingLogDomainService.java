package com.xianzaishi.wms.oplog.domain.service.itf;

import java.util.List;

import com.xianzaishi.wms.oplog.vo.ProductionShelvingLogVO;

public interface IProductionShelvingLogDomainService {
	public Boolean batchAddProductionShelvingLog(
			List<ProductionShelvingLogVO> productionShelvingLogVOs);
}
