package com.xianzaishi.wms.job.service.itf;

import java.util.List;

import com.xianzaishi.wms.job.vo.JobTypeVO;
import com.xianzaishi.wms.job.vo.JobTypeQueryVO;

public interface IJobTypeService {

	public Boolean addJobTypeVO(JobTypeVO jobTypeVO);

	public List<JobTypeVO> queryJobTypeVOList(JobTypeQueryVO jobTypeQueryVO);

	public JobTypeVO getJobTypeVOByID(Long id);

	public Boolean modifyJobTypeVO(JobTypeVO jobTypeVO);

	public Boolean deleteJobTypeVOByID(Long id);

}