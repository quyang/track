package com.xianzaishi.wms.track.service.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.AdjustQueryVO;
import com.xianzaishi.wms.track.vo.AdjustVO;

public interface IAdjustService {

	public Long addAdjustVO(AdjustVO adjustVO);

	public List<AdjustVO> queryAdjustVOList(AdjustQueryVO adjustQueryVO);

	public Integer queryAdjustVOCount(AdjustQueryVO adjustQueryVO);

	public AdjustVO getAdjustVOByID(Long id);

	public Boolean modifyAdjustVO(AdjustVO adjustVO);

	public Boolean deleteAdjustVOByID(Long id);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

	public Boolean accounted(Long id);
}