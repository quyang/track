package com.xianzaishi.wms.barcode.service.impl;

import java.util.Hashtable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.barcode.manage.itf.IBarcodeSequenceManage;
import com.xianzaishi.wms.barcode.service.itf.IBarcodeSequenceService;
import com.xianzaishi.wms.barcode.vo.BarcodeSequenceQueryVO;
import com.xianzaishi.wms.barcode.vo.BarcodeSequenceVO;
import com.xianzaishi.wms.common.exception.BizException;

public class BarcodeSequenceServiceImpl implements IBarcodeSequenceService {
	private Hashtable<Integer, BarcodeSequenceVO> barcodeSequenceVOs = new Hashtable<Integer, BarcodeSequenceVO>();
	// 028*****000000000000000
	@Autowired
	private IBarcodeSequenceManage barcodeSequenceManage = null;

	public IBarcodeSequenceManage getBarcodeSequenceManage() {
		return barcodeSequenceManage;
	}

	public void setBarcodeSequenceManage(
			IBarcodeSequenceManage barcodeSequenceManage) {
		this.barcodeSequenceManage = barcodeSequenceManage;
	}

	public Boolean addBarcodeSequenceVO(BarcodeSequenceVO barcodeSequenceVO) {
		barcodeSequenceManage.addBarcodeSequenceVO(barcodeSequenceVO);
		return true;
	}

	public List<BarcodeSequenceVO> queryBarcodeSequenceVOList(
			BarcodeSequenceQueryVO barcodeSequenceQueryVO) {
		return barcodeSequenceManage
				.queryBarcodeSequenceVOList(barcodeSequenceQueryVO);
	}

	public BarcodeSequenceVO getBarcodeSequenceVOByID(Long id) {
		return (BarcodeSequenceVO) barcodeSequenceManage
				.getBarcodeSequenceVOByID(id);
	}

	public Boolean modifyBarcodeSequenceVO(BarcodeSequenceVO barcodeSequenceVO) {
		return barcodeSequenceManage.modifyBarcodeSequenceVO(barcodeSequenceVO);
	}

	public Boolean deleteBarcodeSequenceVOByID(Long id) {
		return barcodeSequenceManage.deleteBarcodeSequenceVOByID(id);
	}

	public synchronized String getBarcode(Integer type) {
		BarcodeSequenceVO barcodeSequenceVO = barcodeSequenceVOs.get(type);
		if (barcodeSequenceVO == null
				|| barcodeSequenceVO.getSeqno().equals(
						barcodeSequenceVO.getMaxseqno())) {
			barcodeSequenceVO = orderNewSegment(type);
			barcodeSequenceVOs.put(type, barcodeSequenceVO);
		}
		barcodeSequenceVO.setSeqno(barcodeSequenceVO.getSeqno() + 1);
		Long seq = barcodeSequenceVO.getSeqno();
		return changeToBarcode(type, seq);
	}

	private BarcodeSequenceVO orderNewSegment(Integer type) {
		BarcodeSequenceQueryVO queryVO = new BarcodeSequenceQueryVO();
		queryVO.setType(type);
		List<BarcodeSequenceVO> barcodeSequenceVOs = barcodeSequenceManage
				.queryBarcodeSequenceVOList(queryVO);
		if (barcodeSequenceVOs == null || barcodeSequenceVOs.isEmpty()) {
			throw new BizException("请初始化序列。");
		}
		BarcodeSequenceVO barcodeSequenceVO = barcodeSequenceVOs.get(0);
		barcodeSequenceVO.setMaxseqno(barcodeSequenceVO.getSeqno() + 1000);
		if (!barcodeSequenceManage.order(barcodeSequenceVO)) {
			throw new BizException("请重新操作。");
		}
		return barcodeSequenceVO;
	}

	public synchronized List<String> getBarcodes(Integer type, Integer no) {
		throw new BizException("abstract method");
	}

	private String changeToBarcode(Integer type, Long seqno) {
		return new StringBuffer().append("028")
				.append(String.format("%03d", type))
				.append(String.format("%014d", seqno)).toString();
	}
}
