package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.manage.itf.IBookingDeliveryDetailManage;
import com.xianzaishi.wms.track.service.itf.IBookingDeliveryDetailService;
import com.xianzaishi.wms.track.vo.BookingDeliveryDetailQueryVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryDetailVO;

public class BookingDeliveryDetailServiceImpl implements
		IBookingDeliveryDetailService {
	@Autowired
	private IBookingDeliveryDetailManage bookingDeliveryDetailManage = null;

	public IBookingDeliveryDetailManage getBookingDeliveryDetailManage() {
		return bookingDeliveryDetailManage;
	}

	public void setBookingDeliveryDetailManage(
			IBookingDeliveryDetailManage bookingDeliveryDetailManage) {
		this.bookingDeliveryDetailManage = bookingDeliveryDetailManage;
	}

	public Boolean addBookingDeliveryDetailVO(
			BookingDeliveryDetailVO bookingDeliveryDetailVO) {
		bookingDeliveryDetailManage
				.addBookingDeliveryDetailVO(bookingDeliveryDetailVO);
		return true;
	}

	public List<BookingDeliveryDetailVO> queryBookingDeliveryDetailVOList(
			BookingDeliveryDetailQueryVO bookingDeliveryDetailQueryVO) {
		return bookingDeliveryDetailManage
				.queryBookingDeliveryDetailVOList(bookingDeliveryDetailQueryVO);
	}

	public BookingDeliveryDetailVO getBookingDeliveryDetailVOByID(Long id) {
		return (BookingDeliveryDetailVO) bookingDeliveryDetailManage
				.getBookingDeliveryDetailVOByID(id);
	}

	public Boolean modifyBookingDeliveryDetailVO(
			BookingDeliveryDetailVO bookingDeliveryDetailVO) {
		return bookingDeliveryDetailManage
				.modifyBookingDeliveryDetailVO(bookingDeliveryDetailVO);
	}

	public Boolean deleteBookingDeliveryDetailVOByID(Long id) {
		return bookingDeliveryDetailManage
				.deleteBookingDeliveryDetailVOByID(id);
	}

	public List<BookingDeliveryDetailVO> getBookingDeliveryDetailVOByBookingDeliveryID(
			Long id) {
		return bookingDeliveryDetailManage
				.getBookingDeliveryDetailVOByBookingDeliveryID(id);
	}

	public Boolean batchAddBookingDeliveryDetailVO(
			List<BookingDeliveryDetailVO> bookingDeliveryDetailVOs) {
		return bookingDeliveryDetailManage
				.batchAddBookingDeliveryDetailVO(bookingDeliveryDetailVOs);
	}

	public Boolean batchModifyBookingDeliveryDetailVO(
			List<BookingDeliveryDetailVO> bookingDeliveryDetailVOs) {
		return bookingDeliveryDetailManage
				.batchModifyBookingDeliveryDetailVO(bookingDeliveryDetailVOs);
	}

	public Boolean batchDeleteBookingDeliveryDetailVO(
			List<BookingDeliveryDetailVO> bookingDeliveryDetailVOs) {
		return bookingDeliveryDetailManage
				.batchDeleteBookingDeliveryDetailVO(bookingDeliveryDetailVOs);
	}

	public Boolean batchDeleteBookingDeliveryDetailVOByID(
			List<Long> storyDetailIDs) {
		return bookingDeliveryDetailManage
				.batchDeleteBookingDeliveryDetailVOByID(storyDetailIDs);
	}
}
