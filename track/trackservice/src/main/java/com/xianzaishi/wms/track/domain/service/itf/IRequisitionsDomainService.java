package com.xianzaishi.wms.track.domain.service.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.track.vo.RequisitionsDetailQueryVO;
import com.xianzaishi.wms.track.vo.RequisitionsDetailVO;
import com.xianzaishi.wms.track.vo.RequisitionsQueryVO;
import com.xianzaishi.wms.track.vo.RequisitionsVO;

public interface IRequisitionsDomainService {

	// domain
	public Boolean createRequisitionsDomain(RequisitionsVO requisitionsVO);

	public RequisitionsVO getRequisitionsDomainByID(Long requisitionsID);

	public Boolean deleteRequisitionsDomain(Long id);

	// head
	public Boolean addRequisitionsVO(RequisitionsVO requisitionsVO);

	public QueryResultVO<RequisitionsVO> queryRequisitionsVOList(
			RequisitionsQueryVO requisitionsQueryVO);

	public RequisitionsVO getRequisitionsVOByID(Long id);

	public Boolean modifyRequisitionsVO(RequisitionsVO requisitionsVO);

	public Boolean deleteRequisitionsVO(Long requisitionsID);

	// body
	public Boolean createRequisitionsDetailVO(
			RequisitionsDetailVO requisitionsDetailVO);

	public Boolean batchCreateRequisitionsDetailVO(
			List<RequisitionsDetailVO> requisitionsDetailVOs);

	public List<RequisitionsDetailVO> queryRequisitionsDetailVOList(
			RequisitionsDetailQueryVO requisitionsDetailQueryVO);

	public List<RequisitionsDetailVO> getRequisitionsDetailVOListByRequisitionsID(
			Long requisitionsID);

	public RequisitionsDetailVO getRequisitionsDetailVOByID(Long id);

	public Boolean modifyRequisitionsDetailVO(
			RequisitionsDetailVO requisitionsDetailVO);

	/**
	 * 按ID更新Detail
	 * 
	 * @param requisitionsDetailVOs
	 * @return
	 */
	public Boolean batchModifyRequisitionsDetailVOs(
			List<RequisitionsDetailVO> requisitionsDetailVOs);

	public Boolean deleteRequisitionsDetailVO(Long id);

	public Boolean deleteRequisitionsDetailVOByRequisitionsID(
			Long requisitionsID);

	public Boolean batchDeleteRequisitionsDetailVOByRequisitionsID(
			List<Long> storyDetailIDs);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

	public Boolean outgoing(Long id, Long auditor);

	public Boolean outgoingAudit(Long id, Long auditor);

	public Boolean accounted(Long id);

}
