package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.IOutgoingSaleDao;
import com.xianzaishi.wms.track.manage.itf.IOutgoingSaleManage;
import com.xianzaishi.wms.track.vo.OutgoingSaleQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingSaleVO;

public class OutgoingSaleManageImpl implements IOutgoingSaleManage {
	@Autowired
	private IOutgoingSaleDao outgoingSaleDao = null;

	private void validate(OutgoingSaleVO outgoingSaleVO) {
		if (outgoingSaleVO.getAgencyId() == null
				|| outgoingSaleVO.getAgencyId() <= 0) {
			throw new BizException("agencyID error："
					+ outgoingSaleVO.getAgencyId());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IOutgoingSaleDao getOutgoingSaleDao() {
		return outgoingSaleDao;
	}

	public void setOutgoingSaleDao(IOutgoingSaleDao outgoingSaleDao) {
		this.outgoingSaleDao = outgoingSaleDao;
	}

	public Long addOutgoingSaleVO(OutgoingSaleVO outgoingSaleVO) {
		validate(outgoingSaleVO);
		return (Long) outgoingSaleDao.addDO(outgoingSaleVO);
	}

	public List<OutgoingSaleVO> queryOutgoingSaleVOList(
			OutgoingSaleQueryVO outgoingSaleQueryVO) {
		return outgoingSaleDao.queryDO(outgoingSaleQueryVO);
	}

	public OutgoingSaleVO getOutgoingSaleVOByID(Long id) {
		return (OutgoingSaleVO) outgoingSaleDao.getDOByID(id);
	}

	public Boolean modifyOutgoingSaleVO(OutgoingSaleVO outgoingSaleVO) {
		return outgoingSaleDao.updateDO(outgoingSaleVO);
	}

	public Boolean deleteOutgoingSaleVOByID(Long id) {
		return outgoingSaleDao.delDO(id);
	}

	public Boolean submit(Long id) {
		return outgoingSaleDao.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return outgoingSaleDao.audit(id, auditor);
	}

	public Boolean accounted(Long id) {
		return outgoingSaleDao.accounted(id);
	}

	public Integer queryOutgoingSaleVOCount(
			OutgoingSaleQueryVO outgoingSaleQueryVO) {
		return outgoingSaleDao.queryCount(outgoingSaleQueryVO);
	}
}
