package com.xianzaishi.wms.track.dao.impl;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.track.dao.itf.IStorageDetailDao;

public class StorageDetailDaoImpl extends BaseDaoAdapter implements
		IStorageDetailDao {
	public String getVOClassName() {
		return "StorageDetailDO";
	}
}
