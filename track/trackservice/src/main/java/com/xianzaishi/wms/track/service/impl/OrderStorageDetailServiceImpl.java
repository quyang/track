package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.manage.itf.IOrderStorageDetailManage;
import com.xianzaishi.wms.track.service.itf.IOrderStorageDetailService;
import com.xianzaishi.wms.track.vo.OrderStorageDetailQueryVO;
import com.xianzaishi.wms.track.vo.OrderStorageDetailVO;

public class OrderStorageDetailServiceImpl implements
		IOrderStorageDetailService {
	@Autowired
	private IOrderStorageDetailManage orderStorageDetailManage = null;

	public IOrderStorageDetailManage getOrderStorageDetailManage() {
		return orderStorageDetailManage;
	}

	public void setOrderStorageDetailManage(
			IOrderStorageDetailManage orderStorageDetailManage) {
		this.orderStorageDetailManage = orderStorageDetailManage;
	}

	public Boolean addOrderStorageDetailVO(
			OrderStorageDetailVO orderStorageDetailVO) {
		orderStorageDetailManage.addOrderStorageDetailVO(orderStorageDetailVO);
		return true;
	}

	public List<OrderStorageDetailVO> queryOrderStorageDetailVOList(
			OrderStorageDetailQueryVO orderStorageDetailQueryVO) {
		return orderStorageDetailManage
				.queryOrderStorageDetailVOList(orderStorageDetailQueryVO);
	}

	public OrderStorageDetailVO getOrderStorageDetailVOByID(Long id) {
		return (OrderStorageDetailVO) orderStorageDetailManage
				.getOrderStorageDetailVOByID(id);
	}

	public Boolean modifyOrderStorageDetailVO(
			OrderStorageDetailVO orderStorageDetailVO) {
		return orderStorageDetailManage
				.modifyOrderStorageDetailVO(orderStorageDetailVO);
	}

	public Boolean deleteOrderStorageDetailVOByID(Long id) {
		return orderStorageDetailManage.deleteOrderStorageDetailVOByID(id);
	}

	public List<OrderStorageDetailVO> getOrderStorageDetailVOByOrderStorageID(
			Long id) {
		return orderStorageDetailManage
				.getOrderStorageDetailVOByOrderStorageID(id);
	}

	public Boolean batchAddOrderStorageDetailVO(
			List<OrderStorageDetailVO> orderStorageDetailVOs) {
		return orderStorageDetailManage
				.batchAddOrderStorageDetailVO(orderStorageDetailVOs);
	}

	public Boolean batchModifyOrderStorageDetailVO(
			List<OrderStorageDetailVO> orderStorageDetailVOs) {
		return orderStorageDetailManage
				.batchModifyOrderStorageDetailVO(orderStorageDetailVOs);
	}

	public Boolean batchDeleteOrderStorageDetailVO(
			List<OrderStorageDetailVO> orderStorageDetailVOs) {
		return orderStorageDetailManage
				.batchDeleteOrderStorageDetailVO(orderStorageDetailVOs);
	}

	public Boolean batchDeleteOrderStorageDetailVOByID(List<Long> storyDetailIDs) {
		return orderStorageDetailManage
				.batchDeleteOrderStorageDetailVOByID(storyDetailIDs);
	}
}
