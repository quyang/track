package com.xianzaishi.wms.track.service.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.TradeServiceShelvingDetailVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingDetailVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingDetailQueryVO;

public interface ITradeServiceShelvingDetailService {

	public Boolean addTradeServiceShelvingDetailVO(
			TradeServiceShelvingDetailVO tradeServiceShelvingDetailVO);

	public List<TradeServiceShelvingDetailVO> queryTradeServiceShelvingDetailVOList(
			TradeServiceShelvingDetailQueryVO tradeServiceShelvingDetailQueryVO);

	public TradeServiceShelvingDetailVO getTradeServiceShelvingDetailVOByID(
			Long id);

	public Boolean modifyTradeServiceShelvingDetailVO(
			TradeServiceShelvingDetailVO tradeServiceShelvingDetailVO);

	public Boolean deleteTradeServiceShelvingDetailVOByID(Long id);

	public List<TradeServiceShelvingDetailVO> getTradeServiceShelvingDetailVOByTradeServiceShelvingID(
			Long id);

	public Boolean batchAddTradeServiceShelvingDetailVO(
			List<TradeServiceShelvingDetailVO> tradeServiceShelvingDetailVOs);

	public Boolean batchModifyTradeServiceShelvingDetailVO(
			List<TradeServiceShelvingDetailVO> tradeServiceShelvingDetailVOs);

	public Boolean batchDeleteTradeServiceShelvingDetailVO(
			List<TradeServiceShelvingDetailVO> tradeServiceShelvingDetailVOs);

	public Boolean batchDeleteTradeServiceShelvingDetailVOByID(
			List<Long> tradeServiceShelvingDetailIDs);
}