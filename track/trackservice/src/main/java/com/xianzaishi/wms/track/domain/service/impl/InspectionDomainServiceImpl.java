package com.xianzaishi.wms.track.domain.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.track.domain.service.itf.IInspectionDomainService;
import com.xianzaishi.wms.track.service.itf.IInspectionDetailService;
import com.xianzaishi.wms.track.service.itf.IInspectionService;
import com.xianzaishi.wms.track.vo.InspectionDetailQueryVO;
import com.xianzaishi.wms.track.vo.InspectionDetailVO;
import com.xianzaishi.wms.track.vo.InspectionQueryVO;
import com.xianzaishi.wms.track.vo.InspectionVO;

public class InspectionDomainServiceImpl implements IInspectionDomainService {
	@Autowired
	private IInspectionService inspectionService = null;
	@Autowired
	private IInspectionDetailService inspectionDetailService = null;
	@Autowired
	private SkuService skuService = null;
	@Autowired
	private ValueService valueService = null;

	public Boolean createInspectionDomain(InspectionVO inspectionVO) {
		if (inspectionVO.getDetails() == null
				|| inspectionVO.getDetails().isEmpty()) {
			throw new BizException("请填充入库明细！");
		}

		Long inspectionID = inspectionService.addInspectionVO(inspectionVO);

		initDetails(inspectionID, inspectionVO.getDetails());

		inspectionDetailService.batchAddInspectionDetailVO(inspectionVO
				.getDetails());

		return true;
	}

	public InspectionVO getInspectionDomainByID(Long inspectionID) {
		if (inspectionID == null || inspectionID <= 0) {
			throw new BizException("id error");
		}

		InspectionVO inspectionVO = inspectionService
				.getInspectionVOByID(inspectionID);

		inspectionVO.setDetails(inspectionDetailService
				.getInspectionDetailVOByInspectionID(inspectionID));

		return inspectionVO;
	}

	public Boolean deleteInspectionDomain(Long inspectionID) {
		if (inspectionID == null || inspectionID <= 0) {
			throw new BizException("id error");
		}

		List<InspectionDetailVO> inspectionDetailVOs = inspectionDetailService
				.getInspectionDetailVOByInspectionID(inspectionID);

		inspectionDetailService
				.batchDeleteInspectionDetailVO(inspectionDetailVOs);

		inspectionService.deleteInspectionVOByID(inspectionID);

		return true;
	}

	public Boolean addInspectionVO(InspectionVO inspectionVO) {
		inspectionService.addInspectionVO(inspectionVO);
		return true;
	}

	public QueryResultVO<InspectionVO> queryInspectionVOList(
			InspectionQueryVO inspectionQueryVO) {
		QueryResultVO<InspectionVO> queryResultVO = new QueryResultVO<>();
		queryResultVO.setPage(inspectionQueryVO.getPage());
		queryResultVO.setSize(inspectionQueryVO.getSize());
		queryResultVO.setItems(inspectionService
				.queryInspectionVOList(inspectionQueryVO));
		queryResultVO.setTotalCount(inspectionService
				.queryInspectionVOCount(inspectionQueryVO));
		return queryResultVO;
	}

	public InspectionVO getInspectionVOByID(Long id) {
		return inspectionService.getInspectionVOByID(id);
	}

	public Boolean modifyInspectionVO(InspectionVO inspectionVO) {
		return inspectionService.modifyInspectionVO(inspectionVO);
	}

	public Boolean deleteInspectionVO(Long id) {
		return inspectionService.deleteInspectionVOByID(id);
	}

	public Boolean createInspectionDetailVO(
			InspectionDetailVO inspectionDetailVO) {
		initInfo(inspectionDetailVO);
		return inspectionDetailService
				.addInspectionDetailVO(inspectionDetailVO);
	}

	public Boolean batchCreateInspectionDetailVO(
			List<InspectionDetailVO> inspectionDetailVOs) {
		for (InspectionDetailVO inspectionDetailVO : inspectionDetailVOs) {
			initInfo(inspectionDetailVO);
		}
		return inspectionDetailService
				.batchAddInspectionDetailVO(inspectionDetailVOs);
	}

	public List<InspectionDetailVO> queryInspectionDetailVOList(
			InspectionDetailQueryVO inspectionDetailQueryVO) {
		return inspectionDetailService
				.queryInspectionDetailVOList(inspectionDetailQueryVO);
	}

	public List<InspectionDetailVO> getInspectionDetailVOListByInspectionID(
			Long inspectionID) {
		return inspectionDetailService
				.getInspectionDetailVOByInspectionID(inspectionID);
	}

	public InspectionDetailVO getInspectionDetailVOByID(Long id) {
		return inspectionDetailService.getInspectionDetailVOByID(id);
	}

	public Boolean modifyInspectionDetailVO(
			InspectionDetailVO inspectionDetailVO) {
		initInfo(inspectionDetailVO);
		return inspectionDetailService
				.modifyInspectionDetailVO(inspectionDetailVO);
	}

	public Boolean batchModifyInspectionDetailVOs(
			List<InspectionDetailVO> inspectionDetailVOs) {
		for (InspectionDetailVO inspectionDetailVO : inspectionDetailVOs) {
			initInfo(inspectionDetailVO);
		}
		return inspectionDetailService
				.batchModifyInspectionDetailVO(inspectionDetailVOs);
	}

	public Boolean deleteInspectionDetailVO(Long id) {
		return inspectionDetailService.deleteInspectionDetailVOByID(id);
	}

	public Boolean deleteInspectionDetailVOByInspectionID(Long inspectionID) {
		if (inspectionID == null || inspectionID <= 0) {
			throw new BizException("id error");
		}

		List<InspectionDetailVO> inspectionDetailVOs = inspectionDetailService
				.getInspectionDetailVOByInspectionID(inspectionID);

		inspectionDetailService
				.batchDeleteInspectionDetailVO(inspectionDetailVOs);

		return true;
	}

	public Boolean batchDeleteInspectionDetailVOByInspectionID(
			List<Long> storyDetailIDs) {
		return inspectionDetailService
				.batchDeleteInspectionDetailVOByID(storyDetailIDs);
	}

	private void initDetails(Long id,
			List<InspectionDetailVO> inspectionDetailVOs) {
		for (int i = 0; i < inspectionDetailVOs.size(); i++) {
			inspectionDetailVOs.get(i).setInspectionId(id);
			initInfo(inspectionDetailVOs.get(i));
		}
	}

	private void initInfo(InspectionDetailVO inspectionDetailVO) {
		ItemCommoditySkuDTO itemCommoditySkuDTO = skuService.queryItemSku(
				SkuQuery.querySkuById(inspectionDetailVO.getSkuId()))
				.getModule();
		if (itemCommoditySkuDTO == null) {
			throw new BizException("skuID error:"
					+ inspectionDetailVO.getSkuId());
		}
		inspectionDetailVO.setSkuName(itemCommoditySkuDTO.getTitle());
		inspectionDetailVO.setSaleUnit(valueService.queryValueNameByValueId(
				itemCommoditySkuDTO.getSkuUnit()).getModule());
		inspectionDetailVO.setSaleUnitId(new Long(itemCommoditySkuDTO
				.getSkuUnit()));
		inspectionDetailVO.setSaleUnitType(new Integer(itemCommoditySkuDTO
				.querySaleUnitType()));
		// inspectionDetailVO.setSpec(itemCommoditySkuDTO
		// .getSkuSpecificationNum());
		// inspectionDetailVO.setSpecUnit(valueService.queryValueNameByValueId(
		// itemCommoditySkuDTO.getSkuSpecificationUnit()).getModule());
		// inspectionDetailVO.setSpecUnitId(new Long(itemCommoditySkuDTO
		// .getSkuSpecificationUnit()));

		if (inspectionDetailVO.getBookingNoReal() == null
				&& inspectionDetailVO.getBookingNo() != null
				&& !inspectionDetailVO.getBookingNo().isEmpty()) {
			inspectionDetailVO.setBookingNoReal(new BigDecimal(
					inspectionDetailVO.getBookingNo()).multiply(
					new BigDecimal(1000)).intValue());
		}

		if (inspectionDetailVO.getBookingNoReal() == null
				&& inspectionDetailVO.getBookingNo() != null
				&& !inspectionDetailVO.getBookingNo().isEmpty()) {
			inspectionDetailVO.setBookingNoReal(new BigDecimal(
					inspectionDetailVO.getBookingNo()).multiply(
					new BigDecimal(1000)).intValue());
		}

		if (inspectionDetailVO.getQualifiedNoReal() == null
				&& inspectionDetailVO.getQualifiedNo() != null
				&& !inspectionDetailVO.getQualifiedNo().isEmpty()) {
			inspectionDetailVO.setQualifiedNoReal(new BigDecimal(
					inspectionDetailVO.getQualifiedNo()).multiply(
					new BigDecimal(1000)).intValue());
		}
	}

	public IInspectionService getInspectionService() {
		return inspectionService;
	}

	public void setInspectionService(IInspectionService inspectionService) {
		this.inspectionService = inspectionService;
	}

	public IInspectionDetailService getInspectionDetailService() {
		return inspectionDetailService;
	}

	public void setInspectionDetailService(
			IInspectionDetailService inspectionDetailService) {
		this.inspectionDetailService = inspectionDetailService;
	}

	public Boolean submit(Long id) {
		return inspectionService.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return inspectionService.audit(id, auditor);
	}

	public SkuService getSkuService() {
		return skuService;
	}

	public void setSkuService(SkuService skuService) {
		this.skuService = skuService;
	}

	public ValueService getValueService() {
		return valueService;
	}

	public void setValueService(ValueService valueService) {
		this.valueService = valueService;
	}
}
