package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.manage.itf.IRequisitionsDetailManage;
import com.xianzaishi.wms.track.service.itf.IRequisitionsDetailService;
import com.xianzaishi.wms.track.vo.RequisitionsDetailQueryVO;
import com.xianzaishi.wms.track.vo.RequisitionsDetailVO;

public class RequisitionsDetailServiceImpl implements
		IRequisitionsDetailService {
	@Autowired
	private IRequisitionsDetailManage requisitionsDetailManage = null;

	public IRequisitionsDetailManage getRequisitionsDetailManage() {
		return requisitionsDetailManage;
	}

	public void setRequisitionsDetailManage(
			IRequisitionsDetailManage requisitionsDetailManage) {
		this.requisitionsDetailManage = requisitionsDetailManage;
	}

	public Boolean addRequisitionsDetailVO(
			RequisitionsDetailVO requisitionsDetailVO) {
		requisitionsDetailManage.addRequisitionsDetailVO(requisitionsDetailVO);
		return true;
	}

	public List<RequisitionsDetailVO> queryRequisitionsDetailVOList(
			RequisitionsDetailQueryVO requisitionsDetailQueryVO) {
		return requisitionsDetailManage
				.queryRequisitionsDetailVOList(requisitionsDetailQueryVO);
	}

	public RequisitionsDetailVO getRequisitionsDetailVOByID(Long id) {
		return (RequisitionsDetailVO) requisitionsDetailManage
				.getRequisitionsDetailVOByID(id);
	}

	public Boolean modifyRequisitionsDetailVO(
			RequisitionsDetailVO requisitionsDetailVO) {
		return requisitionsDetailManage
				.modifyRequisitionsDetailVO(requisitionsDetailVO);
	}

	public Boolean deleteRequisitionsDetailVOByID(Long id) {
		return requisitionsDetailManage.deleteRequisitionsDetailVOByID(id);
	}

	public List<RequisitionsDetailVO> getRequisitionsDetailVOByRequisitionsID(Long id) {
		return requisitionsDetailManage.getRequisitionsDetailVOByRequisitionsID(id);
	}

	public Boolean batchAddRequisitionsDetailVO(
			List<RequisitionsDetailVO> requisitionsDetailVOs) {
		return requisitionsDetailManage.batchAddRequisitionsDetailVO(requisitionsDetailVOs);
	}

	public Boolean batchModifyRequisitionsDetailVO(
			List<RequisitionsDetailVO> requisitionsDetailVOs) {
		return requisitionsDetailManage.batchModifyRequisitionsDetailVO(requisitionsDetailVOs);
	}

	public Boolean batchDeleteRequisitionsDetailVO(
			List<RequisitionsDetailVO> requisitionsDetailVOs) {
		return requisitionsDetailManage.batchDeleteRequisitionsDetailVO(requisitionsDetailVOs);
	}

	public Boolean batchDeleteRequisitionsDetailVOByID(List<Long> storyDetailIDs) {
		return requisitionsDetailManage
				.batchDeleteRequisitionsDetailVOByID(storyDetailIDs);
	}
}
