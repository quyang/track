package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.manage.itf.IOutgoingDetailManage;
import com.xianzaishi.wms.track.service.itf.IOutgoingDetailService;
import com.xianzaishi.wms.track.vo.OutgoingDetailQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingDetailVO;

public class OutgoingDetailServiceImpl implements IOutgoingDetailService {
	@Autowired
	private IOutgoingDetailManage outgoingDetailManage = null;

	public IOutgoingDetailManage getOutgoingDetailManage() {
		return outgoingDetailManage;
	}

	public void setOutgoingDetailManage(
			IOutgoingDetailManage outgoingDetailManage) {
		this.outgoingDetailManage = outgoingDetailManage;
	}

	public Boolean addOutgoingDetailVO(OutgoingDetailVO outgoingDetailVO) {
		outgoingDetailManage.addOutgoingDetailVO(outgoingDetailVO);
		return true;
	}

	public List<OutgoingDetailVO> queryOutgoingDetailVOList(
			OutgoingDetailQueryVO outgoingDetailQueryVO) {
		return outgoingDetailManage
				.queryOutgoingDetailVOList(outgoingDetailQueryVO);
	}

	public OutgoingDetailVO getOutgoingDetailVOByID(Long id) {
		return (OutgoingDetailVO) outgoingDetailManage
				.getOutgoingDetailVOByID(id);
	}

	public Boolean modifyOutgoingDetailVO(OutgoingDetailVO outgoingDetailVO) {
		return outgoingDetailManage.modifyOutgoingDetailVO(outgoingDetailVO);
	}

	public Boolean deleteOutgoingDetailVOByID(Long id) {
		return outgoingDetailManage.deleteOutgoingDetailVOByID(id);
	}

	public List<OutgoingDetailVO> getOutgoingDetailVOByOutgoingID(Long id) {
		return outgoingDetailManage.getOutgoingDetailVOByOutgoingID(id);
	}

	public Boolean batchAddOutgoingDetailVO(
			List<OutgoingDetailVO> outgoingDetailVOs) {
		return outgoingDetailManage.batchAddOutgoingDetailVO(outgoingDetailVOs);
	}

	public Boolean batchModifyOutgoingDetailVO(
			List<OutgoingDetailVO> outgoingDetailVOs) {
		return outgoingDetailManage
				.batchModifyOutgoingDetailVO(outgoingDetailVOs);
	}

	public Boolean batchDeleteOutgoingDetailVO(
			List<OutgoingDetailVO> outgoingDetailVOs) {
		return outgoingDetailManage
				.batchDeleteOutgoingDetailVO(outgoingDetailVOs);
	}

	public Boolean batchDeleteOutgoingDetailVOByID(List<Long> storyDetailIDs) {
		return outgoingDetailManage
				.batchDeleteOutgoingDetailVOByID(storyDetailIDs);
	}
}
