package com.xianzaishi.wms.oplog.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.oplog.vo.OutgoingSaleLogVO;
import com.xianzaishi.wms.oplog.vo.OutgoingSaleLogDO;
import com.xianzaishi.wms.oplog.vo.OutgoingSaleLogQueryVO;
import com.xianzaishi.wms.oplog.vo.OutgoingSaleLogVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.oplog.manage.itf.IOutgoingSaleLogManage;
import com.xianzaishi.wms.oplog.dao.itf.IOutgoingSaleLogDao;

public class OutgoingSaleLogManageImpl implements IOutgoingSaleLogManage {
	@Autowired
	private IOutgoingSaleLogDao outgoingSaleLogDao = null;

	private void validate(OutgoingSaleLogDO outgoingSaleLogDO) {
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IOutgoingSaleLogDao getOutgoingSaleLogDao() {
		return outgoingSaleLogDao;
	}

	public void setOutgoingSaleLogDao(IOutgoingSaleLogDao outgoingSaleLogDao) {
		this.outgoingSaleLogDao = outgoingSaleLogDao;
	}

	public Boolean addOutgoingSaleLogVO(OutgoingSaleLogVO outgoingSaleLogVO) {
		outgoingSaleLogDao.addDO(outgoingSaleLogVO);
		return true;
	}

	public List<OutgoingSaleLogVO> queryOutgoingSaleLogVOList(
			OutgoingSaleLogQueryVO outgoingSaleLogQueryVO) {
		return outgoingSaleLogDao.queryDO(outgoingSaleLogQueryVO);
	}

	public OutgoingSaleLogVO getOutgoingSaleLogVOByID(Long id) {
		return (OutgoingSaleLogVO) outgoingSaleLogDao.getDOByID(id);
	}

	public Boolean modifyOutgoingSaleLogVO(OutgoingSaleLogVO outgoingSaleLogVO) {
		return outgoingSaleLogDao.updateDO(outgoingSaleLogVO);
	}

	public Boolean deleteOutgoingSaleLogVOByID(Long id) {
		return outgoingSaleLogDao.delDO(id);
	}

	public Boolean batchAddOutgoingSaleLog(
			List<OutgoingSaleLogVO> outgoingSaleLogVOs) {
		return outgoingSaleLogDao
				.batchAddOutgoingSaleLog(outgoingSaleLogVOs);
	}
}
