package com.xianzaishi.wms.track.domain.client.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.domain.client.itf.IBookingDeliveryDomainClient;
import com.xianzaishi.wms.track.domain.service.itf.IBookingDeliveryDomainService;
import com.xianzaishi.wms.track.vo.BookingDeliveryDetailQueryVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryDetailVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryQueryVO;
import com.xianzaishi.wms.track.vo.BookingDeliveryVO;

public class BookingDeliveryDomainClient implements
		IBookingDeliveryDomainClient {
	private static final Logger logger = Logger
			.getLogger(BookingDeliveryDomainClient.class);
	@Autowired
	private IBookingDeliveryDomainService bookingDeliveryDomainService = null;

	public SimpleResultVO<Boolean> createBookingDeliveryDomain(
			BookingDeliveryVO bookingDeliveryVO) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO
					.buildSuccessResult(bookingDeliveryDomainService
							.createBookingDeliveryDomain(bookingDeliveryVO));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<BookingDeliveryVO> getBookingDeliveryDomainByID(
			Long bookingDeliveryID) {
		SimpleResultVO<BookingDeliveryVO> flag = null;
		try {
			flag = SimpleResultVO
					.buildSuccessResult(bookingDeliveryDomainService
							.getBookingDeliveryDomainByID(bookingDeliveryID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> deleteBookingDeliveryDomain(
			Long bookingDeliveryID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO
					.buildSuccessResult(bookingDeliveryDomainService
							.deleteBookingDeliveryDomain(bookingDeliveryID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> addBookingDeliveryVO(
			BookingDeliveryVO bookingDeliveryVO) {
		return SimpleResultVO.buildSuccessResult(bookingDeliveryDomainService
				.addBookingDeliveryVO(bookingDeliveryVO));
	}

	public SimpleResultVO<QueryResultVO<BookingDeliveryVO>> queryBookingDeliveryVOList(
			BookingDeliveryQueryVO bookingDeliveryQueryVO) {
		return SimpleResultVO.buildSuccessResult(bookingDeliveryDomainService
				.queryBookingDeliveryVOList(bookingDeliveryQueryVO));
	}

	public SimpleResultVO<BookingDeliveryVO> getBookingDeliveryVOByID(Long id) {
		return SimpleResultVO.buildSuccessResult(bookingDeliveryDomainService
				.getBookingDeliveryVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyBookingDeliveryVO(
			BookingDeliveryVO bookingDeliveryVO) {
		return SimpleResultVO.buildSuccessResult(bookingDeliveryDomainService
				.modifyBookingDeliveryVO(bookingDeliveryVO));
	}

	public SimpleResultVO<Boolean> deleteBookingDeliveryVO(Long id) {
		return SimpleResultVO.buildSuccessResult(bookingDeliveryDomainService
				.deleteBookingDeliveryVO(id));
	}

	public SimpleResultVO<Boolean> createBookingDeliveryDetailVO(
			BookingDeliveryDetailVO bookingDeliveryDetailVO) {
		return SimpleResultVO.buildSuccessResult(bookingDeliveryDomainService
				.createBookingDeliveryDetailVO(bookingDeliveryDetailVO));
	}

	public SimpleResultVO<Boolean> batchCreateBookingDeliveryDetailVO(
			List<BookingDeliveryDetailVO> bookingDeliveryDetailVOs) {
		return SimpleResultVO.buildSuccessResult(bookingDeliveryDomainService
				.batchCreateBookingDeliveryDetailVO(bookingDeliveryDetailVOs));
	}

	public SimpleResultVO<List<BookingDeliveryDetailVO>> queryBookingDeliveryDetailVOList(
			BookingDeliveryDetailQueryVO bookingDeliveryDetailQueryVO) {
		return SimpleResultVO
				.buildSuccessResult(bookingDeliveryDomainService
						.queryBookingDeliveryDetailVOList(bookingDeliveryDetailQueryVO));
	}

	public SimpleResultVO<List<BookingDeliveryDetailVO>> getBookingDeliveryDetailVOListByBookingDeliveryID(
			Long bookingDeliveryID) {
		return SimpleResultVO
				.buildSuccessResult(bookingDeliveryDomainService
						.getBookingDeliveryDetailVOListByBookingDeliveryID(bookingDeliveryID));
	}

	public SimpleResultVO<BookingDeliveryDetailQueryVO> getBookingDeliveryDetailVOByID(
			Long id) {
		return SimpleResultVO.buildSuccessResult(bookingDeliveryDomainService
				.getBookingDeliveryDetailVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyBookingDeliveryDetailVO(
			BookingDeliveryDetailVO bookingDeliveryDetailVO) {
		return SimpleResultVO.buildSuccessResult(bookingDeliveryDomainService
				.modifyBookingDeliveryDetailVO(bookingDeliveryDetailVO));
	}

	public SimpleResultVO<Boolean> batchModifyBookingDeliveryDetailVOs(
			List<BookingDeliveryDetailVO> bookingDeliveryDetailVOs) {
		return SimpleResultVO.buildSuccessResult(bookingDeliveryDomainService
				.batchModifyBookingDeliveryDetailVOs(bookingDeliveryDetailVOs));
	}

	public SimpleResultVO<Boolean> deleteBookingDeliveryDetailVO(Long id) {
		return SimpleResultVO.buildSuccessResult(bookingDeliveryDomainService
				.deleteBookingDeliveryDetailVO(id));
	}

	public SimpleResultVO<Boolean> deleteBookingDeliveryDetailVOByBookingDeliveryID(
			Long bookingDeliveryID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO
					.buildSuccessResult(bookingDeliveryDomainService
							.deleteBookingDeliveryDetailVOByBookingDeliveryID(bookingDeliveryID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> batchDeleteBookingDeliveryDetailVOByBookingDeliveryID(
			List<Long> storyDetailIDs) {
		return SimpleResultVO
				.buildSuccessResult(bookingDeliveryDomainService
						.batchDeleteBookingDeliveryDetailVOByBookingDeliveryID(storyDetailIDs));
	}

	public IBookingDeliveryDomainService getBookingDeliveryDomainService() {
		return bookingDeliveryDomainService;
	}

	public void setBookingDeliveryDomainService(
			IBookingDeliveryDomainService bookingDeliveryDomainService) {
		this.bookingDeliveryDomainService = bookingDeliveryDomainService;
	}

	public SimpleResultVO<Boolean> submit(Long id) {
		return SimpleResultVO.buildSuccessResult(bookingDeliveryDomainService
				.submit(id));
	}

	public SimpleResultVO<Boolean> confirm(Long id, Long auditor) {
		return SimpleResultVO.buildSuccessResult(bookingDeliveryDomainService
				.confirm(id, auditor));
	}

}
