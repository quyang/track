package com.xianzaishi.wms.job.domain.client.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.job.domain.client.itf.IJobDomainClient;
import com.xianzaishi.wms.job.domain.service.itf.IJobDomainService;
import com.xianzaishi.wms.job.vo.JobQueryVO;
import com.xianzaishi.wms.job.vo.JobVO;

public class JobDomainClient implements IJobDomainClient {

	@Autowired
	private IJobDomainService jobDomainService = null;

	public IJobDomainService getJobDomainService() {
		return jobDomainService;
	}

	public void setJobDomainService(IJobDomainService jobDomainService) {
		this.jobDomainService = jobDomainService;
	}

	public SimpleResultVO<Boolean> addJob(JobVO jobVO) {
		return SimpleResultVO
				.buildSuccessResult(jobDomainService.addJob(jobVO));
	}

	public SimpleResultVO<JobVO> getJob(Long jobID) {
		return SimpleResultVO
				.buildSuccessResult(jobDomainService.getJob(jobID));
	}

	public SimpleResultVO<QueryResultVO<JobVO>> getJobPending(Long jobID) {
		return SimpleResultVO.buildSuccessResult(jobDomainService
				.getJobPending(jobID));
	}

	public SimpleResultVO<Boolean> processed(Long jobID) {
		return SimpleResultVO.buildSuccessResult(jobDomainService
				.processed(jobID));
	}

	public SimpleResultVO<List<JobVO>> queryJobVOList(JobQueryVO jobQueryVO) {
		return SimpleResultVO.buildSuccessResult(jobDomainService
				.queryJobVOList(jobQueryVO));
	}
}
