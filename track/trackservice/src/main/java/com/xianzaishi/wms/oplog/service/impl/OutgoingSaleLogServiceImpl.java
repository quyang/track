package com.xianzaishi.wms.oplog.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.oplog.manage.itf.IOutgoingSaleLogManage;
import com.xianzaishi.wms.oplog.service.itf.IOutgoingSaleLogService;
import com.xianzaishi.wms.oplog.vo.OutgoingSaleLogQueryVO;
import com.xianzaishi.wms.oplog.vo.OutgoingSaleLogVO;

public class OutgoingSaleLogServiceImpl implements IOutgoingSaleLogService {
	@Autowired
	private IOutgoingSaleLogManage outgoingSaleLogManage = null;

	public IOutgoingSaleLogManage getOutgoingSaleLogManage() {
		return outgoingSaleLogManage;
	}

	public void setOutgoingSaleLogManage(
			IOutgoingSaleLogManage outgoingSaleLogManage) {
		this.outgoingSaleLogManage = outgoingSaleLogManage;
	}

	public Boolean addOutgoingSaleLogVO(OutgoingSaleLogVO outgoingSaleLogVO) {
		outgoingSaleLogManage.addOutgoingSaleLogVO(outgoingSaleLogVO);
		return true;
	}

	public List<OutgoingSaleLogVO> queryOutgoingSaleLogVOList(
			OutgoingSaleLogQueryVO outgoingSaleLogQueryVO) {
		return outgoingSaleLogManage
				.queryOutgoingSaleLogVOList(outgoingSaleLogQueryVO);
	}

	public OutgoingSaleLogVO getOutgoingSaleLogVOByID(Long id) {
		return (OutgoingSaleLogVO) outgoingSaleLogManage
				.getOutgoingSaleLogVOByID(id);
	}

	public Boolean modifyOutgoingSaleLogVO(OutgoingSaleLogVO outgoingSaleLogVO) {
		return outgoingSaleLogManage.modifyOutgoingSaleLogVO(outgoingSaleLogVO);
	}

	public Boolean deleteOutgoingSaleLogVOByID(Long id) {
		return outgoingSaleLogManage.deleteOutgoingSaleLogVOByID(id);
	}

	public Boolean batchAddOutgoingSaleLog(
			List<OutgoingSaleLogVO> outgoingSaleLogVOs) {
		return outgoingSaleLogManage
				.batchAddOutgoingSaleLog(outgoingSaleLogVOs);
	}
}
