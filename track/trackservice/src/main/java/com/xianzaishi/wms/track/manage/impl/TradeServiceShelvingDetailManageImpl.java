package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.ITradeServiceShelvingDetailDao;
import com.xianzaishi.wms.track.manage.itf.ITradeServiceShelvingDetailManage;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingDetailQueryVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingDetailVO;

public class TradeServiceShelvingDetailManageImpl implements
		ITradeServiceShelvingDetailManage {
	@Autowired
	private ITradeServiceShelvingDetailDao tradeServiceShelvingDetailDao = null;

	private void validate(
			TradeServiceShelvingDetailVO tradeServiceShelvingDetailVO) {
		if (tradeServiceShelvingDetailVO.getShelveId() == null
				|| tradeServiceShelvingDetailVO.getShelveId() <= 0) {
			throw new BizException("shelveId error："
					+ tradeServiceShelvingDetailVO.getShelveId());
		}
		if (tradeServiceShelvingDetailVO.getSkuId() == null
				|| tradeServiceShelvingDetailVO.getSkuId() <= 0) {
			throw new BizException("skuID error："
					+ tradeServiceShelvingDetailVO.getSkuId());
		}
		if (tradeServiceShelvingDetailVO.getPositionId() == null
				|| tradeServiceShelvingDetailVO.getPositionId() <= 0) {
			throw new BizException("positionID error："
					+ tradeServiceShelvingDetailVO.getPositionId());
		}
		if (tradeServiceShelvingDetailVO.getShelveNoReal() == null
				|| tradeServiceShelvingDetailVO.getShelveNoReal() <= 0) {
			throw new BizException("shelveNo error："
					+ tradeServiceShelvingDetailVO.getShelveNoReal());
		}
		if (tradeServiceShelvingDetailVO.getSkuName() == null
				|| tradeServiceShelvingDetailVO.getSkuName().isEmpty()) {
			throw new BizException("skuName error："
					+ tradeServiceShelvingDetailVO.getSkuName());
		}
		if (tradeServiceShelvingDetailVO.getPositionCode() == null
				|| tradeServiceShelvingDetailVO.getPositionCode().isEmpty()) {
			throw new BizException("positionCode error："
					+ tradeServiceShelvingDetailVO.getPositionCode());
		}
		if (tradeServiceShelvingDetailVO.getSaleUnit() == null
				|| tradeServiceShelvingDetailVO.getSaleUnit().isEmpty()) {
			throw new BizException("saleUnit error："
					+ tradeServiceShelvingDetailVO.getSaleUnit());
		}
		if (tradeServiceShelvingDetailVO.getSaleUnitId() == null
				|| tradeServiceShelvingDetailVO.getSaleUnitId() <= 0) {
			throw new BizException("saleUnitId error："
					+ tradeServiceShelvingDetailVO.getSaleUnitId());
		}
		if (tradeServiceShelvingDetailVO.getSaleUnitType() == null) {
			throw new BizException("saleUnitType error："
					+ tradeServiceShelvingDetailVO.getSaleUnitType());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public ITradeServiceShelvingDetailDao getTradeServiceShelvingDetailDao() {
		return tradeServiceShelvingDetailDao;
	}

	public void setTradeServiceShelvingDetailDao(
			ITradeServiceShelvingDetailDao tradeServiceShelvingDetailDao) {
		this.tradeServiceShelvingDetailDao = tradeServiceShelvingDetailDao;
	}

	public Boolean addTradeServiceShelvingDetailVO(
			TradeServiceShelvingDetailVO tradeServiceShelvingDetailVO) {
		validate(tradeServiceShelvingDetailVO);
		tradeServiceShelvingDetailDao.addDO(tradeServiceShelvingDetailVO);
		return true;
	}

	public List<TradeServiceShelvingDetailVO> queryTradeServiceShelvingDetailVOList(
			TradeServiceShelvingDetailQueryVO tradeServiceShelvingDetailQueryVO) {
		return tradeServiceShelvingDetailDao
				.queryDO(tradeServiceShelvingDetailQueryVO);
	}

	public TradeServiceShelvingDetailVO getTradeServiceShelvingDetailVOByID(
			Long id) {
		return (TradeServiceShelvingDetailVO) tradeServiceShelvingDetailDao
				.getDOByID(id);
	}

	public Boolean modifyTradeServiceShelvingDetailVO(
			TradeServiceShelvingDetailVO tradeServiceShelvingDetailVO) {
		return tradeServiceShelvingDetailDao
				.updateDO(tradeServiceShelvingDetailVO);
	}

	public Boolean deleteTradeServiceShelvingDetailVOByID(Long id) {
		return tradeServiceShelvingDetailDao.delDO(id);
	}

	public List<TradeServiceShelvingDetailVO> getTradeServiceShelvingDetailVOByTradeServiceShelvingID(
			Long id) {
		TradeServiceShelvingDetailQueryVO queryVO = new TradeServiceShelvingDetailQueryVO();
		queryVO.setShelveId(id);
		queryVO.setSize(Integer.MAX_VALUE);
		return tradeServiceShelvingDetailDao.queryDO(queryVO);
	}

	public Boolean batchAddTradeServiceShelvingDetailVO(
			List<TradeServiceShelvingDetailVO> tradeServiceShelvingDetailVOs) {
		return tradeServiceShelvingDetailDao
				.batchAddDO(tradeServiceShelvingDetailVOs);
	}

	public Boolean batchModifyTradeServiceShelvingDetailVO(
			List<TradeServiceShelvingDetailVO> tradeServiceShelvingDetailVOs) {
		return tradeServiceShelvingDetailDao
				.batchModifyDO(tradeServiceShelvingDetailVOs);
	}

	public Boolean batchDeleteTradeServiceShelvingDetailVO(
			List<TradeServiceShelvingDetailVO> tradeServiceShelvingDetailVOs) {
		return tradeServiceShelvingDetailDao
				.batchDeleteDO(tradeServiceShelvingDetailVOs);
	}

	public Boolean batchDeleteTradeServiceShelvingDetailVOByID(
			List<Long> tradeServiceShelvingDetailIDs) {
		return tradeServiceShelvingDetailDao
				.batchDeleteDOByID(tradeServiceShelvingDetailIDs);
	}
}
