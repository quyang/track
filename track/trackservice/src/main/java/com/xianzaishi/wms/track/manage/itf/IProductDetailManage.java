package com.xianzaishi.wms.track.manage.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.ProductDetailQueryVO;
import com.xianzaishi.wms.track.vo.ProductDetailVO;

public interface IProductDetailManage {

	public Boolean addProductDetailVO(ProductDetailVO productDetailVO);

	public List<ProductDetailVO> queryProductDetailVOList(
			ProductDetailQueryVO productDetailQueryVO);

	public ProductDetailVO getProductDetailVOByID(Long id);

	public Boolean modifyProductDetailVO(ProductDetailVO productDetailVO);

	public Boolean deleteProductDetailVOByID(Long id);

	public List<ProductDetailVO> getProductDetailVOByProductID(Long id);

	public Boolean batchAddProductDetailVO(
			List<ProductDetailVO> productDetailVOs);

	public Boolean batchModifyProductDetailVO(
			List<ProductDetailVO> productDetailVOs);

	public Boolean batchDeleteProductDetailVO(
			List<ProductDetailVO> productDetailVOs);

	public Boolean batchDeleteProductDetailVOByID(List<Long> productDetailIDs);

}