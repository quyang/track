package com.xianzaishi.wms.job.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.job.vo.JobTypeVO;
import com.xianzaishi.wms.job.vo.JobTypeQueryVO;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.job.service.itf.IJobTypeService;
import com.xianzaishi.wms.job.manage.itf.IJobTypeManage;

public class JobTypeServiceImpl implements IJobTypeService {
	@Autowired
	private IJobTypeManage jobTypeManage = null;

	public IJobTypeManage getJobTypeManage() {
		return jobTypeManage;
	}

	public void setJobTypeManage(IJobTypeManage jobTypeManage) {
		this.jobTypeManage = jobTypeManage;
	}
	
	public Boolean addJobTypeVO(JobTypeVO jobTypeVO) {
		jobTypeManage.addJobTypeVO(jobTypeVO);
		return true;
	}

	public List<JobTypeVO> queryJobTypeVOList(JobTypeQueryVO jobTypeQueryVO) {
		return jobTypeManage.queryJobTypeVOList(jobTypeQueryVO);
	}

	public JobTypeVO getJobTypeVOByID(Long id) {
		return (JobTypeVO) jobTypeManage.getJobTypeVOByID(id);
	}

	public Boolean modifyJobTypeVO(JobTypeVO jobTypeVO) {
		return jobTypeManage.modifyJobTypeVO(jobTypeVO);
	}

	public Boolean deleteJobTypeVOByID(Long id) {
		return jobTypeManage.deleteJobTypeVOByID(id);
	}
}
