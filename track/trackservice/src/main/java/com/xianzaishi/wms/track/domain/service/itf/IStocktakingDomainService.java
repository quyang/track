package com.xianzaishi.wms.track.domain.service.itf;

import java.util.List;

import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.track.vo.StocktakingDetailQueryVO;
import com.xianzaishi.wms.track.vo.StocktakingDetailVO;
import com.xianzaishi.wms.track.vo.StocktakingQueryVO;
import com.xianzaishi.wms.track.vo.StocktakingVO;

public interface IStocktakingDomainService {

	// domain
	public Boolean createStocktakingDomain(StocktakingVO stocktakingVO);

	public StocktakingVO getStocktakingDomainByID(Long stocktakingID);

	public Boolean deleteStocktakingDomain(Long id);

	// head
	public Boolean addStocktakingVO(StocktakingVO stocktakingVO);

	public QueryResultVO<StocktakingVO> queryStocktakingVOList(
			StocktakingQueryVO stocktakingQueryVO);

	public StocktakingVO getStocktakingVOByID(Long id);

	public Boolean modifyStocktakingVO(StocktakingVO stocktakingVO);

	public Boolean deleteStocktakingVO(Long stocktakingID);

	public Boolean submitStocktakingToCheck(StocktakingVO stocktakingVO);

	public Boolean submitStocktakingToAudit(StocktakingVO stocktakingVO);

	// body
	public Boolean createStocktakingDetailVO(
			StocktakingDetailVO stocktakingDetailVO);

	public Boolean batchCreateStocktakingDetailVO(
			List<StocktakingDetailVO> stocktakingDetailVOs);

	public List<StocktakingDetailVO> queryStocktakingDetailVOList(
			StocktakingDetailQueryVO stocktakingDetailQueryVO);

	public List<StocktakingDetailVO> getStocktakingDetailVOListByStocktakingID(
			Long stocktakingID);

	public StocktakingDetailVO getStocktakingDetailVOByID(Long id);

	public Boolean modifyStocktakingDetailVO(
			StocktakingDetailVO stocktakingDetailVO);

	/**
	 * 按ID更新Detail
	 * 
	 * @param stocktakingDetailVOs
	 * @return
	 */
	public Boolean batchModifyStocktakingDetailVOs(
			List<StocktakingDetailVO> stocktakingDetailVOs);

	public Boolean deleteStocktakingDetailVO(Long id);

	public Boolean deleteStocktakingDetailVOByStocktakingID(Long stocktakingID);

	public Boolean batchDeleteStocktakingDetailVOByStocktakingID(
			List<Long> storyDetailIDs);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

	public Boolean accounted(Long id);

}
