package com.xianzaishi.wms.oplog.manage.itf;

import java.util.List;

import com.xianzaishi.wms.oplog.vo.TradeServiceShelvingLogQueryVO;
import com.xianzaishi.wms.oplog.vo.TradeServiceShelvingLogVO;

public interface ITradeServiceShelvingLogManage {

	public Boolean addTradeServiceShelvingLogVO(
			TradeServiceShelvingLogVO tradeServiceShelvingLogVO);

	public List<TradeServiceShelvingLogVO> queryTradeServiceShelvingLogVOList(
			TradeServiceShelvingLogQueryVO tradeServiceShelvingLogQueryVO);

	public TradeServiceShelvingLogVO getTradeServiceShelvingLogVOByID(Long id);

	public Boolean modifyTradeServiceShelvingLogVO(
			TradeServiceShelvingLogVO tradeServiceShelvingLogVO);

	public Boolean deleteTradeServiceShelvingLogVOByID(Long id);

	public Boolean batchAddTradeServiceShelvingLog(
			List<TradeServiceShelvingLogVO> tradeServiceShelvingLogVOs);
}