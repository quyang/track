package com.xianzaishi.wms.barcode.dao.impl;

import com.xianzaishi.wms.barcode.dao.itf.IBarcodeSequenceDao;
import com.xianzaishi.wms.barcode.vo.BarcodeSequenceVO;
import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.common.exception.BizException;

public class BarcodeSequenceDaoImpl extends BaseDaoAdapter implements
		IBarcodeSequenceDao {
	public String getVOClassName() {
		return "BarcodeSequenceDO";
	}

	public Boolean order(BarcodeSequenceVO barcodeSequenceVO) {
		Boolean flag = false;
		try {
			if (barcodeSequenceVO.getId() != null
					&& barcodeSequenceVO.getId() > 0) {
				int no = simpleSqlMapClientTemplate.update(getVOClassName()
						+ ".order", barcodeSequenceVO);
				if (no == 1) {
					flag = true;
				} else if (no > 1) {
					throw new BizException("影响的数据大于1条。");
				}
			} else {
				throw new BizException("更新数据失败，ID错误：ID = "
						+ barcodeSequenceVO.getId());
			}
		} catch (Exception e) {
			throw new BizException("更新数据失败。", e);
		}
		return flag;
	}
}
