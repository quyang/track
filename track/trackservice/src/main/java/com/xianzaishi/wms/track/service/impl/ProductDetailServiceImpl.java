package com.xianzaishi.wms.track.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.track.manage.itf.IProductDetailManage;
import com.xianzaishi.wms.track.service.itf.IProductDetailService;
import com.xianzaishi.wms.track.vo.ProductDetailQueryVO;
import com.xianzaishi.wms.track.vo.ProductDetailVO;

public class ProductDetailServiceImpl implements IProductDetailService {
	@Autowired
	private IProductDetailManage productDetailManage = null;

	public IProductDetailManage getProductDetailManage() {
		return productDetailManage;
	}

	public void setProductDetailManage(IProductDetailManage productDetailManage) {
		this.productDetailManage = productDetailManage;
	}

	public Boolean addProductDetailVO(ProductDetailVO productDetailVO) {
		productDetailManage.addProductDetailVO(productDetailVO);
		return true;
	}

	public List<ProductDetailVO> queryProductDetailVOList(
			ProductDetailQueryVO productDetailQueryVO) {
		return productDetailManage
				.queryProductDetailVOList(productDetailQueryVO);
	}

	public ProductDetailVO getProductDetailVOByID(Long id) {
		return (ProductDetailVO) productDetailManage.getProductDetailVOByID(id);
	}

	public Boolean modifyProductDetailVO(ProductDetailVO productDetailVO) {
		return productDetailManage.modifyProductDetailVO(productDetailVO);
	}

	public Boolean deleteProductDetailVOByID(Long id) {
		return productDetailManage.deleteProductDetailVOByID(id);
	}

	public List<ProductDetailVO> getProductDetailVOByProductID(Long id) {
		return productDetailManage.getProductDetailVOByProductID(id);
	}

	public Boolean batchAddProductDetailVO(
			List<ProductDetailVO> productDetailVOs) {
		return productDetailManage.batchAddProductDetailVO(productDetailVOs);
	}

	public Boolean batchModifyProductDetailVO(
			List<ProductDetailVO> productDetailVOs) {
		return productDetailManage.batchModifyProductDetailVO(productDetailVOs);
	}

	public Boolean batchDeleteProductDetailVO(
			List<ProductDetailVO> productDetailVOs) {
		return productDetailManage.batchDeleteProductDetailVO(productDetailVOs);
	}

	public Boolean batchDeleteProductDetailVOByID(List<Long> storyDetailIDs) {
		return productDetailManage
				.batchDeleteProductDetailVOByID(storyDetailIDs);
	}
}
