package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.IStocktakingDao;
import com.xianzaishi.wms.track.manage.itf.IStocktakingManage;
import com.xianzaishi.wms.track.vo.StocktakingQueryVO;
import com.xianzaishi.wms.track.vo.StocktakingVO;

public class StocktakingManageImpl implements IStocktakingManage {
	@Autowired
	private IStocktakingDao stocktakingDao = null;

	private void validate(StocktakingVO stocktakingVO) {
		if (stocktakingVO.getAgencyId() == null
				|| stocktakingVO.getAgencyId() <= 0) {
			throw new BizException("agencyID error："
					+ stocktakingVO.getAgencyId());
		}
		if (stocktakingVO.getOperate() == null
				|| stocktakingVO.getOperate() <= 0) {
			throw new BizException("operator error："
					+ stocktakingVO.getOperate());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IStocktakingDao getStocktakingDao() {
		return stocktakingDao;
	}

	public void setStocktakingDao(IStocktakingDao stocktakingDao) {
		this.stocktakingDao = stocktakingDao;
	}

	public Long addStocktakingVO(StocktakingVO stocktakingVO) {
		validate(stocktakingVO);
		return (Long) stocktakingDao.addDO(stocktakingVO);
	}

	public List<StocktakingVO> queryStocktakingVOList(
			StocktakingQueryVO stocktakingQueryVO) {
		return stocktakingDao.queryDO(stocktakingQueryVO);
	}

	public StocktakingVO getStocktakingVOByID(Long id) {
		return (StocktakingVO) stocktakingDao.getDOByID(id);
	}

	public Boolean modifyStocktakingVO(StocktakingVO stocktakingVO) {
		return stocktakingDao.updateDO(stocktakingVO);
	}

	public Boolean deleteStocktakingVOByID(Long id) {
		return stocktakingDao.delDO(id);
	}

	public Boolean submitStocktakingToCheck(StocktakingVO stocktakingVO) {
		stocktakingVO.setStatuOld(0);
		stocktakingVO.setStatu(1);
		return stocktakingDao.updateStocktakingStatu(stocktakingVO);
	}

	public Boolean submitStocktakingToAudit(StocktakingVO stocktakingVO) {
		stocktakingVO.setStatuOld(1);
		stocktakingVO.setStatu(2);
		return stocktakingDao.updateStocktakingStatu(stocktakingVO);
	}

	public Boolean submit(Long id) {
		return stocktakingDao.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return stocktakingDao.audit(id, auditor);
	}

	public Boolean accounted(Long id) {
		return stocktakingDao.accounted(id);
	}

	public Integer queryStocktakingVOCount(StocktakingQueryVO stocktakingQueryVO) {
		return stocktakingDao.queryCount(stocktakingQueryVO);
	}
}
