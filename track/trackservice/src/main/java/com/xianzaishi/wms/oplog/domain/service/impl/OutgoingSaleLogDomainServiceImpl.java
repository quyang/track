package com.xianzaishi.wms.oplog.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.oplog.domain.service.itf.IOutgoingSaleLogDomainService;
import com.xianzaishi.wms.oplog.service.itf.IOutgoingSaleLogService;
import com.xianzaishi.wms.oplog.vo.OutgoingSaleLogVO;

public class OutgoingSaleLogDomainServiceImpl implements
		IOutgoingSaleLogDomainService {

	@Autowired
	private IOutgoingSaleLogService outgoingSaleLogService = null;

	public Boolean batchAddOutgoingSaleLog(
			List<OutgoingSaleLogVO> outgoingSaleLogVOs) {
		return outgoingSaleLogService
				.batchAddOutgoingSaleLog(outgoingSaleLogVOs);
	}

	public IOutgoingSaleLogService getOutgoingSaleLogService() {
		return outgoingSaleLogService;
	}

	public void setOutgoingSaleLogService(
			IOutgoingSaleLogService outgoingSaleLogService) {
		this.outgoingSaleLogService = outgoingSaleLogService;
	}

}
