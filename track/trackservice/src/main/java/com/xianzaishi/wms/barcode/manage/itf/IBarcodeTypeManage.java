package com.xianzaishi.wms.barcode.manage.itf;

import java.util.List;

import com.xianzaishi.wms.barcode.vo.BarcodeTypeVO;
import com.xianzaishi.wms.barcode.vo.BarcodeTypeDO;
import com.xianzaishi.wms.barcode.vo.BarcodeTypeQueryVO;

public interface IBarcodeTypeManage {

	public Boolean addBarcodeTypeVO(BarcodeTypeVO barcodeTypeVO);

	public List<BarcodeTypeVO> queryBarcodeTypeVOList(BarcodeTypeQueryVO barcodeTypeQueryVO);

	public BarcodeTypeVO getBarcodeTypeVOByID(Long id);

	public Boolean modifyBarcodeTypeVO(BarcodeTypeVO barcodeTypeVO);

	public Boolean deleteBarcodeTypeVOByID(Long id);

}