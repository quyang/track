package com.xianzaishi.wms.track.service.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.OutgoingSaleDetailQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingSaleDetailVO;

public interface IOutgoingSaleDetailService {

	public Boolean addOutgoingSaleDetailVO(
			OutgoingSaleDetailVO outgoingSaleDetailVO);

	public List<OutgoingSaleDetailVO> queryOutgoingSaleDetailVOList(
			OutgoingSaleDetailQueryVO outgoingSaleDetailQueryVO);

	public OutgoingSaleDetailVO getOutgoingSaleDetailVOByID(Long id);

	public Boolean modifyOutgoingSaleDetailVO(
			OutgoingSaleDetailVO outgoingSaleDetailVO);

	public Boolean deleteOutgoingSaleDetailVOByID(Long id);

	public List<OutgoingSaleDetailVO> getOutgoingSaleDetailVOByOutgoingSaleID(
			Long id);

	public Boolean batchAddOutgoingSaleDetailVO(
			List<OutgoingSaleDetailVO> outgoingSaleDetailVOs);

	public Boolean batchModifyOutgoingSaleDetailVO(
			List<OutgoingSaleDetailVO> outgoingSaleDetailVOs);

	public Boolean batchDeleteOutgoingSaleDetailVO(
			List<OutgoingSaleDetailVO> outgoingSaleDetailVOs);

	public Boolean batchDeleteOutgoingSaleDetailVOByID(
			List<Long> outgoingSaleDetailIDs);
}