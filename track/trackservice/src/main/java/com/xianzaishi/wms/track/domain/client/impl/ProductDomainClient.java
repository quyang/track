package com.xianzaishi.wms.track.domain.client.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.QueryResultVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.track.domain.client.itf.IProductDomainClient;
import com.xianzaishi.wms.track.domain.service.itf.IProductDomainService;
import com.xianzaishi.wms.track.vo.ProductDetailQueryVO;
import com.xianzaishi.wms.track.vo.ProductDetailVO;
import com.xianzaishi.wms.track.vo.ProductQueryVO;
import com.xianzaishi.wms.track.vo.ProductVO;

public class ProductDomainClient implements IProductDomainClient {
	private static final Logger logger = Logger
			.getLogger(ProductDomainClient.class);
	@Autowired
	private IProductDomainService productDomainService = null;

	public SimpleResultVO<Boolean> createProductDomain(ProductVO productVO) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(productDomainService
					.createProductDomain(productVO));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<ProductVO> getProductDomainByID(Long productID) {
		SimpleResultVO<ProductVO> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(productDomainService
					.getProductDomainByID(productID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> deleteProductDomain(Long productID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(productDomainService
					.deleteProductDomain(productID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> addProductVO(ProductVO productVO) {
		return SimpleResultVO.buildSuccessResult(productDomainService
				.addProductVO(productVO));
	}

	public SimpleResultVO<QueryResultVO<ProductVO>> queryProductVOList(
			ProductQueryVO productQueryVO) {
		return SimpleResultVO.buildSuccessResult(productDomainService
				.queryProductVOList(productQueryVO));
	}

	public SimpleResultVO<ProductVO> getProductVOByID(Long id) {
		return SimpleResultVO.buildSuccessResult(productDomainService
				.getProductVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyProductVO(ProductVO productVO) {
		return SimpleResultVO.buildSuccessResult(productDomainService
				.modifyProductVO(productVO));
	}

	public SimpleResultVO<Boolean> deleteProductVO(Long id) {
		return SimpleResultVO.buildSuccessResult(productDomainService
				.deleteProductVO(id));
	}

	public SimpleResultVO<Boolean> createProductDetailVO(
			ProductDetailVO productDetailVO) {
		return SimpleResultVO.buildSuccessResult(productDomainService
				.createProductDetailVO(productDetailVO));
	}

	public SimpleResultVO<Boolean> batchCreateProductDetailVO(
			List<ProductDetailVO> productDetailVOs) {
		return SimpleResultVO.buildSuccessResult(productDomainService
				.batchCreateProductDetailVO(productDetailVOs));
	}

	public SimpleResultVO<List<ProductDetailVO>> queryProductDetailVOList(
			ProductDetailQueryVO productDetailQueryVO) {
		return SimpleResultVO.buildSuccessResult(productDomainService
				.queryProductDetailVOList(productDetailQueryVO));
	}

	public SimpleResultVO<List<ProductDetailVO>> getProductDetailVOListByProductID(
			Long productID) {
		return SimpleResultVO.buildSuccessResult(productDomainService
				.getProductDetailVOListByProductID(productID));
	}

	public SimpleResultVO<ProductDetailQueryVO> getProductDetailVOByID(Long id) {
		return SimpleResultVO.buildSuccessResult(productDomainService
				.getProductDetailVOByID(id));
	}

	public SimpleResultVO<Boolean> modifyProductDetailVO(
			ProductDetailVO productDetailVO) {
		return SimpleResultVO.buildSuccessResult(productDomainService
				.modifyProductDetailVO(productDetailVO));
	}

	public SimpleResultVO<Boolean> batchModifyProductDetailVOs(
			List<ProductDetailVO> productDetailVOs) {
		return SimpleResultVO.buildSuccessResult(productDomainService
				.batchModifyProductDetailVOs(productDetailVOs));
	}

	public SimpleResultVO<Boolean> deleteProductDetailVO(Long id) {
		return SimpleResultVO.buildSuccessResult(productDomainService
				.deleteProductDetailVO(id));
	}

	public SimpleResultVO<Boolean> deleteProductDetailVOByProductID(
			Long productID) {
		SimpleResultVO<Boolean> flag = null;
		try {
			flag = SimpleResultVO.buildSuccessResult(productDomainService
					.deleteProductDetailVOByProductID(productID));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		return flag;
	}

	public SimpleResultVO<Boolean> batchDeleteProductDetailVOByProductID(
			List<Long> storyDetailIDs) {
		return SimpleResultVO.buildSuccessResult(productDomainService
				.batchDeleteProductDetailVOByProductID(storyDetailIDs));
	}

	public IProductDomainService getProductDomainService() {
		return productDomainService;
	}

	public void setProductDomainService(
			IProductDomainService productDomainService) {
		this.productDomainService = productDomainService;
	}

	public SimpleResultVO<Boolean> submit(Long id) {
		return SimpleResultVO.buildSuccessResult(productDomainService
				.submit(id));
	}

	public SimpleResultVO<Boolean> audit(Long id, Long auditor) {
		return SimpleResultVO.buildSuccessResult(productDomainService.audit(id,
				auditor));
	}

	public SimpleResultVO<Boolean> accounted(Long id) {
		return SimpleResultVO.buildSuccessResult(productDomainService
				.accounted(id));
	}
}
