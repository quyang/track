package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.IAdjustDetailDao;
import com.xianzaishi.wms.track.manage.itf.IAdjustDetailManage;
import com.xianzaishi.wms.track.vo.AdjustDetailQueryVO;
import com.xianzaishi.wms.track.vo.AdjustDetailVO;

public class AdjustDetailManageImpl implements IAdjustDetailManage {
	@Autowired
	private IAdjustDetailDao adjustDetailDao = null;

	private void validate(AdjustDetailVO adjustDetailVO) {
		if (adjustDetailVO.getAdjustId() == null
				|| adjustDetailVO.getAdjustId() <= 0) {
			throw new BizException("adjustID error："
					+ adjustDetailVO.getAdjustId());
		}
		if (adjustDetailVO.getPositionId() == null
				|| adjustDetailVO.getPositionId() <= 0) {
			throw new BizException("positionID error："
					+ adjustDetailVO.getPositionId());
		}
		if (adjustDetailVO.getSkuId() == null || adjustDetailVO.getSkuId() <= 0) {
			throw new BizException("skuID error：" + adjustDetailVO.getSkuId());
		}
		if (adjustDetailVO.getNumberReal() == null
				|| adjustDetailVO.getNumberReal() <= 0) {
			throw new BizException("number error："
					+ adjustDetailVO.getNumberReal());
		}
		if (adjustDetailVO.getSkuName() == null
				|| adjustDetailVO.getSkuName().isEmpty()) {
			throw new BizException("skuName error："
					+ adjustDetailVO.getSkuName());
		}
		if (adjustDetailVO.getPositionCode() == null
				|| adjustDetailVO.getPositionCode().isEmpty()) {
			throw new BizException("positionCode error："
					+ adjustDetailVO.getPositionCode());
		}
		if (adjustDetailVO.getSaleUnit() == null
				|| adjustDetailVO.getSaleUnit().isEmpty()) {
			throw new BizException("saleUnit error："
					+ adjustDetailVO.getSaleUnit());
		}
		if (adjustDetailVO.getSaleUnitId() == null
				|| adjustDetailVO.getSaleUnitId() <= 0) {
			throw new BizException("saleUnitId error："
					+ adjustDetailVO.getSaleUnitId());
		}
		if (adjustDetailVO.getSaleUnitType() == null) {
			throw new BizException("saleUnitType error："
					+ adjustDetailVO.getSaleUnitType());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IAdjustDetailDao getAdjustDetailDao() {
		return adjustDetailDao;
	}

	public void setAdjustDetailDao(IAdjustDetailDao adjustDetailDao) {
		this.adjustDetailDao = adjustDetailDao;
	}

	public Boolean addAdjustDetailVO(AdjustDetailVO adjustDetailVO) {
		validate(adjustDetailVO);
		adjustDetailDao.addDO(adjustDetailVO);
		return true;
	}

	public List<AdjustDetailVO> queryAdjustDetailVOList(
			AdjustDetailQueryVO adjustDetailQueryVO) {
		return adjustDetailDao.queryDO(adjustDetailQueryVO);
	}

	public AdjustDetailVO getAdjustDetailVOByID(Long id) {
		return (AdjustDetailVO) adjustDetailDao.getDOByID(id);
	}

	public Boolean modifyAdjustDetailVO(AdjustDetailVO adjustDetailVO) {
		return adjustDetailDao.updateDO(adjustDetailVO);
	}

	public Boolean deleteAdjustDetailVOByID(Long id) {
		return adjustDetailDao.delDO(id);
	}

	public List<AdjustDetailVO> getAdjustDetailVOByAdjustID(Long id) {
		AdjustDetailQueryVO queryVO = new AdjustDetailQueryVO();
		queryVO.setAdjustId(id);
		queryVO.setSize(Integer.MAX_VALUE);
		return adjustDetailDao.queryDO(queryVO);
	}

	public Boolean batchAddAdjustDetailVO(List<AdjustDetailVO> adjustDetailVOs) {
		return adjustDetailDao.batchAddDO(adjustDetailVOs);
	}

	public Boolean batchModifyAdjustDetailVO(
			List<AdjustDetailVO> adjustDetailVOs) {
		return adjustDetailDao.batchModifyDO(adjustDetailVOs);
	}

	public Boolean batchDeleteAdjustDetailVO(
			List<AdjustDetailVO> adjustDetailVOs) {
		return adjustDetailDao.batchDeleteDO(adjustDetailVOs);
	}

	public Boolean batchDeleteAdjustDetailVOByID(List<Long> adjustDetailIDs) {
		return adjustDetailDao.batchDeleteDOByID(adjustDetailIDs);
	}
}
