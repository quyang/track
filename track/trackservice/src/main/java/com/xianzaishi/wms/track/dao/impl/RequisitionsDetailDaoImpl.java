package com.xianzaishi.wms.track.dao.impl;

import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;
import com.xianzaishi.wms.track.dao.itf.IRequisitionsDetailDao;

public class RequisitionsDetailDaoImpl extends BaseDaoAdapter implements
		IRequisitionsDetailDao {
	public String getVOClassName() {
		return "RequisitionsDetailDO";
	}
}
