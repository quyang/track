package com.xianzaishi.wms.oplog.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.oplog.domain.service.itf.IProductionShelvingLogDomainService;
import com.xianzaishi.wms.oplog.service.itf.IProductionShelvingLogService;
import com.xianzaishi.wms.oplog.vo.ProductionShelvingLogVO;

public class ProductionShelvingLogDomainServiceImpl implements
		IProductionShelvingLogDomainService {

	@Autowired
	private IProductionShelvingLogService productionShelvingLogService = null;

	public Boolean batchAddProductionShelvingLog(
			List<ProductionShelvingLogVO> productionShelvingLogVOs) {
		return productionShelvingLogService
				.batchAddProductionShelvingLog(productionShelvingLogVOs);
	}

	public IProductionShelvingLogService getProductionShelvingLogService() {
		return productionShelvingLogService;
	}

	public void setProductionShelvingLogService(
			IProductionShelvingLogService productionShelvingLogService) {
		this.productionShelvingLogService = productionShelvingLogService;
	}

}
