package com.xianzaishi.wms.track.service.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.InspectionDetailQueryVO;
import com.xianzaishi.wms.track.vo.InspectionDetailVO;

public interface IInspectionDetailService {

	public Boolean addInspectionDetailVO(InspectionDetailVO inspectionDetailVO);

	public List<InspectionDetailVO> queryInspectionDetailVOList(
			InspectionDetailQueryVO inspectionDetailQueryVO);

	public InspectionDetailVO getInspectionDetailVOByID(Long id);

	public Boolean modifyInspectionDetailVO(
			InspectionDetailVO inspectionDetailVO);

	public Boolean deleteInspectionDetailVOByID(Long id);

	public List<InspectionDetailVO> getInspectionDetailVOByInspectionID(
			Long id);

	public Boolean batchAddInspectionDetailVO(
			List<InspectionDetailVO> inspectionDetailVOs);

	public Boolean batchModifyInspectionDetailVO(
			List<InspectionDetailVO> inspectionDetailVOs);

	public Boolean batchDeleteInspectionDetailVO(
			List<InspectionDetailVO> inspectionDetailVOs);

	public Boolean batchDeleteInspectionDetailVOByID(
			List<Long> inspectionDetailIDs);
}