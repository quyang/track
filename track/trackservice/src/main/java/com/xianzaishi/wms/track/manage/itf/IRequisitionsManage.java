package com.xianzaishi.wms.track.manage.itf;

import java.util.List;

import com.xianzaishi.wms.track.vo.RequisitionsQueryVO;
import com.xianzaishi.wms.track.vo.RequisitionsVO;

public interface IRequisitionsManage {

	public Long addRequisitionsVO(RequisitionsVO requisitionsVO);

	public List<RequisitionsVO> queryRequisitionsVOList(
			RequisitionsQueryVO requisitionsQueryVO);

	public Integer queryRequisitionsVOCount(
			RequisitionsQueryVO requisitionsQueryVO);

	public RequisitionsVO getRequisitionsVOByID(Long id);

	public Boolean modifyRequisitionsVO(RequisitionsVO requisitionsVO);

	public Boolean deleteRequisitionsVOByID(Long id);

	public Boolean submit(Long id);

	public Boolean audit(Long id, Long auditor);

	public Boolean outgoing(Long id, Long auditor);

	public Boolean outgoingAudit(Long id, Long auditor);

	public Boolean accounted(Long id);

}