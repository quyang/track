package com.xianzaishi.wms.track.dao.impl;

import java.util.List;

import com.xianzaishi.wms.track.dao.itf.IOutgoingDetailDao;
import com.xianzaishi.wms.track.vo.OutgoingDetailVO;
import com.xianzaishi.wms.track.vo.OutgoingDetailQueryVO;
import com.xianzaishi.wms.track.vo.OutgoingDetailVO;
import com.xianzaishi.wms.common.dao.impl.BaseDaoAdapter;

public class OutgoingDetailDaoImpl extends BaseDaoAdapter implements
		IOutgoingDetailDao {
	public String getVOClassName() {
		return "OutgoingDetailDO";
	}
}
