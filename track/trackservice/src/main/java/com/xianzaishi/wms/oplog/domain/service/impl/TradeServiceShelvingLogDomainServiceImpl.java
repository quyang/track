package com.xianzaishi.wms.oplog.domain.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.oplog.domain.service.itf.ITradeServiceShelvingLogDomainService;
import com.xianzaishi.wms.oplog.service.itf.ITradeServiceShelvingLogService;
import com.xianzaishi.wms.oplog.vo.TradeServiceShelvingLogVO;

public class TradeServiceShelvingLogDomainServiceImpl implements
		ITradeServiceShelvingLogDomainService {

	@Autowired
	private ITradeServiceShelvingLogService tradeServiceShelvingLogService = null;

	public Boolean batchAddTradeServiceShelvingLog(
			List<TradeServiceShelvingLogVO> tradeServiceShelvingLogVOs) {
		return tradeServiceShelvingLogService
				.batchAddTradeServiceShelvingLog(tradeServiceShelvingLogVOs);
	}

	public ITradeServiceShelvingLogService getTradeServiceShelvingLogService() {
		return tradeServiceShelvingLogService;
	}

	public void setTradeServiceShelvingLogService(
			ITradeServiceShelvingLogService tradeServiceShelvingLogService) {
		this.tradeServiceShelvingLogService = tradeServiceShelvingLogService;
	}

}
