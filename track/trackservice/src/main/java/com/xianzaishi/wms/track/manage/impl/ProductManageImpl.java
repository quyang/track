package com.xianzaishi.wms.track.manage.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.track.dao.itf.IProductDao;
import com.xianzaishi.wms.track.manage.itf.IProductManage;
import com.xianzaishi.wms.track.vo.ProductQueryVO;
import com.xianzaishi.wms.track.vo.ProductVO;

public class ProductManageImpl implements IProductManage {
	@Autowired
	private IProductDao productDao = null;

	private void validate(ProductVO productVO) {
		if (productVO.getAgencyId() == null || productVO.getAgencyId() <= 0) {
			throw new BizException("agencyID error：" + productVO.getAgencyId());
		}
		if (productVO.getOperate() == null || productVO.getOperate() <= 0) {
			throw new BizException("Operator error：" + productVO.getOperate());
		}
		if (productVO.getStallsId() == null || productVO.getStallsId() <= 0) {
			throw new BizException("stallsID error：" + productVO.getStallsId());
		}
	}

	private void validate(Long id) {
		if (id == null || id <= 0) {
			throw new BizException("ID error：" + id);
		}
	}

	public IProductDao getProductDao() {
		return productDao;
	}

	public void setProductDao(IProductDao productDao) {
		this.productDao = productDao;
	}

	public Long addProductVO(ProductVO productVO) {
		validate(productVO);
		return (Long) productDao.addDO(productVO);
	}

	public List<ProductVO> queryProductVOList(ProductQueryVO productQueryVO) {
		return productDao.queryDO(productQueryVO);
	}

	public ProductVO getProductVOByID(Long id) {
		return (ProductVO) productDao.getDOByID(id);
	}

	public Boolean modifyProductVO(ProductVO productVO) {
		return productDao.updateDO(productVO);
	}

	public Boolean deleteProductVOByID(Long id) {
		return productDao.delDO(id);
	}

	public Boolean submit(Long id) {
		return productDao.submit(id);
	}

	public Boolean audit(Long id, Long auditor) {
		return productDao.audit(id, auditor);
	}

	public Boolean accounted(Long id) {
		return productDao.accounted(id);
	}

	public Integer queryProductVOCount(ProductQueryVO productQueryVO) {
		return productDao.queryCount(productQueryVO);
	}
}
